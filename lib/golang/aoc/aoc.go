package aoc

import (
	"container/heap"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

type Item struct {
	Value    interface{}
	Priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Priority < pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
	heap.Fix(pq, n)
}

func (pq *PriorityQueue) Pop() interface{} {
	n := len(*pq)
	item := (*pq)[n-1]
	(*pq)[n-1] = nil
	item.index = -1
	*pq = (*pq)[0 : n-1]
	return item
}

func NewPriorityQueue() *PriorityQueue {
	pq := make(PriorityQueue, 0)
	heap.Init(&pq)
	return &pq
}

type LineHandler = func(line string)

func ResourcePath(file string) string {
	for skip := 1; ; skip++ {
		_, path, _, _ := runtime.Caller(skip)
		if strings.Contains(path, "/src/") {
			dir := filepath.Dir(path)
			if !strings.Contains(file, "/") {
				dir = strings.Replace(dir, "src", "resources/inputs", 1)
			}
			return filepath.Join(dir, file)
		}
	}
}

func ReadStrings(file string) []string {
	vals := make([]string, 0)

	Read(file, func(line string) {
		vals = append(vals, line)
	})

	// remove end-of-file empty lines
	for len(vals[len(vals)-1]) == 0 {
		vals = vals[:len(vals)-1]
	}

	return vals
}

func ReadInts(file string) []int {
	vals := make([]int, 0)

	Read(file, func(line string) {
		if val, err := strconv.Atoi(line); err == nil {
			vals = append(vals, val)
		}
	})

	return vals
}

func Read(file string, handler LineHandler) {
	bytes, _ := ioutil.ReadFile(ResourcePath(file))
	eol := regexp.MustCompile(`\r?\n`)

	for _, line := range eol.Split(string(bytes), -1) {
		handler(line)
	}
}

func Sum(list ...int) int {
	sum := 0
	for _, v := range list {
		sum += v
	}
	return sum
}

func Product(list ...int) int {
	product := 1
	for _, v := range list {
		product *= v
	}
	return product
}

func Min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}
