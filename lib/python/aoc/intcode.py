#!/usr/bin/env python3

from typing import Self

R_OK = 0
R_HALT = 1
R_INPUT = 2
R_OUTPUT = 3
R_INVALID = 99

class IntCode:
  def __init__(self: Self, codes: list[int]) -> None:
    self.code = {i: x for i, x in enumerate(codes)}
    self.ptr = 0
    self.relative_base = 0
    self.modes = [0, 0, 0]
    self.inputs = []
    self.outputs = []

    self.ops = {
      1: lambda: self._add(),
      2: lambda: self._mul(),
      3: lambda: self._read(),
      4: lambda: self._write(),
      5: lambda: self._jmp_if_true(),
      6: lambda: self._jmp_if_false(),
      7: lambda: self._set_if_lt(),
      8: lambda: self._set_if_eq(),
      9: lambda: self._set_base(),
      99: lambda: R_HALT,
    }

  # Comparison functions
  # All IntCode instances are created equal
  def __eq__(self: Self, other: Self) -> bool:
    return True
  def __ne__(self: Self, other: Self) -> bool:
    return False
  def __lt__(self: Self, other: Self) -> bool:
    return False
  def __le__(self: Self, other: Self) -> bool:
    return True
  def __gt__(self: Self, other: Self) -> bool:
    return False
  def __ge__(self: Self, other: Self) -> bool:
    return True

  def inspect(self: Self, pos: int) -> int:
    return self.code[pos]

  def edit(self: Self, pos: int, val: int) -> Self:
    self.code[pos] = val
    return self

  def write(self: Self, input: int) -> Self:
    self.inputs.append(input)
    return self

  def read(self: Self) -> int:
    return self.outputs.pop(0)

  def run(self: Self) -> int:
    res = R_OK
    while res == R_OK:
      res = self._step()
    return res

  def copy(self: Self) -> Self:
    new = IntCode([])

    new.code = self.code.copy()
    new.ptr = self.ptr
    new.relative_base = self.relative_base
    new.inputs = self.inputs.copy()
    new.outputs = self.outputs.copy()

    return new

  def _step(self: Self) -> int:
    mode, op = divmod(self.code.get(self.ptr, 0), 100)
    self.ptr += 1
    self.modes = [int(x) for x in reversed(str(mode))] + [0, 0, 0]

    try:
      res = self.ops[op]()
    except KeyError:
      print(f"Invalid opcode {op}")
      res = R_INVALID

    return res

  def _next_value(self: Self) -> int:
    return self._next_instr() if self.modes[0] == 1 else self.code.get(self._next_instr(), 0)

  def _next_instr(self: Self) -> int:
    instr = self.code.get(self.ptr, 0)
    if self.modes.pop(0) == 2:
      instr += self.relative_base
    self.ptr += 1
    return instr

  def _jmp(self: Self, pos: int) -> None:
    self.ptr = pos

  def _add(self: Self) -> int:
    arg1 = self._next_value()
    arg2 = self._next_value()
    pos = self._next_instr()

    self.code[pos] = arg1 + arg2
    return R_OK

  def _mul(self: Self) -> int:
    arg1 = self._next_value()
    arg2 = self._next_value()
    pos = self._next_instr()

    self.code[pos] = arg1 * arg2
    return R_OK

  def _read(self: Self) -> int:
    if len(self.inputs) == 0:
      self.ptr -= 1
      return R_INPUT

    pos = self._next_instr()
    self.code[pos] = self.inputs.pop(0)
    return R_OK

  def _write(self: Self) -> int:
    val = self._next_value()
    self.outputs.append(val)
    return R_OUTPUT

  def _jmp_if_true(self: Self) -> int:
    val = self._next_value()
    pos = self._next_value()

    if val != 0:
      self._jmp(pos)

    return R_OK

  def _jmp_if_false(self: Self) -> int:
    val = self._next_value()
    pos = self._next_value()

    if val == 0:
      self._jmp(pos)

    return R_OK

  def _set_if_lt(self: Self) -> int:
    arg1 = self._next_value()
    arg2 = self._next_value()
    pos = self._next_instr()

    self.code[pos] = 1 if arg1 < arg2 else 0
    return R_OK

  def _set_if_eq(self: Self) -> int:
    arg1 = self._next_value()
    arg2 = self._next_value()
    pos = self._next_instr()

    self.code[pos] = 1 if arg1 == arg2 else 0
    return R_OK

  def _set_base(self: Self) -> int:
    self.relative_base += self._next_value()
    return R_OK
