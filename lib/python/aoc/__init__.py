import __main__ as main
import pathlib

from typing import Iterable

project_dir = pathlib.Path(__file__).resolve().parents[3]
inputs_dir = project_dir / 'resources' / 'inputs'

def inputfile(fn: str) -> pathlib.Path:
  script_dir = pathlib.Path(main.__file__).resolve().parent
  return inputs_dir / script_dir.parent.name / script_dir.name / fn

def manhattan(start: Iterable[int], end: Iterable[int]) -> int:
  return sum(abs(b - a) for a, b in zip(start, end))

def unitdiff(start: complex, end: complex) -> float:
  return abs(end.real - start.real) + abs(end.imag - start.imag)