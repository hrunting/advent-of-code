# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from collections import deque
from queue import PriorityQueue
from typing import Generic, Hashable, Iterable, Self, TypeVar

NT = TypeVar('NT', bound=Hashable)
GT = TypeVar('GT')

class BaseSearch(ABC, Generic[NT, GT]):
  __slots__ = tuple()

  def __init__(self: Self) -> None:
    pass

  @abstractmethod
  def shortest_path(
      self: Self,
      node: NT,
      goal: GT,
      full: bool = False) -> tuple[int, int, Iterable[NT]]:
    pass

  @abstractmethod
  def is_finished(self: Self, node: NT, goal: GT) -> bool:
    pass

  @abstractmethod
  def moves(self: Self, node: NT) -> Iterable[tuple[NT, int]]:
    pass

class BFSSearch(BaseSearch[NT, GT]):
  __slots__ = tuple()

  def shortest_path(
      self: Self,
      node: NT,
      goal: GT,
      full: bool = False) -> tuple[int, int, Iterable[NT]]:

    # steps: the number of nodes traversed so far
    # dist: the total distance traveled to this node
    # path: the path to this node
    steps, dist, path = 0, 0, []

    q: deque[tuple[int, NT, list[NT]]] = deque()
    nq: deque[tuple[int, NT, list[NT]]] = deque()
    visited: set[NT] = set()

    q.append((0, node, []))
    visited.add(node)

    while q:
      dist, node, path = q.popleft()

      if self.is_finished(node, goal):
        break

      if full:
        path = path + [node]

      for (n, cost) in self.moves(node):
        if n not in visited:
          nq.append((dist + cost, n, path))
          visited.add(n)

      if not q:
        q = nq
        steps += 1

    return steps, dist, path + [node]

class DijkstraSearch(BaseSearch[NT, GT]):
  __slots__ = tuple()

  def shortest_path(
      self: Self,
      node: NT,
      goal: GT,
      full: bool = False) -> tuple[int, int, list[NT]]:

    # dist: the total distance traveled to this node
    # steps: the number of nodes traversed so far
    # count: an atomic counter to prevent the need for sortable nodes
    # path: the path to this node
    dist, steps, count, path = 0, 0, 0, []

    q: PriorityQueue[tuple[int, int, int, NT, list[NT]]] = PriorityQueue()
    visited: set[NT] = set()
    seen: dict[NT, int] = {}

    q.put((dist, steps, count, node, []), block=False)
    seen[node] = dist

    while not q.empty():
      dist, steps, _, node, path = q.get(block=False)

      if self.is_finished(node, goal):
        break

      if node in visited:
        continue

      visited.add(node)

      steps += 1
      if full:
        path = path + [node]

      for n, cost in self.moves(node):
        cost += dist
        if n not in seen or cost < seen[n]:
          count += 1
          q.put((cost, steps, count, n, path), block=False)
          seen[n] = cost

    return steps, dist, path + [node]

class AStarSearch(BaseSearch[NT, GT]):
  __slots__ = tuple()

  @abstractmethod
  def estimate(self: Self, node: NT, goal: GT) -> int:
    pass

  def shortest_path(
      self: Self,
      node: NT,
      goal: GT,
      full: bool = False) -> tuple[int, int, list[NT]]:

    # score: the A* cost + estimate
    # dist: the total distance traveled to this node
    # steps: the number of nodes traversed so far
    # count: an atomic counter to prevent the need for sortable nodes
    # path: the path to this node
    score, dist, steps, count, path = 0, 0, 0, 0, []

    q: PriorityQueue[tuple[int, int, int, int, NT, list[NT]]] = PriorityQueue()
    visited: set[NT] = set()
    seen: dict[NT, int] = {}

    q.put((score, dist, steps, count, node, path), block=False)
    seen[node] = dist

    while not q.empty():
      _, dist, steps, _, node, path = q.get(block=False)

      if self.is_finished(node, goal):
        break

      if node in visited:
        continue

      visited.add(node)

      steps += 1
      if full:
        path = path + [node]

      for n, cost in self.moves(node):
        cost += dist
        if n in seen and seen[n] <= cost:
            continue

        score = cost + self.estimate(n, goal)
        count += 1
        q.put((score, cost, steps, count, n, path), block=False)
        seen[n] = cost

    return steps, dist, path + [node]