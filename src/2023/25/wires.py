#!/usr/bin/env python3

import pathlib
import random
import sys

from collections import defaultdict, deque
from itertools import pairwise

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

CUT_NODES = 3

def num_paths(graph: dict[str, set[str]], start: str) -> int:
  visited = set()
  q: deque[str] = deque([start])

  while q:
    src = q.popleft()
    visited.add(src)
    for dest in graph[src]:
      if dest not in visited:
        q.append(dest)

  return len(visited)

def shortest_path(
  graph: dict[str, set[str]],
  start: str,
  end: str,
  excluded: set[tuple[str, str]] = set()
) -> tuple[str, ...]:
  visited = set()
  q: deque[tuple[str, tuple[str, ...]]] = deque([(start, (start, ))])

  while q:
    src, path = q.popleft()

    # convert the path to a list of edges
    if src == end:
      return path

    visited.add(src)
    for dest in graph[src]:
      if dest not in visited and (path[-1], dest) not in excluded:
        q.append((dest, path + (dest, )))

  return ()

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  connections = defaultdict(set)

  for line in input.strip().splitlines():
    wire, connected = line.split(": ")
    for conn in connected.split():
      connections[wire].add(conn)
      connections[conn].add(wire)

  # Randomly select two nodes on the graph.
  #
  # If there's only {CUT_NODES} paths between the nodes (there is no
  # extra path), test each edge found in the paths to
  # see if removing it reduces the number of possible paths.
  #
  # Once {CUT_NODES} such edges are found, remove them and then BFS
  # for the number of connected nodes to one of the two nodes.
  #
  # The result will be the number of connected nodes and the
  # number of all the other nodes.
  while True:
    src, dest = random.sample(tuple(connections.keys()), 2)

    # edges to exclude
    edges: set[tuple[str, str]] = set()

    # Run CUT_NODES + 1 times.
    # A good src/dest pair will be one that fails to find
    # a shortest path within {CUT_NODES} + 1 runs.
    for _ in range(CUT_NODES + 1):
      if path := shortest_path(connections, src, dest, edges):
        edges |= set(pairwise(path))
        continue

      # The src/dest pair is on opposite sides of the partition.
      # Find 3 edges that must be traversed (cutting them reduces
      # the number of possible paths).
      cut_edges: set[tuple[str, str]] = set()

      for edge in edges:
        check_edges = {edge}

        # From the problem statement, the edge to be cut will reduce
        # the number of shortest paths, so if {CUT_NODES} shortest paths
        # are found, the edge is not a cut edge
        for _ in range(CUT_NODES):
          if path := shortest_path(connections, src, dest, check_edges):
            check_edges |= set(pairwise(path))
            continue

          # no shortest path found, this edge is a cut edge
          cut_edges.add(edge)
          break

        # Stop searching for cut edges once the expected number are found
        if len(cut_edges) == CUT_NODES:
          break

      # Remove the cut edges
      for node1, node2 in cut_edges:
        connections[node1].remove(node2)
        connections[node2].remove(node1)

      p = num_paths(connections, src)
      print(f"Subset product: {p * (len(connections) - p)}")
      return

if __name__ == '__main__':
  run()
  sys.exit(0)
