#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Self, Type, TypeVar

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

TCard = TypeVar("TCard", bound="Card")

class Card():
  def __init__(
      self: Self,
      id: int,
      winners: list[int],
      numbers: list[int],
      copies: int = 1) -> None:

    self.id = id
    self.winners = set(winners)
    self.numbers = numbers
    self.copies = copies

    self.matches = sum(1 for n in self.numbers if n in self.winners)
    self.score = int(2 ** (self.matches - 1))

  @classmethod
  def from_line(cls: Type[TCard], line: str) -> TCard:
    card, winners, numbers = re.split(r'\s*[:|]\s*', line)

    _, id = card.split()
    return cls(
      int(id),
      [int(x) for x in winners.split()],
      [int(x) for x in numbers.split()]
    )

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
     cards = [Card.from_line(line.strip()) for line in f.readlines()]

  total_score = 0
  total_cards = 0

  for i, card in enumerate(cards):
    total_score += card.score
    total_cards += card.copies

    for copy in cards[i + 1:i + card.matches + 1]:
      copy.copies += card.copies

  print(f"Score total: {total_score}")
  print(f"Total scratchcards: {total_cards}")

if __name__ == '__main__':
  run()
  sys.exit(0)
