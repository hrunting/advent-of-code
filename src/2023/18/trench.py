#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DIRS = {
  'R': lambda x, y, d: (x + d, y),
  'L': lambda x, y, d: (x - d, y),
  'U': lambda x, y, d: (x, y - d),
  'D': lambda x, y, d: (x, y + d)
}

class Trench():
  def __init__(self: Self, input: str, swapped: bool = False) -> None:
    self.corners = []
    self.perimeter = 0

    x, y = 0, 0
    for dir, travel, color in (line.split() for line in input.strip().splitlines()):
      dist = int(travel)
      if swapped:
        dist, dir = int(color[2:7], 16), "RDLU"[int(color[7:8])]

      self.corners.append((x, y))
      self.perimeter += dist
      x, y = DIRS[dir](x, y, dist)

  def volume(self: Self) -> int:
    return abs(sum(
      (self.corners[i - 1][1] + self.corners[i][1]) * (self.corners[i - 1][0] - self.corners[i][0])
      for i in range(len(self.corners))
    )) // 2 + self.perimeter // 2 + 1

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  t = Trench(input)
  print(f"Volume: {t.volume()}")

  t = Trench(input, True)
  print(f"Swapped volume: {t.volume()}")

if __name__ == '__main__':
  run()
  sys.exit(0)
