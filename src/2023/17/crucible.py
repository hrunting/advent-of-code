#!/usr/bin/env python3

import pathlib
import sys

from typing import Iterable, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.search

# row, column
DIRS = 3
HORIZONTAL = 1
VERTICAL = 2

Position = tuple[int, int]
RunPosition = tuple[int, int, int]

class CrucibleSearch(aoc.search.DijkstraSearch[RunPosition, Position]):
  __slots__ = ("grid", "bounds", "min_steps", "max_steps")

  def __init__(self: Self, grid: str, ultra: bool = False, **kwargs) -> None:
    super().__init__(**kwargs)
    self.grid = [[int(c) for c in row] for row in grid.splitlines()]
    self.bounds = (len(self.grid), len(self.grid[0]))
    self.min_steps, self.max_steps = (4, 11) if ultra else (1, 4)

  def is_finished(self: Self, pos: RunPosition, goal: Position) -> bool:
    return pos[:2] == goal

  def moves(self: Self, node: RunPosition) -> Iterable[tuple[RunPosition, int]]:
    r, c, dirs = node

    moves = []

    for dir in (d for d in (HORIZONTAL, VERTICAL) if dirs & d):
      ndir = ~dir & DIRS
      for run in (1, -1):
        cost = 0
        for step in range(1, self.max_steps):
          nr, nc = (r + run * step, c) if dir == HORIZONTAL else (r, c + run * step)
          if not 0 <= nr < self.bounds[0] or not 0 <= nc < self.bounds[1]:
            break

          cost += self.grid[nr][nc]
          if step >= self.min_steps:
            moves.append(((nr, nc, ndir), cost))

    return moves

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read().strip()

  goal = (input.count("\n"), input.find("\n") - 1)

  for ultra in (False, True):
    s = CrucibleSearch(input, ultra)
    _, heat, _ = s.shortest_path((0, 0, HORIZONTAL | VERTICAL), goal)
    print(f"Least heat loss{' (ultra)' if ultra else ''}: {heat}")

if __name__ == '__main__':
  run()
  sys.exit(0)
