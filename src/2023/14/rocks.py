#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

CYCLES = 1_000_000_000

def cycle(dish: list[tuple[str, ...]]) -> list[tuple[str, ...]]:
  return rotate(dish, 4)

def rotate(dish: list[tuple[str, ...]], turns: int = 1) -> list[tuple[str, ...]]:
  for _ in range(turns):
    result = []

    for row in zip(*(reversed(dish))):
      result.append(tuple())
      rocks = 0
      last_rock = -1

      for i, c in enumerate(row):
        if c == 'O':
          rocks += 1
        elif c == '#':
          result[-1] += ('.',) * (i - last_rock - rocks - 1) + ('O',) * rocks
          result[-1] += ("#",)
          rocks = 0
          last_rock = i
      else:
        result[-1] += ('.',) * (len(row) - last_rock - rocks - 1) + ('O',) * rocks

    dish = result

  return dish

# calculates load for the north beam
# assumes that the north side is to the right
def load(dish: list[tuple[str, ...]]) -> int:
  return sum(sum(i + 1 for i, c in enumerate(row) if c == 'O') for row in dish)

def state(dish: list[tuple[str, ...]]) -> str:
  return ''.join(''.join(row) for row in dish)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    lines = f.read().strip()

  rocks = [tuple(x for x in line) for line in lines.splitlines()]

  # when rotated, north is to the right
  print(f"Total load on north support beams: {load(rotate(rocks))}")

  remaining, rotated = CYCLES, rocks
  states = {state(rotated): remaining}

  while remaining > 0:
    remaining -= 1
    rotated = cycle(rotated)
    next_state = state(rotated)

    if next_state in states:
      (_, remaining) = divmod(remaining, states[next_state] - remaining)
      states = {}

    states[next_state] = remaining

  # need to rotate the dish so that north is the right side
  rotated = list(zip(*(reversed(rotated))))
  print(f"Total load after {CYCLES} cycles: {load(rotated)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
