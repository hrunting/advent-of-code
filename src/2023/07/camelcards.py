#!/usr/bin/env python3

import functools
import pathlib
import sys

from collections import Counter
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

@functools.total_ordering
class Card():
  def __init__(self: Self, cards: str, joker: bool = False) -> None:
    self.cards = cards
    self.joker = joker

    if not joker or 'J' not in cards or cards == 'JJJJJ':
      self.type = self._rank(Counter(cards))

    else:
      replacements = cards.count('J')
      counts = Counter(cards.replace('J', ''))
      self.type = self._rank(counts + Counter(counts.most_common(1)[0][0] * replacements))

  def _rank(self: Self, counts: Counter) -> int:
    rank = 0

    for _, c in counts.most_common():
      match c:
        case 5:
          rank = 6
        case 4:
          rank = 5
        case 3:
          rank = 3
        case 2:
          if rank == 3:
            rank = 4
          elif rank == 1:
            rank = 2
          else:
            rank = 1

    return rank

  def __lt__(self: Self, other: Self) -> bool:
    if self.type != other.type:
      return self.type < other.type

    for a, b in zip(self.cards, other.cards):
      if a == b:
        continue
      if self.joker and (a == 'J' or b == 'J'):
        return a == 'J'
      if a.isdigit() and b.isdigit():
        return int(a) < int(b)
      if a.isdigit() or b.isdigit():
        return a.isdigit()
      for card in 'TJQKA':
        if a == card or b == card:
          return a == card

    return False

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    lines = f.readlines()

  cards = sorted([(Card(card), int(bid)) for (card, bid) in (s.strip().split() for s in lines)])
  winnings = sum(bid * (rank + 1) for rank, (_, bid) in enumerate(cards))
  print(f"Total winnings: {winnings}")

  cards = sorted((Card(card, True), int(bid)) for (card, bid) in (s.strip().split() for s in lines))
  winnings = sum(bid * (rank + 1) for rank, (_, bid) in enumerate(cards))
  print(f"Total joker winnings: {winnings}")

if __name__ == '__main__':
  run()
  sys.exit(0)
