#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DIGIT_WORDS = {
  'zero': 0,
  'one': 1,
  'two': 2,
  'three': 3,
  'four': 4,
  'five': 5,
  'six': 6,
  'seven': 7,
  'eight': 8,
  'nine': 9
}

def num(s: str) -> int:
  if s.isdigit():
    return int(s)
  else:
    return DIGIT_WORDS[s.lower()]

def run() -> None:
  int_codes = []
  num_codes = []

  input_file = aoc.inputfile('input.txt')
  for code in (line.strip() for line in open(input_file).readlines()):
    ints = [int(x) for x in code if x.isdigit()]
    nums = [
      num(x)
      for x in re.findall(
        rf'(?=([0-9]|{"|".join(DIGIT_WORDS.keys())}))',
        code
      )
    ]

    int_codes.append(ints[0] * 10 + ints[-1])
    num_codes.append(nums[0] * 10 + nums[-1])

  print(f"Calibration sum: {sum(int_codes)}")
  print(f"Calibration word-based sum: {sum(num_codes)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
