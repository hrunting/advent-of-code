#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict, deque
from itertools import chain
from typing import Literal, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DIR = (
  tuple[Literal[-1], Literal[0]] |
  tuple[Literal[1], Literal[0]] |
  tuple[Literal[0], Literal[-1]] |
  tuple[Literal[0], Literal[1]]
)

RIGHT = (1, 0)
LEFT = (-1, 0)
UP = (0, -1)
DOWN = (0, 1)

class Tiles():
  def __init__(self: Self, grid: str) -> None:
    self.grid = grid.splitlines()

  def bounds(self: Self) -> tuple[int, int]:
    return len(self.grid[0]), len(self.grid)

  def energize(self: Self, start: tuple[int, int] = (0, 0), dir: DIR = RIGHT) -> int:
    mirrors = defaultdict(set)
    energized = set()
    turns = {
      "/": {RIGHT: [UP], LEFT: [DOWN], UP: [RIGHT], DOWN: [LEFT]},
      "\\": {RIGHT: [DOWN], LEFT: [UP], UP: [LEFT], DOWN: [RIGHT]},
      "-": {RIGHT: [RIGHT], LEFT: [LEFT], UP: [LEFT, RIGHT], DOWN: [LEFT, RIGHT]},
      "|": {RIGHT: [UP, DOWN], LEFT: [UP, DOWN], UP: [UP], DOWN: [DOWN]},
    }

    mx, my = self.bounds()

    q = deque([(start, dir)])

    while q:
      (x, y), dir = q.popleft()

      while 0 <= x < mx and 0 <= y < my and self.grid[y][x] not in turns:
        energized.add((x, y))
        x, y = x + dir[0], y + dir[1]

      if not 0 <= x < mx or not 0 <= y < my or dir in mirrors[x, y]:
        continue

      energized.add((x, y))
      mirrors[x, y].add(dir)

      dirs = turns[self.grid[y][x]][dir]

      for ndir in dirs:
        nx, ny = x + ndir[0], y + ndir[1]
        if 0 <= nx < mx and 0 <= ny < my:
          q.append(((nx, ny), ndir))

    return len(energized)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read().strip()

  tiles = Tiles(input)

  max_energized = 0

  mx, my = tiles.bounds()
  for start, dir in chain(
    (((0, y), RIGHT) for y in range(my)),
    (((x, 0), DOWN) for x in range(mx)),
    (((mx - 1, y), LEFT) for y in range(my)),
    (((x, my - 1), UP) for x in range(mx))
  ):
    max_energized = max(max_energized, tiles.energize(start, dir))

  print(f"Energized tiles: {tiles.energize()}")
  print(f"Max energized tiles: {max_energized}")

if __name__ == '__main__':
  run()
  sys.exit(0)
