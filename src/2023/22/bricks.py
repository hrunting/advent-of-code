#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict
from dataclasses import dataclass
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

@dataclass
class Point():
  x: int
  y: int
  z: int

  def dot(self: Self, other: Self) -> int:
    return self.x * other.x + self.y * other.y + self.z * other.z

  def cross(self: Self, other: Self) -> Self:
    return self.__class__(
      self.y * other.z - other.y * self.z,
      self.z * other.x - other.z * self.x,
      self.x * other.y - other.x * self.y
    )

  def norm2(self: Self) -> int:
    return self.x * self.x + self.y * self.y + self.z * self.z

  def norm(self: Self) -> float:
    return self.norm2() ** 0.5

  def __eq__(self: Self, other: Self) -> bool:
    return self.x == other.x and self.y == other.y and self.z == other.z

  def __add__(self: Self, other: Self) -> Self:
    return self.__class__(self.x + other.x, self.y + other.y, self.z + other.z)

  def __sub__(self: Self, other: Self) -> Self:
    return self.__class__(self.x - other.x, self.y - other.y, self.z - other.z)

  def __mul__(self: Self, other: Self) -> Self:
    return self.__class__(self.x * other.x, self.y * other.y, self.z * other.z)

  def __hash__(self: Self) -> int:
    return hash((self.x, self.y, self.z))

  def __str__(self: Self) -> str:
    return f"P({self.x},{self.y},{self.z})"

  def __repr__(self: Self) -> str:
    return str(self)

@dataclass
class Segment():
  first: Point
  second: Point

  def contains(self: Self, point: Point) -> bool:
    ab = (self.second - self.first).norm()
    ap = (point - self.first).norm()
    pb = (self.second - point).norm()
    return ab == ap + pb

  def intersects(self: Self, other: Self) -> bool:
    da = self.second - self.first
    db = other.second - other.first
    dc = other.first - self.first

    if dc.dot(da.cross(db)) != 0.0:
      return False

    try:
      s = dc.cross(db).dot(da.cross(db)) / da.cross(db).norm2()
      t = dc.cross(da).dot(da.cross(db)) / da.cross(db).norm2()
      return 0.0 <= s <= 1.0 and 0.0 <= t <= 1.0
    except ZeroDivisionError:
      return any((
        self.contains(other.first),
        self.contains(other.second),
        other.contains(self.first),
        other.contains(self.second)
      ))

  def drop(self: Self, dist: int) -> Self:
    return self.__class__(
      self.first.__class__(self.first.x, self.first.y, self.first.z - dist),
      self.second.__class__(self.second.x, self.second.y, self.second.z - dist)
    )

  def __eq__(self: Self, other: Self) -> bool:
    return self.first == other.first and self.second == other.second

  def __hash__(self: Self) -> int:
    return hash((self.first, self.second))

  def __str__(self: Self) -> str:
    return f"S({self.first}/{self.second})"

  def __repr__(self: Self) -> str:
    return str(self)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  levels: defaultdict[int, set[Segment]] = defaultdict(set)

  for brick in input.strip().splitlines():
    c1, c2 = brick.split("~")
    x1, y1, z1 = (int(x) for x in c1.split(","))
    x2, y2, z2 = (int(x) for x in c2.split(","))

    if z1 > z2:
      x1, y1, z1, x2, y2, z2 = x2, y2, z2, x1, y1, z1

    segment = Segment(Point(x1, y1, z1), Point(x2, y2, z2))
    levels[z1].add(segment)

  max_z = max(levels.keys())

  lower = set(levels[1])
  for z in range(2, max_z + 1):
    moves = []
    lower_segs = sorted((s for s in lower if s.second.z < z), key=lambda s: -s.second.z)

    for segment in levels[z]:
      for seg in lower_segs:
        moved = segment.drop(segment.first.z - seg.second.z)
        if moved.intersects(seg):
          moved = moved.drop(-1)
          break

      else:
        moved = segment.drop(segment.first.z - 1)

      if moved != segment:
        moves.append((segment, moved))
      else:
        lower.add(segment)

    for up, down in moves:
      levels[up.first.z].discard(up)
      levels[down.first.z].add(down)
      lower.add(down)

  by_height = defaultdict(set)
  for blocks in levels.values():
    for block in blocks:
      by_height[block.second.z].add(block)

  under: dict[Segment, set[Segment]] = {}
  for z in range(max_z, 0, -1):
    for top in levels[z]:
      under[top] = set()
      lowered = top.drop(1)

      for bottom in (s for s in by_height[z - 1] if s.intersects(lowered)):
        under[top].add(bottom)

  safe_count = len(under) - len(set.union(*(blocks for blocks in under.values() if len(blocks) == 1)))
  print(f"Safe to disintegrate: {safe_count}")

  total = 0
  bricks: list[Segment] = sorted(under.keys(), key=lambda x: x.first.z)
  for i, brick in enumerate(bricks):
    falls = {brick}
    for other in bricks[i + 1:]:
      if other.first.z > brick.first.z and not under[other] - falls:
        falls.add(other)
    total += len(falls) - 1

  print(f"Total number of dropped blocks: {total}")

if __name__ == '__main__':
  run()
  sys.exit(0)
