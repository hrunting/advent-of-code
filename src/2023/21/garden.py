#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from itertools import count

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

GENERAL_SOLUTION = True

def walk_from(map: list[str], x: int, y: int, max_steps: int) -> int:
  visited = {}
  mx, my = len(map[0]), len(map)

  q: deque[tuple[int, int, int]] = deque([(x, y, 0)])
  while q:
    x, y, steps = q.popleft()
    steps += 1

    if steps > max_steps:
      continue

    for nx, ny in ((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)):
      if (nx, ny) not in visited and map[ny % my][nx % mx] != "#":
        visited[nx, ny] = steps
        q.append((nx, ny, steps))

  return sum(1 for x in visited.values() if x % 2 == max_steps % 2)

# solution for specific problem input
def large_steps(map: list[str], x: int, y: int, max_steps: int) -> int:
  size = len(map)
  iterations, extra = divmod(max_steps, size)

  f0, f1, f2 = (walk_from(map, x, y, size * i + extra) for i in range(0, 3))

  a = (f0 - 2 * f1 + f2) // 2
  b = f1 - f0 - a
  c = f0

  return a * iterations * iterations + b * iterations + c

# general solution for all inputs
# based on https://www.reddit.com/r/adventofcode/comments/18nevo3/comment/keb7zhu/?context=3
def visited_plots(map: list[str], x: int, y: int, max_steps: int) -> int:
  visited = {}
  mx, my = len(map[0]), len(map)

  edge_counts = [0] * mx
  first_derivatives = [0] * mx
  second_derivatives = [0] * mx

  edge = {(x, y)}
  counts = (0, 0)
  visited = set()
  step = 0

  # first, determine at what step the counts start increasing
  # by a steady quadratic amount
  for step in count(step):
    if step >= max_steps or step >= 2 * mx and not any(second_derivatives):
      break

    expansion = set()

    for x, y in edge:
      for nx, ny in ((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)):
        if (nx, ny) not in visited and map[ny % my][nx % mx] != "#":
          visited.add((nx, ny))
          expansion.add((nx, ny))

    counts = (counts[1], len(expansion) + counts[0])

    mod_step = step % mx
    if step >= mx:
      delta = len(expansion) - edge_counts[mod_step]
      second_derivatives[mod_step] = delta - first_derivatives[mod_step]
      first_derivatives[mod_step] = delta

    edge = expansion
    edge_counts[mod_step] = len(edge)

  # the loop will be a cycle of the width
  # determine how many full-sized loops are remaining
  loops = (max_steps - step) // mx

  # loops will start at the current step, which may not be aligned
  # with the loop cycle, rotate the tracking lists so that each loop
  # will start and stop with the current offset
  #
  # this is needed because the first and last item in a cycle
  # have the same parity, and those two items need to be at the offset
  offset = step % mx
  edge_counts = edge_counts[offset:] + edge_counts[:offset]
  first_derivatives = first_derivatives[offset:] + first_derivatives[:offset]

  # the loop is always adding the edge counts over and over again
  # it is adding multiples of the derivatives over and over again
  # pre-sum those now to not do it on every loop
  #
  # look at the final steps below for the incremental algorithm being looped
  edge_sums = (sum(edge_counts[::2]), sum(edge_counts[1::2]))
  deriv_sums = (sum(first_derivatives[::2]), sum(first_derivatives[1::2]))

  for loop in range(1, loops + 1):

    # perform one cycle of the loop
    # count = prev_count + edge sum + number of derivative increases so far
    counts = tuple(c + e + d * loop for c, e, d in zip(counts, edge_sums, deriv_sums))

    # our cycle is odd (that's how the parity works)
    # so flip the counts
    counts = (counts[1], counts[0])

    step += mx

  # increase all of the edge counts by the total number of derivative increases
  edge_counts = [c + first_derivatives[i] * loops for i, c in enumerate(edge_counts)]

  # unrotate the tracking lists
  edge_counts = edge_counts[-offset:] + edge_counts[:-offset]
  first_derivatives = first_derivatives[-offset:] + first_derivatives[:-offset]

  # incrementally increase the counts seen until the reaching the desired step
  for mod_step in (x % mx for x in range(step, max_steps)):
    edge_counts[mod_step] += first_derivatives[mod_step]
    counts = (counts[1], counts[0] + edge_counts[mod_step])

  return counts[1]

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  max_steps = 64
  inf_steps = 26501365

  map = input.strip().splitlines()

  x, y = next((x, y) for y, row in enumerate(map) for x, c in enumerate(row) if c == 'S')

  if GENERAL_SOLUTION:
    print(f"Total plots reached in {max_steps} steps: {visited_plots(map, x, y, max_steps)}")
    print(f"Total plots reached in {inf_steps} steps: {visited_plots(map, x, y, inf_steps)}")

  else:
    print(f"Total plots reached in {max_steps} steps: {walk_from(map, x, y, max_steps)}")
    print(f"Total plots reached in {max_steps} steps: {large_steps(map, x, y, max_steps)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
