#!/usr/bin/env python3

import pathlib
import sys

from functools import cache

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

@cache
def arrangements(pattern: str, counts: tuple[int], idx: int, cidx: int) -> int:
  if idx >= len(pattern):
    return int(cidx == len(counts))
  if cidx == len(counts):
    return 0 if "#" in pattern[idx:] else 1

  result = 0

  if pattern[idx] in ".?":
    result += arrangements(pattern, counts, idx + 1, cidx)
  if pattern[idx] in "#?":
    rem = len(pattern) - idx
    if (
      counts[cidx] <= rem and
      "." not in pattern[idx:idx + counts[cidx]] and
      (counts[cidx] == rem or pattern[idx + counts[cidx]] != "#")
    ):
      result += arrangements(pattern, counts, idx + counts[cidx] + 1, cidx + 1)

  return result

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    lines = [line.strip() for line in f.readlines()]

  total_arrangements = 0
  folded_arrangements = 0

  for line in lines:
    pattern, spring_lens = line.split()
    counts = tuple(int(x) for x in spring_lens.split(","))

    total_arrangements += arrangements(pattern, counts, 0, 0)
    folded_arrangements += arrangements('?'.join((pattern, ) * 5), counts * 5, 0, 0)

  print(f"Sum of simple arrangements: {total_arrangements}")
  print(f"Sum of folded arrangements: {folded_arrangements}")

if __name__ == '__main__':
  run()
  sys.exit(0)
