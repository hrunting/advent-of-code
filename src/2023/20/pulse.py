#!/usr/bin/env python3

import math
import pathlib
import sys

from collections import deque
from typing import Generator, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Module():
  def __init__(self: Self, name: str, targets: tuple[str, ...]) -> None:
    self.name = name
    self.targets = targets
    self.inputs = set()

  def send(self: Self, src: str, high: bool) -> Generator[tuple[str, str, bool], None, None]:
    return (_ for _ in ())

  def reset(self: Self) -> None:
    pass

class FlipFlop(Module):
  def __init__(self: Self, *args, **kwargs) -> None:
    super().__init__(*args, **kwargs)
    self.on = False

  def send(self: Self, _: str, high: bool) -> Generator[tuple[str, str, bool], None, None]:
    if high:
      return (_ for _ in ())

    self.on = not self.on
    return ((self.name, t, self.on) for t in self.targets)

  def reset(self: Self) -> None:
    super().reset()
    self.on = False

class Conjunction(Module):
  def __init__(self: Self, *args, **kwargs) -> None:
    super().__init__(*args, **kwargs)
    self.received = set()

  def send(self: Self, src: str, high: bool) -> Generator[tuple[str, str, bool], None, None]:
    if high:
      self.received.add(src)
    else:
      self.received.discard(src)

    return ((self.name, t, self.received != self.inputs) for t in self.targets)

  def reset(self: Self) -> None:
    super().reset()
    self.received = set()

class Broadcaster(Module):
  def __init__(self: Self, *args, **kwargs) -> None:
    super().__init__(*args, **kwargs)

  def send(self: Self, _: str, high: bool) -> Generator[tuple[str, str, bool], None, None]:
    return ((self.name, t, high) for t in self.targets)

def parse_input(input: str) -> dict[str, Module]:
  modules = {}
  for line in input.strip().splitlines():
    node, target_list = line.split(" -> ")
    targets = tuple(target_list.split(", "))

    if node[0] == '%':
      modules[node[1:]] = FlipFlop(node[1:], targets)
    elif node[0] == '&':
      modules[node[1:]] = Conjunction(node[1:], targets)
    else:
      modules[node] = Broadcaster(node, targets)

  srcs = list(modules.keys())
  for src in srcs:
    for dest in modules[src].targets:
      if dest not in modules:
        modules[dest] = Module(dest, tuple())
      modules[dest].inputs.add(src)

  return modules

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  modules = parse_input(input)

  pulses = {False: 0, True: 0}

  rx_presses = 0
  input_node = next(n.name for n in modules.values() if "rx" in n.targets)
  components = {}
  presses = 0

  while presses < 1000 or not rx_presses:
    presses += 1

    q: deque[tuple[str, str, bool]] = deque([("button", "broadcaster", False)])
    while q:
      src, dest, pulse = q.popleft()

      if presses <= 1000:
        pulses[pulse] += 1

      if not rx_presses and pulse and src in modules[input_node].inputs and src not in components:
        components[src] = presses
        if set(components.keys()) == modules[input_node].inputs:
          rx_presses = math.lcm(*(components.values()))

      for next_src, next_dest, next_pulse in modules[dest].send(src, pulse):
        q.append((next_src, next_dest, next_pulse))

  print(f"Pulse product: {pulses[True] * pulses[False]}")
  print(f"Total presses required: {rx_presses}")

if __name__ == '__main__':
  run()
  sys.exit(0)
