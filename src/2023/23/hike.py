#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict, deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DIRS = ((-1, 0), (1, 0), (0, -1), (0, 1))

Node = tuple[int, int]

def is_walkable(map: list[str], x: int, y: int, dx: int, dy: int) -> bool:
  return (
    dx == -1 and map[y][x] == '<' or
    dx == 1 and map[y][x] == '>' or
    dy == -1 and map[y][x] == '^' or
    dy == 1 and map[y][x] == 'v'
  )

def build_graph(
  map: list[str],
  start: Node,
  end: Node,
  slippery: bool
) -> dict[int, set[tuple[int, int]]]:
  my, mx = len(map), len(map[0])

  nodes = {start: 0, end: 1} | {
    node: i
    for i, node in enumerate((
      (y, x)
      for y in range(my)
      for x in range(mx)
      if map[y][x] == "." and sum(
        map[ny][nx] in "<>^v"
        for ny, nx in ((y - 1, x), (y + 1, x), (y, x - 1), (y, x + 1))
        if 0 <= ny < my and 0 <= nx < mx
      ) > 1
    ), start=2)
  }

  graph = defaultdict(set)
  stack: list[tuple[Node, int, Node]] = [(start, 0, start)]
  visited = set()

  while stack:
    node, steps, root = stack.pop()
    visited.add(node)

    if node == end:
      graph[nodes[end]].add((nodes[root], steps))
      graph[nodes[root]].add((nodes[end], steps))
      continue

    if node in nodes:
      root = node
      steps = 0

    (y, x) = node
    for dy, dx, ny, nx in ((dy, dx, y + dy, x + dx) for dy, dx in DIRS):
      if not 0 <= nx < mx or not 0 <= ny < my or map[ny][nx] == '#':
        continue

      if (ny, nx) in nodes and (ny, nx) != root:
        walkable = is_walkable(map, x, y, dx, dy)
        if not slippery or walkable:
          graph[nodes[root]].add((nodes[ny, nx], steps + 1))
        if not slippery or not walkable:
          graph[nodes[ny, nx]].add((nodes[root], steps + 1))

      if (ny, nx) in visited:
        continue

      stack.append(((ny, nx), steps + 1, root))

  if not slippery:
    graph[1] = set()

    # walk the edges (nodes with 3 or fewer attached edges) and
    # remove any edges that go back towards the start node
    # consider the start and end nodes to be "visited" already
    path: deque[tuple[tuple[int, int], int]] = deque([(edge, 0) for edge in graph[0]])
    seen = 3

    while path:
      (node, steps), src = path.popleft()
      if seen & 1 << node:
        continue

      graph[node].discard((src, steps))

      seen |= 1 << node

      for dest, steps in graph[node]:
        if len(graph[dest]) <= 3:
          path.append(((dest, steps), node))

  return graph

def longest_path(
  map: list[str],
  start: Node,
  end: Node,
  slippery: bool = True
) -> int:

  # start node in the graph will be 0
  # end node in the graph will be 1
  graph = build_graph(map, start, end, slippery)

  max_steps = 0
  stack: list[tuple[int, int, int]] = [(0, 0, 0)]

  while stack:
    node, steps, visited = stack.pop()
    if node == 1:
      max_steps = max(max_steps, steps)
      continue

    for hop, dist in graph[node]:
      if visited & 1 << hop:
        continue

      stack.append((hop, steps + dist, visited | 1 << node))

  return max_steps

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  map = input.strip().splitlines()

  longest_hike = longest_path(map, (0, 1), (len(map) - 1, len(map[0]) - 2))
  print(f"Longest slippery hike: {longest_hike}")

  longest_hike = longest_path(map, (0, 1), (len(map) - 1, len(map[0]) - 2), False)
  print(f"Longest non-slippery hike: {longest_hike}")

if __name__ == '__main__':
  run()
  sys.exit(0)
