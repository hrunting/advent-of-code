#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

NORTH = 0 - 1j
SOUTH = 0 + 1j
EAST = 1 + 0j
WEST = -1 + 0j

LEFT = 0 - 1j
RIGHT = 0 + 1j

def pipe_map(lines: list[str]) -> tuple[dict[complex, str], complex]:
  pipes = {}
  start = 0 + 0j

  for i, line in enumerate(lines):
    for r, c in enumerate(line):
      pos = complex(r, i)
      pipes[pos] = c
      if c == 'S':
        start = pos

  possible_starts = {
    SOUTH: {'|', 'J', 'L'},
    EAST: { '-', '7', 'J'},
    NORTH: { '|', 'F', '7'},
    WEST: { '-', 'F', 'L'}
  }

  next_dirs = set(
    dir
    for dir in (NORTH, SOUTH, EAST, WEST)
    if start + dir in pipes and pipes[start + dir] in possible_starts[dir]
  )

  if next_dirs == {NORTH, EAST}:
    pipes[start] = 'L'
  elif next_dirs == {NORTH, WEST}:
    pipes[start] = 'J'
  elif next_dirs == {SOUTH, EAST}:
    pipes[start] = 'F'
  elif next_dirs == {SOUTH, WEST}:
    pipes[start] = '7'
  elif next_dirs == {NORTH, SOUTH}:
    pipes[start] = '|'
  elif next_dirs == {EAST, WEST}:
    pipes[start] = '-'

  return pipes, start

def next_step(pipes: dict[complex, str], pos: complex, dir: complex) -> tuple[complex, complex]:
  if pipes[pos] == 'F' or pipes[pos] == 'J':
    dir *= LEFT if dir.real else RIGHT
  elif pipes[pos] == 'L' or pipes[pos] == '7':
    dir *= RIGHT if dir.real else LEFT

  return pos + dir, dir

def map_pipes(pipes: dict[complex, str], start: complex) -> tuple[int, list[complex]]:

  # this is the direction the mapper is going
  # when it enters the starting node
  starting_dirs = {
    'F': 0 - 1j,   # going north
    'L': -1 + 0j,  # going west
    '7': 1 + 0j,   # going east
    'J': 0 + 1j,   # going south
    '|': 0 + 1j,   # going south (could be either)
    '-': 1 + 0j    # going east (could be either)
  }

  sections = []
  pos = start
  dir = starting_dirs[pipes[pos]]

  # walk around the loop until we reach the start
  while True:
    sections.append(pos)
    pos, dir = next_step(pipes, pos, dir)
    if pos == start:
      break

  return len(sections) // 2, sections

def sanitize_pipes(pipes: dict[complex, str], sections: set[complex]) -> dict[complex,str]:
  return {pos: c if pos in sections else "." for pos, c in pipes.items()}

# shoelace algorithm
# https://en.m.wikipedia.org/wiki/Shoelace_formula
def total_area(pipes: dict[complex, str], sections: list[complex]) -> int:
  corners = [pos for pos in sections if pipes[pos] not in {'.', '|', '-'}]
  return abs(int(sum(
    (corners[i - 1].imag + corners[i].imag) * (corners[i - 1].real - corners[i].real)
    for i in range(len(corners))
  ))) // 2

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    lines = [line.strip() for line in f.readlines()]

  pipes, start = pipe_map(lines)
  max_steps, sections = map_pipes(pipes, start)

  # total area returned is space enclosed by the perimeter, not tiles
  # subtract out half the perimeter to adjust
  enclosed_tiles = total_area(sanitize_pipes(pipes, set(sections)), sections) - max_steps + 1

  print(f"Distance to furthest point: {max_steps}")
  print(f"Enclosed tiles: {enclosed_tiles}")

if __name__ == '__main__':
  run()
  sys.exit(0)
