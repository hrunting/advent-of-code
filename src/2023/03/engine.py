#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import TextIO

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse_input(f: TextIO) -> tuple[dict[tuple[int, int], str|int], list[int]]:
  engine = {}
  parts = []
  idx = -1

  for y, line in enumerate(f.readlines()):
    is_part = False

    for x, c in enumerate(line.strip()):
      if c.isdigit():
        if not is_part:
          is_part = True
          parts.append(0)
          idx += 1

        parts[idx] = parts[idx] * 10 + int(c)
        engine[(x, y)] = idx

      else:
        is_part = False
        if c != ".":
          engine[(x, y)] = c

  return engine, parts

def adjacents(x: int, y: int) -> list[tuple[int, int]]:
  return [
    (x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
    (x - 1, y), (x + 1, y),
    (x - 1, y + 1), (x, y + 1), (x + 1, y + 1)
  ]

def part_numbers(engine: dict[tuple[int, int], str|int], parts: list[int]) -> list[int]:
  part_ids = set()

  for (x, y), symbol in engine.items():
    if isinstance(symbol, int):
      continue

    for ax, ay in adjacents(x, y):
      id = engine.get((ax, ay), '')
      if isinstance(id, int):
        part_ids.add(id)

  return [parts[id] for id in part_ids]

def gear_ratios(engine: dict[tuple[int, int], str|int], parts: list[int]) -> list[int]:
  ratios = []

  for (x, y), symbol in engine.items():
    if symbol != '*':
      continue

    ids = {
      int(engine[ax, ay])
      for ax, ay in adjacents(x, y)
      if isinstance(engine.get((ax, ay), ''), int)
    }

    if len(ids) == 2:
      ratios.append(math.prod(parts[id] for id in ids))

  return ratios

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
     engine, parts = parse_input(f)

  part_nums = part_numbers(engine, parts)
  ratios = gear_ratios(engine, parts)

  print(f"Sum of all part numbers: {sum(part_nums)}")
  print(f"Sum of all gear ratios: {sum(ratios)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
