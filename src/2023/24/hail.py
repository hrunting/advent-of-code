#!/usr/bin/env python3

import pathlib
import re
import sys

from dataclasses import dataclass
from fractions import Fraction
from itertools import combinations
from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

@dataclass
class Coord():
  x: float
  y: float
  z: float

  def dot(self: Self, other: Self) -> float:
    return self.x * other.x + self.y * other.y + self.z * other.z

  def cross(self: Self, other: Self) -> Self:
    return self.__class__(
      self.y * other.z - other.y * self.z,
      self.z * other.x - other.z * self.x,
      self.x * other.y - other.x * self.y
    )

  def norm2(self: Self) -> float:
    return self.x * self.x + self.y * self.y + self.z * self.z

  def norm(self: Self) -> float:
    return self.norm2() ** 0.5

  def __eq__(self: Self, other: Self) -> bool:
    return self.x == other.x and self.y == other.y and self.z == other.z

  def __add__(self: Self, other: Self) -> Self:
    return self.__class__(self.x + other.x, self.y + other.y, self.z + other.z)

  def __sub__(self: Self, other: Self) -> Self:
    return self.__class__(self.x - other.x, self.y - other.y, self.z - other.z)

  def __mul__(self: Self, other: Self|float) -> Self:
    if isinstance(other, float) or isinstance(other, int):
      return self.__class__(self.x * other, self.y * other, self.z * other)
    return self.__class__(self.x * other.x, self.y * other.y, self.z * other.z)

  def __hash__(self: Self) -> int:
    return hash((self.x, self.y, self.z))

  def __str__(self: Self) -> str:
    return f"C({self.x},{self.y},{self.z})"

  def __repr__(self: Self) -> str:
    return str(self)

@dataclass
class Segment():
  first: Coord
  second: Coord

  def contains(self: Self, point: Coord) -> bool:
    ab = (self.second - self.first).norm()
    ap = (point - self.first).norm()
    pb = (self.second - point).norm()
    return ab == ap + pb

  def intersection(self: Self, other: Self) -> Optional[Coord]:
    da = self.second - self.first
    db = other.second - other.first
    dc = other.first - self.first

    if dc.dot(da.cross(db)) != 0.0:
      return None

    try:
      s = dc.cross(db).dot(da.cross(db)) / da.cross(db).norm2()
      t = dc.cross(da).dot(da.cross(db)) / da.cross(db).norm2()
      if 0.0 <= s <= 1.0 and 0.0 <= t <= 1.0:
        return self.first + da * s
      return None
    except ZeroDivisionError:
      if self.contains(other.first):
        return other.first
      elif other.contains(self.first):
        return self.first
      else:
        return None

  def __eq__(self: Self, other: Self) -> bool:
    return self.first == other.first and self.second == other.second

  def __hash__(self: Self) -> int:
    return hash((self.first, self.second))

  def __str__(self: Self) -> str:
    return f"S({self.first} / {self.second})"

  def __repr__(self: Self) -> str:
    return str(self)

@dataclass
class Vector():
  start: Coord
  velocity: Coord

  def segment(self: Self, min_c: Coord, max_c: Coord) -> Segment:
    t1 = float('-inf')
    t2 = float('inf')

    for x, dx, min_x, max_x in (
      (self.start.x, self.velocity.x, min_c.x, max_c.x),
      (self.start.y, self.velocity.y, min_c.y, max_c.y),
      (self.start.z, self.velocity.z, min_c.z, max_c.z)
    ):
      if min_x == max_x:
        continue

      if min_x <= x <= max_x:
        t1 = max(t1, 0.0)
        t2 = min(t2, ((min_x if dx < 0 else max_x) - x) / dx)
      elif min_x > x and dx > 0:
        t1 = max(t1, (min_x - x) / dx)
        t2 = min(t2, (max_x - x) / dx)
      elif max_x < x and dx < 0:
        t1 = max(t1, (max_x - x) / dx)
        t2 = min(t2, (min_x - x) / dx)

    return Segment(self.start + self.velocity * t1, self.start + self.velocity * t2)

  def __eq__(self: Self, other: Self) -> bool:
    return self.start == other.start and self.velocity == other.velocity

  def __hash__(self: Self) -> int:
    return hash((self.start, self.velocity))

  def __str__(self: Self) -> str:
    return f"V({self.start} @ {self.velocity})"

  def __repr__(self: Self) -> str:
    return str(self)

def find_intersections(readings: list[tuple[int, int, int, int, int, int]], win_min: int, win_max: int) -> int:
  vectors_2d = [Vector(Coord(x, y, 0), Coord(vx, vy, 0)) for x, y, _, vx, vy, _ in readings]
  segments = [v.segment(Coord(win_min, win_min, 0), Coord(win_max, win_max, 0)) for v in vectors_2d]

  intersections = 0
  for s1, s2 in combinations(segments, 2):
    if (c := s1.intersection(s2)) and win_min <= c.x <= win_max and win_min <= c.y <= win_max:
      intersections += 1

  return intersections

def find_magic_rock(readings: list[tuple[int, int, int, int, int, int]]) -> tuple[int, int, int, int, int, int]:
  min_v = min(min(-abs(vx), -abs(vy), -abs(vz)) for _, _, _, vx, vy, vz in readings)
  max_v = max(max(abs(vx), abs(vy), abs(vz)) for _, _, _, vx, vy, vz in readings)

  vxs = set(range(min_v, max_v + 1))
  vys = set(range(min_v, max_v + 1))
  vzs = set(range(min_v, max_v + 1))

  for (x1, y1, z1, vx1, vy1, vz1), (x2, y2, z2, vx2, vy2, vz2) in combinations(readings, 2):
    if vx1 == vx2:
      vxs &= {v for v in range(min_v, max_v) if v != vx1 and (x2 - x1) % (v - vx1) == 0}
    if vy1 == vy2:
      vys &= {v for v in range(min_v, max_v) if v != vy1 and (y2 - y1) % (v - vy1) == 0}
    if vz1 == vz2:
      vzs &= {v for v in range(min_v, max_v) if v != vz1 and (z2 - z1) % (v - vz1) == 0}

  rock_vxs = range(min_v, max_v + 1) if len(vxs) != 1 else range(tuple(vxs)[0], tuple(vxs)[0] + 1)
  rock_vys = range(min_v, max_v + 1) if len(vys) != 1 else range(tuple(vys)[0], tuple(vys)[0] + 1)
  rock_vzs = range(min_v, max_v + 1) if len(vzs) != 1 else range(tuple(vzs)[0], tuple(vzs)[0] + 1)

  (x1, y1, z1, vx1, vy1, vz1) = readings[0]
  (x2, y2, z2, vx2, vy2, vz2) = readings[1]

  rock = (0, 0, 0, 0, 0, 0)

  candidates = []
  for rock_vx in rock_vxs:
    for rock_vy in rock_vys:
      m1 = Fraction(vy1 - rock_vy, vx1 - rock_vx)
      m2 = Fraction(vy2 - rock_vy, vx2 - rock_vx)

      if m1 == m2:
        continue

      c1 = y1 - m1 * x1
      c2 = y2 - m2 * x2

      rock_x = Fraction(c2 - c1, m1 - m2)
      rock_y = m1 * rock_x + c1

      for i, (x3, y3, z3, vx3, vy3, vz3) in enumerate(readings[2:]):
        m3 = Fraction(vy3 - rock_vy, vx3 - rock_vx)
        c3 = y3 - m3 * x3

        if m1 == m3:
          break

        cx = Fraction(c3 - c1, m1 - m3)
        if cx != rock_x:
          break

        cy = m3 * cx + c3
        if cy != rock_y:
          break
      else:
        candidates.append((rock_x, rock_vx, rock_y, rock_vy))

  for (rock_x, rock_vx, rock_y, rock_vy) in candidates:
    for rock_vz in rock_vzs:
      rock_z = z1 + (vz1 - rock_vz) * Fraction(rock_x - x1, vx1 - rock_vx)
      for x3, _, z3, vx3, _, vz3 in readings[1:]:
        cz = z3 + (vz3 - rock_vz) * Fraction(rock_x - x3, vx3 - rock_vx)
        if cz != rock_z:
          break

      else:
        rock = (int(rock_x), int(rock_y), int(rock_z), rock_vx, rock_vy, rock_vz)
        break

  return rock

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    input = f.read()

  mn, mx = 200000000000000, 400000000000000

  readings = []
  for line in input.strip().splitlines():
    x, y, z, vx, vy, vz = re.split(r'[ ]?[@,][ ]?', line)
    readings.append((int(x), int(y), int(z), int(vx), int(vy), int(vz)))

  print(f"2D intersections: {find_intersections(readings, mn, mx)}")

  rock = find_magic_rock(readings)
  print(f"Rock position: {rock[:3]} @ {rock[3:]} = {sum(rock[:3])}")

if __name__ == '__main__':
  run()
  sys.exit(0)
