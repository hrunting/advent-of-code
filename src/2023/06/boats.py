#!/usr/bin/env python3

import math
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def bounds(time: int, distance: int) -> tuple[int, int]:
  discriminant_root = (-time * -time - 4 * distance) ** 0.5

  lo = (time + discriminant_root) / 2
  hi = (time - discriminant_root) / 2

  if lo > hi:
    lo, hi = hi, lo

  return int(math.floor(lo + 1)), int(math.ceil(hi - 1))

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
     times = [int(s) for s in f.readline().strip()[11:].split()]
     distances = [int(s) for s in f.readline().strip()[11:].split()]

  margin_of_error = 1
  for time, distance in zip(times, distances):
    lo, hi = bounds(time, distance)
    margin_of_error *= (hi - lo) + 1

  print(f"Margin of error: {margin_of_error}")

  time = int(''.join((str(n) for n in times)))
  distance = int(''.join((str(n) for n in distances)))

  lo, hi = bounds(time, distance)
  print(f"Margin of error (long race): {(hi - lo) + 1}")

if __name__ == '__main__':
  run()
  sys.exit(0)
