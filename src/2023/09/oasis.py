#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def next_value(reading: tuple[int, ...]) -> int:
  diffs = tuple(b - a for a, b in zip(reading, reading[1:]))
  if any(x != 0 for x in diffs):
    return next_value(diffs) + reading[-1]
  return reading[-1]

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    readings = [tuple(int(n) for n in x.strip().split()) for x in f.readlines()]

  values = [next_value(history) for history in readings]
  print(f"Sum of next values: {sum(values)}")

  values = [next_value(history[::-1]) for history in readings]
  print(f"Sum of first values: {sum(values)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
