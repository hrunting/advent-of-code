#!/usr/bin/env python3

import pathlib
import sys

from itertools import combinations
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Map():
  def __init__(self: Self, lines: list[str]) -> None:
    self.galaxies = set()
    self.empty_rows = set()
    self.empty_cols = set()

    for y, line in enumerate(lines):
      if line.count("#") == 0:
        self.empty_rows.add(y)
      for x, c in enumerate(line):
        if c == '#':
          self.galaxies.add((x, y))
        elif y == 0 and all(row[x] != '#' for row in lines):
          self.empty_cols.add(x)

  def shortest_paths(self: Self, expansion: int = 2) -> list[int]:
    paths = []
    expansion -= 1

    for (x1, y1), (x2, y2) in combinations(self.galaxies, 2):
      paths.append(
        aoc.manhattan((x1, y1), (x2, y2)) +
        expansion * len(set(range(y1, y2, 1 if y1 < y2 else -1)) & self.empty_rows) +
        expansion * len(set(range(x1, x2, 1 if x1 < x2 else -1)) & self.empty_cols)
      )

    return paths

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    lines = [line.strip() for line in f.readlines()]

  sky = Map(lines)
  paths = sky.shortest_paths()
  print(f"Sum of shortest paths: {sum(paths)}")

  paths  = sky.shortest_paths(1_000_000)
  print(f"Sum of older shortest paths: {sum(paths)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
