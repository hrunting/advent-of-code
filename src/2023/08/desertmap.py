#!/usr/bin/env python3

import math
import pathlib
import re
import sys

from itertools import cycle

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def steps_to_ending(
    loc: str,
    ending: str,
    route: list[int],
    paths: dict[str, tuple[str, str]]) -> int:
  steps = 0
  directions = cycle(route)

  while not loc.endswith(ending):
    loc = paths[loc][next(directions)]
    steps += 1

  return steps

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    route = [0 if c == 'L' else 1 for c in f.readline().strip()]
    paths = {}

    for line in (s.strip() for s in f.readlines()):
      if not line:
        continue

      src, left, right = re.findall(r'(\S+) = \((\S+), (\S+)\)', line)[0]
      paths[src] = (left, right)

  steps = steps_to_ending('AAA', 'ZZZ', route, paths)
  print(f"Total steps to get to ZZZ: {steps}")

  # LCM works because the input is structured so that every
  # path ends on a Z-ending node at the end of a route loop
  nodes = [node for node in paths.keys() if node[-1] == 'A']
  steps = math.lcm(*(steps_to_ending(node, 'Z', route, paths) for node in nodes))
  print(f"Total steps to get to Z nodes: {steps}")

if __name__ == '__main__':
  run()
  sys.exit(0)
