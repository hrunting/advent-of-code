#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  ops = {
    "addr": lambda r, A, B: r[A] + r[B],
    "addi": lambda r, A, B: r[A] + B,
    "mulr": lambda r, A, B: r[A] * r[B],
    "muli": lambda r, A, B: r[A] * B,
    "banr": lambda r, A, B: r[A] & r[B],
    "bani": lambda r, A, B: r[A] & B,
    "borr": lambda r, A, B: r[A] | r[B],
    "bori": lambda r, A, B: r[A] | B,
    "setr": lambda r, A, _: r[A],
    "seti": lambda r, A, _: A,
    "gtir": lambda r, A, B: 1 if A > r[B] else 0,
    "gtri": lambda r, A, B: 1 if r[A] > B else 0,
    "gtrr": lambda r, A, B: 1 if r[A] > r[B] else 0,
    "eqir": lambda r, A, B: 1 if A == r[B] else 0,
    "eqri": lambda r, A, B: 1 if r[A] == B else 0,
    "eqrr": lambda r, A, B: 1 if r[A] == r[B] else 0
  }

  def __init__(self: Self) -> None:
    self.ip = 0
    self.ip_register: Optional[int] = None
    self.registers = [0, 0, 0, 0, 0, 0]

  def __repr__(self):
    return f'{self.registers}'

  def op(self, op, A, B, C):

    # #ip is not an instruction
    if op == "#ip":
      self.ip_register = A
      return self.registers[self.ip_register]

    # otherwise, we have an instruction
    else:
      if self.ip_register is not None:
        self.registers[self.ip_register] = self.ip

      self.registers[C] = Machine.ops[op](self.registers, A, B)

      if self.ip_register is not None:
        self.ip = self.registers[self.ip_register]
        self.ip += 1

      return self.registers[C]

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file) if x.strip() != '']

  machine, first, _ = run_program(lines, fastest=True)
  print(f'Machine: {machine}  Fastest: {first}')

  first, last = run_compiled()
  print(f'Fastest Halt Value: {first}')
  print(f'Slowest Halt Value: {last}')

def run_program(
    code: list[str],
    register: int = 0,
    fastest: bool = True) -> tuple[Machine, Optional[int], Optional[int]]:

  ops = 0

  machine = Machine()
  machine.registers[0] = register

  program = []
  for line in code:
    instr, rest = line.split(' ', 1)
    if instr == '#ip':
      machine.op(instr, int(rest), None, None)

    else:
      A, B, C = (int(x) for x in rest.split(' '))
      program.append((instr, A, B, C))

  end = len(program)

  seen = set()
  least = None
  most = None

  while machine.ip < end:
    (instr, A, B, C) = program[machine.ip]

    # line 28 is our magic check that exits the program
    if machine.ip == 28:
      if least is None:
        least = machine.registers[1]
        if fastest:
          machine.registers[0] = machine.registers[1]

      if machine.registers[1] in seen:
        machine.registers[0] = machine.registers[1]
      else:
        seen.add(machine.registers[1])
        most = machine.registers[1]

    machine.op(instr, A, B, C)
    ops += 1

  return machine, least, most

def run_compiled(register: int = 0) -> tuple[Optional[int], Optional[int]]:

  # 0:  R1 = 123
  # 1:  R1 = R1 & 456
  # 2:  if R1 != 72:
  # 3:   goto 0
  # 4:
  # 5:  R1 = 0
  # 6:  R2 = R1 | 65536
  # 7:  R1 = 10605201
  # 8:  R5 = R2 & 255
  # 9:  R1 += R5
  # 10: R1 &= 16777215
  # 11: R1 *= 65899
  # 12: R1 &= 16777215
  # 13: if R5 = (R2 < 256):
  # 14:   goto 28
  # 15: goto 19
  # 16: 
  # 17: R5 = 0
  # 18: R4 = R5 + 1
  # 19: R4 *= 256
  # 20: if R4 = (R4 > R2):
  # 21:   goto 26
  # 22: goto 24
  # 23:
  # 24: R5 += 1
  # 25: goto 18
  # 26: R2 = R5
  # 27: goto 8
  # 28: if R5 = (R1 == R0):
  # 29:   exit
  # 30: goto 6

  r0, r1, r2, r3, r4, r5 = register, 0, 0, 0, 0, 0  # noqa: F841

  seen = set()
  least = None
  most = None

  while True:
    r2 = r1 | 65536
    r1 = 10605201

    while True:
      r5 = r2 & 255
      r1 += r5
      r1 &= 16777215
      r1 *= 65899
      r1 &= 16777215

      if 256 > r2:
        if least is None:
          least = r1

        if r1 not in seen:
          seen.add(r1)
          most = r1
        else:
          r0 = r1

        break

      # r5 = 0
      # while True:
      #   r4 = (r5 + 1) * 256
      #   if r4 > r2:
      #     r2 = r5
      #     break
      #   r5 += 1

      r2 //= 256

    if r0 == r1:
      break

  return least, most

if __name__ == '__main__':
  run()
  sys.exit(0)
