#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  chain = []

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    while True:
      c = input.read(1)
      if not c or c == "\r" or c == "\n":
        break

      if len(chain) > 0 and chain[-1] != c and chain[-1].lower() == c.lower():
        chain.pop()
      else:
        chain.append(c)

  print(f'Chain Length: {len(chain)}')

  chains = {'': []}

  with open(input_file) as input:
    while True:
      c = input.read(1)
      if not c or c == "\r" or c == "\n":
        break

      if c.lower() not in chains:
        chains[c.lower()] = [x for x in chains['']]

      for (x, chain) in chains.items():
        if x.lower() != c.lower():
          if len(chain) > 0 and chain[-1] != c and chain[-1].lower() == c.lower():
            chain.pop()
          else:
            chain.append(c)

  minimum_chain = min([len(x) for x in chains.values()])

  print(f'Minimum Length: {minimum_chain}')

if __name__ == '__main__':
  run()
  sys.exit(0)
