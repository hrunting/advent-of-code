#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Cell:
  def __init__(self: Self, x: int, y: int) -> None:
    self.x = x
    self.y = y
    self.neighbors: list[Self] = []

  def __repr__(self: Self) -> str:
    return f'{self.x, self.y}'

  # we flip these so that we can sort on read order
  def pos(self: Self) -> tuple[int, int]:
    return (self.y, self.x)

  def add_neighbor(self: Self, cell: Self) -> None:
    self.neighbors = sorted(list(set(self.neighbors + [cell])), key=lambda x: x.pos())
    cell.neighbors = sorted(list(set(cell.neighbors + [self])), key=lambda x: x.pos())

class Unit:
  def __init__(self: Self, cell: Cell, type: str, damage: int = 3, hp: int = 200) -> None:
    self.cell = cell
    self.type = type
    self.damage = damage
    self.hp = hp
    self.alive = True

  def __repr__(self: Self) -> str:
    return f'{self.type} {self.cell} hp={self.hp}'

  def pos(self: Self) -> tuple[int, int]:
    return self.cell.pos()

  def move(self: Self, cell: Cell) -> None:
    self.cell = cell

  def attack(self: Self, damage: int) -> None:
    if self.alive:
      self.hp -= damage
      if self.hp <= 0:
        self.alive = False

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file)]

  elfpower = 3
  cells, units = parse_map(lines, elfpower)
  rounds, hp = battle(cells, units)
  print(f'Outcome: {rounds * hp} ({rounds} * {hp})')

  while True:
    elfpower += 1

    cells, units = parse_map(lines, elfpower)
    elfcount = sum(1 for u in units if u.type == 'E')

    rounds, hp = battle(cells, units)
  
    if elfcount == sum(1 for u in units if u.type == 'E' and u.alive):
      print(f'Outcome: {rounds * hp} ({rounds} * {hp} @ {elfpower})')
      break

def battle(cells: dict[tuple[int, int], Cell], units: list[Unit]) -> tuple[int, int]:
  rounds = 0
  
  enemies_remaining = True
  while enemies_remaining:
    units = [u for u in sorted(units, key=lambda x: x.pos()) if u.alive]

    for unit in units:
      if not unit.alive:
        continue

      enemies = sorted([u for u in units if u.alive and u.type != unit.type], key=lambda x: x.hp)
      if len(enemies) == 0:
        enemies_remaining = False
        break

      target = next((e for e in enemies for n in unit.cell.neighbors if e.cell == n), None)
      if target is None:
        unit_cells = {u.cell for u in units if u != unit and u.alive}
        in_range = [c for e in enemies for c in e.cell.neighbors if c not in unit_cells]

        closest, dist = find_closest(in_range, unit.cell, unit_cells)
        if dist is None:
          continue

        for neighbor in unit.cell.neighbors:
          c, d = find_closest(closest, neighbor, unit_cells)
          if d is not None and d + 1 == dist and c[0] == closest[0]:
            unit.move(neighbor)
            target = next((e for e in enemies for n in unit.cell.neighbors if e.cell == n), None)
            break

      if target is not None:
        target.attack(unit.damage)
          
    if enemies_remaining:
      rounds += 1
 
  return rounds, sum([u.hp for u in units if u.alive])

def find_closest(targets: list[Cell], start: Cell, exclude: set[Cell]) -> tuple[list[Cell], int]:
  closest = []
  closest_dist = -1
  
  seen = set()

  q: deque[tuple[Cell, int]] = deque([(start, 0)])

  while q:
    cell, dist = q.popleft()
    if closest_dist > 0 and dist > closest_dist:
      break

    if cell in seen or cell in exclude:
      continue

    seen.add(cell)

    if cell in targets:
      closest.append(cell)
      closest_dist = dist

    for n in [c for c in cell.neighbors if c not in seen]:
      q.append((n, dist + 1))

  return sorted(closest, key=lambda x: x.pos()), closest_dist

def parse_map(lines: list[str], elfpower: int) -> tuple[dict[tuple[int, int], Cell], list[Unit]]:
  cells = {}
  units = []
    
  for y, line in enumerate(lines):
    for x, c in enumerate(line):
      if c == '#':
        continue

      cell = Cell(x, y)
      cells[cell.pos()] = cell

      if x > 0 and (y, x - 1) in cells:
        cell.add_neighbor(cells[(y, x - 1)])
      if y > 0 and (y - 1, x) in cells:
        cell.add_neighbor(cells[(y - 1, x)])

      if c == "E" or c == "G":
        unit = Unit(cell, c, damage=elfpower if c == "E" else 3)
        units.append(unit)

  return (cells, units)

if __name__ == '__main__':
  run()
  sys.exit(0)
