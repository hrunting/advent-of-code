#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  points = []

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    for line in input:
      (x, y) = (int(x) for x in re.split(r'\D+', line.strip()))
      points.append((x, y))

  points.sort()

  max_x = max([x for (x, _) in points])
  max_y = max([y for (_, y) in points])

  grid = {}
  areas = defaultdict(int)
  sum_grid = defaultdict(int)

  for cell in [(x, y) for x in range(max_x + 1) for y in range(max_y + 1)]:
    for point in points:
      dist = distance(cell, point)

      if cell not in grid or grid[cell][1] > dist:
        grid[cell] = (point, dist)
      elif cell in grid and grid[cell][1] == dist:
        grid[cell] = (None, dist)

      sum_grid[cell] += dist

  for (cell, (point, dist)) in grid.items():
    if point is not None:
      if cell[0] == 0 or cell[0] == max_x or cell[1] == 0 or cell[1] == max_y:
        areas[point] = -1
      elif areas[point] >= 0:
        areas[point] += 1

  point_area = max(areas.values())
  safe_area = sum([1 for v in sum_grid.values() if v < 10000])

  print(f'Safest Point Area: {point_area}')
  print(f'Largest Safe Area: {safe_area}')

def distance(p: tuple[int, int], q: tuple[int, int]) -> int:
  return abs(p[0] - q[0]) + abs(p[1] - q[1])

if __name__ == '__main__':
  run()
  sys.exit(0)
