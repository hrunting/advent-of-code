#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def starting_dist(s: tuple[int, int]) -> int:
  dist = 1
  while dist < s[1] - s[0]:
    dist *= 2
  return dist

def run() -> None:
  bots = {}

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as f:
    lines = [x for x in f]

  for line in lines:
    (x, y, z, rng) = (int(x) for x in re.findall(r'(?:-)?\d+', line))
    bots[(x, y, z)] = rng

  max_point = sorted(bots.keys(), key=lambda x: bots[x])[-1]
  print(f'Max Radius: {bots[max_point]} {max_point}')

  # Part 1
  in_range = [x for x in bots.keys() if distance(max_point, x) <= bots[max_point]]
  print(f'In Range: {len(in_range)}')

  # Part 2
  #
  # I needed to refer to the subreddit for a solution, reimplemented here
  #
  # The logic here is to create a bounding box that encompasses all of the
  # points in the space, and then do a binary search through the space,
  # subdividing each box and looking for which box has the most
  # in-range points. That region becomes the bounding box for the next
  # iteration of the binary search
  #
  # I think this solution probably fails in certain scenarios where
  # a region contains lots of non-overlapping points and another region
  # contains a few points that all overlap with each other

  xs = [p[0] for p in bots.keys()]
  ys = [p[1] for p in bots.keys()]
  zs = [p[2] for p in bots.keys()]

  xs = (min(xs), max(xs))
  ys = (min(ys), max(ys))
  zs = (min(zs), max(zs))

  x_dist = starting_dist(xs)
  y_dist = starting_dist(ys)
  z_dist = starting_dist(zs)

  while True:
    most = 0
    best = None
    best_dist = -1

    for x in range(xs[0], xs[1] + 1, x_dist):
      for y in range(ys[0], ys[1] + 1, y_dist):
        for z in range(zs[0], zs[1] + 1, z_dist):
          points = 0

          for p, rng in bots.items():
            pdist = distance(p, (x, y, z))
            if (pdist - rng) <= 0:
              points += 1

          if points > most:
            most = points
            best = (x, y, z)
            best_dist = distance((x, y, z), (0, 0, 0))

          elif points > 0 and points == most:
            target_dist = distance((x, y, z), (0, 0, 0))
            if best_dist < 0 or target_dist < best_dist:
              best = (x, y, z)
              best_dist = target_dist

    if x_dist == 1 and y_dist == 1 and z_dist == 1:
      break

    if best is None:
      xs = (-x_dist, x_dist)
      ys = (-y_dist, y_dist)
      zs = (-z_dist, z_dist)

    else:
      xs = (best[0] - x_dist, best[0] + x_dist)
      ys = (best[1] - y_dist, best[1] + y_dist)
      zs = (best[2] - z_dist, best[2] + z_dist)

    if x_dist > 1:
      x_dist //= 2
    if y_dist > 1:
      y_dist //= 2
    if z_dist > 1:
      z_dist //= 2

  print(f'Distance: {best_dist} {best}')

def distance(a: tuple[int, int, int], b: tuple[int, int, int]) -> int:
  return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])

if __name__ == '__main__':
  run()
  sys.exit(0)
