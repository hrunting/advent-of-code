#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict, deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    (players, marbles) = (int(x) for x in re.findall(r'\d+', input.read()))

  score = game(players, marbles)
  print(f'Players: {players}  Marbles: {marbles} = Score: {score}')

  score = game(players, marbles * 100)
  print(f'Players: {players}  Marbles: {marbles * 100} = Score: {score}')

def game(players: int, marbles: int) -> int:
  scores = defaultdict(int)
  ring = deque([0])

  for marble in range(1, marbles + 1):
    if marble % 23 == 0:
      ring.rotate(-7)
      scores[marble % players] += marble + ring.popleft()
      ring.rotate(1)

    else:
      ring.rotate(1)
      ring.appendleft(marble)

  max_score = max(scores.values())
  return max_score

if __name__ == '__main__':
  run()
  sys.exit(0)
