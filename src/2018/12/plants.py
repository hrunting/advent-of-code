#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  (pots, potsum) = sum_pots(input_file, 20)
  print(f'Gen: 20  Pots: {pots}')
  print(f'Sum of Pots: {potsum}')

  (pots, potsum) = sum_pots(input_file, 50000000000)
  print(f'Gen: 20  Pots: {pots}')
  print(f'Sum of Pots: {potsum}')

def sum_pots(input_file: pathlib.Path, generations: int) -> tuple[str, int]:
  matches = []

  with open(input_file) as input:
    pots = input.readline().strip().replace('initial state: ', '')

    input.readline()

    for line in input:
      (pattern, result) = line.strip().split(" => ")

      if result == pattern[2]:
        continue

      pattern = "(?=(" + pattern.replace('.', "\\.") + "))"
      matches.append([re.compile(pattern), result])

  gen = 0
  leftpot = 0
  seen = {}

  while gen < generations:
    while pots[0:4] != "....":
      pots = '.' + pots
      leftpot -= 1

    while pots[-4:] != '....':
      pots = pots + '.'

    while pots[0:5] == '.....':
      pots = pots[1:]
      leftpot += 1

    next = pots

    for (pattern, result) in matches:
      for m in pattern.finditer(pots):
        idx = m.start(1) + 2
        if (next[idx] != result):
          next = next[0:idx] + result + next[(idx + 1):]

    gen += 1

    if next in seen:
      (prev_gen, prev_leftpot) = seen[next]
      gen_delta = gen - prev_gen
      leftpot_delta = leftpot - prev_leftpot

      repeats = (generations - gen) // gen_delta

      gen += gen_delta * repeats
      leftpot += leftpot_delta * repeats

    seen[next] = (gen, leftpot)

    pots = next

  potsum = sum(i for i in range(leftpot, len(pots) + leftpot) if pots[i - leftpot] == '#')
  return(pots, potsum)

if __name__ == '__main__':
  run()
  sys.exit(0)
