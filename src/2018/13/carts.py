#!/usr/bin/env python3

import pathlib
import sys

from typing import Any, Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Coord = tuple[int, int]
NodeMap = dict[Coord, dict[str, Any]]
CartMap = dict[Coord, dict[str, Any]]

others = {
  '^': ['<', '>'],
  '>': ['^', 'v'],
  'v': ['>', '<'],
  '<': ['v', '^']
}

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  (map, carts) = build_map(input_file)

  first_crash = None

  while True:
    for coord in sorted(carts.keys()):
      if coord in carts:
        crash = move_cart(map, carts, carts[coord])
        if crash is not None:
          if first_crash is None:
            first_crash = crash

          del carts[coord]
          del carts[crash]

    if len(carts.keys()) == 1:
      break

  print(f'First Crash: {first_crash}')
  print(f'Last Cart: {list(carts.keys())[0]}')

def move_cart(map: NodeMap, carts: CartMap, cart: dict[str, Any]) -> Optional[Coord]:
  node = map[cart['coord']]
  next = node[cart['dir']]

  if next is None:
    for dir in others[cart['dir']]:
      next = node[dir]
      if next is not None:
        cart['dir'] = dir
        break

  else:
    if node[others[cart['dir']][0]] is not None:
      if cart['turn'] == 0:
        cart['dir'] = others[cart['dir']][0]
      elif cart['turn'] == 2:
        cart['dir'] = others[cart['dir']][1]

      next = node[cart['dir']]
      cart['turn'] = (cart['turn'] + 1) % 3

  if next['coord'] in carts:
    return next['coord']

  del carts[cart['coord']]
  cart['coord'] = next['coord']
  carts[cart['coord']] = cart

  return None

def build_map(f: pathlib.Path) -> tuple[NodeMap, CartMap]:
  nodes = {}
  carts = {}

  with open(f) as input:
    y = 0

    for line in input:
      x = 0

      for c in line.rstrip():
        if c == ' ':
          x += 1
          continue

        coord = (x, y)

        node = {
          '*': c,
          '^': None,
          '>': None,
          'v': None,
          '<': None,
          'coord': coord
        }

        cart = None

        if c == '-' or c == '>' or c == '<':
          node['<'] = nodes[(x - 1, y)]
          nodes[(x - 1, y)]['>'] = node

          if c != '-':
            cart = new_cart(c, coord)

        elif c == '|' or c == '^' or c == 'v':
          node['^'] = nodes[(x, y - 1)]
          nodes[(x, y - 1)]['v'] = node

          if c != '|':
            cart = new_cart(c, coord)

        elif c == '/' and (x - 1, y) in nodes and nodes[(x - 1, y)]['*'] in {'-', '+', '\\'}:
          node['<'] = nodes[(x - 1, y)]
          nodes[(x - 1, y)]['>'] = node

          node['^'] = nodes[(x, y - 1)]
          nodes[(x, y - 1)]['v'] = node

        elif c == '\\':
          if (x, y - 1) in nodes and nodes[(x, y - 1)]['*'] in {'|', '+', '/'}:
            node['^'] = nodes[(x, y - 1)]
            nodes[(x, y - 1)]['v'] = node

          else:
            node['<'] = nodes[(x - 1, y)]
            nodes[(x - 1, y)]['>'] = node

        elif c == '+':
          node['<'] = nodes[(x - 1, y)]
          nodes[(x - 1, y)]['>'] = node

          node['^'] = nodes[(x, y - 1)]
          nodes[(x, y - 1)]['v'] = node

        nodes[(x, y)] = node

        if cart is not None:
          carts[coord] = cart

        x += 1

      y += 1

  return (nodes, carts)

def new_cart(dir: str, coord: tuple[int, int]) -> dict[str, Any]:
  cart = {
    'coord': coord,
    'dir': dir,
    'turn': 0
  }

  return cart

if __name__ == '__main__':
  run()
  sys.exit(0)
