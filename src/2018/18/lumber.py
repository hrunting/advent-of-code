#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DEBUG = False

class Acre:
  def __init__(self: Self, type: str) -> None:
    self.adjacent = []

    self._type = type

    self.tree_count = 0
    self.yard_count = 0

  def __repr__(self: Self) -> str:
    return f'{self._type}'

  def type(self: Self, val: Optional[str] = None) -> str:
    if val is not None:
      if self._type != val:

        for acre in self.adjacent:
          acre.tree_count -= 1 if self._type == '|' else 0
          acre.yard_count -= 1 if self._type == '#' else 0

          acre.tree_count += 1 if val == '|' else 0
          acre.yard_count += 1 if val == '#' else 0

        self._type = val

    return self._type

  def next(self: Self) -> str:
    if self._type == '.':
      return '|' if self.tree_count >= 3 else '.'
    elif self._type == '|':
      return '#' if self.yard_count >= 3 else '|'
    else:
      return '#' if self.yard_count >= 1 and self.tree_count >= 1 else '.'

  def add_adjacent(self: Self, acre: Self) -> None:
    self.adjacent.append(acre)
    acre.adjacent.append(self)

    acre.tree_count += 1 if self._type == '|' else 0
    acre.yard_count += 1 if self._type == '#' else 0
    self.tree_count += 1 if acre._type == '|' else 0
    self.yard_count += 1 if acre._type == '#' else 0

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file) if x.strip() != '']

  (map, score) = age(lines, 10)
  if DEBUG:
    print_map(map)
    print()
  print(f'10 Minute Score: {score}')

  (map, score) = age(lines, 1000000000)
  if DEBUG:
    print_map(map)
    print()
  print(f'1B Minute Score: {score}')

def age(lines: list[str], minutes: int) -> tuple[list[list[Acre]], int]:
  map = []

  for y, line in enumerate(lines):
    row = []

    for x, type in enumerate(line):
      acre = Acre(type)
      row.append(acre)

      if x > 0:
        acre.add_adjacent(row[x - 1])

        if y > 0:
          acre.add_adjacent(map[y - 1][x - 1])

      if y > 0:
        acre.add_adjacent(map[y - 1][x])

        if x < len(map[y - 1]) - 1:
          acre.add_adjacent(map[y - 1][x + 1])

    map.append(row)

  if DEBUG:
    print_map(map)
    print()

  check = set([acre for row in map for acre in row])

  checksum = ''.join(f'{x}' for row in map for x in row)
  cycle = 0

  seen = {}
  seen[checksum] = cycle

  while cycle < minutes:
    next = set()

    for acre, n in [(acre, acre.next()) for acre in check]:
      if n != acre.type():
        acre.type(n)
        next.add(acre)
        next |= set(acre.adjacent)

    check = next

    cycle += 1
    checksum = ''.join(f'{x}' for row in map for x in row)

    if checksum in seen:
      period = cycle - seen[checksum]
      cycle += ((minutes - cycle) // period) * period
      seen = {}

    seen[checksum] = cycle

  wooded = sum([1 for y in map for x in y if x.type() == '|'])
  lumber = sum([1 for y in map for x in y if x.type() == '#'])

  return(map, wooded * lumber)

def print_map(map: list[list[Acre]]) -> None:
  for x in range(len(map[0])):
    for y in range(len(map)):
      print(map[x][y], end='')
    print()

if __name__ == '__main__':
  run()
  sys.exit(0)
