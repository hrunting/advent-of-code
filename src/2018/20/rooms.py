#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Room:
  dirs = {
    'N': 0,
    'E': 1,
    'S': 2,
    'W': 3
  }

  def __init__(self: Self, x: int, y: int) -> None:

    # N, E, S, W
    self.neighbors: list[Optional[Room]] = [None, None, None, None]
    self.x = x
    self.y = y

  def __repr__(self: Self) -> str:
    return f'{self.x, self.y}'

  def neighbor(self: Self, dir: str, room: Optional[Self] = None) -> Optional[Self]:
    if room is not None:
      self.neighbors[Room.dirs[dir]] = room
      room.neighbors[(Room.dirs[dir] + 2) % 4] = self

    return self.neighbors[Room.dirs[dir]]

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  map = open(input_file).read().rstrip()[1:-1]

  room = Room(0, 0)
  grid: dict[tuple[int, int], Room] = { (0, 0): room }

  stack = []
  roots = { room }
  start, end = { room }, set()

  for c in map:
    if c in "NESW":
      neighbors = set()

      for r in roots:
        if r.neighbor(c) is not None:
          neighbor = r.neighbor(c)

        else:
          x = r.x + (1 if c == 'E' else (-1 if c == 'W' else 0))
          y = r.y + (1 if c == 'S' else (-1 if c == 'N' else 0))

          if (x, y) not in grid:
            grid[(x, y)] = Room(x, y)

          neighbor = grid[(x, y)]
          r.neighbor(c, neighbor)

        neighbors.add(neighbor)

      roots = neighbors

    elif c == '|':
      end.update(roots)
      roots = start

    elif c == '(':
      stack.append((start, end))
      start, end = roots, set()

    elif c == ')':
      roots.update(end)
      start, end = stack.pop()

  visited = set()

  farthest = 0
  distances = {}

  q: deque[tuple[Room, int]] = deque([(room, 0)])

  while q:
    r, dist = q.popleft()

    if r in visited:
      continue

    visited.add(r)

    farthest = max(dist, farthest)
    distances[r] = max(dist, distances.get(r, 0))

    for x in [n for n in r.neighbors if n is not None and n not in visited]:
      q.append((x, dist + 1))

  print(f'Farthest Doors: {farthest}')
  print(f'Distant Doors: {len([x for x in distances.values() if x >= 1000])}')


if __name__ == '__main__':
  run()
  sys.exit(0)
