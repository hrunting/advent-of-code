#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict
from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with input_file.open() as input:
    lines = sorted([line.strip() for line in input])

  guards = process_log(lines)

  (guard, minute) = strategy1(guards)
  print(f'Strategy #1 Result: {guard * minute} (ID={guard} minute={minute})')

  (guard, minute) = strategy2(guards)
  print(f'Strategy #2 Result: {guard * minute} (ID={guard} minute={minute})')

def process_log(lines: list[str]) -> defaultdict[int, Any]:
  guard_re = re.compile(r'\d+\] Guard #(\d+)')
  asleep_re = re.compile(r'(\d+)\] falls')
  wake_re = re.compile(r'(\d+)\] wakes')

  guards: defaultdict[int, Any] = defaultdict(lambda: {'minutes': 0, 'frequency': defaultdict(int)})

  guard = None
  asleep = None

  for line in lines:
    m = guard_re.search(line)
    if m is not None:
      guard = int(m.group(1))
      asleep = None
      continue

    m = asleep_re.search(line)
    if m is not None:
      asleep = int(m.group(1))
      continue

    m = wake_re.search(line)
    if m is not None:
      wake = int(m.group(1))

      if guard is not None and asleep is not None:
        guards[guard]['minutes'] += wake - asleep
        for minute in range(asleep, wake):
          guards[guard]['frequency'][minute] += 1

      asleep = None

  return guards

def strategy1(guards: defaultdict[int, Any]) -> tuple[int, int]:
  max_guard = -1
  max_minutes = 0

  for (guard, data) in guards.items():
    if data['minutes'] > max_minutes:
      max_guard = guard
      max_minutes = data['minutes']

  max_minute = -1
  max_frequency = 0

  for (minute, frequency) in guards[max_guard]['frequency'].items():
    if frequency > max_frequency:
      max_minute = minute
      max_frequency = frequency

  return (max_guard, max_minute)

def strategy2(guards: defaultdict[int, Any]) -> tuple[int, int]:
  max_guard = -1
  max_minute = -1
  max_frequency = 0

  for (guard, data) in guards.items():
    for (minute, frequency) in data['frequency'].items():
      if frequency > max_frequency:
        max_guard = guard
        max_minute = minute
        max_frequency = frequency

  return (max_guard, max_minute)

if __name__ == '__main__':
  run()
  sys.exit(0)
