#!/usr/bin/env python3

import inspect
import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DEBUG = False

def run():
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file) if x.strip() != '']

  map, fountain, min_y = parse_map(lines)
  (x, y) = fountain

  sys.setrecursionlimit(len(map[0]) + len(inspect.stack()))

  pour(map, x, y)

  if DEBUG:
    print_map(map)

  print(f'Water tiles: {len([1 for x in map for y in x[min_y:] if y[0] in {"~", "|"}])}')
  print(f'Retained tiles: {len([1 for x in map for y in x[min_y:] if y[0] == "~"])}')

def print_map(map: list[list[list[str]]]) -> None:
  for y in range(len(map[0])):
    for x in range(len(map)):
      print(map[x][y][0], end='')
    print()

def pour(map: list[list[list[str]]], x: int, y: int, depth: int = 0):
  if y >= len(map[0]) - 1:
    return

  if map[x][y + 1][0] == '.':
    map[x][y + 1][0] = '|'
    pour(map, x, y + 1, depth + 1)

  if map[x][y + 1][0] in {'#', '~'}:
    bound_left, bound_right = False, False
    left, right = (x - 1, x + 1)
    filled = [map[x][y]]

    while not bound_left and left > 0:
      if map[left][y][0] == '#':
        bound_left = True

      elif map[left][y][0] == '|':
        break

      elif map[left][y][0] == '.':
        map[left][y][0] = '|'

        if map[left][y + 1][0] == '.':
          map[left][y + 1][0] = '|'
          pour(map, left, y + 1, depth + 1)

        if map[left][y + 1][0] in {'#', '~'}:
          filled.append(map[left][y])
          left -= 1

        else:
          break

    while not bound_right and right > 0:
      if map[right][y][0] == '#':
        bound_right = True

      elif map[right][y][0] == '|':
        break

      elif map[right][y][0] == '.':
        map[right][y][0] = '|'

        if map[right][y + 1][0] == '.':
          map[right][y + 1][0] = '|'
          pour(map, right, y + 1, depth + 1)

        if map[right][y + 1][0] in {'#', '~'}:
          filled.append(map[right][y])
          right += 1

        else:
          break

    if bound_left and bound_right:
      for col in filled:
        col[0] = '~'

  return

def parse_map(lines: list[str]) -> tuple[list[list[list[str]]], tuple[int, int], int]:
  clay = {}

  fountain = (500, 0)

  min_x = -1
  min_y = -1
  max_x = -1
  max_y = -1

  for line in lines:
    dir1 = pos1 = pos2 = rng2 = ""

    if m := re.match(r'([xy])=(\d+), [xy]=(\d+)(?:\.\.(\d+))', line):
      dir1, pos1, pos2, rng2 = m.groups()

    if rng2 == '':
      rng2 = pos2

    pos1 = int(pos1)
    pos2 = int(pos2)
    rng2 = int(rng2)

    rng2 += 1

    if dir1 == 'x':
      for y in range(pos2, rng2):
        clay[(pos1, y)] = '#'
        min_x = pos1 - 1 if min_x < 0 or pos1 - 1 < min_x else min_x
        min_y = y if min_y < 0 or y < min_y else min_y
        max_x = pos1 + 1 if max_x < 0 or pos1 + 1 > max_x else max_x
        max_y = y + 1 if max_y < 0 or y + 1 > max_y else max_y

    else:
      for x in range(pos2, rng2):
        clay[(x, pos1)] = '#'
        min_x = x - 1 if min_x < 0 or x - 1 < min_x else min_x
        min_y = pos1 if min_y < 0 or pos1 < min_y else min_y
        max_x = x + 1 if max_x < 0 or x + 1 > max_x else max_x
        max_y = pos1 + 1 if max_y < 0 or pos1 + 1 > max_y else max_y

  map = []

  for x in range(min_x, max_x + 1):
    column = []
    for y in range(max_y):
      type = '+' if fountain == (x, y) else '#' if (x, y) in clay else '.'
      column.append([type])

    map.append(column)

  return map, (fountain[0] - min_x, 0), min_y

if __name__ == '__main__':
  run()
  sys.exit(0)
