#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  ops = {
    "addr": lambda r, A, B: r[A] + r[B],
    "addi": lambda r, A, B: r[A] + B,
    "mulr": lambda r, A, B: r[A] * r[B],
    "muli": lambda r, A, B: r[A] * B,
    "banr": lambda r, A, B: r[A] & r[B],
    "bani": lambda r, A, B: r[A] & B,
    "borr": lambda r, A, B: r[A] | r[B],
    "bori": lambda r, A, B: r[A] | B,
    "setr": lambda r, A, _: r[A],
    "seti": lambda r, A, _: A,
    "gtir": lambda r, A, B: 1 if A > r[B] else 0,
    "gtri": lambda r, A, B: 1 if r[A] > B else 0,
    "gtrr": lambda r, A, B: 1 if r[A] > r[B] else 0,
    "eqir": lambda r, A, B: 1 if A == r[B] else 0,
    "eqri": lambda r, A, B: 1 if r[A] == B else 0,
    "eqrr": lambda r, A, B: 1 if r[A] == r[B] else 0
  }

  def __init__(self: Self) -> None:
    self.registers = [0, 0, 0, 0]

  def __repr__(self: Self) -> str:
    return f'{self.registers}'

  def op(self: Self, op: str, A: int, B: int, C: int):
    self.registers[C] = Machine.ops[op](self.registers, A, B)
    return self.registers[C]

def run() -> None:
  machine = Machine()

  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file) if x.strip() != '']

  samples = []
  program = []

  while len(lines) > 0:
    line = lines.pop(0)

    if line.startswith('Before: '):
      before = [int(x) for x in re.findall(r'\d+', line)]
      (code, A, B, C) = (int(x) for x in re.findall(r'\d+', lines.pop(0)))
      after = [int(x) for x in re.findall(r'\d+', lines.pop(0))]

      sample = {'code': code, 'ops': set()}

      for op in Machine.ops.keys():
        machine.registers = [x for x in before]
        machine.op(op, A, B, C)
        if machine.registers == after:
          sample['ops'].add(op)

      samples.append(sample)

    else:
      program.append((int(x) for x in re.findall(r'\d+', line)))

  print(f'3-Op Samples: {len([x for x in samples if len(x["ops"]) >= 3])}')

  codes = {}

  for sample in samples:
    if sample["code"] in codes:
      codes[sample["code"]] = codes[sample["code"]] & sample["ops"]
    else:
      codes[sample["code"]] = sample["ops"]

  
  while True:
    assigned = [x for x in codes.keys() if len(codes[x]) == 1]
    if len(assigned) == len(codes.keys()):
      break

    for good in assigned:
      for bad in [x for x in codes.keys() if len(codes[x]) > 1]:
        codes[bad] = codes[bad] - codes[good]

  for code in codes.keys():
    codes[code] = next(iter(codes[code]))

  machine.registers = [0, 0, 0, 0]

  for (code, A, B, C) in program:
    machine.op(codes[code], A, B, C)

  print(f'Machine: {machine}')

if __name__ == '__main__':
  run()
  sys.exit(0)
