#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    score = input.read().strip()
    score_length = len(score)
    score_count = int(score)

  scores = ["3", "7"]
  ending = "37"

  elf1 = 0
  elf2 = 1

  while score not in ending:

    new = str(int(scores[elf1]) + int(scores[elf2]))
    scores.extend(list(new))
    ending = (ending + new)[-(score_length + 1):]

    elf1 = (elf1 + int(scores[elf1]) + 1) % len(scores)
    elf2 = (elf2 + int(scores[elf2]) + 1) % len(scores)

  print(f'Score: {"".join(scores[score_count:score_count + 10])}')
  print(f'Recipes: {len(scores) - len(ending) + ending.index(score)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
