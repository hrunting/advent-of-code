#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  frequency = 0
  frequencies = { frequency }

  result_frequency = None
  repeat_frequency = None

  input_file = aoc.inputfile('input.txt')

  while repeat_frequency is None:
    with input_file.open() as input:
      for line in input:
        frequency += int(line.strip())

        if repeat_frequency is None and frequency in frequencies:
          repeat_frequency = frequency

        frequencies.add(frequency)

    if result_frequency is None:
      result_frequency = frequency

  print(f'Result Frequency: {result_frequency}')
  print(f'Repeat Frequency: {repeat_frequency}')

if __name__ == '__main__':
  run()
  sys.exit(0)
