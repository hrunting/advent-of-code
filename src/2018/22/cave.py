#!/usr/bin/env python3

import heapq
import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Map = list[list[tuple[int, int]]]

DEBUG = False

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as f:
    (depth, ) = (int(x) for x in re.findall(r'\d+', f.readline()))
    tx, ty = (int(x) for x in re.findall(r'\d+', f.readline()))

  map = build_map(depth, tx, ty)

  risk = sum([x[1] for y in map[:ty + 1] for x in y[:tx + 1]])
  print(f'Risk: {risk}')

  minutes = find_target(map, depth, tx, ty)
  print(f'Minutes: {minutes}')

  if DEBUG:
    print_map(map, tx, ty)

def find_target(map: Map, depth: int, tx: int, ty: int) -> int:
  minutes = 0

  dist = tx + ty

  # 0: no tools
  # 1: torch
  # 2: climbing gear
  allowed_tools = {
    0: {1, 2},
    1: {0, 2},
    2: {0, 1}
  }

  # time spent, item equipped, cell
  q: list[tuple[int, int, tuple[int, int, int]]] = [(minutes, dist, (0, 0, 1))]
  visited = set()

  while True:
    minutes, dist, (rx, ry, tool) = heapq.heappop(q)

    if (rx, ry, tool) in visited:
      continue

    visited.add((rx, ry, tool))

    r_tools = allowed_tools[map[ry][rx][1]]

    if rx == tx and ry == ty:
      if tool == 1:
        break

      else:
        heapq.heappush(q, (minutes + 7, dist, (rx, ry, 1)))

    else:
      for x, y in [(rx + 1, ry), (rx, ry + 1), (rx - 1, ry), (rx, ry - 1)]:
        if x < 0 or y < 0:
          continue

        if y >= len(map):
          add_map_layer(map, depth)

        if x >= len(map[0]):
          add_map_column(map, depth)

        dist = abs(tx - x) + abs(ty - y)

        for t in [e for e in allowed_tools[map[y][x][1]] if e in r_tools]:
          heapq.heappush(q, (minutes + (1 if t == tool else 8), dist, (x, y, t)))

  return minutes

def erosion(geo_idx: int, depth: int) -> int:
  return (geo_idx + depth) % 20183

def build_map(depth: int, tx: int, ty: int) -> Map:
  map = []

  for y in range(ty + 1):
    layer = []
    for x in range(tx + 1):
      if x == 0 and y == 0:
        geologic_index = 0

      elif x == tx and y == ty:
        geologic_index = 0

      elif x == 0:
        geologic_index = y * 48271

      elif y == 0:
        geologic_index = x * 16807

      else:
        geologic_index = layer[x - 1][0] * map[y - 1][x][0]

      erosion_level = erosion(geologic_index, depth)

      layer.append((erosion_level, erosion_level % 3))

    map.append(layer)

  return map

def add_map_layer(map: Map, depth: int) -> None:
  layer = []
  y = len(map)

  for x in range(len(map[0])):
    if x == 0:
      geologic_index = y * 48271
    else:
      geologic_index = layer[x - 1][0] * map[y - 1][x][0]

    erosion_level = erosion(geologic_index, depth)
    layer.append((erosion_level, erosion_level % 3))

  map.append(layer)

def add_map_column(map: Map, depth: int) -> None:
  x = len(map[0])

  for y, layer in enumerate(map):
    if y == 0:
      geologic_index = x * 16807
    else:
      geologic_index = layer[x - 1][0] * map[y - 1][x][0]

    erosion_level = erosion(geologic_index, depth)
    layer.append((erosion_level, erosion_level % 3))

def print_map(map: Map, tx: int, ty: int) -> None:
  types = {
    0: '.',
    1: '=',
    2: '|'
  }

  for y, layer in enumerate(map):
    for x, region in enumerate(layer):
      if x == 0 and y == 0:
        print('M', end='')
      elif x == tx and y == ty:
        print('T', end='')
      else:
        print(f'{types[layer[x][1]]}', end='')
    print()

if __name__ == '__main__':
  run()
  sys.exit(0)
