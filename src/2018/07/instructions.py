#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict
from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  order = step_order(build_nodes(input_file))
  print(f'Step Order: {order}')

  (order, seconds) = process(build_nodes(input_file))
  print(f'Step Order: {order}')
  print(f'Process Time: {seconds}')


def build_nodes(input_file: pathlib.Path) -> defaultdict[str, Any]:
  step_re = re.compile(r'^Step (\S) must be finished before step (\S) ')

  nodes: defaultdict[str, Any] = defaultdict(lambda: { 'step': None, 'prev': set(), 'next': set()})

  with open(input_file) as input:
    for line in input:
      line = line.strip()
      m = step_re.match(line)
      if m is not None:
        (first, second) = m.group(1, 2)

        nodes[first]['step'] = first
        nodes[first]['next'].add(second)

        nodes[second]['step'] = second
        nodes[second]['prev'].add(first)

  return nodes

def step_order(nodes: defaultdict[str, Any]) -> str:
  order = ''

  while len(nodes.keys()) > 0:
    step = sorted([n for n, d in nodes.items() if len(d['prev']) == 0]).pop(0)

    order += step

    for next in nodes[step]['next']:
      nodes[next]['prev'].remove(step)

    del nodes[step]

  return order

def process(nodes: defaultdict[str, Any], workers: int = 5, seconds: int = 60) -> tuple[str, int]:
  jobs = []
  second = 0
  order = ''

  while len(jobs) > 0 or len(nodes.keys()) > 0:
    if len(jobs) > 0:
      (second, node) = jobs.pop(0)

      order += node['step']

      for next in node['next']:
        nodes[next]['prev'].remove(node['step'])

    while len(jobs) < workers and len(nodes.keys()) > 0:
      ordered = sorted([n for n, d in nodes.items() if len(d['prev']) == 0])
      if len(ordered) == 0:
        break

      step = ordered.pop(0)

      jobs.append((second + seconds + ord(step) - ord('A') + 1, nodes[step]))
      jobs = sorted(jobs)

      del nodes[step]

  return (order, second)

if __name__ == '__main__':
  run()
  sys.exit(0)
