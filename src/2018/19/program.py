#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  ops = {
    "addr": lambda r, A, B: r[A] + r[B],
    "addi": lambda r, A, B: r[A] + B,
    "mulr": lambda r, A, B: r[A] * r[B],
    "muli": lambda r, A, B: r[A] * B,
    "banr": lambda r, A, B: r[A] & r[B],
    "bani": lambda r, A, B: r[A] & B,
    "borr": lambda r, A, B: r[A] | r[B],
    "bori": lambda r, A, B: r[A] | B,
    "setr": lambda r, A, _: r[A],
    "seti": lambda r, A, _: A,
    "gtir": lambda r, A, B: 1 if A > r[B] else 0,
    "gtri": lambda r, A, B: 1 if r[A] > B else 0,
    "gtrr": lambda r, A, B: 1 if r[A] > r[B] else 0,
    "eqir": lambda r, A, B: 1 if A == r[B] else 0,
    "eqri": lambda r, A, B: 1 if r[A] == B else 0,
    "eqrr": lambda r, A, B: 1 if r[A] == r[B] else 0
  }

  def __init__(self: Self) -> None:
    self.ip = 0
    self.ip_register = None
    self.registers = [0, 0, 0, 0, 0, 0]

  def __repr__(self: Self) -> str:
    return f'{self.registers}'

  def op(self: Self, op: str, A: int, B: int, C: int) -> int:

    # #ip is not an instruction
    if op == "#ip":
      self.ip_register = A
      return self.registers[self.ip_register]

    # otherwise, we have an instruction
    else:
      if self.ip_register is not None:
        self.registers[self.ip_register] = self.ip
      
      self.registers[C] = Machine.ops[op](self.registers, A, B)

      if self.ip_register is not None:
        self.ip = self.registers[self.ip_register]
        self.ip += 1

      return self.registers[C]

def run():
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file) if x.strip() != '']

  machine = run_program(lines)
  print(f'Machine: {machine}')

  r0 = run_compiled(0)
  print(f'Register 0 (r0=0): {r0}')

  r0 = run_compiled(1)
  print(f'Register 0 (r0=1): {r0}')


def run_program(code, register=0):
  machine = Machine()
  machine.registers[0] = register
  
  program = []
  for line in code:
    instr, rest = line.split(' ', 1)
    if instr == '#ip':
      machine.op(instr, int(rest), -1, -1)

    else:
      A, B, C = (int(x) for x in rest.split(' '))
      program.append((instr, A, B, C))

  end = len(program)

  while machine.ip < end:
    (instr, A, B, C) = program[machine.ip]
    machine.op(instr, A, B, C)
    if register == 1:
      print(f'{instr} {A} {B} {C} -> {machine}')

  return machine


# The program compiles down to this
#
# R0, R1, R2, R3, R4, R5 = 0, 0, 0, 0, 0, 0
# R5 = (27 * 28 + 29) * 30 * 14 * 32
# R4 = ((R4 + 2)**2) * 19 * 11 + 158 + R5
#
# R3 = 1
# 
# while R3 <= R4:
#   R1 = 1
#
#   while R1 <= R4:
#     if R4 == R3 * R1:
#       R0 += R3
#     R1 += 1
#
#   R3 += 1
#
# The inner loop adds the factors of R4 to R0
# so let's optimize that out
def run_compiled(r0):
  r4 = ((0 + 2)**2) * 19 * 11 + 158
  if r0 != 0:
    r4 += (27 * 28 + 29) * 30 * 14 * 32

  r0 = 0
  r3 = 1

  while r3 <= r4:
    if r4 % r3 == 0:
      r0 += r3

    r3 += 1

  return r0


if __name__ == '__main__':
  run()
  sys.exit(0)
