#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Group:
  def __init__(
      self: Self, army: str, units: int, hits: int, immune: set[str],
      weak: set[str], strength: str, damage: int, initiative: int):

    self.army = army
    self.units = units
    self.hits = hits
    self.immune = immune
    self.weak = weak
    self.strength = strength
    self.damage = damage
    self.initiative = initiative

  def __repr__(self: Self) -> str:
    return (
      f'A={self.army} U={self.units} H={self.hits} Im={self.immune} '
      f'We:{self.weak} S={self.strength} D={self.damage} I={self.initiative}'
    )

  def power(self: Self) -> int:
    return self.units * self.damage

  def projected_damage(self: Self, group: Self) -> int:
    power = self.power()

    if self.strength in group.immune:
      power *= 0
    elif self.strength in group.weak:
      power *= 2

    return power

  def attack(self: Self, group: Self) -> None:
    if group.units > 0:
      damage = self.projected_damage(group)
      group.units -= (damage // group.hits)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file)]

  boost = 0
  army, units, groups = fight(lines, boost)

  print(f'Remaining Units: {units}  Boost: {boost}')
  
  min_boost = 1
  max_boost = None
  remaining_units = 0
  boost = 1

  while True:
    army, units, _ = fight(lines, boost)

    if army is not None and army == 'immune system':
      max_boost, boost = boost, min_boost + ((boost - min_boost) // 2)
      remaining_units = units
    else:
      min_boost = boost
      boost += boost if max_boost is None else ((max_boost - boost + 1) // 2)

    if max_boost is not None and boost == max_boost:
      break

  print(f'Remaining Units: {remaining_units}  Boost: {boost}')


def fight(lines: list[str], boost: int) -> tuple[Optional[Group], int, list[Group]]:
  groups = []
  army = None

  for line in lines:
    if line == '':
      continue
    if line.startswith('Immune'):
      army = 'immune system'
    elif line.startswith('Infection'):
      army = 'infection'
    else:
      m = re.search(
        r'^(\d+) units.*with (\d+) hit points(?: \(([^\)]+)\))?.*'
          r'does (\d+) (\S+) damage .*initiative (\d+)$',
        line
      )

      if not m:
        continue
      (units, hp, qualities, damage, attack, initiative) = m.groups()

      units = int(units)
      hp = int(hp)
      damage = int(damage)
      initiative = int(initiative)

      immune = set()
      weak = set()
      if qualities is not None:
        for quality, types in re.findall(r'(\S+) to ([^;]+)', qualities):
          types = types.split(r', ')
          quality = immune if quality == 'immune' else weak
          quality |= set(types)

      if army == 'immune system':
        damage += boost

      if army is not None:
        groups.append(Group(army, units, hp, immune, weak, attack, damage, initiative))

  while True:
    targets = find_targets(groups)
    if len(targets) == 0:
      break

    for attacker, target in targets:
      if attacker.units > 0:
        attacker.attack(target)

  winners = set([x.army for x in groups if x.units > 0])

  if len(winners) == 1:
    alive = [x for x in groups if x.units > 0]
    return alive[0].army, sum([x.units for x in alive]), groups
  else:
    return None, -1, groups

def find_targets(groups: list[Group]) -> list[tuple[Group, Group]]:
  pairings = []

  immune_system = set([x for x in groups if x.army == 'immune system' and x.units > 0])
  infection = set([x for x in groups if x.army == 'infection' and x.units > 0])

  if len(immune_system) > 0 and len(infection) > 0:
    for group in sorted(groups, key=lambda x: (-x.power(), -x.initiative)):
      target_groups = immune_system if group.army == 'infection' else infection
      targets = sorted(
        [x for x in target_groups if group.projected_damage(x) > 0],
        key=lambda x: (-group.projected_damage(x), -x.power(), -x.initiative)
      )

      if len(targets):
        target_groups.remove(targets[0])
        pairings.append((group, targets[0]))

  return sorted(pairings, key=lambda x: -x[0].initiative)

if __name__ == '__main__':
  run()
  sys.exit(0)
