#!/usr/bin/env python3

import pathlib
import sys

from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

# cache of node values
values = {}

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    license = [int(x) for x in input.read().strip().split(' ')]

  (tree, _)  = process_node(license)

  csum = checksum(tree)
  print(f'Checksum: {csum}')

  value = node_value(tree)
  print(f'Root Value: {value}')

def process_node(license: list[int], idx: int = 0) -> tuple[dict[str, Any], int]:
  node = {
    'idx': idx,
    'childs': license[idx],
    'metas': license[idx + 1],
    'child_nodes': [],
    'metadata': []
  }

  idx += 2

  for _ in range(node['childs']):
    (child, idx) = process_node(license, idx=idx)
    node['child_nodes'].append(child)

  for _ in range(node['metas']):
    node['metadata'].append(license[idx])
    idx += 1

  return (node, idx)

def checksum(node: dict[str, Any]) -> int:
  total = 0

  for child in node['child_nodes']:
    total += checksum(child)

  total += sum(node['metadata'])
  return total

def node_value(node: dict[str, Any]) -> int:
  if node['idx'] in values:
    return values[node['idx']]

  total = 0

  if node['childs'] == 0:
    total = sum(node['metadata'])

  else:
    for idx in [x - 1 for x in node['metadata']]:
      if idx >= 0 and idx < node['childs']:
        total += node_value(node['child_nodes'][idx])

  values[node['idx']] = total
  return total

if __name__ == '__main__':
  run()
  sys.exit(0)
