#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict
from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  cuts = []

  input_file = aoc.inputfile('input.txt')

  with input_file.open() as input:
    for line in input:
      (id, left, top, width, height) = tuple([int(x) for x in re.findall(r'\d+', line.strip())])
      cuts.append({
        'id': id,
        'left': left,
        'top': top,
        'width': width,
        'height': height,
        'right': left + width,
        'bottom': top + height,
        'overlaps': set()
      })

  (inches, cut) = overlaps(cuts)

  print(f'Overlapping Cuts: {inches}')
  print(f'Unique Cut ID: {cut}')

def overlaps(cuts: list[dict[str, Any]]) -> tuple[int, str]:
  fabric = defaultdict(set)

  for cut in cuts:
    for x in range(cut['left'], cut['right']):
      for y in range(cut['top'], cut['bottom']):
        fabric[(x, y)].add(cut['id'])

  overlapping_sum = 0
  unique_ids = { x['id'] for x in cuts }

  for sqin in fabric.keys():
    if len(fabric[sqin]) > 1:
      overlapping_sum += 1
      unique_ids -= fabric[sqin]

  unique_id, = tuple(unique_ids)

  return (overlapping_sum, unique_id)


if __name__ == '__main__':
  run()
  sys.exit(0)
