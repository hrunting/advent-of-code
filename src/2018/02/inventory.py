#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict
from typing import Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def frequency(s: str) -> dict[int, set[str]]:
  counts = {}
  frequencies = {}

  for c in s:
    counts[c] = counts.get(c, 0) + 1

  for (k, v) in counts.items():
    frequencies[v] = frequencies.get(v, set()) | { k }

  return frequencies

# We can take advantage of knowing that there will be only one
# pair of IDs with a single-character difference to perform a binary
# search across the list of IDs.
#
# Any IDs that share the same characters in the front half of their
# ID are candidates. The same holds true for any IDs that share the
# same characters in the back half of their IDs. Once you have your
# candidates, take the list of differing halves for each candidate
# and run them through the same algorithm.
#
# Keep doing this until you either run out of candidates (dead end)
# or you get to a candidate with single-character differing halves
# (your solution ID)
#
# NOTE:
# The straightforward way is to loop over each word, looping over
# each other word to look for character differences. How do we
# learn if we only do things the straightforward way?

def find_single_diff(lines: list[str]) -> Optional[str]:
  common = None

  left = defaultdict(set)
  right = defaultdict(set)

  line_length = len(lines[0])
  split_idx = line_length // 2

  for line in lines:
    front, back = line[0:split_idx], line[split_idx:]
    left[front].add(back)
    right[back].add(front)

  # first, check the left side for any candidates
  candidates = [(k, v) for (k, v) in left.items() if len(v) > 1]

  # if the line length is greater than two, then our
  # right half will definitely have multiple characters to which
  # we can apply our algorithm
  if line_length > 2:
    for (same, diffs) in candidates:
      common = find_single_diff(list(diffs))
      if common is not None:
        common = same + common
        break

  # otherwise, the right half must be a single character
  # and any candidate must be our solution
  elif len(candidates):
    (common, _) = candidates[0]

  # we did not find a solution ID in our left half
  # let's check the right half now (similar logic)
  if common is None:

    candidates = [(k, v) for (k, v) in right.items() if len(v) > 1]

    # if the split index is more than one, then our
    # left half has multiple characters to which we can apply
    # our algorithms
    if split_idx > 1:
      for (same, diffs) in candidates:
        common = find_single_diff(list(diffs))
        if common is not None:
          common = common + same
          break

    # otherwise, the left half must be a single character
    # and any candidate must be our solution
    elif len(candidates):
      (common, _) = candidates[0]

  # return whatever we have found
  # if nothing has been found, then this will be None
  return common

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with input_file.open() as input:
    lines = [line.strip() for line in input]

  two_char_ids = 0
  three_char_ids = 0

  for line in lines:
    char_frequencies = frequency(line.strip())
    if len(char_frequencies.get(2, set())) > 0:
      two_char_ids += 1
    if len(char_frequencies.get(3, set())) > 0:
      three_char_ids += 1

  checksum = two_char_ids * three_char_ids
  print(f'Checksum: {checksum}')

  common = find_single_diff(lines)
  print(f'Common Letters: {common}')

if __name__ == '__main__':
  run()
  sys.exit(0)
