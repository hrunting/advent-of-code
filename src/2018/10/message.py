#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Vector = tuple[int, int, int, int]

def run() -> None:
  stars = []

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    for line in input:
      stars.append(tuple(int(x) for x in re.findall(r'-?\d+', line)))

  (message, seconds) = find_message(stars)
  render_sky(message)
  print(f'Seconds: {seconds}')

def find_message(stars: list[Vector]) -> tuple[list[Vector], int]:
  seconds = 0
  (dx, dy) = distances(stars)

  while True:
    nstars = [(x + vx, y + vy, vx, vy) for (x, y, vx, vy) in stars]
    (ndx, ndy) = distances(nstars)

    if ndx > dx or ndy > dy:
      break

    stars, dx, dy = nstars, ndx, ndy
    seconds += 1

  return (stars, seconds)

def distances(stars: list[Vector]) -> tuple[int, int]:
  dx = max([x for (x, _, _, _) in stars]) - min([x for (x, _, _, _) in stars])
  dy = max([y for (_, y, _, _) in stars]) - min([y for (_, y, _, _) in stars])
  return (dx, dy)

def render_sky(sky: list[tuple[int, int, int, int]]) -> None:
  stars = [(x, y) for (x, y, _, _) in sky]

  min_x = min(x for (x, _) in stars)
  min_y = min(y for (_, y) in stars)
  max_x = max(x for (x, _) in stars)
  max_y = max(y for (_, y) in stars)

  for y in range(min_y, max_y + 1):
    if y > min_y:
      print()

    for x in range(min_x, max_x + 1):
      if (x, y) in stars:
        print('#', end='')
      else:
        print('.', end='')

  print()

if __name__ == '__main__':
  run()
  sys.exit(0)
