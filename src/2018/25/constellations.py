#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as f:
    lines = [x.strip() for x in f]

  points = []

  for line in lines:
    (x, y, z, q) = (int(x) for x in re.findall(r'(?:-)?\d+', line))
    points.append((x, y, z, q))

  dist = 3
  links = {}
  chains = {}

  for point in points:
    existing_chain_point = None

    for link, chain_point in links.items():
      if distance(point, link) <= dist:
        chain = chains[chain_point]
        chain.add(point)

        if existing_chain_point is None:
          existing_chain_point = chain_point
        elif existing_chain_point != chain_point:
          chains[existing_chain_point] |= chains[chain_point]
          del(chains[chain_point])

          for cp_link, cp in links.items():
            if cp == chain_point:
              links[cp_link] = existing_chain_point

    if existing_chain_point is None:
      existing_chain_point = point
      chains[existing_chain_point] = set([point])

    links[point] = existing_chain_point

  print(f'Chains: {len(chains.keys())}')

def distance(a: tuple[int, int, int, int], b: tuple[int, int, int, int]) -> int:
  return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2]) + abs(a[3] - b[3])

if __name__ == '__main__':
  run()
  sys.exit(0)
