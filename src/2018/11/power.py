#!/usr/bin/env python3

import pathlib
import sys

from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  max_all = {}
  max_size = 1
  max_power = 0

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    serial = int(input.read().strip())

  grid: list[list[dict[str, Any]]] = []

  for x in range(300):
    grid.append([])

    for y in range(300):
      cell = {
        'coord': (x + 1, y + 1),
        'rackid': x + 11,
        'power': ((((x + 11) * (y + 1) + serial) * (x + 11) // 100) % 10) - 5,
        'sizes': [None for x in range(300 - x)]
      }

      cell['sizes'][0] = cell['power']
      if cell['power'] > max_power:
        max_all = cell
        max_size = 1
        max_power = cell['power']

      grid[x].append(cell)

  cell = max_grid(grid, 3)
  print(f'Max 3x3 Square X,Y: {cell["coord"]}')

  max_power = 0

  for size in range(1, 300):
    for x in range(300 - size):
      for y in range(300 - size):
        cell = grid[x][y]
        cell['sizes'][size] = sum((
          cell['sizes'][size - 1],
          sum(
            grid[x + i][y + size]['power'] + grid[x + size][y + i]['power']
              for i in range(0, size)
          ),
          grid[x + size][y + size]['power']
        ))

        if cell['sizes'][size] > max_power:
          max_all = cell
          max_size = size + 1
          max_power = cell['sizes'][size]

  print(f'Max Square X,Y + size: {max_all["coord"]} + {max_size}')

def max_grid(grid: list[list[dict[str, Any]]], size: int) -> dict[str, Any]:
  max_square = (None, {})

  for x in range(300 - size + 1):
    for y in range(300 - size + 1):
      square = sum(grid[x + ix][y + iy]['power'] for ix in range(size) for iy in range(size))
      if max_square[0] is None or max_square[0] < square:
        max_square = (square, grid[x][y])

  return max_square[1]

if __name__ == '__main__':
  run()
  sys.exit(0)
