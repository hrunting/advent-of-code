#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  width, height = 25, 6
  size = width * height

  encoded = list(input_file.open().read().strip())
  layers = [encoded[i:i + size] for i in range(0, len(encoded), size)]

  min_zeroes = min(layers, key=lambda x: x.count("0"))
  checksum = min_zeroes.count("1") * min_zeroes.count("2")

  print(f"Min-zero checksum: {checksum}")

  flattened = [next(pixel for pixel in column if pixel != "2") for column in zip(*layers)]
  pixels = "".join([" " if pixel == "0" else "X" for pixel in flattened])
  image = "\n".join([pixels[i:i + width] for i in range(0, len(pixels), width)])

  print(f"{image}")

if __name__ == '__main__':
  run()
  sys.exit(0)
