#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def run_program(computer: intcode.IntCode, inputs: list[int]) -> list[int]:
  for input in inputs:
    computer.write(input)

  outputs = []

  while True:
    res = computer.run()
    if res == intcode.R_HALT:
      break
    elif res == intcode.R_OUTPUT:
      outputs.append(computer.read())
    else:
      print(f"Error: unexpected result '{res}'")
      return []

  return outputs

def test(codes: list[int]) -> Optional[int]:
  computer = intcode.IntCode(codes)
  outputs = run_program(computer, [1])

  if outputs is None or len(outputs) > 1:
    print(f"Error: invalid op codes {outputs[:-1]}")
    return None
  
  return outputs[0]

def boost(codes: list[int]) -> Optional[int]:
  computer = intcode.IntCode(codes)
  outputs = run_program(computer, [2])

  if outputs is None or len(outputs) > 1:
    print(f"Error: invalid output {outputs}")
    return None
  
  return outputs[0]

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  output = test(codes)
  print(f"Keycode: {output}")

  coordinates = boost(codes)
  print(f"Coordinates: {coordinates}")

if __name__ == '__main__':
  run()
  sys.exit(0)
