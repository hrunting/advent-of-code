#!/usr/bin/env python3

import itertools
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def amplify(codes: list[int]) -> tuple[int, str]:
  max_sequence = []
  max_output = 0

  for sequence in itertools.permutations(list(range(5))):
    signal = 0
    
    for phase in sequence:
      computer = intcode.IntCode(codes).write(phase).write(signal)
      if computer.run() == intcode.R_OUTPUT:
        signal = computer.read()

    if signal > max_output:
      max_output = signal
      max_sequence = sequence

  return max_output, "".join(str(x) for x in max_sequence)

def feedback(codes: list[int]) -> tuple[int, str]:
  max_sequence = []
  max_output = 0

  for sequence in itertools.permutations(list(range(5, 10))):
    result = intcode.R_OK
    signal = 0

    loop = [intcode.IntCode(codes).write(phase) for phase in sequence]

    while result is not intcode.R_HALT:
      for amplifier in loop:
        result = amplifier.write(signal).run()
        if result == intcode.R_OUTPUT:
          signal = amplifier.read()

    if signal > max_output:
      max_output = signal
      max_sequence = sequence

  return max_output, "".join(str(x) for x in max_sequence)

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  codes = [int(x) for x in open(input_file).readline().strip().split(",")]

  output, seq = amplify(codes)
  print(f"Max signal: {output}  Sequence: {seq}")

  output, seq = feedback(codes)
  print(f"Max signal: {output}  Sequence: {seq}")

if __name__ == '__main__':
  run()
  sys.exit(0)
