#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def run_network(codes: list[int], address: Optional[int] = None) -> tuple[int, int]:
  computers = {}
  q = {}

  initialized = set()
  active = set()
  idle = set()

  nat = (0, 0)
  nat_sent_y = None

  for i in range(50):
    computers[i] = intcode.IntCode(codes)
    q[i] = []
    active.add(i)

  run = True
  x: int = 0
  y: int = 0

  while run:

    # start off ssuming all nodes are idle
    all_idle = True

    for i, c in computers.items():

      # this node has halted (probably a bug)
      if i not in active:
        continue

      res = c.run()

      if res == intcode.R_HALT:
        active -= {i}
        break

      elif res == intcode.R_INPUT:

        # first read request is for initialization
        if i not in initialized:
          initialized.add(i)
          c.write(i)

          idle.discard(i)
          all_idle = False

        # send something from the queue if it's available
        elif len(q[i]):
          x, y = q[i].pop(0)
          c.write(x)
          c.write(y)

          idle.discard(i)
          all_idle = False

        # send the idle packet
        # two idle packets in a row means this node is idle
        else:
          c.write(-1)
          if i not in idle:
            idle.add(i)
            all_idle = False

      elif res == intcode.R_OUTPUT:

        # if this node sends a packet, it is not idle
        idle.discard(i)
        all_idle = False

        # read the packet (three reads)
        dest = c.read()
        _ = c.run()
        x = c.read()
        _ = c.run()
        y = c.read()

        # if the destination is available, queue it up
        if dest in q:
          q[dest].append((x, y))

        # store the NAT packet
        else:
          if address is not None and address == dest:
            run = False
            break

          if dest == 255:
            nat = (x, y)

      # this node hit a bug
      elif res == intcode.R_INVALID:
        print(f"Computer {i} error @ {c.ptr}: {','.join(str(code) for code in c.code)}")
        run = False
        break

    # all systems are idle
    # send a packet to queue 0 from the NAT
    if all_idle:
      nx, ny = nat

      # processing is complete if a NAT Y address repeats
      if nat_sent_y is not None and nat_sent_y == ny:
        x, y = nx, ny
        run = False

      # send the last NAT packet to node 0
      else:
        nat_sent_y = ny
        q[0].append((nx, ny))

  return (x, y)

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  x, y = run_network(codes, 255)
  print(f"Packet at address 255: {(x, y)}")

  _, y = run_network(codes)
  print(f"First resent NAT Y address {y}")

if __name__ == '__main__':
  run()
  sys.exit(0)
