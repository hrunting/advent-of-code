#!/usr/bin/env python3

import pathlib
import re
import sys

from itertools import combinations
from math import gcd

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def cmp(a: int, b: int) -> int:
  return int(a > b) - int(a < b)

def velocity(x1: int, y1: int, z1: int, x2: int, y2: int, z2: int) -> list[int]:
  return [cmp(x1, x2), cmp(y1, y2), cmp(z1, z2)]

def parse(s: str) -> list[int]:
  if m := re.search(r'^<x=(\S+), y=(\S+), z=(\S+)>$', s):
    return [int(x) for x in m.groups()]
  return []

def move(p: list[int], v: list[int]) -> list[int]:
  for n in range(len(p)):
    p[n] += v[n]
  return p

def energy(p: list[int], v: list[int]) -> int:
  return sum(abs(x) for x in p) * sum(abs(x) for x in v)

def lcm(a: int, b: int) -> int:
  return a * b // gcd(a, b)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = open(input_file).readlines()

  # Part 1:
  # Find total energy after 1000 iterations
  # Iterate through a straightforward implementation of the problem
  # Track positions and velocities as lists of [x, y, z] lists
  STEPS = 1000
  positions = [parse(x.strip()) for x in lines]
  velocities = [[0, 0, 0] for _ in positions]

  for _ in range(STEPS):
    for i1, i2 in combinations(range(len(positions)), 2):
      for n, vel in enumerate(velocity(*positions[i1], *positions[i2])):
        velocities[i1][n] -= vel
        velocities[i2][n] += vel

    for i in range(len(positions)):
      positions[i] = move(positions[i], velocities[i])

  total_energy = sum(energy(positions[i], velocities[i]) for i in range(len(positions)))
  print(f"Total energy: {total_energy}")

  # Part 2:
  # Find the number of iterations for the universe to repeat
  # Each plane operates independently (i.e. x relationships have no effect on y or z)
  # Calculate number of steps for each plane to independently repeat
  # Total steps is the least common multiple of the three planes' cycles
  #
  # Make sure that both position AND velocity reach the original state
  steps = [0, 0, 0]
  positions = [parse(x.strip()) for x in lines]

  for i in (range(3)):
    orig_plane = [x[i] for x in positions]
    orig_velocities = [0 for _ in positions]

    plane = [x[i] for x in positions]
    velocities = [0 for _ in positions]

    while True:
      for i1, i2 in combinations(range(len(plane)), 2):
        pull = cmp(plane[i1], plane[i2])
        velocities[i1] -= pull
        velocities[i2] += pull

      for n, vel in enumerate(velocities):
        plane[n] += velocities[n]

      steps[i] += 1

      # have we reached our initial state for this plane?
      if plane == orig_plane and velocities == orig_velocities:
        break

  total_steps = lcm(lcm(steps[0], steps[1]), steps[2])
  print(f"Repetition iterations: {total_steps}")

if __name__ == '__main__':
  run()
  sys.exit(0)
