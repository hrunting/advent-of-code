#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def phase(sequence: list[int]) -> list[int]:
  output = [0 for _ in sequence]

  for i in range(0, len(sequence)):
    step = i + 1
    mult = 1

    for n in range(step - 1, len(sequence), step * 2):
      output[i] += sum(sequence[n:n + step]) * mult
      mult *= -1
   
    output[i] = abs(output[i]) % 10

  return output

def simple_phase(sequence: list[int]) -> list[int]:
  sequence_sum = sum(sequence)
  for n in range(len(sequence)):
    v = sequence[n]
    sequence[n] = sequence_sum % 10
    sequence_sum -= v

  return sequence

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  line = open(input_file).read().strip()

  sequence = [int(x) for x in line]

  msg = sequence.copy()
  for _ in range(100):
    msg = phase(msg)

  output = "".join(str(n) for n in msg[:8])
  print(f"First eight of final output: {output}")

  # Part 2's offset is > halfway through the expanded sequence
  # so we can use a much simpler algorithm for processing it
  offset = int("".join(str(n) for n in sequence[:7]))
  msg = (sequence * 10000)[offset:]

  for n in range(100):
    msg = simple_phase(msg)

  output = "".join(str(n) for n in msg[:8])
  print(f"Embedded message: {output}")

if __name__ == '__main__':
  run()
  sys.exit(0)
