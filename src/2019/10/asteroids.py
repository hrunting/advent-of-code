#!/usr/bin/env python3

import itertools
import math
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

# angle here is negative to make radial order straightforward
# -π (top right) to +π (top left)
def slope(x1: int, y1: int, x2: int, y2: int) -> float:
  return -math.atan2(x2 - x1, y2 - y1)

# manhattan distance
def distance(x1: int, y1: int, x2: int, y2: int) -> int:
  return abs(x2 - x1) + abs(y2 - y1)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  data = input_file.open().read()

  grid = [[x for x in line] for line in data.splitlines()]
  asteroids = {(x, y) for y, row in enumerate(grid) for x, cell in enumerate(row) if cell == "#"}

  count, station = max([
    (len(set(slope(*a, *other) for other in asteroids if other != a)), a)
      for a in asteroids
  ])

  print(f"Best Count: {count} @ {station}")

  # target asteroids ordered by distance from station grouped by slope
  asteroids = sorted(asteroids, key=lambda a: distance(*station, *a))
  slopes = {k: [*v] for k, v in itertools.groupby(asteroids[1:], key=lambda a: slope(*station, *a))}

  # target asteroids ordered by depth in list and slope
  # ziplongest produces a list of n-length tuples that we flatten
  targets = list(
    itertools.chain.from_iterable(
      itertools.zip_longest(
        *(slopes[k] for k in sorted(slopes.keys()))
      )
    )
  )

  print(f"200th Asteroid: {targets[199][0] * 100 + targets[199][1]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
