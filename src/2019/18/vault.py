#!/usr/bin/env python3

import heapq
import math
import pathlib
import sys

from typing import Any, Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Pos = tuple[int, int]
Cell = str

def parse_grid(lines: list[str]) -> tuple[dict[Pos, Cell], dict[Cell, Pos], Pos]:
  grid = {}
  keys = {}

  pos = (0, 0)

  for y, row in enumerate(lines):
    for x, cell in enumerate(row):
      grid[(x, y)] = cell

      if cell == "@":
        pos = (x, y)
      elif cell >= "a" and cell <= "z":
        keys[cell] = (x, y)

  return grid, keys, pos

def render_grid(grid: dict[Pos, Cell]) -> None:
  min_x = min(x for x, _ in grid.keys())
  min_y = min(y for _, y in grid.keys())
  max_x = max(x for x, _ in grid.keys())
  max_y = max(y for _, y in grid.keys())

  grid_str = "\n".join(
    ("".join(grid[(x, y)] for x in range(min_x, max_x + 1)))
      for y in range(min_y, max_y + 1)
  )
  print(f"{grid_str}\n")

def update_grid(grid: dict[Pos, Cell], pos: Pos) -> list[Pos]:
  all_pos = []

  grid[pos] = "#"
  for dx, dy in ((0, -1), (1, 0), (0, 1), (-1, 0)):
    pt = (pos[0] + dx, pos[1] + dy)
    grid[pt] = "#"

  for dx, dy in ((1, -1), (1, 1), (-1, 1), (-1, -1)):
    pt = (pos[0] + dx, pos[1] + dy)
    all_pos.append(pt)
    grid[pt] = "@"

  return all_pos

def find_key_paths(
    grid: dict[Pos, Cell],
    keys: dict[Cell, Pos],
    starts: list[Pos]) -> dict[Cell, dict[Cell, dict[str, Any]]]:

  keypaths = {k: {} for k in keys.keys()}
  keypaths.update({str(i): {} for i in range(len(starts))})

  for key, rec in keypaths.items():
    pos = keys[key] if key >= "a" and key <= "z" else starts[int(key)]
    find_all_keys(grid, pos, rec)

  return keypaths

def find_all_keys(grid: dict[Pos, Cell], pos: Pos, rec: dict[Cell, dict[str, Any]]) -> None:
  visited = set()

  q: list[tuple[int, Pos, set[str]]] = [(0, pos, set())]

  while (len(q)):
    steps, pos, doors = heapq.heappop(q)
    visited.add(pos)

    # we have found a key
    if steps > 0 and grid[pos] >= "a" and grid[pos] <= "z":
      rec[grid[pos]] = {"pos" : pos, "steps": steps, "doors": doors}

    # we have found a door
    elif grid[pos] >= "A" and grid[pos] <= "Z":
      doors = doors | {grid[pos].lower()}

    for dx, dy in ((0, -1), (1, 0), (0, 1), (-1, 0)):
      nx, ny = pos[0] + dx, pos[1] + dy
      if grid.get((nx, ny), "#") != "#" and (nx, ny) not in visited:
        heapq.heappush(q, (steps + 1, (nx, ny), doors))

  return

def find_keys(
    grid: dict[Pos, Cell],
    keypaths: dict[Cell, dict[Cell, dict[str, Any]]],
    path: Optional[str] = None) -> tuple[int, str]:

  # no path means we are initializing
  # track our recursive state within our function object
  if path is None:
    find_keys.bots = sorted([x for x in keypaths.keys() if x[0] >= "0" and x[0] <= "9"], key=int)
    find_keys.complete = set(x for x in keypaths.keys() if x >= "a" and x <="z")
    find_keys.found = set()
    find_keys.cache = {}
    path = ""

  if find_keys.found == find_keys.complete:
    return 0, path

  cachekey = "".join(find_keys.bots) + "".join(sorted(find_keys.found))
  if cachekey not in find_keys.cache:
    steps = math.inf
    min_path = ""

    for bot, bot_key in enumerate(find_keys.bots):
      for k, rec in keypaths[bot_key].items():
        if k in find_keys.found or not rec["doors"] <= find_keys.found:
          continue

        find_keys.bots[bot] = k
        find_keys.found.add(k)

        ksteps, kpath = find_keys(grid, keypaths, path + k)

        if rec["steps"] + ksteps < steps:
          steps = rec["steps"] + ksteps
          min_path = kpath

        find_keys.found.remove(k)
        find_keys.bots[bot] = bot_key

    find_keys.cache[cachekey] = (steps, min_path)

  return find_keys.cache[cachekey]

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = open(input_file).read().strip()

  grid, keys, pos = parse_grid(lines.split("\n"))
  render_grid(grid)

  keypaths = find_key_paths(grid, keys, [pos])
  steps, keypath = find_keys(grid, keypaths)
  print(f"Steps to find all keys: {steps}\nPath: {keypath}")

  all_pos = update_grid(grid, pos)
  keypaths = find_key_paths(grid, keys, all_pos)
  steps, keypath = find_keys(grid, keypaths)
  print(f"Steps to find all keys: {steps}\nPath: {keypath}")

if __name__ == '__main__':
  run()
  sys.exit(0)
