#!/usr/bin/env python3

import sys
import re

def generate_candidates(low: int, high: int) -> list[str]:
  s_low = str(low)
  s_high = str(high)

  sequences = [[str(x)] for x in range(10)]
  while len(sequences[0][0]) < 6:
    for i in range(10):
      sequences[i] = [str(i) + seq for pool in sequences[i:] for seq in pool]
  
  return [
    seq
      for pool in sequences
      for seq in pool
      if seq >= s_low and seq <= s_high and re.search(r"([0-9])\1", seq) is not None
  ]

def run() -> None:
  start = 236491
  stop = 713787

  candidates = generate_candidates(start, stop)
  print(f"Basic passwords: {len(candidates)}")

  filtered = [seq for seq in candidates if
    re.search(r"([0-9])(?!\1)([0-9])\2(?!\2)", seq) is not None or 
    re.search(r"^([0-9])\1(?!\1)", seq) or
    re.search(r"([0-9])(?!\1)([0-9])\2$", seq)]
  
  print(f"Strict passwords: {len(filtered)}")

if __name__ == '__main__':
  run()
  sys.exit(0)