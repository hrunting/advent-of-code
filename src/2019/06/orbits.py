#!/usr/bin/env python3

import pathlib
import sys

from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_orbits(node: dict[str, Any], start: int = 0) -> int:
  count = start
  for child in node["satellites"]:
    count += count_orbits(child, start + 1)
  return count

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  lines = input_file.open().readlines()
  orbits = [x.strip().split(")") for x in lines]

  nodes = {}

  for obj, satellite in orbits:
    if obj not in nodes:
      nodes[obj] = {"id": obj, "orbits": None, "satellites": []}
    if satellite not in nodes:
      nodes[satellite] = {"id": satellite, "orbits": None, "satellites": []}

    nodes[satellite]["orbits"] = nodes[obj]
    nodes[obj]["satellites"].append(nodes[satellite])

  # Part 1
  orbit_count = count_orbits(nodes["COM"])
  print(f"Total orbits: {orbit_count}")

  # Part 2
  done = False
  visited = set()
  hops = 0

  q: list[tuple[dict[str, Any], int]] = [(nodes["YOU"]["orbits"], 0)]

  while len(q) > 0 and not done:
    node, hops = q.pop(0)

    if node["id"] in visited:
      continue
    visited.add(node["id"])

    for x in node["satellites"]:
      if x["id"] == "SAN":
        done = True
        break

      q.append((x, hops + 1))
  
    if node["orbits"] is not None:
      q.append((node["orbits"], hops + 1))
    
  print(f"Orbital transfers: {hops}")

if __name__ == '__main__':
  run()
  sys.exit(0)
