#!/usr/bin/env python3

import pathlib
import re
import sys

from multiprocessing import Pipe, Process
from multiprocessing.connection import Connection
from typing import Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

TIMEOUT = 1.0
MOVES = {"n": "north", "e": "east", "s": "south", "w": "west"}
RETURNS = {"n": "s", "e": "w", "s": "n", "w": "e"}

def write(computer: intcode.IntCode, s: str) -> None:
  for x in s:
    computer.write(ord(x))

def read(computer: intcode.IntCode) -> tuple[int, str]:
  output = ""
  res = intcode.R_OUTPUT
  while res == intcode.R_OUTPUT:
    output += chr(computer.read())
    res = computer.run()

  return res, output

def run_droid(codes: list[int], conn: Optional[Connection] = None) -> None:
  def input() -> str:
    if conn:
      return conn.recv()
    return sys.stdin.readline()

  def output(data: str) -> None:
    if conn:
      return conn.send(data)
    sys.stdout.write(data)
    sys.stdout.flush()

  def cleanup() -> None:
    if conn:
      conn.close()

  computer = intcode.IntCode(codes)

  while True:
    res = computer.run()
    if res == intcode.R_HALT:
      break

    elif res == intcode.R_INPUT:
      try:
        data = input()
        write(computer, data)
      except EOFError:
        break

    elif res == intcode.R_OUTPUT:
      res, data = read(computer)
      output(data)

  cleanup()
  return

def parse_output(output: str) -> \
    tuple[Optional[str], Optional[str], list[str], list[str], str, bool]:

  room = None        # the room we entered (if any)
  taken = None       # any items taken or dropped
  checkpoint = ""    # any checkpoint output

  doors = []         # the doors we see in the room we entered
  items = []         # the items we see in the room we entered

  collector = None   # either doors or items, if we are collecting entries

  lines = [line.strip() for line in output.strip().split("\n")]
  for line in lines:
    if not line:
      collector = None

    elif collector is not None:
      collector.append(line[2:])

    elif m := re.match(r'== (.+?) ==', line):
      room = m.group(1)
      doors = []
      items = []

    elif line.startswith('Doors here lead:'):
      collector = doors

    elif line.startswith('Items here:'):
      collector = items

    elif m := re.match(r'You take the (.+?)\.', line):
      taken = m.group(1)

    elif m := re.match(r'You drop the (.+?)\.', line):
      taken = "-" + m.group(1)

    elif m := re.search(r'Droids on this ship are (\S+) than', line):
      checkpoint = m.group(1)

    elif m := re.search(r'get in by typing (\d+) on the keypad', line):
      checkpoint = m.group(1)

  return room, taken, doors, items, checkpoint, lines[-1] == "Command?"

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  # allow for operating the droid manually
  if "--manual" in sys.argv:
    return run_droid(codes)

  # The droid has to explore a series of rooms. The rooms form an acyclic
  # graph (a tree), and the starting point is the root of the tree.
  # In each room, there may be one or more items. The droid picks up items,
  # but some of the items cause the droid to fail:
  #
  # - enter an infinite loop (droid runs without exiting or communicating)
  # - get stuck (droid communicates, but cannot respond to actions)
  # - exit unexpectedly (droid communicates, then terminates)
  #
  # One of the final rooms is a security checkpoint. The last door out of
  # this room leads to a pressure plate. If the droid does not weigh the
  # right amount, the pressure plate kicks the droid back through the door
  # into the security checkpoint. The password is through the door.
  #
  # The strategy we use to find the password is:
  #
  # 1. Explore all of the rooms, picking up every item.
  #    If an item causes a droid failure, terminate the droid, then
  #    start it again with knowledge of the item to avoid. When finished
  #    the droid will be at the starting point with every good item.
  #
  # 2. Navigate to the security checkpoint with all of the items.
  #    The initial exploration reaches the security checkpoint, so
  #    the droid has the path from the starting point to the checkpoint.
  #
  # 3. Try combinations of items until one allows the droid through the door.

  # these items persist across droid restarts
  password = None
  bad_items = set()

  # every loop through here represents one run of the droid
  # if a droid fails, we return to the start of this loop
  while password is None:
    mode = "explore"

    bag = []              # items droid currently carries

    item = None           # the last item the droid attempted to take or drop
    move = "."            # the last move the droid attempted to make

    room = ""             # the current room the droid thinks it is in
    path = ""             # the path to the current room
    doors = []            # doors the droid sees in the current room
    items = []            # items the droid sees in the current room

    visited = set()       # the set of all paths visited
    security_path = ""    # the path to the security checkpoint

    tests = []            # item groups the droid is testing at the checkpoint

    # start up a droid with a communication pipe
    controller, program = Pipe()
    droid = Process(target=run_droid, args=(codes, program))
    droid.start()

    # the droid should always respond
    # if the droid does not respond, then it is considered failed
    while controller.poll(TIMEOUT):
      try:
        output = controller.recv()

      # the remote end shutdown, the droid failed
      except EOFError:
        if bag:
          bad_items.add(bag[-1])
        break

      # process the output and apply any changes
      entered_room, took_item, doors_seen, items_seen, checkpoint, waiting = parse_output(output)

      # if the output indicates we took or dropped an item, update our bag
      if took_item:
        if took_item.startswith("-"):
          bag.remove(took_item[1:])
        else:
          bag.append(took_item)

        item = None

      # if the output indicates we moved into a new room
      # update our current room status and the path to that room
      if move and entered_room and entered_room != room:
        room = entered_room
        doors = doors_seen
        items = items_seen

        # our last move may have been going back out the way we came in
        if path and path[-1] == RETURNS[move[0]]:
          path = path[:-1]
        else:
          path += move[0]

        # we will need the path to the security checkpoint later
        # to go back there to try to access the password
        if room == "Security Checkpoint":
          security_path = path

        # we do not need to visit rooms twice unless we are backing
        # out of them
        visited.add(path)
        move = None

      # if the droid is not waiting for a command at the end of its output,
      # it means the program exited and the run is over
      #
      # if the run returned a checkpoint output, that will be the password
      # if not, it means the last item we added to our bag was a bad item
      if not waiting:
        if checkpoint:
          password = checkpoint
        else:
          bad_items.add(bag[-1])

        break

      # we tell the droid to do specific things based on what mode we are in
      #
      # modes can fall through (ie. we can switch from "explore" mode to
      # "travel") without sending a command in order to let the next mode
      # send the appropriate command
      #
      # the order of the mode comparisons is important
      # fall-through transitions do not require a droid response
      # circle-back transitions require a droid response (a loop pass)
      #
      # - explore - falls through to travel
      # - travel - falls through to empty
      # - empty - falls through to taking
      # - evaluating - falls through to dropping or taking
      #
      # - dropping - circles back to dropping or taking
      # - taking - circles back to entering
      # - entering - circles back to evaluating

      # we are exploring the map and finding all of the items
      if mode == "explore":

        # if we processed all of our lines and the last action was a move
        # or item take that did not achieve the expected result,
        # then we didn't complete the action and we are stuck
        #
        # possible situations:
        # - the last item we successfully took was a bad item (failure)
        # - we are in the security room and the item check failed (normal)
        if move or item:
          if item or room != "Security Checkpoint":
            if bag:
              bad_items.add(bag[-1])
            break

          doors = [d for d in doors if d != move]
          move = None

        # ignore any known bad items in the list of items we see
        items = [item for item in items if item not in bad_items]

        # if we see items while exploring take them
        if items:
          item = items.pop()
          controller.send(f"take {item}\n")
          continue

        # try go through every door we see in the room
        # the last door we go through will be the door we entered through
        doors = sorted(reversed(doors), key=lambda d: path[-1] != RETURNS[d[0]])
        while len(doors):
          move = doors.pop()
          if path + move[0] in visited:
            continue
          controller.send(f"{move}\n")
          break

        # if we end up not having any doors to go through, then we have
        # explored everything and found our way back to our starting point
        #
        # switch to traveling to the security checkpoint
        else:
          mode = "travel"

      # we are now traveling to the security checkpoint
      if mode == "travel":

        # if we are not yet at the security checkpoint,
        # enter the next room in the path to the room
        if room != "Security Checkpoint":
          move = MOVES[security_path[len(path)]]
          controller.send(f"{move}\n")

        # we are at the security checkpoint
        # empty our bag of items and setup the first test scenario
        #
        # the test is a stack of scenarios, each scenario tracking:
        # - group of items carried in
        # - items left to try carrying in with that group
        else:
          tests.append(([], bag.copy()))
          mode = "empty"

      # we are emptying our bag of items in the security checkpoint
      if mode == "empty":
        if bag:
          item = bag[0]
          controller.send(f"drop {item}\n")
          item = "-" + item

        # when finished emptying the bag
        # start taking items to try carrying into the password room
        else:
          mode = "taking"

      # we entered the password room and it rejected us for the wrong weight
      # we need to determine whether we need to add or drop items
      if mode == "evaluating":

        # too heavy and need to drop weight or
        # we tried everything we can and we're still not heavy enough
        # so drop an item and try with the next one
        if checkpoint == "lighter" or not tests[-1][1]:
          mode = "dropping"

        # too light and we need to try entering with another item
        else:
          mode = "taking"

      # we just added an item to our bag
      # we are ready to enter the password room
      if mode == "entering":
        door = next(d for d in doors if d != RETURNS[path[-1]])
        controller.send(f"{door}\n")
        mode = "evaluating"

      # we are not too heavy and we can try adding another item to
      # see if the password room will accept us now
      if mode == "taking":
        taken, remaining = tests[-1]
        item = remaining[0]
        tests.append((taken + [item], remaining[1:]))
        controller.send(f"take {item}\n")
        mode = "entering"

      # we are too heavy and need to drop the last item we tried
      if mode == "dropping":
        taken, remaining = tests.pop()

        item = taken[-1]
        controller.send(f"drop {item}\n")
        item = "-" + item

        # remove this element from the stack's remaining list
        # if the stack still has elements, switch to taking elements
        tests[-1][1].pop(0)
        if tests[-1][1]:
          mode = "taking"

    # the while loop exited "normally" (ie. timed out)
    # the game did not output anything for a while
    # it probably got stuck in an endless loop when we took an item
    else:
      bad_items.add(item if item else bag[-1])

    # if the droid is not responding, terminate it
    droid.terminate()
    controller.close()

  print(f"Password: {password}")

if __name__ == '__main__':
  run()
  sys.exit(0)
