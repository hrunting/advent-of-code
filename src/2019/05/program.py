#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def program(codes: list[int], input: int) -> int:
  output = -1

  computer = intcode.IntCode(codes).write(input)
  res = intcode.R_OK

  while res != intcode.R_HALT:
    res = computer.run()
    if res == intcode.R_OUTPUT:
      output = computer.read()

  return output

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  codes = [int(x) for x in open(input_file).readline().strip().split(",")]

  output = program(codes, 1)
  print(f"A/C Diagnostic Output: {output}")

  output = program(codes, 5)
  print(f"Thermal Radiators Diagnostic Code: {output}")

if __name__ == '__main__':
  run()
  sys.exit(0)
