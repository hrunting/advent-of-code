#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def program(codes: list[int], p1: int, p2: int) -> int:
  computer = intcode.IntCode(codes).edit(1, p1).edit(2, p2)
  computer.run()
  return computer.code[0]

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  codes = [int(x) for x in open(input_file).readline().strip().split(",")]
  result = program(codes, 12, 2)

  print(f"Position 0 Value: {result}")

  for noun in range(0, 100):
    for verb in range(0, 100):
      result = program(codes, noun, verb)

      if result == 19690720:
        print(f"100 * noun + verb = {100 * noun + verb}")
        return

if __name__ == '__main__':
  run()
  sys.exit(0)
