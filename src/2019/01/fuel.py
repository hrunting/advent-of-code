#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def fuel_required(mass: int) -> int:
  return mass // 3 - 2

def run() -> None:
  module_fuel = 0
  total_fuel = 0
  input_file = aoc.inputfile('input.txt')

  with input_file.open() as input:
    for line in input:
      mass = int(line.strip())
      fuel = fuel_required(mass)

      module_fuel += fuel

      while fuel > 0:
        total_fuel += fuel
        fuel = fuel_required(fuel)

  print(f'Module Fuel: {module_fuel}')
  print(f'Total Fuel: {total_fuel}')

if __name__ == '__main__':
  run()
  sys.exit(0)
