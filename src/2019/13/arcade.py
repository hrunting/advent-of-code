#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def cmp(a: int, b: int) -> int:
  return int(a > b) - int(a < b)

def arcade(codes: list[int], free: bool = False) -> tuple[dict[tuple[int, int], int], int]:
  tiles = dict()
  score = 0

  ball = 0
  paddle = 0

  computer = intcode.IntCode(codes)
  if free:
    computer.edit(0, 2)

  outputs = []

  while True:
    res = computer.run()
    if res == intcode.R_HALT:
      break

    elif res == intcode.R_OUTPUT:
      outputs.append(computer.read())

      if len(outputs) == 3:
        if outputs[0] == -1 and outputs[1] == 0:
          score = outputs[2]

        else:
          tiles[(outputs[0], outputs[1])] = outputs[2]

          if outputs[2] == 3:
            paddle = outputs[0]
          elif outputs[2] == 4:
            ball = outputs[0]

        outputs = []

    elif res == intcode.R_INPUT:
      computer.write(cmp(ball, paddle))

  return tiles, score

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  tiles, _ = arcade(codes)
  print(f"Block tiles: {list(tiles.values()).count(2)}")

  _, score = arcade(codes, True)
  print(f"Game score: {score}")

if __name__ == '__main__':
  run()
  sys.exit(0)
