#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Deck:
  def __init__(self: Self, size: int) -> None:
    self.deck = deque(range(size))
    self.dir = 1

  def new(self: Self) -> None:
    self.dir = -self.dir

  def cut(self: Self, n: int) -> None:
    self.deck.rotate(-n * self.dir)

  def deal(self: Self, incr: int) -> None:
    deck = deque(range(len(self.deck)))

    while len(self.deck):
      deck[0] = self.deck.popleft() if self.dir == 1 else self.deck.pop()
      deck.rotate(-incr)

    self.deck = deck
    self.dir = 1

  def cards(self: Self) -> list[int]:
    return list(self.deck) if self.dir == 1 else list(self.deck)[::-1]

def run():
  card = 2019
  size = 10007

  input_file = aoc.inputfile('input.txt')
  lines = open(input_file).read().strip()

  deck = Deck(size)
  for cmd in lines.split("\n"):
     op, *_, n = cmd.split(" ")
     if op == "cut":
       deck.cut(int(n))
     elif op == "deal" and n == "stack":
       deck.new()
     elif op == "deal":
       deck.deal(int(n))

  idx = deck.cards().index(card)
  print(f"Position of card {card}: {idx}")

  # Part 2 requires knowledge of math that I simply do not have
  # To solve, I implemented a variation of the logic found here:
  #
  # https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnkaju/
  # https://github.com/mcpower/adventofcode/blob/501b66084b0060e0375fc3d78460fb549bc7dfab/2019/22/a-improved.py
  pos = 2020
  size = 119315717514047
  iterations = 101741582076661

  # at the start, the first card is 0
  # every card after that is 1 higher than the next
  offset_diff = 0
  increment_mul = 1

  # each step is a linear equation
  # combine all of our linear equations into a single linear equation
  # representing a single iteration
  for cmd in lines.split("\n"):
    op, *_, n = cmd.split(" ")

    # make the nth card of the deck the front of the deck
    # nth card == offset + increment * n
    # so increase our offset by n * the increment multiplier
    #
    # we do not change the increment between the first card and the second
    # so we make no change to our increment multiplier
    if op == "cut":
      offset_diff += int(n) * increment_mul

    # dealing a new stack reverses the multiplier
    # we start counting "down" instead of counting "up"
    # so we negate our increment multiplier
    #
    # in addition, our "last" card is now our "first"
    # the "last" card is the next card in the switched direction
    # so after we negate, increase the offset by the reversed multiplier
    elif op == "deal" and n == "stack":
      increment_mul *= -1
      offset_diff += increment_mul

    # dealing the cards in increments, so that card c goes into position c * n
    # the first card remains the same (card 0 goes into position 0),
    # so we make no change to our offset
    #
    # the next card will be the card where i * n == 1 (the second card)
    # that is the same as i == 1 / n or i == n ^ -1 (the inverse of n)
    # this is modular arithmetic here, so this is the modular inverse
    # Fermat's Theorem says that n ^ (mod - 1) = 1
    # so we set our increment multiplier to the inverse of THAT: n ^ (mod - 2)
    elif op == "deal":
      increment_mul *= pow(int(n), size - 2, size)

    # keep our numbers from getting too big
    increment_mul %= size
    offset_diff %= size

  # how big is our increment after n iterations?
  # after every iteration, the increment is increment multipler times larger
  # so our increment is multipler ^ iterations
  increment = pow(increment_mul, iterations, size)

  # how big is our offset after n iterations
  # - 0 : offset == 0
  # - 1 : offset == offset(0) + multiplier^0 * offset_diff
  # - 2 : offset == offset(1) + multiplier^1 * offset_diff
  # - 3 : offset == offset(2) + multiplier^2 * offset_diff
  # - . : ...
  # - n : offset == offset(n - 1) + multiplier^(n - 1) * offset_diff
  #
  # that is a geometric series, equation s = a(1 - r^n)/(1 - r)
  # rewritten: a * (1 - r^n) * (1 - r)^-1
  # rewritten: offset_diff * (1 - multiplier^iterations) * (1 - multiplier)^-1
  # increment == multipler^iterations (just calculated)
  # so ...
  offset = offset_diff * (1 - increment) * pow((1 - increment_mul) % size, size - 2, size)
  offset %= size

  # and the card at position n is the same equation from earlier
  # card = offset + increment * n (mod deck size)
  card = (offset + pos * increment) % size
  print(f"Card at position {pos} after {iterations} iterations: {card}")

if __name__ == '__main__':
  run()
  sys.exit(1)
