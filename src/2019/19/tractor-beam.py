#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def map_points(computer: intcode.IntCode, grid: int = 50) -> tuple[int, list[list[int]]]:
  map: list[list[int]] = [[0] * grid for _ in range(grid)]
  pts: list[tuple[int, int]] = [(0, 0)]

  min_x = 0
  max_x = 0

  x = y = 0

  vm = computer.copy()
  
  while True:
    res = vm.run()
    if res == intcode.R_HALT:
      vm = computer.copy()

    if res == intcode.R_INPUT:
      if len(pts) == 0:
        break

      x, y = pts.pop(0)
      if x >= grid or y >= grid:
        continue

      vm.write(x)
      vm.write(y)

    elif res == intcode.R_OUTPUT:
      data = vm.read()

      if data == 1:
        if x > max_x:
          max_x = x

        for mx in range(x, min(max_x + 1, grid)):
          map[y][mx] = data

        pts.append((max_x + 1, y))

      else:
        if x == min_x:

          # this row is probably a blank row
          if x > y * 2:
            for mx in range(x, grid):
              map[y][mx] = data

            min_x = 0
            max_x = 0
            pts.append((min_x, y + 1))

          else:
            map[y][x] = data
            pts.append((x + 1, y))
            min_x += 1

        else:
          for mx in range(x, grid):
            map[y][mx] = data
          if y + 1 < grid:
            for mx in range(0, min(min_x, grid)):
              map[y + 1][mx] = data

          min_x = min_x
          pts.append((min_x, y + 1))

  return sum(x for y in map for x in y if x == 1), map

def render_beam(map: list[list[int]]) -> str:
  max_x = len(map[0])
  max_y = len(map)
  return "\n".join("".join("#" if map[y][x] else "." for x in range(max_x)) for y in range(max_y))

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  computer = intcode.IntCode(codes)
  points, map = map_points(computer)
  print(f"Beam:\n{render_beam(map)}\n")
  print(f"Points covered: {points}")

  points, map = map_points(computer, 10000)

  x = y = 0
  for y in range(10000 - 99):
    x = max((i for i, v in enumerate(map[y]) if v == 1), default=0)
    if x < 99:
      continue
    if map[y][x - 99] != 1:
      continue
    if map[y + 99][x - 99] != 1:
      continue

    break

  print(f"Coordinate: ({x -99}, {y}) = ({(x - 99) * 10000 + y})")

if __name__ == '__main__':
  run()
  sys.exit(0)
