#!/usr/bin/env python3

import collections
import math
import pathlib
import re
import sys

from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse_unit(unit: str) -> tuple[int, str]:
  units, label = unit.split(" ")
  return int(units), label

def parse_reactions(lines: list[str]) -> dict[str, Any]:
  reactions = dict()

  for line in lines:
    parts = [parse_unit(x) for x in re.split(r"(?: => )|(?:, )", line)]
    units, component = parts.pop()

    reactions[component] = {
      "name": component,
      "units": units,
      "components": {c: u for u, c in parts}
    }

  return reactions

def ore_for_fuel(reactions: dict[str, Any], fuel: int = 1) -> int:
  required = collections.defaultdict(int)
  required["FUEL"] = fuel

  stack = [reactions["FUEL"]]
  while len(stack) > 0:
    reaction = stack.pop()
    needed = math.ceil(required[reaction["name"]] / reaction["units"])

    for component, units in reaction["components"].items():
      required[component] += needed * units

      if component in reactions:
        stack.append(reactions[component])

    required[reaction["name"]] -= needed * reaction["units"]

  return required["ORE"]

def fuel_from_ore(reactions: dict[str, Any], ore: int) -> int:
  low_fuel = ore // ore_for_fuel(reactions)
  high_fuel = low_fuel * 2

  while high_fuel > low_fuel:
    mid_fuel = (high_fuel + low_fuel) // 2
    if mid_fuel == low_fuel:
      break

    fuel_ore = ore_for_fuel(reactions, mid_fuel)

    if fuel_ore > ore:
      high_fuel = mid_fuel
    else:
      low_fuel = mid_fuel

  return low_fuel

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = open(input_file).read().strip()

  reactions = parse_reactions(lines.split("\n"))

  ore = ore_for_fuel(reactions)
  print(f"Ore required: {ore}")

  fuel = fuel_from_ore(reactions, 1000000000000)
  print(f"Fuel produced: {fuel}")

if __name__ == '__main__':
  run()
  sys.exit(0)
