#!/usr/bin/env python3

import heapq
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def move(p: tuple[int, int], d: tuple[int, int]) -> tuple[int, int]:
  return (p[0] + d[0], p[1] + d[1])

def droid(codes: list[int]) -> tuple[int, dict[tuple[int, int], str]]:
  dirs = {1: (0, 1), 2: (0, -1), 3: (-1, 0), 4: (1, 0)}

  pos = (0, 0)
  locs: dict[tuple[int, int], str] = {pos: " "}

  check = []
  min_steps = -1

  computer = intcode.IntCode(codes)
  for x in dirs:
    heapq.heappush(check, (1, computer.copy(), move((0, 0), dirs[x]), x))

  while len(check):
    steps, computer, pos, mv = heapq.heappop(check)

    while True:
      res = computer.run()
      if res == intcode.R_HALT:
        pass

      elif res == intcode.R_INPUT:
        computer.write(mv)
        continue

      elif res == intcode.R_OUTPUT:
        status = computer.read()
        if status == 0:
          locs[pos] = "#"

        elif status == 1:
          locs[pos] = " "

          for mv in dirs:
            newpos = move(pos, dirs[mv])
            if newpos not in locs:
              heapq.heappush(check, (steps + 1, computer.copy(), newpos, mv))

        elif status == 2:
          locs[pos] = "O"
          if min_steps < 0:
            min_steps = steps

      break

  return min_steps, locs

def fill_oxygen(locs: dict[tuple[int, int], str]) -> int:
  minutes = 0

  pos = next(x for x in locs if locs[x] == "O")
  fill = []
  heapq.heappush(fill, (0, pos))

  while len(fill):
    minutes, pos = heapq.heappop(fill)
    for adj in ((0, 1), (0, -1), (1, 0), (-1, 0)):
      adj_pos = move(pos, adj)
      if adj_pos in locs and locs[adj_pos] == " ":
        locs[adj_pos] = "O"
        heapq.heappush(fill, (minutes + 1, adj_pos))

  return minutes

def fmt_grid(locs: dict[tuple[int, int], str]) -> str:
  min_x = min(x for x, _ in locs.keys())
  min_y = min(y for _, y in locs.keys())
  max_x = max(x for x, _ in locs.keys())
  max_y = max(y for _, y in locs.keys())

  return "\n".join(
    "".join(locs.get((x, y), "-") for x in range(min_x, max_x + 1, 1))
      for y in range(min_y, max_y + 1, 1)
  )

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  moves, locs = droid(codes)

  print(f"Grid:\n{fmt_grid(locs)}\n")
  print(f"Moves required to find oxygen: {moves}")

  minutes = fill_oxygen(locs)
  print(f"Minutes to fill oxygen: {minutes}")

if __name__ == '__main__':
  run()
  sys.exit(0)
