#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc
import aoc.intcode as intcode

def bot(codes: list[int], start: int = 0) -> dict[complex, int]:
  panel = 0 + 0j
  panels = dict() if start == 0 else {panel: start}
  direction = -1j

  computer = intcode.IntCode(codes)
  state = "run"

  while True:
    res = computer.run()
    if res == intcode.R_HALT:
      break
    elif res == intcode.R_INPUT:
      computer.write(panels.get(panel, 0))
    elif res == intcode.R_OUTPUT and state == "run":
      color = computer.read()
      panels[panel] = color
      state = "turn"
    elif res == intcode.R_OUTPUT and state == "turn":
      turn = computer.read()
      direction *= 1j if turn == 1 else -1j
      panel += direction
      state = "run"

  return panels

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  program = open(input_file).read().strip()
  codes = [int(x) for x in program.split(",")]

  panels = bot(codes)
  print(f"Panels painted: {len(panels.keys())}")

  panels = bot(codes, 1)
  cols = [int(x.real) for x in panels.keys()]
  rows = [int(x.imag) for x in panels.keys()]

  print("Registration code:")
  for y in range(min(rows), max(rows) + 1):
    print("".join(
      "#" if panels.get(x + y * 1j, 0) == 1 else " "
        for x in range(min(cols), max(cols))
    ))

if __name__ == '__main__':
  run()
  sys.exit(0)
