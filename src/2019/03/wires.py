#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def distance(p: complex) -> int:
  return int(abs(p.real) + abs(p.imag))

def map_path(wire: list[str]) -> list[complex]:
  dirs = {"U": 1j, "D": -1j, "R": 1, "L": -1}
  ptr = 0
  path = []

  for run in wire:
    d, length = run[0], int(run[1:])
    for _ in range(length):
      ptr += dirs[d]
      path.append(ptr)

  return path

def find_intersections(path1: list[complex], path2: list[complex]) -> set[complex]:
  return set(path1) & set(path2)

def pathlen(path1: list[complex], path2: list[complex], pos: complex) -> int:
  return path1.index(pos) + path2.index(pos) + 2

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  with input_file.open() as f:
    wire1 = f.readline().strip().split(",")
    wire2 = f.readline().strip().split(",")

  path1 = map_path(wire1)
  path2 = map_path(wire2)

  intersections = find_intersections(path1, path2)
  closest = min([distance(x) for x in intersections])
  lowest = min([pathlen(path1, path2, x) for x in intersections])

  print(f"Closest intersection distance: {closest}")
  print(f"Lowest signal delay: {lowest}")

if __name__ == '__main__':
  run()
  sys.exit(0)
