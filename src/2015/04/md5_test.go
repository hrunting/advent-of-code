package main

import (
	"testing"
)

type test struct {
	input  string
	result int
}

var md5Tests = []test{
	{"abcdef", 609043},
	{"pqrstuv", 1048970},
}

func TestFindMD5Number(t *testing.T) {
	for _, test := range md5Tests {
		result := findMD5Number(test.input, 5, 1)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
