package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
)

const SECRET_KEY string = "yzbqklnj"

func findMD5Number(secret string, zeroes int, start int) int {
	i := start - 1

	prefix := make([]byte, (zeroes-1)/2)
	keyByte := byte((zeroes % 2) * 15)

	for {
		i += 1
		sum := md5.Sum([]byte(fmt.Sprintf("%s%d", secret, i)))
		if bytes.Equal(prefix, sum[0:len(prefix)]) && sum[len(prefix)] <= keyByte {
			break
		}
	}

	return i
}

func main() {
	num := findMD5Number(SECRET_KEY, 5, 1)
	fmt.Printf("Lowest five-zero number: %d\n", num)

	num = findMD5Number(SECRET_KEY, 6, num)
	fmt.Printf("Lowest six-zero number: %d\n", num)
}
