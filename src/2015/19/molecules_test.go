package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var moleculeTests = []test{
	{[]string{`H => HO`, `H => OH`, `O => HH`, ``, `HOH`}, 4},
	{[]string{`H => HO`, `H => OH`, `O => HH`, ``, `HOHOHO`}, 7},
	{[]string{`H => OO`, ``, `H2O`}, 1},
}

var medicineStepTests = []test{
	{[]string{`e => H`, `e => O`, `H => HO`, `H => OH`, `O => HH`, ``, `HOH`}, 3},
	{[]string{`e => H`, `e => O`, `H => HO`, `H => OH`, `O => HH`, ``, `HOHOHO`}, 6},
	{[]string{`e => XXXX`, `e => yy`, `y => XX`, ``, `XXXX`}, 1},
}

func TestReplacements(t *testing.T) {
	for _, test := range moleculeTests {
		result := moleculesFromLines(test.input)
		if len(result) != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestMedicineSteps(t *testing.T) {
	for _, test := range medicineStepTests {
		result := medicineSteps(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
