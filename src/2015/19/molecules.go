package main

import (
	"container/heap"
	"fmt"
	"regexp"
	"sort"
	"strings"
	"unicode"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type void struct{}

type testMolecule struct {
	molecule string
	steps    int
}

func parseLines(lines []string) (string, map[string][]string) {
	medicine := lines[len(lines)-1]
	replacements := make(map[string][]string)
	re := regexp.MustCompile(`^(\S+) => (\S+)$`)

	for _, line := range lines[0 : len(lines)-2] {
		m := re.FindStringSubmatch(line)
		if _, ok := replacements[m[1]]; !ok {
			replacements[m[1]] = []string{}
		}

		replacements[m[1]] = append(replacements[m[1]], m[2])
	}

	return medicine, replacements
}

func invertMap(m map[string][]string) map[string][]string {
	inverted := make(map[string][]string)

	for k, list := range m {
		for _, v := range list {
			if _, ok := inverted[v]; !ok {
				inverted[v] = []string{}
			}
			inverted[v] = append(inverted[v], k)
		}
	}

	return inverted
}

func candidates(medicine, molecule string, replacements []string) []string {
	replaced := []string{}

	var sb strings.Builder
	for i, c := range medicine {
		if i+len(molecule) <= len(medicine) && medicine[i:i+len(molecule)] == molecule {
			soFar := sb.String()
			for _, r := range replacements {
				sb.WriteString(r)
				sb.WriteString(medicine[i+len(molecule):])
				replaced = append(replaced, sb.String())

				sb.Reset()
				sb.WriteString(soFar)
			}
		}

		sb.WriteRune(c)
	}

	return replaced
}

func molecules(unit string, replacements map[string][]string) []string {
	seen := make(map[string]void)
	molecules := []string{}

	for m, r := range replacements {
		for _, molecule := range candidates(unit, m, r) {
			if _, ok := seen[molecule]; !ok {
				molecules = append(molecules, molecule)
				seen[molecule] = void{}
			}
		}
	}

	return molecules
}

func moleculesFromLines(lines []string) []string {
	medicine, replacements := parseLines(lines)
	return molecules(medicine, replacements)
}

// Original solution
// This works, but certain random order of key processing can
// cause extreme runtimes
func medicineStepsBruteForce(lines []string) int {

	medicine, replacements := parseLines(lines)
	replacements = invertMap(replacements)
	steps := -1

	seen := make(map[string]void)

	q := aoc.NewPriorityQueue()
	heap.Push(q, &aoc.Item{Value: testMolecule{medicine, 0}, Priority: len(medicine)})

	// We'll prioritize solutions that get us to our shorted result quicker
	// Note that this is not perfect; some edge cases may cause extended runtimes
	// if they dive down a non-optimal candidate
	for q.Len() > 0 {
		item := heap.Pop(q).(*aoc.Item)
		test := item.Value.(testMolecule)

		if test.molecule == "e" {
			steps = test.steps
			break
		}

		for _, m := range molecules(test.molecule, replacements) {
			if _, ok := seen[m]; !ok {
				if len(m) > 1 && strings.Contains(m, "e") {
					continue
				}
				heap.Push(q, &aoc.Item{Value: testMolecule{m, test.steps + 1}, Priority: len(m)})
				seen[m] = void{}
			}
		}
	}

	return steps
}

func reverse(s string) string {
	runes := make([]rune, len(s))
	n := 0
	for _, r := range s {
		runes[n] = r
		n++
	}

	runes = runes[0:n]
	for i, j := 0, len(runes)-1; i <= j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	return string(runes)
}

func reverseMap(m map[string][]string) map[string]string {
	reversed := make(map[string]string)
	for k, v := range m {
		for _, s := range v {
			reversed[reverse(s)] = reverse(k)
		}
	}
	return reversed
}

// Regex solution
// We process the regex from right to left (reverse everything)
// because the pattern always consistently ends (on Ar), but does not
// always consistently start with a pure element or a compound element (Rn)
//
// This may not always end in "e" (might end in "ee" or "eee"),
// but it always returns the right number of steps
func medicineSteps(lines []string) int {
	medicine, replacements := parseLines(lines)

	medicine = reverse(medicine)
	revReplacements := reverseMap(replacements)

	// favor longer matches first
	revKeys := make([]string, 0, len(revReplacements))
	for k := range revReplacements {
		revKeys = append(revKeys, k)
	}
	sort.Slice(revKeys, func(i, j int) bool { return len(revKeys[j]) < len(revKeys[i]) })

	steps := 0
	re := regexp.MustCompile(`(` + strings.Join(revKeys, "|") + `)`)

	for replaced := true; replaced; {
		replaced = false
		medicine = re.ReplaceAllStringFunc(medicine, func(m string) string {
			steps += 1
			replaced = true
			return revReplacements[m]
		})
	}

	return steps
}

// Math solution
// From the reddit forum explaining how the grammar works
func medicineStepsMath(lines []string) int {
	medicine, _ := parseLines(lines)

	countYs := strings.Count(medicine, "Y")
	countRns := strings.Count(medicine, "Rn")
	countArs := strings.Count(medicine, "Ar")

	tokens := 0
	for _, c := range medicine {
		if unicode.IsUpper(c) {
			tokens += 1
		}
	}

	return tokens - (countRns + countArs) - 2*countYs - 1
}

func main() {
	lines := aoc.ReadStrings("input.txt")

	// other solutions for finding medicine steps
	// these statements prevent unused type and function warnings
	_ = medicineStepsBruteForce
	_ = medicineStepsMath

	fmt.Printf("Distinct Molecules: %d\n", len(moleculesFromLines(lines)))
	fmt.Printf("Medicine Steps: %d\n", medicineSteps(lines))
}
