package main

import (
	"testing"
)

type test struct {
	input      string
	iterations int
	result     string
}

var iterationTests = []test{
	{"1", 1, "11"},
	{"11", 1, "21"},
	{"21", 1, "1211"},
	{"1211", 1, "111221"},
	{"111221", 1, "312211"},
	{"1", 5, "312211"},
}

func TestIterations(t *testing.T) {
	for _, test := range iterationTests {
		result := lookAndSay(test.input, test.iterations)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
