package main

import (
	"fmt"
	"strconv"
	"strings"
)

func lookAndSayIteration(input []int) []int {
	result := []int{}
	chr := input[0]
	cnt := 1

	for _, c := range input[1:] {
		if c == chr {
			cnt += 1
		} else {
			result = append(result, cnt, chr)
			chr = c
			cnt = 1
		}
	}

	result = append(result, cnt, chr)
	return result
}

func lookAndSay(input string, iterations int) string {
	result := stringToIntSlice(input)

	for iterations > 0 {
		result = lookAndSayIteration(result)
		iterations -= 1
	}

	return intSliceToString(result)
}

func stringToIntSlice(input string) []int {
	slice := []int{}

	for _, c := range input {
		i, _ := strconv.Atoi(string(c))
		slice = append(slice, i)
	}

	return slice
}

func intSliceToString(input []int) string {
	var sb strings.Builder

	for _, i := range input {
		sb.WriteString(strconv.Itoa(i))
	}

	return sb.String()
}

func main() {
	const INPUT = "3113322113"

	result := lookAndSay(INPUT, 40)
	fmt.Printf("Length after 40 iterations: %d\n", len(result))

	result = lookAndSay(result, 10)
	fmt.Printf("Length after 50 iterations: %d\n", len(result))
}
