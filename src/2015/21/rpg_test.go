package main

import (
	"testing"
)

type test struct {
	player character
	boss   character
	result bool
}

var tests = []test{
	{character{8, 5, 5}, character{12, 7, 2}, true},
}

func TestSimulation(t *testing.T) {
	for _, test := range tests {
		result := runSimulation(&test.player, &test.boss)
		if result != test.result {
			t.Error(
				"For", test.player, " & ", test.boss,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
