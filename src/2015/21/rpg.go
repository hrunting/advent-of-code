package main

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type character struct {
	hitPoints int
	damage    int
	armor     int
}

type item struct {
	typ    string
	name   string
	cost   int
	damage int
	armor  int
}

type kit struct {
	cost      int
	damage    int
	armor     int
	weapon    item
	armorItem item
	rings     []item
}

func newKit() *kit {
	return &kit{0, 0, 0, item{}, item{}, []item{}}
}

func (k *kit) addItem(i item) (err error) {
	switch i.typ {
	case "weapon":
		if k.weapon != (item{}) {
			err = errors.New("kit already contains a weapon")
		} else {
			k.weapon = i
		}
	case "armor":
		if k.armorItem != (item{}) {
			err = errors.New("kit already contains armor")
		} else {
			k.armorItem = i
		}
	case "ring":
		if len(k.rings) > 1 {
			err = errors.New("kit already contains 2 rings")
		} else if len(k.rings) == 1 && k.rings[0].name == i.name {
			err = fmt.Errorf("kit already contains ring '%s'", i.name)
		} else {
			k.rings = append(k.rings, i)
		}
	}

	if err == nil {
		k.cost += i.cost
		k.damage += i.damage
		k.armor += i.armor
	}

	return
}

var weapons = []item{
	{"weapon", "Dagger", 8, 4, 0},
	{"weapon", "Shortsword", 10, 5, 0},
	{"weapon", "Warhammer", 25, 6, 0},
	{"weapon", "Longsword", 40, 7, 0},
	{"weapon", "Greataxe", 74, 8, 0},
}

var armor = []item{
	{"armor", "Leather", 13, 0, 1},
	{"armor", "Chaimmail", 31, 0, 2},
	{"armor", "Splitmail", 53, 0, 3},
	{"armor", "Bandedmail", 75, 0, 4},
	{"armor", "Platemail", 102, 0, 5},
}

var rings = []item{
	{"ring", "Damage +1", 25, 1, 0},
	{"ring", "Damage +2", 50, 2, 0},
	{"ring", "Damage +3", 100, 3, 0},
	{"ring", "Damage +1", 20, 0, 1},
	{"ring", "Defense +2", 40, 0, 2},
	{"ring", "Defense +3", 80, 0, 3},
}

func characterFromLines(lines []string) *character {
	c := character{0, 0, 0}
	re := regexp.MustCompile(`^([^:]+):\s+(\d+)$`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)
		val, _ := strconv.Atoi(m[2])
		switch m[1] {
		case "Hit Points":
			c.hitPoints = val
		case "Damage":
			c.damage = val
		case "Armor":
			c.armor = val
		}
	}

	return &c
}

func getAllKits() []*kit {
	kits := []*kit{}

	for _, weapon := range weapons {
		k := newKit()
		k.addItem(weapon)
		kits = append(kits, k)

		for _, armor := range armor {
			k = newKit()
			k.addItem(weapon)
			k.addItem(armor)
			kits = append(kits, k)

			for i, ring1 := range rings {
				k = newKit()
				k.addItem(weapon)
				k.addItem(ring1)
				kits = append(kits, k)

				k = newKit()
				k.addItem(weapon)
				k.addItem(armor)
				k.addItem(ring1)
				kits = append(kits, k)

				for _, ring2 := range rings[i+1:] {
					k = newKit()
					k.addItem(weapon)
					k.addItem(ring1)
					k.addItem(ring2)
					kits = append(kits, k)

					k = newKit()
					k.addItem(weapon)
					k.addItem(armor)
					k.addItem(ring1)
					k.addItem(ring2)
					kits = append(kits, k)
				}
			}
		}
	}

	return kits
}

func runSimulation(player, boss *character) bool {
	if player.damage <= boss.armor {
		return false
	} else if boss.damage <= player.armor {
		return true
	}

	playerZeroLeft := player.hitPoints / (boss.damage - player.armor)
	if player.hitPoints%(boss.damage-player.armor) != 0 {
		playerZeroLeft += 1
	}

	bossZeroLeft := boss.hitPoints / (player.damage - boss.armor)
	if boss.hitPoints%(player.damage-boss.armor) != 0 {
		bossZeroLeft += 1
	}

	return playerZeroLeft >= bossZeroLeft
}

func minGoldWin(lines []string, hitPoints int) int {
	gold := 0
	boss := characterFromLines(lines)
	kits := getAllKits()

	for _, k := range kits {
		player := &character{hitPoints, k.damage, k.armor}
		if runSimulation(player, boss) {
			if gold == 0 || k.cost < gold {
				gold = k.cost
			}
		}
	}

	return gold
}

func maxGoldLoss(lines []string, hitPoints int) int {
	gold := 0
	boss := characterFromLines(lines)
	kits := getAllKits()

	for _, k := range kits {
		player := &character{hitPoints, k.damage, k.armor}
		if !runSimulation(player, boss) {
			if k.cost > gold {
				gold = k.cost
			}
		}
	}

	return gold
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Minimum gold needed for win: %d\n", minGoldWin(lines, 100))
	fmt.Printf("Maximum gold and still lose: %d\n", maxGoldLoss(lines, 100))
}
