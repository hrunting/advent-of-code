package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type aunt struct {
	id        int
	qualities map[string]int
}

func auntsFromLines(lines []string) []aunt {
	aunts := make([]aunt, len(lines))
	re := regexp.MustCompile(`^Sue (\d+): (.+)$`)

	for i, line := range lines {
		m := re.FindStringSubmatch(line)

		id, _ := strconv.Atoi(m[1])
		pairs := strings.Split(m[2], ", ")

		qualities := make(map[string]int, (len(m)-1)/2)
		for _, pair := range pairs {
			parts := strings.Split(pair, ": ")
			key := parts[0]
			val, _ := strconv.Atoi(parts[1])
			qualities[key] = val
		}

		aunts[i] = aunt{id, qualities}
	}

	return aunts
}

var analysis map[string]int = map[string]int{
	"children":    3,
	"cats":        7,
	"samoyeds":    2,
	"pomeranians": 3,
	"akitas":      0,
	"vizslas":     0,
	"goldfish":    5,
	"trees":       3,
	"cars":        2,
	"perfumes":    1,
}

func findAuntSue(lines []string, real bool) int {
	aunts := auntsFromLines(lines)
	auntId := -1

	for _, a := range aunts {
		auntId = a.id

		for k, v := range a.qualities {
			if real && (k == "cats" || k == "trees") {
				if analysis[k] >= v {
					auntId = -1
					break
				}
			} else if real && (k == "pomeranians" || k == "goldfish") {
				if analysis[k] <= v {
					auntId = -1
					break
				}
			} else if analysis[k] != v {
				auntId = -1
				break
			}
		}

		if auntId >= 0 {
			break
		}
	}

	return auntId
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Gift Aunt: %d\n", findAuntSue(lines, false))
	fmt.Printf("Real Gift Aunt: %d\n", findAuntSue(lines, true))
}
