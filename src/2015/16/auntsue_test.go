package main

import (
	"testing"
)

type test struct {
	input  []string
	real   bool
	result int
}

var example = []string{
	`Sue 1: children: 1, cars: 8, vizslas: 7`,
	`Sue 2: akitas: 10, perfumes: 10, children: 5`,
	`Sue 3: cars: 2, pomeranians: 3, vizslas: 0`,
	`Sue 4: goldfish: 5, children: 8, perfumes: 3`,
	`Sue 5: goldfish: 4, cats: 8, perfumes: 1`,
}

var tests = []test{
	{example, false, 3},
	{example, true, 5},
}

func TestFindAuntSue(t *testing.T) {
	for _, test := range tests {
		result := findAuntSue(test.input, test.real)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
