package main

import (
	"testing"
)

type test struct {
	input  string
	result bool
}

var simpleTests = []test{
	{"ugknbfddgicrmopn", true},
	{"aaa", true},
	{"jchzalrnumimnmhp", false},
	{"haegwjzuvuyypxyu", false},
	{"dvszwmarrgswjxmb", false},
}

var betterTests = []test{
	{"qjhvhtzxzqqjkmpb", true},
	{"xxyxx", true},
	{"uurcxstgmygtbstg", false},
	{"ieodomkazucvgmuy", false},
}

func TestSimpleNiceStrings(t *testing.T) {
	for _, test := range simpleTests {
		result := isSimpleNiceString(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestBetterNiceStrings(t *testing.T) {
	for _, test := range betterTests {
		result := isBetterNiceString(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
