package main

import (
	"fmt"
	"regexp"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

func hasDoubles(s string) bool {
	var last rune

	for _, c := range s {
		if c == last {
			return true
		}

		last = c
	}

	return false
}

func isSimpleNiceString(s string) bool {
	badpairs := regexp.MustCompile(`(?:ab|cd|pq|xy)`)
	vowels := regexp.MustCompile(`[aeiou].*[aeiou].*[aeiou]`)

	return !badpairs.MatchString(s) && vowels.MatchString(s) && hasDoubles(s)
}

func isBetterNiceString(s string) bool {
	digrams := make(map[string]int)
	hasRepeatPair := false
	hasSeparatedRepeatChar := false

	for i := range s[:len(s)-1] {
		digram := s[i : i+2]
		if pos, ok := digrams[digram]; ok {
			if i > pos+1 {
				hasRepeatPair = true
			}
		} else {
			digrams[digram] = i
		}

		if i < len(s)-2 {
			if s[i] == s[i+2] {
				hasSeparatedRepeatChar = true
			}
		}
	}

	return hasRepeatPair && hasSeparatedRepeatChar
}

func countNiceStrings(lines []string, validator func(s string) bool) int {
	niceStrings := 0

	for _, s := range lines {
		if validator(s) {
			niceStrings += 1
		}
	}

	return niceStrings
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Nice Strings: %d\n", countNiceStrings(lines, isSimpleNiceString))
	fmt.Printf("Nice Strings (New Rules): %d\n", countNiceStrings(lines, isBetterNiceString))
}
