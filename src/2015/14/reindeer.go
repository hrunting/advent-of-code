package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type reindeer struct {
	name     string
	speed    int
	runTime  int
	restTime int

	unitDistance int
	unitTime     int
}

func (r *reindeer) distanceAtTime(time int) int {
	if r.unitDistance == 0 {
		r.unitDistance = r.speed * r.runTime
		r.unitTime = r.runTime + r.restTime
	}

	units, remainder := time/r.unitTime, time%r.unitTime

	distance := units * r.unitDistance
	if remainder > r.runTime {
		distance += r.unitDistance
	} else {
		distance += remainder * r.speed
	}

	return distance
}

func reindeerFromLines(lines []string) []reindeer {
	herd := make([]reindeer, 0)
	re := regexp.MustCompile(`^(\S+) can fly (\d+) km/s for (\d+) second(?:s)?, but then must rest for (\d+) second(?:s)?\.`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)

		speed, _ := strconv.Atoi(m[2])
		runTime, _ := strconv.Atoi(m[3])
		restTime, _ := strconv.Atoi(m[4])

		r := reindeer{m[1], speed, runTime, restTime, 0, 0}
		herd = append(herd, r)
	}

	return herd
}

func winningScore(lines []string, time int) int {
	scoreBoard := make(map[string]int)
	maxScore := 0
	herd := reindeerFromLines(lines)

	for _, r := range herd {
		scoreBoard[r.name] = 0
	}

	for t := 1; t <= time; t++ {
		distances := make(map[int][]string)
		maxDistance := 0
		for _, r := range herd {
			distance := r.distanceAtTime(t)
			if _, ok := distances[distance]; !ok {
				distances[distance] = []string{}
			}
			distances[distance] = append(distances[distance], r.name)

			if distance > maxDistance {
				maxDistance = distance
			}
		}

		for _, name := range distances[maxDistance] {
			scoreBoard[name] += 1
			if scoreBoard[name] > maxScore {
				maxScore = scoreBoard[name]
			}
		}
	}

	return maxScore
}

func winningDistance(lines []string, time int) int {
	herd := reindeerFromLines(lines)
	distance := 0

	for _, r := range herd {
		if d := r.distanceAtTime(time); d > distance {
			distance = d
		}
	}

	return distance
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Winning Distance: %d\n", winningDistance(lines, 2503))
	fmt.Printf("Winning Score: %d\n", winningScore(lines, 2503))
}
