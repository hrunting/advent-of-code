package main

import (
	"testing"
)

type test struct {
	input  []string
	time   int
	result int
}

var example = []string{
	`Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.`,
	`Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.`,
}

var distanceTests = []test{
	{example, 1, 16},
	{example, 10, 160},
	{example, 11, 176},
	{example, 1000, 1120},
}

var scoreTests = []test{
	{example, 1, 1},
	{example, 139, 139},
	{example, 140, 139},
	{example, 1000, 689},
}

func TestWinningDistance(t *testing.T) {
	for _, test := range distanceTests {
		result := winningDistance(test.input, test.time)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestWinningScore(t *testing.T) {
	for _, test := range scoreTests {
		result := winningScore(test.input, test.time)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
