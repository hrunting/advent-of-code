package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type instruction struct {
	command  string
	register string
	offset   int
}

type computer struct {
	registers    map[string]uint64
	instructions []instruction
	ptr          int
}

func (c *computer) isRunning() bool {
	return c.ptr < len(c.instructions)
}

func (c *computer) step() {
	offset := 1

	switch i := c.instructions[c.ptr]; i.command {
	case "hlf":
		c.registers[i.register] /= 2
	case "tpl":
		c.registers[i.register] *= 3
	case "inc":
		c.registers[i.register] += 1
	case "jmp":
		offset = i.offset
	case "jie":
		if c.registers[i.register]%2 == 0 {
			offset = i.offset
		}
	case "jio":
		if c.registers[i.register] == 1 {
			offset = i.offset
		}
	}

	c.ptr += offset
}

func newComputer(code []string) *computer {
	registers := make(map[string]uint64)
	cmds := make([]instruction, 0, len(code))

	re := regexp.MustCompile(`^(\S+) (\S+)(?:, (\S+))?$`)
	for _, line := range code {
		m := re.FindStringSubmatch(line)

		cmd := instruction{}
		cmd.command = m[1]

		switch cmd.command {
		case "hlf", "tpl", "inc":
			cmd.register = m[2]
		case "jmp":
			cmd.offset, _ = strconv.Atoi(m[2])
		case "jie", "jio":
			cmd.register = m[2]
			cmd.offset, _ = strconv.Atoi(m[3])
		}

		cmds = append(cmds, cmd)
		if cmd.register != "" {
			registers[cmd.register] = 0
		}
	}

	return &computer{registers, cmds, 0}
}

func runProgram(code []string, init map[string]uint64, register string) uint64 {
	c := newComputer(code)

	for k, v := range init {
		c.registers[k] = v
	}

	for c.isRunning() {
		c.step()
	}

	return c.registers[register]
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Value in register B: %d\n", runProgram(lines, map[string]uint64{}, "b"))
	fmt.Printf("Value in register B (init: a=1): %d\n", runProgram(lines, map[string]uint64{"a": 1}, "b"))
}
