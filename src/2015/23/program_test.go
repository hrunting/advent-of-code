package main

import (
	"testing"
)

type test struct {
	input  []string
	result uint64
}

var tests = []test{
	{[]string{`inc a`, `jio a, +2`, `tpl a`, `inc a`}, 2},
}

func TestProgram(t *testing.T) {
	for _, test := range tests {
		result := runProgram(test.input, map[string]uint64{}, "a")
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
