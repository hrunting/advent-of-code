package main

import (
	"testing"
)

type test struct {
	input  string
	result int
}

var santaTests = []test{
	{">", 2},
	{"^>v<", 4},
	{"^v^v^v^v^v", 2},
}

var robotTests = []test{
	{"^v", 3},
	{"^>v<", 3},
	{"^v^v^v^v^v", 11},
}

func TestSingleWorker(t *testing.T) {
	for _, test := range santaTests {
		result := visitHouses(test.input, 1)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestFirstBasementPosition(t *testing.T) {
	for _, test := range robotTests {
		result := visitHouses(test.input, 2)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
