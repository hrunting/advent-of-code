package main

import (
	"fmt"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type coord struct{ x, y int }

func move(pos coord, dir rune) coord {
	switch dir {
	case '^':
		pos = coord{pos.x, pos.y + 1}
	case '>':
		pos = coord{pos.x + 1, pos.y}
	case 'v':
		pos = coord{pos.x, pos.y - 1}
	case '<':
		pos = coord{pos.x - 1, pos.y}
	}

	return pos
}

func visitHouses(path string, count int) int {
	homes := make(map[coord]int)

	workers := make([]coord, count)
	worker_idx := 0

	homes[coord{0, 0}] = len(workers)

	for _, c := range path {
		workers[worker_idx] = move(workers[worker_idx], c)
		homes[workers[worker_idx]] += 1
		worker_idx = (worker_idx + 1) % len(workers)
	}

	return len(homes)
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Homes visited: %d\n", visitHouses(lines[0], 1))
	fmt.Printf("Homes visited (w/ robot): %d\n", visitHouses(lines[0], 2))
}
