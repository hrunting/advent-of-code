package main

import (
	"container/heap"
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type character struct {
	hitPoints    int
	damage       int
	armor        int
	mana         int
	activeSpells map[string]spell
}

func (c *character) clone() *character {
	n := *c

	spells := make(map[string]spell, len(c.activeSpells))
	for k, v := range c.activeSpells {
		spells[k] = v
	}

	n.activeSpells = spells
	return &n
}

type spell struct {
	name  string
	mana  int
	turns int
	cast  func(player, boss *character)
	end   func(player, boss *character)
}

type gameState struct {
	player *character
	boss   *character
	spent  int
}

var noop = func(p, b *character) {}
var spells = []spell{
	{"Magic Missle", 53, 0, func(p, b *character) { b.hitPoints -= 4 }, noop},
	{"Drain", 73, 0, func(p, b *character) { b.hitPoints -= 2; p.hitPoints += 2 }, noop},
	{"Shield", 113, 6, func(p, b *character) { p.armor = 7 }, func(p, b *character) { p.armor -= 7 }},
	{"Poison", 173, 6, func(p, b *character) { b.hitPoints -= 3 }, noop},
	{"Recharge", 229, 5, func(p, b *character) { p.mana += 101 }, noop},
}

func newCharacter() *character {
	return &character{0, 0, 0, 0, make(map[string]spell)}
}

func newCharacterFromLines(lines []string) *character {
	c := newCharacter()
	re := regexp.MustCompile(`^([^:]+):\s+(\d+)$`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)
		val, _ := strconv.Atoi(m[2])
		switch m[1] {
		case "Hit Points":
			c.hitPoints = val
		case "Damage":
			c.damage = val
		case "Armor":
			c.armor = val
		}
	}

	return c
}

func applySpells(p, b *character) {
	for name, s := range p.activeSpells {
		s.cast(p, b)
		s.turns -= 1
		if s.turns <= 0 {
			s.end(p, b)
			delete(p.activeSpells, name)
		} else {
			p.activeSpells[name] = s
		}
	}
}

func minManaWin(lines []string, hitPoints, manaPoints int, hard bool) int {
	boss := newCharacterFromLines(lines)

	player := newCharacter()
	player.hitPoints = hitPoints
	player.mana = manaPoints

	manaSpent := 0

	q := aoc.NewPriorityQueue()
	heap.Push(q, &aoc.Item{Value: gameState{player, boss, 0}, Priority: boss.hitPoints})

	for q.Len() > 0 {
		state := heap.Pop(q).(*aoc.Item).Value.(gameState)

		// end of boss turn

		// the boss has lost, we have found our winning path
		// it's possible that the boss lost at the end of the player turn
		// but it does not really matter when we check
		if state.boss.hitPoints <= 0 {
			manaSpent = state.spent
			break
		}

		// begin player turn

		// in hard mode, player loses a point before anything else happens
		// that may make the player lose
		if hard {
			state.player.hitPoints -= 1
		}

		// this is a player-losing state, no need to check this state further
		if state.player.hitPoints <= 0 {
			continue
		}

		applySpells(state.player, state.boss)

		// process each spell and create a state for each possible choice
		for _, s := range spells {

			// spell with an effect has already been cast
			// this spell is not a possibility for us
			if _, ok := state.player.activeSpells[s.name]; ok {
				continue
			}

			// player does not have enough mana to cast this spell
			// this spell is not a possibility for us
			if state.player.mana < s.mana {
				continue
			}

			// create copies of our player and boss states for this path
			p := state.player.clone()
			b := state.boss.clone()

			// run the player turn
			// spend the mana and cast the spell
			spent := state.spent + s.mana
			p.mana -= s.mana
			p.activeSpells[s.name] = s

			// begin boss turn

			applySpells(p, b)

			if b.hitPoints > 0 {
				if b.damage > p.armor {
					p.hitPoints -= b.damage - p.armor
				} else {
					p.hitPoints -= 1
				}
			}

			heap.Push(q, &aoc.Item{
				Value:    gameState{p, b, spent},
				Priority: spent*1000 + aoc.Min(boss.hitPoints, 0),
			})
		}
	}

	return manaSpent
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Minimum mana needed for win: %d\n", minManaWin(lines, 50, 500, false))
	fmt.Printf("Minimum mana needed for hard win: %d\n", minManaWin(lines, 50, 500, true))
}
