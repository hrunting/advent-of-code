package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var tests = []test{
	{[]string{`Hit Points: 13`, `Damage: 8`}, 226},
	{[]string{`Hit Points: 14`, `Damage: 8`}, 641},
}

func TestMinMana(t *testing.T) {
	for _, test := range tests {
		result := minManaWin(test.input, 10, 250, false)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestMinManaHard(t *testing.T) {
	for _, test := range tests {
		result := minManaWin(test.input, 10, 250, true)

		// for all tested possibilities, the player loses
		if result != 0 {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
