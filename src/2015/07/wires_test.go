package main

import (
	"testing"
)

type test struct {
	wire   string
	result uint16
}

var assembleTests = []test{
	{"d", 72},
	{"e", 507},
	{"f", 492},
	{"g", 114},
	{"h", 65412},
	{"i", 65079},
	{"x", 123},
	{"y", 456},
}

func TestAssemble(t *testing.T) {
	commands := []string{
		"123 -> x",
		"456 -> y",
		"x AND y -> d",
		"x OR y -> e",
		"x LSHIFT 2 -> f",
		"y RSHIFT 2 -> g",
		"NOT x -> h",
		"NOT y -> i",
	}

	for _, test := range assembleTests {
		result := assembleAndReadSignal(commands, test.wire, nil)
		if result != test.result {
			t.Error(
				"For", test.wire,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
