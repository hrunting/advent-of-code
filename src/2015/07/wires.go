package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type wire struct {
	id     string
	signal chan uint16
}

func (w *wire) send(s uint16) {
	for {
		w.signal <- s
	}
}

func noopGate(src, dest *wire) {
	dest.send(<-src.signal)
}

func sendGate(signal uint16, w *wire) {
	w.send(signal)
}

func complementGate(src, dest *wire) {
	dest.send(^(<-src.signal))
}

func lshiftGate(src *wire, shift uint16, dest *wire) {
	signal := <-src.signal
	dest.send(signal << shift)
}

func rshiftGate(src *wire, shift uint16, dest *wire) {
	dest.send((<-src.signal) >> shift)
}

func andGate(src1, src2, dest *wire) {
	dest.send((<-src1.signal) & (<-src2.signal))
}

func orGate(src1, src2, dest *wire) {
	dest.send((<-src1.signal) | (<-src2.signal))
}

func getWire(wires map[string]*wire, id string) *wire {
	w, ok := wires[id]
	if !ok {

		if val, err := strconv.Atoi(id); err == nil {
			// the wire ID we were given is an integer, not a wire ID
			// create a virtual wire that just sends the integer as a signal
			w = &wire{"[" + id + "]", make(chan uint16)}
			go sendGate(uint16(val), w)

		} else {
			// otherwise, create a wire annd save it in our lookup table
			w = &wire{id, make(chan uint16)}
			wires[id] = w
		}
	}

	return w
}

func assembleAndReadSignal(lines []string, readID string, override map[string]uint16) uint16 {
	wires := make(map[string]*wire)

	for id, val := range override {
		w := getWire(wires, id)
		go sendGate(val, w)
	}

	wirePattern := regexp.MustCompile(`^([a-z]+) -> ([a-z]+)$`)
	signalPattern := regexp.MustCompile(`^(\d+) -> ([a-z]+)$`)
	complementPattern := regexp.MustCompile(`^NOT ([a-z]+) -> ([a-z]+)$`)
	opPattern := regexp.MustCompile(`^(\S+) (AND|OR|LSHIFT|RSHIFT) ([a-z0-9]+) -> ([a-z]+)$`)

	for _, line := range lines {
		if re := wirePattern.FindStringSubmatch(line); re != nil {
			src := getWire(wires, re[1])
			dest := getWire(wires, re[2])

			if _, ok := override[dest.id]; !ok {
				go noopGate(src, dest)
			}
		} else if re := signalPattern.FindStringSubmatch(line); re != nil {
			val, _ := strconv.ParseUint(re[1], 10, 16)
			w := getWire(wires, re[2])

			if _, ok := override[w.id]; !ok {
				go sendGate(uint16(val), w)
			}
		} else if re := complementPattern.FindStringSubmatch(line); re != nil {
			src := getWire(wires, re[1])
			dest := getWire(wires, re[2])

			if _, ok := override[dest.id]; !ok {
				go complementGate(src, dest)
			}
		} else if re := opPattern.FindStringSubmatch(line); re != nil {
			x := getWire(wires, re[1])
			op := re[2]
			z := getWire(wires, re[4])

			if _, ok := override[z.id]; !ok {
				if op[1:] == "SHIFT" {
					val, _ := strconv.ParseUint(re[3], 10, 16)
					switch op {
					case "LSHIFT":
						go lshiftGate(x, uint16(val), z)
					case "RSHIFT":
						go rshiftGate(x, uint16(val), z)
					}
				} else {
					y := getWire(wires, re[3])
					switch op {
					case "AND":
						go andGate(x, y, z)
					case "OR":
						go orGate(x, y, z)
					}
				}
			}
		}
	}

	return <-wires[readID].signal
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	signal := assembleAndReadSignal(lines, "a", nil)
	fmt.Printf("Wire 'a' Value: %d\n", signal)

	signal = assembleAndReadSignal(lines, "a", map[string]uint16{"b": signal})
	fmt.Printf("Wire 'a' Value (override): %d\n", signal)
}
