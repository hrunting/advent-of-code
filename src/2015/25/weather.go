package main

import (
	"fmt"
	"math/big"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

const BASE = 252533
const MOD = 33554393

func codeLocation(line string) (uint64, uint64) {
	re := regexp.MustCompile(`at row (\d+), column (\d+)`)
	m := re.FindStringSubmatch(line)

	row, _ := strconv.Atoi(m[1])
	column, _ := strconv.Atoi(m[2])

	return uint64(row), uint64(column)
}

func codeAtGrid(row, column uint64) uint64 {
	base := new(big.Int).SetUint64(BASE)
	mod := new(big.Int).SetUint64(MOD)

	// the exponent is the index in the row/column setup
	// the first row is the sum of a +1 sequence
	// find the sum that ends the "triangle" before our column
	// then add the column count to get the index
	exp := new(big.Int).SetUint64((row+column-2)*(row+column-1)/2 + column - 1)

	return 20151125 * new(big.Int).Exp(base, exp, mod).Uint64() % MOD
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	row, column := codeLocation(lines[0])
	code := codeAtGrid(row, column)

	fmt.Printf("Code at %d, %d: %d\n", row, column, code)
}
