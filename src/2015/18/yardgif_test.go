package main

import (
	"testing"
)

type test struct {
	input      []string
	iterations int
	result     int
}

var example = []string{
	`.#.#.#`,
	`...##.`,
	`#....#`,
	`..#...`,
	`#.#..#`,
	`####..`,
}

var tests = []test{
	{example, 0, 15},
	{example, 1, 11},
	{example, 2, 8},
	{example, 3, 4},
	{example, 4, 4},
}

var stuckTests = []test{
	{example, 0, 17},
	{example, 1, 18},
	{example, 2, 18},
	{example, 3, 18},
	{example, 4, 14},
	{example, 5, 17},
}

func TestIterations(t *testing.T) {
	for _, test := range tests {
		result := countActiveLights(test.input, false, test.iterations)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestStuckIterations(t *testing.T) {
	for _, test := range stuckTests {
		result := countActiveLights(test.input, true, test.iterations)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
