package main

import (
	"fmt"
	"strings"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type lights struct {
	lights []int
	xsize  int
	ysize  int
	stuck  bool
}

func (g *lights) idx(x, y int) int {
	return y*g.xsize + x
}

func (g *lights) on(x, y int) {
	g.lights[g.idx(x, y)] = 1
}

func (g *lights) off(x, y int) {
	idx := g.idx(x, y)

	if g.stuck {
		switch idx {
		case 0:
			fallthrough
		case g.xsize - 1:
			fallthrough
		case len(g.lights) - g.xsize:
			fallthrough
		case len(g.lights) - 1:
			return
		}
	}

	g.lights[idx] = 0
}

func (g *lights) light(x, y int) int {
	if x < 0 || y < 0 || x >= g.xsize || y >= g.ysize {
		return 0
	} else {
		return g.lights[g.idx(x, y)]
	}
}

func (g *lights) countActive() int {
	return aoc.Sum(g.lights...)
}

func (g *lights) nextIteration() *lights {
	next := newLights(g.xsize, g.ysize, g.stuck)

	for i, light := range g.lights {
		neighbors := 0
		x, y := i%g.ysize, i/g.ysize

		for dy := -1; dy <= 1; dy++ {
			for dx := -1; dx <= 1; dx++ {
				if dx == 0 && dy == 0 {
					continue
				}

				neighbors += g.light(x+dx, y+dy)
			}
		}

		if light == 1 && (neighbors == 2 || neighbors == 3) {
			next.lights[i] = 1
		} else if light == 0 && neighbors == 3 {
			next.lights[i] = 1
		}
	}

	return next
}

func (g *lights) asString() string {
	var sb strings.Builder

	for y := 0; y < g.ysize; y++ {
		if y != 0 {
			sb.WriteString("\n")
		}

		for x := 0; x < g.xsize; x++ {
			if g.lights[g.idx(x, y)] == 1 {
				sb.WriteString("#")
			} else {
				sb.WriteString(".")
			}
		}
	}

	return sb.String()
}

func newLights(xsize, ysize int, stuck bool) *lights {
	size := xsize * ysize
	grid := lights{
		make([]int, size),
		xsize,
		ysize,
		stuck,
	}

	if stuck {
		grid.lights[0] = 1
		grid.lights[xsize-1] = 1
		grid.lights[size-xsize] = 1
		grid.lights[size-1] = 1
	}

	return &grid
}

func lightsFromLines(lines []string, stuck bool) *lights {
	grid := newLights(len(lines[0]), len(lines), stuck)

	for y, line := range lines {
		for x, c := range line {
			if c == '#' {
				grid.on(x, y)
			}
		}
	}

	return grid
}

func countActiveLights(lines []string, stuck bool, steps int) int {
	grid := lightsFromLines(lines, stuck)
	for i := 0; i < steps; i++ {
		grid = grid.nextIteration()
	}
	return grid.countActive()
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Lights On After 100 Steps: %d\n", countActiveLights(lines, false, 100))
	fmt.Printf("Lights On After 100 Steps (Broken): %d\n", countActiveLights(lines, true, 100))
}
