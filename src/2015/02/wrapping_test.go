package main

import (
	"testing"
)

type test struct {
	input  string
	result int
}

var wrappingAreaTests = []test{
	{"2x3x4", 58},
	{"1x1x10", 43},
}

var ribbonLengthTests = []test{
	{"2x3x4", 34},
	{"1x1x10", 14},
}

func TestWrappingArea(t *testing.T) {
	for _, test := range wrappingAreaTests {
		b := parseBox(test.input)
		result := b.wrappingArea()
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestRibbonLengths(t *testing.T) {
	for _, test := range ribbonLengthTests {
		b := parseBox(test.input)
		result := b.ribbonLength()
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
