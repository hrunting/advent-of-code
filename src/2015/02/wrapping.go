package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type box struct {
	length, width, height int
}

func (b *box) wrappingArea() int {
	sides := []int{
		b.length * b.width,
		b.width * b.height,
		b.length * b.height,
	}

	sort.Ints(sides)
	return 2*(aoc.Sum(sides...)) + sides[0]
}

func (b *box) ribbonLength() int {
	perimeters := []int{
		2 * (b.length + b.width),
		2 * (b.width + b.height),
		2 * (b.length + b.height),
	}

	sort.Ints(perimeters)
	return perimeters[0] + b.length*b.width*b.height
}

func parseBox(s string) box {
	lengths := strings.Split(s, "x")

	length, _ := strconv.Atoi(lengths[0])
	width, _ := strconv.Atoi(lengths[1])
	height, _ := strconv.Atoi(lengths[2])

	return box{length, width, height}
}

func totalWrappingArea(lines []string) int {
	area := 0

	for _, line := range lines {
		b := parseBox(line)
		area += b.wrappingArea()
	}

	return area
}

func totalRibbonLength(lines []string) int {
	ribbon := 0

	for _, line := range lines {
		b := parseBox(line)
		ribbon += b.ribbonLength()
	}

	return ribbon
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Total Area: %d sf\n", totalWrappingArea(lines))
	fmt.Printf("Ribbon Length: %d ft\n", totalRibbonLength(lines))
}
