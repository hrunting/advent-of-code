package main

import (
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

func sumNumbers(lines []string) int {
	sum := 0
	re := regexp.MustCompile(`(\-?\d+)`)

	for _, line := range lines {
		matches := re.FindAllString(line, -1)
		for _, m := range matches {
			x, _ := strconv.Atoi(m)
			sum += x
		}
	}

	return sum
}

func sumNonRedArray(dec *json.Decoder) int {
	sum := 0

Loop:
	for {
		token, err := dec.Token()
		if err == io.EOF {
			break Loop
		}

		switch t := token.(type) {
		case json.Delim:
			switch t {
			case ']':
				break Loop
			case '[':
				sum += sumNonRedArray(dec)
			case '{':
				sum += sumNonRedObject(dec)
			}
		case json.Number:
			val, _ := t.Int64()
			sum += int(val)
		}
	}

	return sum
}

func sumNonRedObject(dec *json.Decoder) int {
	sum := 0
	isRed := false
	isKey := true

Loop:
	for {
		token, err := dec.Token()
		if err == io.EOF {
			break Loop
		}

		switch t := token.(type) {
		case json.Delim:
			switch t {
			case '}':
				break Loop
			case '[':
				sum += sumNonRedArray(dec)
			case '{':
				sum += sumNonRedObject(dec)
			}
		case json.Number:
			if !isKey {
				val, _ := t.Int64()
				sum += int(val)
			}
		case string:
			if !isKey {
				isKey = true
				if t == "red" {
					isRed = true
				}
			}
		}

		isKey = !isKey
	}

	if isRed {
		sum = 0
	}

	return sum
}

func sumNonRedNumbers(lines []string) int {
	sum := 0

	for _, line := range lines {
		dec := json.NewDecoder(strings.NewReader(line))
		dec.UseNumber()
		for {
			token, err := dec.Token()
			if err == io.EOF {
				break
			}

			if token.(json.Delim) == '[' {
				sum += sumNonRedArray(dec)
			} else {
				sum += sumNonRedObject(dec)
			}
		}
	}

	return sum
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Sum: %d\n", sumNumbers(lines))
	fmt.Printf("Non-red Sum: %d\n", sumNonRedNumbers(lines))
}
