package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var sumTests = []test{
	{[]string{`[1,2,3]`}, 6},
	{[]string{`{"a":2,"b":4}`}, 6},
	{[]string{`[[[3]]]`}, 3},
	{[]string{`{"a":{"b":4},"c":-1}`}, 3},
	{[]string{`{"a":[-1,1]}`}, 0},
	{[]string{`[-1,{"a":1}]`}, 0},
	{[]string{`[]`}, 0},
	{[]string{`{}`}, 0},
	{[]string{
		`[1,2,3]`,
		`{"a":2,"b":4}`,
		`[[[3]]]`,
		`{"a":{"b":4},"c":-1}`,
		`{"a":[-1,1]}`,
		`[-1,{"a":1}]`,
		`[]`,
		`{}`,
	}, 18},
}

var nonRedTests = []test{
	{[]string{`[1,2,3]`}, 6},
	{[]string{`[1,{"c":"red","b":2},3]`}, 4},
	{[]string{`{"d":"red","e":[1,2,3,4],"f":5}`}, 0},
	{[]string{`[1,"red",5]`}, 6},
	{[]string{
		`[1,2,3]`,
		`[1,{"c":"red","b":2},3]`,
		`{"d":"red","e":[1,2,3,4],"f":5}`,
		`[1,"red",5]`,
	}, 16},
}

func TestSum(t *testing.T) {
	for _, test := range sumTests {
		result := sumNumbers(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestNonRedSum(t *testing.T) {
	for _, test := range nonRedTests {
		result := sumNonRedNumbers(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
