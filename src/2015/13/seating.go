package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type void struct{}
type set map[string]void

type graph map[string]map[string]int

func (g graph) addPath(from, to string, weight int) {
	if _, ok := g[from]; !ok {
		g[from] = make(map[string]int)
	}

	if _, ok := g[to]; !ok {
		g[to] = make(map[string]int)
	}

	g[from][to] += weight
	g[to][from] += weight
}

func (g graph) addMe() {
	for person := range g {
		g.addPath(person, "me", 0)
	}
}

func (g graph) findLongestSubDistance(node string, visited set, startNode string) int {
	distance := 0
	visited[node] = void{}

	// we have visited all of the nodes
	if len(visited) == len(g) {
		distance = g[node][startNode]
	} else {
		for k, weight := range g[node] {
			if _, ok := visited[k]; !ok {
				subDistance := g.findLongestSubDistance(k, visited, startNode) + weight
				if subDistance > distance {
					distance = subDistance
				}
			}
		}
	}

	delete(visited, node)
	return distance
}

func (g graph) findLongestDistance() int {
	distance := 0

	for node := range g {
		visited := set{}
		pathDistance := g.findLongestSubDistance(node, visited, node)
		if pathDistance > distance {
			distance = pathDistance
		}
	}

	return distance
}

func newGraphFromLines(lines []string) *graph {
	g := &graph{}

	re := regexp.MustCompile(`^(\S+) would (gain|lose) (\d+) happiness units by sitting next to (\S+)\.$`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)

		weight, _ := strconv.Atoi(m[3])
		if m[2] == "lose" {
			weight *= -1
		}

		g.addPath(m[1], m[4], weight)
	}

	return g
}

func optimalHappiness(lines []string, withMe bool) int {
	graph := newGraphFromLines(lines)
	if withMe {
		graph.addMe()
	}
	return graph.findLongestDistance()
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Optimal Happiness: %d\n", optimalHappiness(lines, false))
	fmt.Printf("Optimal Happiness (with me): %d\n", optimalHappiness(lines, true))
}
