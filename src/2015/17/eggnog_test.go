package main

import (
	"testing"
)

type test struct {
	input  []int
	limit  int
	result int
}

var example = []int{20, 15, 10, 5, 5}

var comboTests = []test{
	{example, 25, 4},
}

var minComboTests = []test{
	{example, 25, 3},
}

func TestCombinations(t *testing.T) {
	for _, test := range comboTests {
		result := combinations(test.input, test.limit)
		if len(result) != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestMinimumCombinations(t *testing.T) {
	for _, test := range minComboTests {
		result := minimumCombinations(test.input, test.limit)
		if len(result) != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
