package main

import (
	"fmt"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

func combinations(bottles []int, total int) [][]int {
	combos := make([][]int, 0)

	for i, bottle := range bottles {
		if bottle == total {
			combos = append(combos, []int{bottle})

		} else if bottle < total {
			subCombos := combinations(bottles[i+1:], total-bottle)
			for _, c := range subCombos {
				combo := make([]int, len(c)+1)
				copy(combo, c)
				combo[len(combo)-1] = bottle
				combos = append(combos, combo)
			}
		}
	}

	return combos
}

func minimumCombinations(bottles []int, total int) [][]int {
	combos := combinations(bottles, total)

	minCount := len(bottles)
	minCombos := make([][]int, 0)

	for _, c := range combos {
		if len(c) < minCount {
			minCount = len(c)
			minCombos = [][]int{c}
		} else if len(c) == minCount {
			minCombos = append(minCombos, c)
		}
	}

	return minCombos
}

func main() {
	bottles := aoc.ReadInts("input.txt")
	fmt.Printf("%v\n", bottles)
	fmt.Printf("Combinations: %d\n", len(combinations(bottles, 150)))
	fmt.Printf("Minimum Combinations: %d\n", len(minimumCombinations(bottles, 150)))
}
