package main

import (
	"fmt"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

func decodedDifference(lines []string) int {
	chars := 0
	memory := 0

	for _, line := range lines {
		chars += len(line)

		i := 0
		for i < len(line) {
			switch line[i] {
			case '"':
			case '\\':
				if line[i+1] == 'x' {
					i += 3
				} else {
					i += 1
				}

				fallthrough
			default:
				memory += 1
			}

			i += 1
		}
	}

	return chars - memory
}

func encodedDifference(lines []string) int {
	chars := 0
	encoded := 0

	for _, line := range lines {
		chars += len(line)
		encoded += 2

		for _, c := range line {
			if c == '\\' || c == '"' {
				encoded += 1
			}

			encoded += 1
		}
	}

	return encoded - chars
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Decoded Difference: %d\n", decodedDifference(lines))
	fmt.Printf("Encoded Difference: %d\n", encodedDifference(lines))
}
