package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var decodedDiffTests = []test{
	{
		[]string{
			`""`,
			`"abc"`,
			`"aaa\"aaa"`,
			`"\x27"`,
		},
		12,
	},
}

var encodedDiffTests = []test{
	{
		[]string{
			`""`,
			`"abc"`,
			`"aaa\"aaa"`,
			`"\x27"`,
		},
		19,
	},
}

func TestDecodedDiff(t *testing.T) {
	for _, test := range decodedDiffTests {
		result := decodedDifference(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestEncodedDiff(t *testing.T) {
	for _, test := range encodedDiffTests {
		result := encodedDifference(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
