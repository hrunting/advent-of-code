package main

import (
	"testing"
)

type test struct {
	input  string
	result int
}

var floorTests = []test{
	{"(())", 0},
	{"()()", 0},
	{"(((", 3},
	{"(()(()(", 3},
	{"))(((((", 3},
	{"())", -1},
	{"))(", -1},
	{")))", -3},
	{")())())", -3},
}

var basementTests = []test{
	{")", 1},
	{"()())", 5},
	{"(", -1},
}

func TestFinalFloor(t *testing.T) {
	for _, test := range floorTests {
		result := finalFloor(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestFirstBasementPosition(t *testing.T) {
	for _, test := range basementTests {
		result := firstBasementPosition(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
