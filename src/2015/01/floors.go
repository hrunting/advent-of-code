package main

import (
	"fmt"
	"strings"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

func finalFloor(s string) int {
	return strings.Count(s, "(") - strings.Count(s, ")")
}

func firstBasementPosition(s string) int {
	floor := 0
	for i, c := range s {
		switch c {
		case '(':
			floor += 1
		case ')':
			floor -= 1
		}

		if floor < 0 {
			return i + 1
		}
	}

	return -1
}

func main() {
	// lines := aoc.ReadStrings("../../../resources/inputs/2015/01/input.txt")
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Floor: %d\n", finalFloor(lines[0]))
	fmt.Printf("First Basement Position: %d\n", firstBasementPosition(lines[0]))
}
