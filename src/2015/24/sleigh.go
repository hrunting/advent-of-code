package main

import (
	"fmt"
	"sort"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type void struct{}

func groupByWeight(weights []int, weightLeft int) [][]int {
	groups := make([][]int, 0)

	totalWeight := aoc.Sum(weights...)
	candidates := [][]int{}

	for i := len(weights) - 1; i >= 0; i-- {
		w := weights[i]
		cnt := len(candidates)
		for j := 0; j < cnt; j++ {
			sum := candidates[j][0]
			if sum+w <= weightLeft {
				c := make([]int, len(candidates[j])+1)
				copy(c, candidates[j])
				c[len(candidates[j])] = w
				c[0] += w
				candidates = append(candidates, c)
			}
		}

		// we do not need to add our weight if the remaining
		// weight to process can't sum up with it to 512
		totalWeight -= w
		if weightLeft-w <= totalWeight {
			candidates = append(candidates, []int{w, w})
		}
	}

	for _, c := range candidates {
		if c[0] == weightLeft {
			groups = append(groups, c[1:])
		}
	}

	return groups
}

func idealGroup(weights []int, compartments int) [][]int {
	groups := groupByWeight(weights, aoc.Sum(weights...)/compartments)
	sort.Slice(groups, func(i, j int) bool {
		if len(groups[i]) == len(groups[j]) {
			return aoc.Product(groups[i]...) < aoc.Product(groups[j]...)
		}

		return len(groups[i]) < len(groups[j])
	})

	var descend func([][]int, int, map[int]void) [][]int
	descend = func(groups [][]int, level int, seen map[int]void) [][]int {
	COMPARTMENT:
		for _, compartment := range groups {
			levelSeen := make(map[int]void, len(weights))
			for k, v := range seen {
				levelSeen[k] = v
			}

			for _, w := range compartment {
				if _, ok := levelSeen[w]; ok {
					continue COMPARTMENT
				}

				levelSeen[w] = void{}
			}

			if level > 1 {
				results := descend(groups, level-1, levelSeen)
				if len(results) > 0 {
					results = append([][]int{compartment}, results...)
				}
				return results
			} else if level == 1 && len(levelSeen) == len(weights) {
				return [][]int{compartment}
			}
		}

		return [][]int{}
	}

	return descend(groups, compartments, make(map[int]void, len(weights)))
}

func idealQuantumEntanglement(weights []int, compartments int) int {
	group := idealGroup(weights, compartments)
	return aoc.Product(group[0]...)
}

func main() {
	weights := aoc.ReadInts("input.txt")
	fmt.Printf("Ideal Quantum Entanglement (3 compartments): %d\n", idealQuantumEntanglement(weights, 3))
	fmt.Printf("Ideal Quantum Entanglement (4 compartments): %d\n", idealQuantumEntanglement(weights, 4))
}
