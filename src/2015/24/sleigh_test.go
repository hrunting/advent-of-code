package main

import (
	"testing"
)

type test struct {
	input        []int
	compartments int
	result       int
}

var tests = []test{
	{[]int{1, 2, 3, 4, 5, 7, 8, 9, 10, 11}, 3, 99},
	{[]int{1, 2, 3, 4, 5, 7, 8, 9, 10, 11}, 4, 44},
}

func TestIdealQE(t *testing.T) {
	for _, test := range tests {
		result := idealQuantumEntanglement(test.input, test.compartments)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
