package main

import (
	"fmt"
)

func incrementPasswordBytes(pw []byte) {
	for i := len(pw) - 1; i >= 0; i-- {

		// skip invalid characters i, o, and l
		if pw[i] == byte('h') || pw[i] == byte('k') || pw[i] == byte('n') {
			pw[i] += 1
		}

		// if we can increment without wrapping, we are done
		if pw[i] += 1; pw[i] <= byte('z') {
			break
		}

		// wrap, and proceed to next significant digit
		pw[i] = byte('a')
	}
}

func isValidPasswordBytes(pw []byte) bool {
	doubles := 0
	runs := 0

	lastDouble := byte(0)
	lastDoubleIndex := -2

	for i, b := range pw {

		// make sure password does not contain invalid characters
		if b == byte('i') || b == byte('l') || b == byte('o') {
			return false
		}

		// look for non-overlapping, distinct doubles
		if i < len(pw)-1 && i > lastDoubleIndex+1 && b == pw[i+1] && b != lastDouble {
			doubles += 1
			lastDouble = b
			lastDoubleIndex = i
		}

		// look for three character runs
		if i < len(pw)-2 && b == pw[i+1]-1 && b == pw[i+2]-2 {
			runs += 1
		}
	}

	return doubles > 1 && runs > 0
}

func nextPassword(previous string) string {
	pw := []byte(previous)

	for {
		incrementPasswordBytes(pw)
		if isValidPasswordBytes(pw) {
			break
		}
	}

	return string(pw)
}

func main() {
	const INPUT = "vzbxkghb"

	result := nextPassword(INPUT)
	fmt.Printf("Next Password #1: %s\n", result)

	result = nextPassword(result)
	fmt.Printf("Next Password #2: %s\n", result)
}
