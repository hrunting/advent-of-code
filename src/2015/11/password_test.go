package main

import (
	"testing"
)

type passwordTest struct {
	input  string
	result bool
}

type nextPasswordTest struct {
	input  string
	result string
}

var passwordTests = []passwordTest{
	{"hijklnmn", false},
	{"abbceffg", false},
	{"abbcegjk", false},
	{"abcdffaa", true},
	{"ghkaabcc", true},
}

var nextPasswordTests = []nextPasswordTest{
	{"abcdefgh", "abcdffaa"},
	{"ghijklmn", "ghjaabcc"},
}

func TestPassword(t *testing.T) {
	for _, test := range passwordTests {
		result := isValidPasswordBytes([]byte(test.input))
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestNextPassword(t *testing.T) {
	for _, test := range nextPasswordTests {
		result := nextPassword(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
