package main

import (
	"fmt"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

const INPUT int = 33100000

func firstHouse(min int, maxHouses int) (house int) {
	divisors := map[int][]int{1: {}}
	visited := map[int]int{}
	multiplier := 10
	n := 1

	if maxHouses > 0 {
		multiplier += 1
	}

	for {
		visited[n] = 1
		divisors[n] = append(divisors[n], n)
		if aoc.Sum(divisors[n]...)*multiplier >= min {
			house = n
			break
		}

		for _, divisor := range divisors[n] {
			next := n + divisor
			if _, ok := divisors[next]; !ok {
				divisors[next] = []int{}
			}

			if maxHouses < 0 || visited[divisor] < maxHouses {
				divisors[next] = append(divisors[next], divisor)
				visited[divisor] += 1
			}
		}

		n += 1
	}

	return
}

func main() {
	fmt.Printf("Lowest house to get %d presents: %d\n", INPUT, firstHouse(INPUT, -1))
	fmt.Printf("Lowest house to get %d presents (50 houses max): %d\n", INPUT, firstHouse(INPUT, 50))
}
