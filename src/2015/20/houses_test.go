package main

import (
	"testing"
)

type test struct {
	input  int
	result int
}

var houseTests = []test{
	{10, 1},
	{29, 2},
	{60, 4},
	{130, 8},
}

var lazyTests = []test{
	{11, 1},
	{32, 2},
	{1830, 60},
}

func TestFirstHouse(t *testing.T) {
	for _, test := range houseTests {
		result := firstHouse(test.input, -1)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestLazyFirstHouse(t *testing.T) {
	for _, test := range lazyTests {
		result := firstHouse(test.input, 50)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
