package main

import (
	"container/heap"
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type void struct{}
type set map[string]void

type graph map[string]map[string]int

type pathState struct {
	node    string
	visited set
}

func (s set) copy() set {
	c := make(set)
	for k, v := range s {
		c[k] = v
	}
	return c
}

func (g graph) addPath(from, to string, weight int) {
	if _, ok := g[from]; !ok {
		g[from] = make(map[string]int)
	}

	if _, ok := g[to]; !ok {
		g[to] = make(map[string]int)
	}

	g[from][to] = weight
	g[to][from] = weight
}

func (g graph) findShortestDistance() int {
	distance := 0
	q := aoc.NewPriorityQueue()

	for k := range g {
		visited := make(set)
		visited[k] = void{}
		heap.Push(q, &aoc.Item{Value: pathState{k, visited}, Priority: 0})
	}

	for q.Len() > 0 {
		item := heap.Pop(q).(*aoc.Item)
		state := item.Value.(pathState)

		if len(state.visited) == len(g) {
			distance = item.Priority
			break
		}

		for k, weight := range g[state.node] {
			if _, ok := state.visited[k]; !ok {
				visited := state.visited.copy()
				visited[k] = void{}
				heap.Push(q, &aoc.Item{Value: pathState{k, visited}, Priority: item.Priority + weight})
			}
		}
	}

	return distance
}

func (g graph) findLongestSubDistance(node string, visited set) int {
	distance := 0
	visited[node] = void{}

	for k, weight := range g[node] {
		if _, ok := visited[k]; !ok {
			subDistance := g.findLongestSubDistance(k, visited) + weight
			if subDistance > distance {
				distance = subDistance
			}
		}
	}

	delete(visited, node)
	return distance
}

func (g graph) findLongestDistance() int {
	distance := 0

	for node := range g {
		visited := set{node: void{}}
		pathDistance := g.findLongestSubDistance(node, visited)
		if pathDistance > distance {
			distance = pathDistance
		}
	}

	return distance
}

func newGraphFromLines(lines []string) *graph {
	g := &graph{}

	re := regexp.MustCompile(`^(\S+) to (\S+) = (\d+)$`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)
		weight, _ := strconv.Atoi(m[3])
		g.addPath(m[1], m[2], weight)
	}

	return g
}

func shortestDistance(lines []string) int {
	graph := newGraphFromLines(lines)
	return graph.findShortestDistance()
}

func longestDistance(lines []string) int {
	graph := newGraphFromLines(lines)
	return graph.findLongestDistance()
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Shortest Distance: %d\n", shortestDistance(lines))
	fmt.Printf("Longest Distance: %d\n", longestDistance(lines))
}
