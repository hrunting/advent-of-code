package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var shortestTests = []test{
	{
		[]string{
			`London to Dublin = 464`,
			`London to Belfast = 518`,
			`Dublin to Belfast = 141`,
		},
		605,
	},
}

var longestTests = []test{
	{
		[]string{
			`London to Dublin = 464`,
			`London to Belfast = 518`,
			`Dublin to Belfast = 141`,
		},
		982,
	},
}

func TestShortestDistance(t *testing.T) {
	for _, test := range shortestTests {
		result := shortestDistance(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestLongestDistance(t *testing.T) {
	for _, test := range longestTests {
		result := longestDistance(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
