package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
)

type void struct{}

type ingredient struct {
	name       string
	capacity   int
	durability int
	flavor     int
	texture    int
	calories   int
}

type recipe struct {
	ingredients []ingredient
	amounts     []int
}

type candidate struct {
	recipe   recipe
	score    int
	calories int
}

func (r *recipe) calories() int {
	calories := 0
	for i, ig := range r.ingredients {
		calories += ig.calories * r.amounts[i]
	}
	return calories
}

func (r *recipe) score() int {
	capacity := 0
	durability := 0
	flavor := 0
	texture := 0

	for i, ig := range r.ingredients {
		capacity += ig.capacity * r.amounts[i]
		durability += ig.durability * r.amounts[i]
		flavor += ig.flavor * r.amounts[i]
		texture += ig.texture * r.amounts[i]
	}

	if capacity < 0 || durability < 0 || flavor < 0 || texture < 0 {
		return 0
	}

	return capacity * durability * flavor * texture
}

func (r *recipe) amountsAsString() string {
	return fmt.Sprintf("%v", r.amounts)
}

func (r *recipe) clone() recipe {
	c := recipe{append([]ingredient{}, r.ingredients...), append([]int{}, r.amounts...)}
	return c
}

func ingredientsFromLines(lines []string) []ingredient {
	ingredients := make([]ingredient, 0)
	re := regexp.MustCompile(`^(\S+): capacity ([-0-9]+), durability ([-0-9]+), flavor ([-0-9]+), texture ([-0-9]+), calories ([-0-9]+)$`)

	for _, line := range lines {
		m := re.FindStringSubmatch(line)

		capacity, _ := strconv.Atoi(m[2])
		durability, _ := strconv.Atoi(m[3])
		flavor, _ := strconv.Atoi(m[4])
		texture, _ := strconv.Atoi(m[5])
		calories, _ := strconv.Atoi(m[6])

		c := ingredient{m[1], capacity, durability, flavor, texture, calories}
		ingredients = append(ingredients, c)
	}

	return ingredients
}

func startingRecipe(ingredients []ingredient) recipe {
	amounts := make([]int, len(ingredients))
	teaspoons := 0

	for i := range ingredients {
		amounts[i] = 100*(i+1)/len(ingredients) - teaspoons
		teaspoons += amounts[i]
	}

	return recipe{ingredients, amounts}
}

func bestScore(lines []string, calories int) int {
	cr := startingRecipe(ingredientsFromLines(lines))

	maxScore := cr.score()
	if calories > 0 && cr.calories() != calories {
		maxScore = 0
	}

	c := candidate{cr, maxScore, cr.calories()}
	candidates := []candidate{c}
	seen := make(map[string]void)

	for len(candidates) > 0 {
		c, candidates = candidates[0], candidates[1:]

		for i := range c.recipe.amounts {
			for j := range c.recipe.amounts {
				if i != j && c.recipe.amounts[j] > 0 {
					r := c.recipe.clone()
					r.amounts[i] += 1
					r.amounts[j] -= 1

					key := r.amountsAsString()
					if _, ok := seen[key]; ok {
						continue
					}
					seen[key] = void{}

					score := r.score()
					cal := r.calories()
					if (score >= c.score && (calories < 0 || cal <= calories)) || (cal >= calories && cal <= c.calories) {
						if score > maxScore && (calories < 0 || cal == calories) {
							maxScore = score
						}
						candidates = append(candidates, candidate{r, score, cal})
					}
				}
			}
		}
	}

	return maxScore
}

func main() {
	lines := aoc.ReadStrings("input.txt")
	fmt.Printf("Best Score: %d\n", bestScore(lines, -1))
	fmt.Printf("Best Score (500 Calories) %d\n", bestScore(lines, 500))
}
