package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var example = []string{
	`Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8`,
	`Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3`,
}

var scoreTests = []test{
	{example, 62842880},
}

var calorieTests = []test{
	{example, 57600000},
}

func TestBestScore(t *testing.T) {
	for _, test := range scoreTests {
		result := bestScore(test.input, -1)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestCalorieScore(t *testing.T) {
	for _, test := range calorieTests {
		result := bestScore(test.input, 500)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
