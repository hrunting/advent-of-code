package main

import (
	"testing"
)

type test struct {
	input  []string
	result int
}

var onOffTests = []test{
	{[]string{"turn on 0,0 through 999,999"}, 1000000},
	{[]string{"turn on 0,0 through 999,999", "toggle 0,0 through 999,0"}, 999000},
	{[]string{"turn on 0,0 through 999,999", "toggle 0,0 through 999,0", "turn off 499,499 through 500,500"}, 998996},
	{[]string{"turn on 0,0 through 999,999", "toggle 0,0 through 999,0", "turn off 499,499 through 500,500", "toggle 499,499 through 500,500"}, 999000},
}

var brightnessTests = []test{
	{[]string{"turn on 0,0 through 0,0"}, 1},
	{[]string{"toggle 0,0 through 999,999"}, 2000000},
	{[]string{"turn on 0,0 through 999,999", "turn off 0,0 through 999,0", "toggle 0,1 through 999,1"}, 1001000},
}

func TestOnOff(t *testing.T) {
	for _, test := range onOffTests {
		result := runLights(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}

func TestBrightness(t *testing.T) {
	for _, test := range brightnessTests {
		result := runBrightLights(test.input)
		if result != test.result {
			t.Error(
				"For", test.input,
				"expected", test.result,
				"got", result,
			)
		}
	}
}
