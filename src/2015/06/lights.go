package main

import (
	"fmt"
	"regexp"
	"strconv"

	"bitbucket.org/hrunting/advent-of-code/lib/golang/aoc"
	"github.com/bits-and-blooms/bitset"
)

type lightOp struct {
	opFunc   func(*bitset.BitSet, *bitset.BitSet) *bitset.BitSet
	lightSet *bitset.BitSet
}

func (o *lightOp) apply(lights *bitset.BitSet) *bitset.BitSet {
	return o.opFunc(lights, o.lightSet)
}

type brightOp struct {
	amount         int
	x1, y1, x2, y2 int
}

func (o *brightOp) apply(lights []int) []int {
	return updateBrightness(lights, o.amount, o.x1, o.y1, o.x2, o.y2)
}

func turnOn(lights *bitset.BitSet, opSet *bitset.BitSet) *bitset.BitSet {
	return lights.Union(opSet)
}

func turnOff(lights *bitset.BitSet, opSet *bitset.BitSet) *bitset.BitSet {
	return lights.Intersection(opSet.Complement())
}

func toggle(lights *bitset.BitSet, opSet *bitset.BitSet) *bitset.BitSet {
	return lights.Intersection(opSet.Complement()).Union(lights.Complement().Intersection(opSet))
}

func parseOps(lines []string) []lightOp {
	ops := make([]lightOp, 0)
	pattern := regexp.MustCompile(`^(.*) (\d+),(\d+) through (\d+),(\d+)`)

	for _, line := range lines {
		res := pattern.FindStringSubmatch(line)
		var opFunc func(*bitset.BitSet, *bitset.BitSet) *bitset.BitSet

		switch res[1] {
		case "turn on":
			opFunc = turnOn
		case "turn off":
			opFunc = turnOff
		case "toggle":
			opFunc = toggle
		}

		op := lightOp{opFunc, bitset.New(1000000)}

		min_x, _ := strconv.Atoi(res[2])
		min_y, _ := strconv.Atoi(res[3])
		max_x, _ := strconv.Atoi(res[4])
		max_y, _ := strconv.Atoi(res[5])

		for y := min_y; y <= max_y; y++ {
			for x := min_x; x <= max_x; x++ {
				pos := y*1000 + x
				op.lightSet = op.lightSet.Set(uint(pos))
			}
		}

		ops = append(ops, op)
	}

	return ops
}

func updateBrightness(lights []int, amount, min_x, min_y, max_x, max_y int) []int {
	updatedLights := make([]int, len(lights))
	copy(updatedLights, lights)

	for y := min_y; y <= max_y; y++ {
		for x := min_x; x <= max_x; x++ {
			if amount > 0 || updatedLights[y*1000+x] > 0 {
				updatedLights[y*1000+x] += amount
			}
		}
	}

	return updatedLights
}

func parseBrightnessOps(lines []string) []brightOp {
	ops := make([]brightOp, 0)
	pattern := regexp.MustCompile(`^(.*) (\d+),(\d+) through (\d+),(\d+)`)

	for _, line := range lines {
		res := pattern.FindStringSubmatch(line)
		brightness := 0

		switch res[1] {
		case "turn on":
			brightness = 1
		case "turn off":
			brightness = -1
		case "toggle":
			brightness = 2
		}

		min_x, _ := strconv.Atoi(res[2])
		min_y, _ := strconv.Atoi(res[3])
		max_x, _ := strconv.Atoi(res[4])
		max_y, _ := strconv.Atoi(res[5])

		ops = append(ops, brightOp{brightness, min_x, min_y, max_x, max_y})
	}

	return ops
}

func runLights(lines []string) int {
	ops := parseOps(lines)
	lights := bitset.New(1000000)
	for _, op := range ops {
		lights = op.apply(lights)
	}

	return int(lights.Count())
}

func runBrightLights(lines []string) int {
	brightOps := parseBrightnessOps(lines)
	brightLights := make([]int, 1000000)
	for _, op := range brightOps {
		brightLights = op.apply(brightLights)
	}

	return aoc.Sum(brightLights...)
}

func main() {
	lines := aoc.ReadStrings("input.txt")

	// I thought I would be creative and prepare for "run this 1000 times" ...
	fmt.Printf("Active Lights: %d\n", runLights(lines))

	// ... but it turns out, the second problem is a different application altogether
	fmt.Printf("Total Brightness: %d\n", runBrightLights(lines))
}
