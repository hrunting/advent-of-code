#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def plot_course(course: list[tuple[str, int]]) -> tuple[int, int]:
  horizontal, depth = 0, 0
  for dir, units in course:
    if dir == "forward":
      horizontal += units
    elif dir == "down":
      depth += units
    elif dir == "up":
      depth -= units
  return horizontal, depth

def plot_aim_course(course: list[tuple[str, int]]) -> tuple[int, int]:
  horizontal, depth, aim = 0, 0, 0
  for dir, units in course:
    if dir == "forward":
      horizontal += units
      depth += units * aim
    elif dir == "down":
      aim += units
    elif dir == "up":
      aim -= units
  return horizontal, depth

def run() -> None:
  course = []

  input_file = aoc.inputfile('input.txt')
  for line in open(input_file).readlines():
    dir, x = line.strip().split(" ")
    course.append((dir, int(x)))

  position, depth = plot_course(course)
  print(f'Position/depth product: {position * depth}')

  position, depth = plot_aim_course(course)
  print(f'Aimed position/depth product: {position * depth}')

if __name__ == '__main__':
  run()
  sys.exit(0)
