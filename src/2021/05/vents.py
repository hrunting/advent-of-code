#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_vent_lines() -> list[list[list[int]]]:
  vents = []

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    for line in input:
      line = line.strip()
      vents.append([[int(n) for n in x.split(",")] for x in line.split(" -> ")])

  return vents

def count_overlapping_points(vents: list[list[list[int]]], diagonals: bool = False) -> int:
  grid = defaultdict(int)

  for i, [p1, p2] in enumerate(vents):
    x_step = (p2[0] - p1[0]) // max(1, abs(p2[0] - p1[0]))
    y_step = (p2[1] - p1[1]) // max(1, abs(p2[1] - p1[1]))

    if x_step == 0:
      for y in range(p1[1], p2[1] + y_step, y_step):
        grid[(p1[0], y)] += 1

    elif y_step == 0:
      for x in range(p1[0], p2[0] + x_step, x_step):
        grid[(x, p1[1])] += 1

    elif diagonals:
      for x, y in zip(range(p1[0], p2[0] + x_step, x_step), range(p1[1], p2[1] + y_step, y_step)):
        grid[(x, y)] += 1

  return sum(1 if x > 1 else 0 for x in grid.values())

def run() -> None:
  vents = read_vent_lines()

  count = count_overlapping_points(vents)
  print(f'Overlapping points: {count}')

  count = count_overlapping_points(vents, True)
  print(f'Overlapping points (w/ diagonals): {count}')

if __name__ == '__main__':
  run()
  sys.exit(0)
