#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def move_all(floor: list[list[str]]) -> tuple[list[list[str]], int]:
  steps = 0
  moved = True

  while moved:
    moved = False

    next_floor = [row.copy() for row in floor]
    for y, row in enumerate(floor):
      for x, c in enumerate(row):
        if c == "." and row[x - 1] == ">":
          next_floor[y][x - 1], next_floor[y][x] = ".", ">"
          moved = True
    floor = next_floor

    next_floor = [row.copy() for row in floor]
    for y, row in enumerate(floor):
      for x, c in enumerate(row):
        if c == "." and floor[y - 1][x] == "v":
          next_floor[y - 1][x], next_floor[y][x] = ".", "v"
          moved = True
    floor = next_floor

    steps += 1

  return floor, steps

def read_input() -> list[list[str]]:
  return [[c for c in line.rstrip()] for line in open(aoc.inputfile('input.txt')).readlines()]

def run() -> None:
  floor = read_input()
  floor, steps = move_all(floor)
  print(f'First step with no moves: {steps}')

if __name__ == '__main__':
  run()
  sys.exit(0)
