#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_increases(heights: list[int], window: int) -> int:
  count = 0
  prev_sum = sum(heights[0:window])

  for i in range(1, len(heights) - window + 1):
    next_sum = prev_sum - heights[i - 1] + heights[i + window - 1]
    if next_sum > prev_sum:
      count += 1

    prev_sum = next_sum

  return count

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  heights = [int(x) for x in open(input_file).readlines()]
  print(f'Measurement increases {count_increases(heights, 1)}')
  print(f'Sliding window increases {count_increases(heights, 3)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
