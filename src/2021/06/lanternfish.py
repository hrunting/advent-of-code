#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_fish(fish: list[int], days: int) -> int:
  fish_pool = defaultdict(lambda: [0, 0])

  for day in fish:
    fish_pool[day][0] += 1

  while (day := min(fish_pool.keys())) < days:
    fish_pool[day + 9][0] += sum(fish_pool[day])
    fish_pool[day + 7][1] += sum(fish_pool[day])
    del(fish_pool[day])

  return sum(x[0] + x[1] for x in fish_pool.values())

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    fish = [int(x) for x in input.read().strip().split(",")]

  count = count_fish(fish, 80)
  print(f'Lanternfish after day 80: {count}')

  count = count_fish(fish, 256)
  print(f'Lanternfish after day 256: {count}')

if __name__ == '__main__':
  run()
  sys.exit(0)
