#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def play_real_game(pos1: int, pos2: int) -> tuple[int, int]:
  outcomes = defaultdict(int)
  for i in range(1, 4):
   for j in range(1, 4):
     for k in range(1, 4):
       outcomes[i + j + k] += 1

  positions = {i: defaultdict(int) for i in range(1, 11)}
  for pos in positions.keys():
    for score in outcomes:
      positions[pos][(pos - 1 + score) % 10 + 1] += outcomes[score]

  p1_wins, p2_wins = 0, 0
  p1_scores = defaultdict(lambda: defaultdict(int), {0: {pos1: 1}})
  p2_scores = defaultdict(lambda: defaultdict(int), {0: {pos2: 1}})

  while p1_scores and p2_scores:
    next_scores = defaultdict(lambda: defaultdict(int))

    for score, pos_list in p1_scores.items():
      for pos, universes in pos_list.items():
        for next_pos, universe_count in positions[pos].items():
          next_score = score + next_pos
          next_outcomes = universes * universe_count

          if next_score >= 21:

            # sum all the universes where the other player loses
            p1_wins += next_outcomes * sum(x for pl in p2_scores.values() for x in pl.values())

          else:

            # otherwise, record the number of universes active at the next played position
            next_scores[next_score][next_pos] += next_outcomes

    p1_scores = next_scores

    # swap the players and play again
    p1_scores, p2_scores = p2_scores, p1_scores
    p1_wins, p2_wins = p2_wins, p1_wins

  return p1_wins, p2_wins

def play_test_game(pos1: int, pos2: int) -> tuple[int, int, int]:
  players = [[pos1, 0], [pos2, 0]]
  player = 0
  rolls = 0
  dice_val = 0

  while players[0][1] < 1000 and players[1][1] < 1000:
    rolls += 3
    dice_val += 3
    players[player][0] = (players[player][0] - 1 + (dice_val * 3 - 3)) % 10 + 1
    players[player][1] += players[player][0]
    player = 1 - player

  return rolls, max(p[1] for p in players), min(p[1] for p in players)

def read_game() -> tuple[int, int]:
  with open(aoc.inputfile('input.txt')) as f:
    player1 = f.readline()
    player2 = f.readline()
    return (int(player1.strip()[28:]), int(player2.strip()[28:]))

def run() -> None:
  pos1, pos2 = read_game()

  rolls, win_score, lose_score = play_test_game(pos1, pos2)
  print(f'Losing score multiple: {rolls * lose_score} ({rolls} * {lose_score})')

  p1, p2  = play_real_game(pos1, pos2)
  print(f'Universes won by best player: {max(p1, p2)}')
  print(f'Universes won by worst player: {min(p1, p2)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
