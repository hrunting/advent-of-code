#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def low_points(heightmap: list[list[int]]) -> list[tuple[int, int]]:
  min_locations = []
  for y, row in enumerate(heightmap):
    for x, cell in enumerate(row):
      adjacents = []
      if x > 0:
        adjacents.append(row[x - 1])
      if x < len(row) - 1:
        adjacents.append(row[x + 1])
      if y > 0:
        adjacents.append(heightmap[y - 1][x])
      if y < len(heightmap) - 1:
        adjacents.append(heightmap[y + 1][x])

      if cell < min(adjacents):
        min_locations.append((x, y))

  return min_locations

def risk_level(heightmap: list[list[int]]) -> int:
  return sum(heightmap[y][x] + 1 for x, y in low_points(heightmap))

def basin_area(heightmap: list[list[int]])-> int:
  basin_points = low_points(heightmap)
  basins = []

  for x, y in basin_points:
    seen = set()
    stack = [(x, y)]

    while len(stack):
      x, y = stack.pop()
      if (x, y) in seen or heightmap[y][x] == 9:
        continue

      seen.add((x, y))

      if x > 0:
        stack.append((x - 1, y))
      if x < len(heightmap[0]) - 1:
        stack.append((x + 1, y))
      if y > 0:
        stack.append((x, y - 1))
      if y < len(heightmap) - 1:
        stack.append((x, y + 1))

    basins.append(len(seen))

  basins.sort(reverse=True)
  return basins[0] * basins[1] * basins[2]

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  heightmap = [[int(x) for x in line.strip()] for line in open(input_file).readlines()]

  print(f'Risk Level: {risk_level(heightmap)}')
  print(f'Basin area product: {basin_area(heightmap)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
