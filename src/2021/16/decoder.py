#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Packet:
  @classmethod
  def read_packet(cls: type[Self], bits: str) -> tuple[Self, int]:
    ptr = 0

    version = int(bits[ptr:ptr + 3], 2)
    type = int(bits[ptr + 3:ptr + 6], 2)

    ptr += 6

    if type == 4:
      value = ""
      while ptr < len(bits):
        more_digits = int(bits[ptr], 2)
        value += bits[ptr + 1:ptr + 5]
        ptr += 5

        if more_digits == 0:
          break

      return cls(version, type, int(value, 2)), ptr

    packets = []

    length_type = int(bits[ptr], 2)
    ptr += 1

    if length_type == 0:
      length = int(bits[ptr:ptr + 15], 2)
      ptr += 15

      while length > 0:
        packet, bits_read = Packet.read_packet(bits[ptr:ptr + length])
        ptr += bits_read
        length -= bits_read
        packets.append(packet)

    else:
      count = int(bits[ptr:ptr + 11], 2)
      ptr += 11

      while count > 0:
        packet, bits_read = Packet.read_packet(bits[ptr:])
        ptr += bits_read
        count -= 1
        packets.append(packet)

    return Packet(version, type, 0, packets), ptr

  def __init__(self: Self, version: int, type: int, value: int, packets: list[Self] = []) -> None:
    self.version = version
    self.type = type
    self.value = value
    self.packets = packets

  def sum_version(self: Self) -> int:
    version = 0
    packets = [self]
    while packets:
      packet = packets.pop()
      version += packet.version
      packets.extend(packet.packets)
    return version

  def result(self: Self) -> int:
    if self.type == 4:
      return self.value

    results = [p.result() for p in self.packets]

    if self.type == 0:
      return sum(results)
    elif self.type == 1:
      return math.prod(results)
    elif self.type == 2:
      return min(results)
    elif self.type == 3:
      return max(results)
    elif self.type == 5:
      return int(results[0] > results[1])
    elif self.type == 6:
      return int(results[0] < results[1])
    elif self.type == 7:
      return int(results[0] == results[1])
    else:
      return 0

def run() -> None:
  hex = open(aoc.inputfile('input.txt')).read().strip()

  packet, _ = Packet.read_packet(bin(int(hex, 16))[2:].zfill(len(hex) * 4))
  print(f'Packet version sum: {packet.sum_version()}')
  print(f'Transmission value: {packet.result()}')

if __name__ == '__main__':
  run()
  sys.exit(0)
