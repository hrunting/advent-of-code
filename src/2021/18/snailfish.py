#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Number:
  def __init__(self: Self, n: int) -> None:
    self.val = n
    self.prev: Optional[Self] = None
    self.next: Optional[Self] = None

  def __str__(self: Self) -> str:
    return str(self.val)

  def magnitude(self: Self) -> int:
    return self.val

  def increase_depth(self: Self) -> None:
    pass

  def first(self: Self) -> Self:
    return self

  def last(self: Self) -> Self:
    return self

  def split(self: Self) -> tuple[Self, Self]:
    left = Number(self.val // 2)
    right = Number(int(self.val / 2 + 0.5))

    left.next, right.prev = right, left

    if self.prev:
      left.prev = self.prev
      left.prev.next = left

    if self.next:
      right.next = self.next
      right.next.prev = right

    return left, right

class Pair:
  @classmethod
  def add(cls: type[Self], pair1: Self, pair2: Self) -> Self:
    pair1.reduce()
    pair2.reduce()

    last = pair1.last()
    first = pair2.first()
    last.next = first
    first.prev = last

    pair = Pair(pair1, pair2)
    pair.reduce()

    return pair

  def __init__(self: Self, left: Number | Self, right: Number | Self, depth: int = 0) -> None:
    self.depth = depth
    self.left = left
    self.right = right

    self.left.increase_depth()
    self.right.increase_depth()

  def __str__(self: Self) -> str:
    return "[" + str(self.left) + "," + str(self.right) + "]"

  def __explode_pair(self: Self, pair: Self) -> Number:
    num = Number(0)
         
    if isinstance(pair.left, Number) and pair.left.prev:
      pair.left.prev.val += pair.left.val
      num.prev = pair.left.prev
      num.prev.next = num

    if isinstance(pair.right, Number) and pair.right.next:
      pair.right.next.val += pair.right.val
      num.next = pair.right.next
      num.next.prev = num

    return num

  def __explode_step(self: Self) -> bool:
    if isinstance(self.left, Pair):
      if self.left.depth >= 4:
        self.left = self.__explode_pair(self.left)
        return True
      elif self.left.__explode_step():
        return True

    if isinstance(self.right, Pair):
      if self.right.depth >= 4:
        self.right = self.__explode_pair(self.right)
        return True
      elif self.right.__explode_step():
        return True

    return False

  def __split_number(self: Self, num: Number) -> Self:
    left, right = num.split()
    return Pair(left, right, self.depth + 1)

  def __split_step(self: Self) -> bool:
    if isinstance(self.left, Pair):
      if self.left.__split_step():
        return True
    else:
      if self.left.val >= 10:
        self.left = self.__split_number(self.left)
        return True

    if isinstance(self.right, Pair):
      if self.right.__split_step():
        return True
    else:
      if self.right.val >= 10:
        self.right = self.__split_number(self.right)
        return True

    return False

  def reduce(self: Self) -> None:
    while self.__explode_step() or self.__split_step():
      pass

  def increase_depth(self: Self) -> None:
    self.depth += 1
    self.left.increase_depth()
    self.right.increase_depth()

  def first(self: Self) -> Number:
    return self.left.first()

  def last(self: Self) -> Number:
    return self.right.last()

  def magnitude(self) -> int:
    return self.left.magnitude() * 3 + 2 * self.right.magnitude()

def read_number(number: str) -> Pair:
  nodes: list[Number | Pair] = []
  head = prev = Number(0)
  num: Optional[Number] = None

  for c in number[1:-1]:
    if c in {',', ']'}:
      if num is not None:
        nodes.append(num)
        num = None

      if c == ']':
        right, left = nodes.pop(), nodes.pop()
        nodes.append(Pair(left, right))

    elif c != '[':
      if num is None:
        num = Number(0)
        prev.next, num.prev, prev = num, prev, num
      num.val = num.val * 10 + int(c)

  else:
    if num is not None:
      nodes.append(num)

  if head.next:
    head.next.prev = None

  right, left = nodes.pop(), nodes.pop()
  return Pair(left, right)

def run() -> None:
  lines = [line.strip() for line in open(aoc.inputfile('input.txt')).readlines()]

  summed = read_number(lines[0])
  for line in lines[1:]:
    summed = Pair.add(summed, read_number(line))
  print(f'Magnitude of total sum: {summed.magnitude()}')

  max_magnitude = 0
  for idx1 in range(len(lines)):
    for idx2 in range(len(lines)):
      if idx1 == idx2:
        continue

      summed = Pair.add(read_number(lines[idx1]), read_number(lines[idx2]))
      max_magnitude = max(max_magnitude, summed.magnitude())
  print(f'Max magnitude of any two sums: {max_magnitude}')

if __name__ == '__main__':
  run()
  sys.exit(0)
