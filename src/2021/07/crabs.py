#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def optimized_position(positions: list[int]) -> tuple[int, int]:
  median_start = len(positions) // 2
  median_end = median_start + (len(positions) + 1) % 2

  sorted_positions = sorted(positions)
  best_position = sorted_positions[median_start]
  best_fuel = sum(abs(x - best_position) for x in positions)

  if median_end != median_start:
    position = sorted_positions[median_end]
    fuel = sum(abs(x - position) for x in positions)
    if fuel < best_fuel:
      best_fuel = fuel
      best_position = position

  return best_position, best_fuel


def optimized_crab_position(positions: list[int]) -> tuple[int, int]:
  position = sum(positions) // len(positions)
  fuel = sum(abs(x - position) * (abs(x - position) + 1) // 2 for x in positions)

  if position < sum(positions) / len(positions):
    next_fuel = sum(abs(x - (position + 1)) * (abs(x - (position + 1)) + 1) // 2 for x in positions)
    if next_fuel < fuel:
      position += 1
      fuel = next_fuel

  return position, fuel

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    positions = [int(x) for x in input.read().strip().split(",")]

  position, fuel = optimized_position(positions)
  print(f'Minimized fuel: {fuel} @ position {position}')

  position, fuel = optimized_crab_position(positions)
  print(f'Minimized crab-engineering fuel: {fuel} @ position {position}')

if __name__ == '__main__':
  run()
  sys.exit(0)
