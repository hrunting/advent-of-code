#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def fold_page(page: set[tuple[int, int]], horizontal: bool, index: int) -> set[tuple[int, int]]:
  folded_page = set()
  for x, y in page:
    if horizontal:
      if x < index:
        folded_page.add((x, y))
      elif x > index:
        folded_page.add((index * 2 - x, y))

    else:
      if y < index:
        folded_page.add((x, y))
      elif y > index:
        folded_page.add((x, index * 2 - y))

  return folded_page

def read_page() -> tuple[set[tuple[int, int]], list[tuple[bool, int]]]:
  page = set()
  folds = []

  with open(aoc.inputfile('input.txt')) as input:
    for line in input.readlines():
      line = line.strip()

      if "," in line:
        x, y = (int(n) for n in line.split(","))
        page.add((x, y))

      elif line.startswith('fold along'):
        dir = line[11] == 'x'
        index = int(line[13:])
        folds.append((dir, index))

  return page, folds

def run() -> None:
  page, folds = read_page()

  horizontal, index = folds[0]
  folded_page = fold_page(page, horizontal, index)
  print(f'Visible dots after 1 fold: {len(folded_page)}')

  for horizontal, index in folds[1:]:
    folded_page = fold_page(folded_page, horizontal, index)

  max_x = max(x for x, _ in folded_page)
  max_y = max(y for _, y in folded_page)

  for y in range(max_y + 1):
    print(''.join('#' if (x, y) in folded_page else ' ' for x in range(max_x + 1)))

if __name__ == '__main__':
  run()
  sys.exit(0)
