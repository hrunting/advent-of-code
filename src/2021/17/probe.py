#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def find_min_x_velocity(x1: int, x2: int) -> int:
  velocity = distance = 0
  while not x1 <= distance <= x2:
    velocity += 1
    distance += velocity

  return velocity

def max_height_velocity(target: tuple[int, int, int, int]) -> tuple[tuple[int, int], int]:
  x1, y1, x2, y2 = target
  min_x_velocity = find_min_x_velocity(x1, x2)
  max_y_velocity = abs(y1) - 1
  return ((min_x_velocity, max_y_velocity), max_y_velocity * (abs(y1)) // 2)

def initial_velocities(target: tuple[int, int, int, int]) -> set[tuple[int, int]]:
  x1, y1, x2, y2 = target

  velocities = set()

  min_x_velocity = find_min_x_velocity(x1, x2)
  max_x_velocity = x2

  min_y_velocity = y1
  max_y_velocity = abs(y1) - 1

  for x in range(min_x_velocity, max_x_velocity + 1):
    for y in range(min_y_velocity, max_y_velocity + 1):
      x_velocity, y_velocity = x, y
      x_pos, y_pos = 0, 0
      while x_pos <= x2 and y_pos >= y1:
        if x_pos >= x1 and y_pos <= y2:
          velocities.add((x, y))
          break

        x_pos += x_velocity
        y_pos += y_velocity

        x_velocity -= min(1, x_velocity)
        y_velocity -= 1

  return velocities

def parse_spec(spec: str) -> tuple[int, int, int, int]:
  if m := re.search(r"x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)", spec):
    x1, x2, y1, y2 = m.groups()
    return int(x1), int(y1), int(x2), int(y2)
  return 0, 0, 0, 0

def run() -> None:
  spec = open(aoc.inputfile('input.txt')).read().strip()
  target = parse_spec(spec)

  velocity, height = max_height_velocity(target)
  print(f'Maximum height: {height} @ velocity {velocity}')

  velocities = initial_velocities(target)
  print(f'Distinct initial velocities: {len(velocities)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
