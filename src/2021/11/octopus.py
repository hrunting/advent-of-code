#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

GRID_SIZE = 10

def step_flashes(grid: dict[tuple[int, int], int]) -> int:
  flashes = 0
  octopuses = grid.keys()

  while octopuses:
    adjacent = []

    for x, y in octopuses:
      if not 0 <= x < GRID_SIZE or not 0 <= y < GRID_SIZE:
        continue

      grid[(x, y)] += 1
      if grid[(x, y)] == 10:
        adjacent.extend([
          (x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
          (x, y - 1), (x, y + 1),
          (x + 1, y - 1), (x + 1, y), (x + 1, y + 1)
        ])

    octopuses = adjacent

  for x, y in grid.keys():
    if grid[(x, y)] >= 10:
      flashes += 1
      grid[(x, y)] = 0

  return flashes

def count_flashes(grid: dict[tuple[int, int], int], steps: int) -> int:
  grid = grid.copy()

  flashes = 0
  for _ in range(steps):
    flashes += step_flashes(grid)

  return flashes

def synchronized_flash(grid: dict[tuple[int, int], int]) -> int:
  grid = grid.copy()

  step = 1
  while True:
    if step_flashes(grid) == GRID_SIZE ** 2:
      return step
    step += 1

def read_grid() -> dict[tuple[int, int], int]:
  input_file = aoc.inputfile('input.txt')
  grid = {}

  for y, line in enumerate(open(input_file).readlines()):
    for x, val in enumerate(line.strip()):
      grid[(x, y)] = int(val)

  return grid

def run() -> None:
  grid = read_grid()
  print(f'Flashes after 100 steps: {count_flashes(grid, 100)}')
  print(f'First synchronized flash: {synchronized_flash(grid)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
