#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run_insertions(template: str, rules: dict[str, str], iterations: int) -> tuple[int, int]:
  digrams = defaultdict(int)
  letters = defaultdict(int)

  for i in range(len(template) - 1):
    digrams[template[i:i + 2]] += 1
    letters[template[i]] += 1

  letters[template[-1]] += 1

  for _ in range(iterations):
    new_digrams = defaultdict(int)
    for digram, count in digrams.items():
      letters[rules[digram]] += count
      new_digrams[digram[0] + rules[digram]] += count
      new_digrams[rules[digram] + digram[1]] += count
    digrams = new_digrams

  return min(letters.values()), max(letters.values())

def read_template() -> tuple[str, dict[str, str]]:
  rules = {}
  template = ""

  with open(aoc.inputfile('input.txt')) as input:
    template = input.readline().strip()
    input.readline()
    while line := input.readline().strip():
      pair, element = line.split(" -> ")
      rules[pair] = element

  return template, rules

def run() -> None:
  template, rules = read_template()

  least, most = run_insertions(template, rules, 10)
  print(f'Most/least common element difference (10 steps): {most - least}')

  least, most = run_insertions(template, rules, 40)
  print(f'Most/least common element difference (40 steps): {most - least}')

if __name__ == '__main__':
  run()
  sys.exit(0)
