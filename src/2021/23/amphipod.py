#!/usr/bin/env python3

import pathlib
import sys

from functools import total_ordering
from queue import PriorityQueue
from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Amphipod = tuple[str, int, int]
Grid = dict[tuple[int, int], str]

@total_ordering
class Burrow:
  HALL_SLOTS = [0, 1, 3, 5, 7, 9, 10]
  ROOM_SLOTS = [2, 4, 6, 8]

  def __init__(self: Self, grid: Grid, depth: int = 0, energy: int = 0) -> None:
    self.grid = grid
    self.energy = energy
    self.depth = depth if depth else max(y for _, y in self.grid)

  def __str__(self: Self) -> str:
    return "\n".join((
      "#" * 13,
      "#" + "".join(self.grid[x, 0] for x in range(11)) + '#',
      "###" + "#".join(self.grid[x, 1] for x in self.ROOM_SLOTS) + "###",
      "\n".join("  #" + "#".join(
        self.grid[x, y]
          for x in self.ROOM_SLOTS) + "#  "
          for y in range(2, self.depth + 1)
      ),
      "  " + "#" * 9 + "  "
    ))

  def __eq__(self: Self, other: Self) -> bool:
    return self.energy == other.energy

  def __lt__(self: Self, other: Self) -> bool:
    return self.energy < other.energy

  def copy(self: Self) -> Self:
    return type(self)(self.grid.copy(), self.depth, self.energy)

  def unfold(self: Self) -> Self:
    grid = self.grid.copy()
    for x, y in self.grid.keys():
      if y >= 2:
        grid[x, y + 2] = self.grid[x, y]

    grid[2, 2] = 'D'
    grid[4, 2] = 'C'
    grid[6, 2] = 'B'
    grid[8, 2] = 'A'
    grid[2, 3] = 'D'
    grid[4, 3] = 'B'
    grid[6, 3] = 'A'
    grid[8, 3] = 'C'

    return Burrow(grid, self.depth + 2, self.energy)

  def movable(self: Self) -> list[Amphipod]:
    pods = []

    for hx in self.HALL_SLOTS:
      if hx == 0 and self.grid[hx + 1, 0] != ".":
        continue
      if hx == 10 and self.grid[hx - 1, 0] != ".":
        continue
      if "A" <= self.grid[hx, 0] <= "D":
        pods.append((self.grid[hx, 0], hx, 0))

    for rx in self.ROOM_SLOTS:
      for ry in range(1, self.depth + 1):
        if "A" <= self.grid[rx, ry] <= "D":
          pods.append((self.grid[rx, ry], rx, ry))
          break

    return pods

  def resolved(self: Self) -> bool:
    for x in self.ROOM_SLOTS:
      for y in range(1, self.depth + 1):
        if ord(self.grid[x, y]) != ord('A') + ((x - 2) // 2):
          return False
    return True

  def state(self: Self) -> tuple[str, ...]:
    return tuple(self.grid.values())

  def is_home(self: Self, pod: Amphipod) -> bool:
    t, x, y = pod

    if 2 + (ord(t) - ord('A')) * 2 != x:
      return False

    for by in range(self.depth, y, -1):
      if self.grid[x, by] != t:
        return False

    return True

  def moves(self: Self, pod: Amphipod) -> list[Self]:
    t, x, y = pod

    dests = set()

    # in-room amphipod can move to hallway
    if y != 0:
      for hx in self.HALL_SLOTS:
        if self.grid[hx, 0] != ".":
          if hx < x:
            dests.clear()
          else:
            break

        else:
          dests.add((hx, 0))

    # hallway amphipod can move to room
    else:
      room_x = {'A': 2, 'B': 4, 'C': 6, 'D': 8}[t]
      dir = 1 if x < room_x else -1
      for hx in range(x + dir, room_x, dir):
        if self.grid[hx, 0] != ".":
          break

      else:
        for room_y in range(self.depth, 0, -1):
          if self.grid[room_x, room_y] == t:
            continue
          if self.grid[room_x, room_y] == ".":
            dests.add((room_x, room_y))
          break

    moves = []
    for dx, dy in dests:
      burrow = self.copy()
      burrow.energy += (abs(x - dx) + y + dy) * (10 ** (ord(t) - ord('A')))
      burrow.grid[dx, dy], burrow.grid[x, y] = self.grid[x, y], self.grid[dx, dy]
      moves.append(burrow)

    return moves

  def resolve(self: Self) -> int:
    seen = set()
    burrow = self

    q = PriorityQueue()
    q.put(self)

    while not q.empty():
      burrow = q.get()

      if burrow.resolved():
        break

      state = burrow.state()
      if state in seen:
        continue
      seen.add(state)

      for pod in burrow.movable():
        if burrow.is_home(pod):
          continue

        burrows = burrow.moves(pod)
        for new_burrow in burrows:
          q.put(new_burrow)

    return burrow.energy

def read_input() -> Burrow:
  grid = {}
  lines = [line.rstrip() for line in open(aoc.inputfile('input.txt')).readlines()]
  for y, line in enumerate(lines):
    for x, c in enumerate(line):
      if c in {' ', '#'}:
        continue
      grid[x - 1, y - 1] = c

  return Burrow(grid)

def run() -> None:
  burrow = read_input()
  energy = burrow.resolve()
  print(f'Minimum energy required: {energy}')

  burrow = burrow.unfold()
  energy = burrow.resolve()
  print(f'Minimum energy required (unfolded): {energy}')

if __name__ == '__main__':
  run()
  sys.exit(0)
