#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict, deque
from typing import Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Cave= str
Path = list[Cave]
Visited = set[Cave]

def find_paths(caves: dict[Cave, set[Cave]], twice: bool = False) -> list[list[str]]:
  paths = []

  candidates: deque[tuple[Path, Visited, Optional[Cave]]] = deque([(["start"], {"start"}, None)])

  while candidates:
    path, visited, single = candidates.popleft()
    for cave in caves[path[-1]]:
      if cave == "end":
        if not single or single in visited:
          paths.append(path + [cave])
      elif cave not in visited:
        if cave.isupper():
          candidates.append((path + [cave], visited, single))
        else:
          candidates.append((path + [cave], visited | {cave}, single))
          if twice and not single:
            candidates.append((path + [cave], visited, cave))

  return paths

def read_caves() -> dict[Cave, set[Cave]]:
  caves = defaultdict(set)

  with open(aoc.inputfile('input.txt')) as input:
    for line in input.readlines():
      cave1, cave2 = line.strip().split("-")
      caves[cave1].add(cave2)
      caves[cave2].add(cave1)

  return caves

def run() -> None:
  caves = read_caves()

  paths = find_paths(caves)
  print(f'Number of paths: {len(paths)}')

  paths = find_paths(caves, True)
  print(f'Number of caves (twice-visited): {len(paths)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
