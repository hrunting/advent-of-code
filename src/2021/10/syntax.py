#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

PARTNERS = {'(': ')', '[': ']', '{': '}', '<': '>'}

def find_corrupted_character(line: str) -> str:
  stack = []

  for char in line:
    if char in PARTNERS:
      stack.append(char)
    else:
      open_char = stack.pop()
      if PARTNERS[open_char] != char:
        return char

  return ""

def find_incomplete_sequence(line: str) -> str:
  stack = []

  for char in line:
    if char in PARTNERS:
      stack.append(char)
    else:
      open_char = stack.pop()
      if PARTNERS[open_char] != char:
        return ""

  return ''.join(PARTNERS[c] for c in reversed(stack))

def score_corrupted(lines: list[str]) -> int:
  scoring = {')': 3, ']': 57, '}': 1197, '>': 25137}
  score = 0

  for line in lines:
    if corrupt_char := find_corrupted_character(line):
      score += scoring.get(corrupt_char, 0)

  return score

def score_sequence(sequence: str) -> int:
  scoring = {')': 1, ']': 2, '}': 3, '>': 4}
  score = 0

  for char in sequence:
    score = score * 5 + scoring[char]

  return score

def score_incomplete(lines: list[str]) -> int:
  scores = []

  for line in lines:
    if sequence := find_incomplete_sequence(line):
      scores.append(score_sequence(sequence))

  scores.sort()
  return scores[len(scores) // 2]

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [line.strip() for line in open(input_file).readlines()]

  print(f'Corrupted lines syntax score: {score_corrupted(lines)}')
  print(f'Incomplete lines autocomplete score: {score_incomplete(lines)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
