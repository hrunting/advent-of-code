#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_power_report(report: list[str]) -> tuple[int, int]:
  vals = [0] * len(report[0])
  for line in report:
    for i, v in enumerate(line):
      vals[i] += int(v)

  threshold = len(report) // 2
  gamma = int(''.join('1' if val > threshold else '0' for val in vals), 2)
  epsilon = int(''.join('0' if val > threshold else '1' for val in vals), 2)

  return gamma, epsilon

def read_life_report(report: list[str]) -> tuple[int, int]:
  o2_report = report.copy()
  co2_report = report.copy()

  for i in range(len(report[0])):
    if len(o2_report) > 1:
      o2_bit = '1' if sum(int(x[i]) for x in o2_report) >= (len(o2_report) / 2) else '0'
      o2_report = [x for x in o2_report if x[i] == o2_bit]

    if len(co2_report) > 1:
      co2_bit = '0' if sum(int(x[i]) for x in co2_report) >= (len(co2_report) / 2) else '1'
      co2_report = [x for x in co2_report if x[i] == co2_bit]

  return int(o2_report[0], 2), int(co2_report[0], 2)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  report = [line.strip() for line in open(input_file).readlines()]

  gamma, epsilon = read_power_report(report)
  print(f'Power consumption: {gamma * epsilon}')

  o2_rating, co2_rating = read_life_report(report)
  print(f'Life support rating: {o2_rating * co2_rating}')

if __name__ == '__main__':
  run()
  sys.exit(0)
