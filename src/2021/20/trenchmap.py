#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Algorithm = list[int]
Coord = tuple[int, int]
Image = defaultdict[Coord, int]

def enhance_image(
    algo: Algorithm,
    image: Image,
    low_coords: Coord,
    high_coords: Coord,
    steps: int) -> tuple[Image, Coord, Coord]:

  infinite_bit = 0

  min_x, min_y = low_coords
  max_x, max_y = high_coords

  for _ in range(steps):
    next_image = defaultdict(int)

    for x in range(min_x - 1, max_x + 2):
      for y in range(min_y - 1, max_y + 2):
        val = 0

        for cx, cy in (
            (x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
            (x - 1, y), (x, y), (x + 1, y),
            (x - 1, y + 1), (x, y + 1), (x + 1, y + 1)):
          val <<= 1
          val |= image[cx, cy] if min_x <= cx <= max_x and min_y <= cy <= max_y else infinite_bit

        if algo[val] == 1:
          next_image[x, y] = algo[val]

    image = next_image
    infinite_bit = algo[511 * infinite_bit]

    min_x -= 1
    min_y -= 1
    max_x += 1
    max_y += 1

  return image, (min_x, min_y), (max_x, max_y)

def read_image_data() -> tuple[Algorithm, Image, Coord, Coord]:
  x = y = 0
  with open(aoc.inputfile('input.txt')) as file:
    algo_string, image_string = file.read().strip().split("\n\n")

    algo = [1 if c == '#' else 0 for c in algo_string]

    image = defaultdict(int)
    for y, line in enumerate(image_string.split("\n")):
      for x, c in enumerate(line):
        if c == '#':
          image[x, y] = 1
  return algo, image, (0, 0), (x, y)

def run() -> None:
  algo, image, low_coords, high_coords = read_image_data()

  enhanced, _, _ = enhance_image(algo, image, low_coords, high_coords, 2)
  print(f'Lit pixels after 2 steps: {sum(enhanced.values())}')

  enhanced, _, _ = enhance_image(algo, image, low_coords, high_coords, 50)
  print(f'Lit pixels after 50 steps: {sum(enhanced.values())}')

if __name__ == '__main__':
  run()
  sys.exit(0)
