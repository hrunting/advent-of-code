#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Board:
  def __init__(self: Self, rows: list[list[int]]) -> None:
    self.id = ','.join(str(n) for row in rows for n in row)
    self.rows = rows.copy()
    self.winners = []
    self.reset()

  def __eq__(self: Self, other: Self):
    return self.id == other.id

  def __hash__(self: Self) -> int:
    return hash(self.id)

  def reset(self: Self) -> None:
    self.winners = [set(row) for row in self.rows]
    self.winners.extend(set(row[i] for row in self.rows) for i in range(len(self.rows[0])))

  def play(self: Self, n: int) -> bool:
    winning = False
    for candidate in self.winners:
      candidate.discard(n)
      winning = winning or len(candidate) == 0
    return winning

  def unmarked(self: Self) -> set[int]:
    return set(n for nums in self.winners for n in list(nums))

  def __str__(self: Self) -> str:
    return "\n".join(' '.join([f'{n:2}' for n in row]) for row in self.rows)

def readBoards() -> tuple[list[int], set[Board]]:
  numbers = []
  boards = set()

  input_file = aoc.inputfile('input.txt')
  with open(input_file) as input:
    rows = []
    numbers = [int(x) for x in input.readline().strip().split(",")]

    for line in input:
      line = line.strip()
      if line == "":
        if rows:
          boards.add(Board(rows))
          rows = []

      else:
        rows.append([int(x) for x in line.split()])

    else:
      if rows:
        boards.add(Board(rows))

  return numbers, boards

def play(numbers: list[int], boards: set[Board], last: bool = False) -> int:
  finished = set()

  for n in numbers:
    for board in boards:
      if board in finished:
        continue

      if board.play(n):
        if not last or len(boards) - len(finished) == 1:
          return sum(board.unmarked()) * n

        else:
          finished.add(board)

  return -1

def run() -> None:
  numbers, boards = readBoards()

  winning_score = play(numbers, boards)
  print(f'Winning score: {winning_score}')

  # conceptually, we should reset the boards here
  # but we don't need to because the same played numbers
  # will simply result in the current state

  winning_score = play(numbers, boards, True)
  print(f'Squid winning score: {winning_score}')

if __name__ == '__main__':
  run()
  sys.exit(0)
