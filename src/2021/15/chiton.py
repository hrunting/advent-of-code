#!/usr/bin/env python3

import pathlib
import queue
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def find_lowest_risk(map: dict[tuple[int, int], int]) -> int:
  max_x = max(x for x, _ in map.keys())
  max_y = max(y for _, y in map.keys())

  q = queue.PriorityQueue()
  q.put((0, (0, 0)))

  dists = {(0, 0): 0}

  while not q.empty():
    risk, (x, y) = q.get()
    for adj in {(-1, 0), (1, 0), (0, -1), (0, 1)}:
      nx, ny = x + adj[0], y + adj[1]
      if nx < 0 or nx > max_x or ny < 0 or ny > max_y:
        continue

      n_risk = risk + map[nx, ny]

      if nx == max_x and ny == max_y:
        return n_risk

      if (nx, ny) in dists and dists[nx, ny] <= n_risk:
        continue

      dists[nx, ny] = n_risk
      q.put((n_risk, (nx, ny)))

  return 0

def expand_map(map: dict[tuple[int, int], int]) -> dict[tuple[int, int], int]:
  max_x = max(x for x, _ in map.keys())
  max_y = max(y for y, _ in map.keys())

  expanded = map.copy()

  # expand in the X direction
  keys = list(expanded.keys())
  for x_add in range(1, 5):
    for x, y in keys:
      expanded[x + (max_x + 1) * x_add, y] = (expanded[x, y] - 1 + x_add) % 9 + 1

  # expand the expanded chart in the Y direction
  keys = list(expanded.keys())
  for y_add in range(1, 5):
    for x, y in keys:
      expanded[x, y + (max_y + 1) * y_add] = (expanded[x, y] - 1 + y_add) % 9 + 1

  return expanded

def read_map() -> dict[tuple[int, int], int]:
  map = {}
  with open(aoc.inputfile('input.txt')) as input:
    map = {
      (x, y): int(risk)
        for y, line in enumerate(input.readlines())
        for x, risk in enumerate(line.strip())
    }

  return map

def run() -> None:
  map = read_map()

  lowest_risk = find_lowest_risk(map)
  print(f'Lowest total risk: {lowest_risk}')

  lowest_risk = find_lowest_risk(expand_map(map))
  print(f'Lowest risk total (expanded map): {lowest_risk}')

if __name__ == '__main__':
  run()
  sys.exit(0)
