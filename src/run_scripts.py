#!/usr/bin/env python3

import argparse
import contextlib as cl
import io
import math
import multiprocessing as mp
import pathlib
import runpy
import sys
import time
import traceback

RUNDIR = pathlib.Path.cwd()
SRCDIR = pathlib.Path(__file__).resolve().parents[0]

def run_script(script: pathlib.Path, q: mp.Queue, verbose: bool = False) -> None:
  error = None
  start = time.monotonic_ns()

  try:
    if verbose:
      runpy.run_path(str(script.relative_to(RUNDIR)), run_name="__main__")
    else:
      with io.StringIO() as buffer, cl.redirect_stdout(buffer), cl.redirect_stderr(buffer):
        runpy.run_path(str(script.relative_to(RUNDIR)), run_name="__main__")
  except SystemExit:
    pass
  except Exception as e:
    with io.StringIO() as buffer:
      traceback.print_exception(e, file=buffer, limit=-5)
      error = buffer.getvalue()

  q.put((script.relative_to(SRCDIR), time.monotonic_ns() - start, error))

def run() -> None:
  parser = argparse.ArgumentParser()
  parser.add_argument("-y", "--year", type=int, help="Run solutions for given year")
  parser.add_argument("-v", "--verbose", help="Show each solution's output", action="store_true")
  args = parser.parse_args()

  year = str(args.year) if args.year else "*"

  stats = []
  files = sorted(SRCDIR.glob(f"{year}/**/*.py"))
  q = mp.Queue()

  width = max(len(str(f.relative_to(SRCDIR))) for f in files) + 1
  total_runtime = 0.0
  errors = False

  for file in files:
    rfile = file.relative_to(SRCDIR)

    if args.verbose:
      print(f"{rfile}:", flush=True)
    else:
      print(f"{str(rfile):<{width}}: ", end="", flush=True)

    p = mp.Process(target=run_script, args=(file, q, args.verbose))
    p.start()
    p.join()

    program, runtime, error = q.get()
    total_runtime += runtime
    errors = errors or error is not None

    if args.verbose:
      if error:
        print(error, flush=True)
      print('', flush=True)
    else:
      print(f"{(runtime / 1000000):0.2f}ms{' *' if error else ''}", flush=True)
      if error:
        print(error, flush=True)

    stats.append((program, runtime, error))

  if args.verbose:
    print("Timings:", flush=True)
    for script, runtime, error in stats:
      print(f"{str(script):<{width}}: {(runtime / 1000000):0.2f}ms{' *' if error else ''}", flush=True)

  print(('-' * width) + '+' + ('-' * (int(math.log10(total_runtime)) + 10 + (2 * errors))), flush=True)
  print(f"{'Total runtime':<{width}}: {(total_runtime / 1000000):0.2f}ms{' *' if errors else ''}", flush=True)

if __name__ == "__main__":
  run()
  sys.exit(0)