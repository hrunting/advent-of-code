#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import Iterable, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

from aoc.search import AStarSearch

Position = tuple[int, int]
TimePosition = tuple[int, int, int]

class BlizzardSearch(AStarSearch[TimePosition, Position]):
  __slots__ = ('map', 'cycle')

  def __init__(self: Self, map: tuple[str, ...], **kwargs) -> None:
    super().__init__(**kwargs)
    self.map = map
    self.cycle = math.lcm(len(self.map), len(self.map[0]))

  def is_finished(self: Self, node: TimePosition, goal: Position) -> bool:
    return node[0] == goal[0] and node[1] == goal[1]

  def estimate(self: Self, node: TimePosition, goal: Position) -> int:
    return aoc.manhattan(node, goal)

  def moves(self: Self, node: TimePosition) -> Iterable[tuple[TimePosition, int]]:
    r, c, t = node
    nt = t + 1
    step = nt % self.cycle

    return (
      ((nr, nc, nt), 1)
      for nr, nc in ((r + 1, c), (r, c + 1), (r, c), (r - 1, c), (r, c - 1))
      if (0 <= nr < len(self.map) and 0 <= nc < len(self.map[0]) \
          and self.map[nr][(nc + step) % len(self.map[0])] != '<' \
          and self.map[nr][(nc - step) % len(self.map[0])] != '>' \
          and self.map[(nr + step) % len(self.map)][nc] != '^' \
          and self.map[(nr - step) % len(self.map)][nc] != 'v')
        or (nr == -1 and nc == 0) \
        or (nr == len(self.map) and nc == len(self.map[0]) - 1)
    )

def read_basin(file: pathlib.Path) -> tuple[str, ...]:
  map = tuple()
  with open(file) as f:
    map = tuple(
      line[1:-1]
      for line in (line.rstrip() for line in f.readlines())
      if line[1] != '#' and line[-2] != '#'
    )
  return map

def run() -> None:
  file = aoc.inputfile('input.txt')
  map = read_basin(file)

  goals = [(len(map), len(map[0]) - 1)]

  s = BlizzardSearch(map)
  _, cost, path = s.shortest_path((-1, 0, 0), goals[0])
  print(f'Fastest path to goal: {cost}')

  summed = cost
  goals += [(-1, 0)] + goals
  for goal in goals:
    s = BlizzardSearch(map)
    _, cost, path = s.shortest_path(path[-1], goal)
    summed += cost
  print(f'Fastest path w/ forgotten snacks: {summed}')

if __name__ == '__main__':
  run()
  sys.exit(0)
