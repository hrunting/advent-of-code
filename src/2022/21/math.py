#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

RoutineMap = dict[str, Callable[[], int | float]]
MonkeyInputs = tuple[str, str]

def sign(x: int | float) -> float:
  return math.copysign(1, x)

def read_language(file: pathlib.Path) -> tuple[RoutineMap, MonkeyInputs]:
  routines = {}
  root_monkeys = ('','')

  # These closures will be evaluated when they are needed.
  #
  # We need to wrap these lambdas instead of assigning them
  # directly because the arguments need to remain fixed at
  # lambda creation time.
  def yell(i: str) -> Callable[[], int]:
    return lambda: int(i)

  def apply(a: str, b: str, op: str) -> Callable[[], int | float]:
    match op:
      case "+":
        return lambda: routines[a]() + routines[b]()
      case "-":
        return lambda: routines[a]() - routines[b]()
      case "*":
        return lambda: routines[a]() * routines[b]()
      case _:
        return lambda: routines[a]() / routines[b]()

  with open(file) as f:
    while line := f.readline().rstrip():
      monkey, rest = line.split(": ")

      if rest[0].isnumeric():
        routines[monkey] = yell(rest)
      else:
        a, op, b = rest.split(" ")
        routines[monkey] = apply(a, b, op)

        # save the components of the root monkey
        # for later equation solving
        if monkey == "root":
          root_monkeys = (a, b)

  return routines, root_monkeys

def solve_equation(
    language: dict[str, Callable[[], int | float]],
    a: str, b: str) -> int:

  # save for later since we will be modifying the human yell
  human = language["humn"]

  '''
  Incrementing the value of the human yell typically increments or
  decrements the result by a fixed amount. Sometimes, though, the
  result may jump by a different amount periodically.

  Our stategy here will be to repeatedly use the standard result
  increment to determine a candidate based on the differences in
  the candidate's results. Once the difference is less than the
  fixed increment, we just increment or decrement the candidate
  until the results match.

  Two assumptions here:

  1. The difference between candidates 0 and 1 is the fixed
     increment.

  2. One side of the root equation (either a or b) does not change
     as the human yell changes.
  '''

  language["humn"] = lambda: 0
  a0, b0 = language[a](), language[b]()

  language["humn"] = lambda: 1
  a1, b1 = language[a](), language[b]()

  if a1 == a0:
    a, a1, a0, b, b1, b0 = b, b1, b0, a, a1, a0

  diff = a1 - a0
  candidate = 0
  language["humn"] = lambda: candidate

  while a0 != b0:
    if abs(b0 - a0) >= abs(diff):
      candidate = int(candidate + (b0 - a0) / diff)
    else:
      candidate += 1 if sign(diff) == sign(b0 - a0) else -1

    a0 = language[a]()

  # restore the original human yell
  language["humn"] = human

  return candidate

def run() -> None:
  file = aoc.inputfile('input.txt')

  language, (a, b) = read_language(file)
  print(f"Root monkey yells {int(language['root']())}")

  yell = solve_equation(language, a, b)
  print(f'Human needs to yell {yell}')

if __name__ == '__main__':
  run()
  sys.exit(0)
