#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

ORIGIN = (500, 0)

class Scan():
  __slots__ = ('floor', 'map', 'room', 'x', 'y')

  @classmethod
  def init_from_file(cls: type[Self], file: pathlib.Path, floor: bool = False) -> Self:
    scan = cls(floor)

    with open(file) as f:
      while line := f.readline().rstrip():
        points = [(int(x), int(y)) for x, y in (point.split(",") for point in line.split(" -> "))]
        for (x1, y1), (x2, y2) in zip(points, points[1:]):
          scan.map |= {(x, y1) for x in range(min(x1, x2), max(x1, x2) + 1)}
          scan.map |= {(x1, y) for y in range(min(y1, y2), max(y1, y2) + 1)}

    scan.room = scan.map.copy()
    scan.x = (min(x for x, _ in scan.map), max(x for x, _ in scan.map) + 1)
    scan.y = (min(y for _, y in scan.map), max(y for _, y in scan.map) + 1)

    return scan

  def __init__(self: Self, floor: bool = False) -> None:
    self.floor = floor

    self.map: set[tuple[int, int]] = set()
    self.room = set()

    self.x = (ORIGIN[0], ORIGIN[0] + 1)
    self.y = (ORIGIN[1], ORIGIN[1] + 1)

  def __str__(self: Self) -> str:
    def c(x: int, y: int) -> str:
      return \
        '#' if (x, y) in self.map else \
        'o' if (x, y) in self.room else \
        '+' if (x, y) == ORIGIN else \
        '.'

    return "\n".join(
      "".join(c(x, y) for x in range(self.x[0], self.x[1]))
      for y in range(self.y[0], self.y[1] + int(self.floor)))

  def drop_sand(self: Self) -> int:
    x, y = ORIGIN
    max_y = self.y[1] + int(self.floor)

    while True:
      y += 1

      # continue falling if left/middle/right is not blocked
      if y < max_y:
        if (x, y) not in self.room:
          continue
        elif (x - 1, y) not in self.room:
          x -= 1
          continue
        elif (x + 1, y) not in self.room:
          x += 1
          continue

      # sand falls forever
      if y == max_y and not self.floor:
        break

      y -= 1
      self.room.add((x, y))
      self.x = (min(self.x[0], x), max(self.x[1], x + 1))

      # the area is filled to the hole
      if (x, y) == ORIGIN:
        break

      # drop the next grain
      x, y = ORIGIN

    return len(self.room) - len(self.map)
        
def run() -> None:
  file = aoc.inputfile('input.txt')

  scan = Scan.init_from_file(file)
  sand = scan.drop_sand()
  print(f"Sand units at rest: {sand}")

  scan.floor = True
  sand = scan.drop_sand()
  print(f"Sand units at rest (blocked): {sand}")

if __name__ == '__main__':
  run()
  sys.exit(0)
