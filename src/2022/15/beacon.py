#!/usr/bin/env python3

import pathlib
import re
import sys

from itertools import zip_longest
from typing import Self, Generator

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

MapPoint = tuple[int, int]
SensorMap = dict[MapPoint, int]
BeaconSet = set[MapPoint]

class RangeSet():
  def __init__(self: Self) -> None:
    self.ranges = []

  def __contains__(self: Self, n: int) -> bool:
    return any(r[0] <= n <= r[1] for r in self.ranges)

  def add(self: Self, rng: MapPoint) -> None:
    idx = next((i for i, r in enumerate(self.ranges) if not r[1] < rng[0]), len(self.ranges))
    if idx == len(self.ranges):
      self.ranges.append(rng)
    elif self.ranges[idx][0] > rng[1]:
      self.ranges.insert(idx, rng)
    else:
      self.ranges[idx] = (min(self.ranges[idx][0], rng[0]), max(self.ranges[idx][1], rng[1]))
      while idx + 1 < len(self.ranges) and self.ranges[idx + 1][0] <= self.ranges[idx][1]:
        self.ranges[idx] = (
          self.ranges[idx][0],
          max(self.ranges[idx][1],
          self.ranges.pop(idx + 1)[1])
        )

    return

  def iter(self: Self) -> Generator[MapPoint, None, None]:
    return (r for r in self.ranges)

def intersect(p1: MapPoint, p2: MapPoint, q1: MapPoint, q2: MapPoint) -> MapPoint | None:
  divergence = (q2[1] - q1[1]) * (p2[0] - p1[0]) - (q2[0] - q1[0]) * (p2[1] - p1[1])

  # lines are parallel and either co-linear or will never intersect
  if divergence == 0:
    return None

  ua = ((q2[0] - q1[0]) * (p1[1] - q1[1]) - (q2[1] - q1[1]) * (p1[0] - q1[0])) / divergence
  ub = ((p2[0] - p1[0]) * (p1[1] - q1[1]) - (p2[1] - p1[1]) * (p1[0] - q1[0])) / divergence

  # segments do not intersect
  if ua < 0 or ua > 1 or ub < 0 or ub > 1:
    return None

  return p1[0] + int(ua * (p2[0] - p1[0])), p1[1] + int(ua * (p2[1] - p1[1]))

def read_sensors(file: pathlib.Path) -> tuple[SensorMap, BeaconSet]:
  map, beacons = {}, set()

  parser = re.compile(r'.+? x=([0-9-]+), y=([0-9-]+): .+? x=([0-9-]+), y=([0-9-]+)')

  with open(file) as f:
    while line := f.readline().rstrip():
      if m := parser.match(line):
        sx, sy, bx, by = (int(d) for d in m.groups())
        map[(sx, sy)] = abs(sx - bx) + abs(sy - by)
        beacons.add((bx, by))

  return map, beacons

def count_no_beacons(map: SensorMap, beacons: BeaconSet, row: int) -> int:
  ranges = RangeSet()

  for (x, y), r in map.items():
    if y - r <= row <= y + r:
      x_dist = r - abs(row - y)
      ranges.add((x - x_dist, x + x_dist))

  count = -sum(1 for (x, y) in beacons if y == row and x in ranges)
  count += sum(hi - lo + 1 for lo, hi in ranges.iter())
  return count

def distress_frequency(map: SensorMap, beacons: BeaconSet, bound: int) -> int:
  sensors: list[MapPoint] = list(map.keys())
  edges: dict[MapPoint, list[tuple[MapPoint, MapPoint]]] = {}

  # create a map for the bounding boxes of each sensor
  # we transform the four corners to segments:
  # A, B, C, D translate to (A, B), (B, C), (C, D), (D, A)
  for (x, y) in sensors:
    r = map[(x, y)]
    points = [(x + r, y), (x, y - r), (x - r, y), (x, y + r)]
    edges[(x, y)] = list(zip_longest(points, points[1:], fillvalue=points[0]))

  # compare each sensor's edges to every remaining sensor's edges
  for i, (p1, p2) in ((i, line) for i, s in enumerate(sensors) for line in edges[s]):
    for q1, q2 in (line for s in sensors[i:] for line in edges[s]):
      if (intersection := intersect(p1, p2, q1, q2)) is None:
        continue

      ix, iy = intersection

      # if point adjacent to the intersection is outside every sensor's range
      for (x, y) in ((ix, iy + 1), (ix + 1, iy), (ix, iy - 1), (ix - 1, iy)):

        # candidate is out of bounds, skip it
        if not 0 <= x <= bound or not 0 <= y <= bound:
          continue

        for (sx, sy), r in map.items():
          if abs(sx - x) + abs(sy - y) <= r:
            break

        else:

          # no sensor has this point within its range
          # this must be our distress beacon
          return x * 4000000 + y

  return 0

def run() -> None:
  file, row = aoc.inputfile('input.txt'), 2000000
  map, beacons = read_sensors(file)

  count = count_no_beacons(map, beacons, row)
  print(f'Positions that cannot contain beacon (row {row}): {count}')

  frequency = distress_frequency(map, beacons, row * 2)
  print(f'Distress beacon tuning frequency: {frequency}')

if __name__ == '__main__':
  run()
  sys.exit(0)
