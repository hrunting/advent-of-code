#!/usr/bin/env python3

import pathlib
import sys

from typing import Self, TextIO

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

SMALL_DIR_SIZE = 100_000
FS_SIZE = 70_000_000
REQUIRED_SIZE = 30_000_000

class Dir:
  def __init__(self: Self, name: str) -> None:
    self.parent = None
    self.name: str = name
    self.dirs: dict[str, Self] = {}
    self.files: dict[str, int] = {}

  def add_dir(self: Self, dir: Self) -> None:
    dir.parent = self
    self.dirs[dir.name] = dir

  def add_file(self: Self, name: str, size: int) -> None:
    self.files[name] = size

  def size(self: Self) -> int:
    return sum(dir.size() for dir in self.dirs.values()) + sum(self.files.values())

def parse_dir_structure(f: TextIO) -> Dir:

  # assume that first line switches into the root directory
  assert(f.readline().rstrip() == "$ cd /")
  cwd = Dir("/")

  while line := f.readline().rstrip():

    # track directory structures once we change into them
    # if we don't change into them, we will assume they are empty
    if line.startswith("$ cd "):
      dirname = line[5:]
      if dirname == ".." and cwd.parent is not None:
        cwd = cwd.parent
      else:
        dir = Dir(dirname)
        cwd.add_dir(dir)
        cwd = dir

    # skip directory listing lines
    elif line.startswith("$ ls"):
      pass

    # we only care about directories we go into
    elif line.startswith("dir "):
      pass

    # a file is "<size> <name>"
    else:
      size, name = line.split(" ")
      if cwd is not None:
        cwd.add_file(name, int(size))

  while cwd.parent is not None:
    cwd = cwd.parent

  return cwd

def sum_small_directories(cwd: Dir) -> int:
  summed = 0
  dirs = [cwd]

  while len(dirs):
    cwd = dirs.pop()
    size = cwd.size()

    if size <= SMALL_DIR_SIZE:
      summed += size

    dirs.extend(list(cwd.dirs.values()))

  return summed

def find_directory_to_delete(cwd: Dir) -> Dir:
  free_space_needed = REQUIRED_SIZE - (FS_SIZE - cwd.size())

  freed_space = cwd.size()
  delete_dir = cwd
  dirs = list(cwd.dirs.values())

  while len(dirs):
    cwd = dirs.pop()
    size = cwd.size()

    if free_space_needed <= size < freed_space:
      freed_space = size
      delete_dir = cwd

    dirs.extend(list(cwd.dirs.values()))

  return delete_dir

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    topdir = parse_dir_structure(f)

  print(f'Total sizes of small directories: {sum_small_directories(topdir)}')
  print(f'Size of smallest directory to free up space: {find_directory_to_delete(topdir).size()}')

if __name__ == '__main__':
  run()
  sys.exit(0)
