#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  elves = []
  input_file = aoc.inputfile('input.txt')

  elf_total = 0
  for calories in (line.strip() for line in open(input_file).readlines()):
    if calories == "":
      elves.append(elf_total)
      elf_total = 0
    else:
      elf_total += int(calories)
  else:
    elves.append(elf_total)

  print(f'Maximum calories carried {max(elves)}')
  print(f'Calories carried by top 3 elves {sum(sorted(elves)[-3:])}')

if __name__ == '__main__':
  run()
  sys.exit(0)
