#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def from_base_5(s: str) -> int:
  num = 0

  place = 1
  for c in reversed(s):
    match c:
      case '=':
        num += place * -2
      case '-':
        num += place * -1
      case _:
        num += place * int(c)
    place *= 5

  return num

def to_base_5(num: int) -> str:
  base_5 = ""

  while num > 0:
    match num % 5:
      case 4:
        base_5 += "-"
        num += 1
      case 3:
        base_5 += "="
        num += 2
      case x:
        base_5 += str(x)
    num //= 5

  return base_5[::-1]

def run() -> None:
  total = 0

  with open(aoc.inputfile('input.txt')) as f:
    while line := f.readline().rstrip():
      total += from_base_5(line)

  print(f'SNAFU number: {to_base_5(total)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
