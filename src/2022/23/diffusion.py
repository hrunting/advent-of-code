#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from typing import Generator

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_positions(file: pathlib.Path) -> set[complex]:
  with open(file) as f:
    map = {
      x + y * 1j
        for y, line in enumerate(f.readlines())
        for x in range(len(line))
        if line[x] == '#'
    }
  return map

def simulation(map: set[complex]) -> Generator[set[complex], None, None]:
  dirs = deque([
    (-1 - 1j, -1j, 1 - 1j),
    (-1 + 1j, 1j, 1 + 1j),
    (-1 - 1j, -1, -1 + 1j),
    (1 - 1j, 1, 1 + 1j),
  ])

  while True:
    moves = 0
    moved = set()

    for elf in map:
      elf_moves = []
      for left, move, right in dirs:
        if {elf + left, elf + move, elf + right}.isdisjoint(map):
          elf_moves.append(move)
        elif elf_moves:
          break

      if elf_moves and len(elf_moves) < len(dirs):
        move = elf + elf_moves[0]

        # this move is valid, add the elf to the new state
        if move not in moved:
          moved.add(move)
          moves += 1

        # an elf already moved to this position
        # they must have come from the opposite direction
        # add ourselves and move the other elf back
        else:
          moved ^= {elf, move, move - elf + move}
          moves -= 1

      # this elf did not move
      else:
        moved.add(elf)

    yield moved

    if moves == 0:
      return

    map = moved
    dirs.rotate(-1)

def bounds(map: set[complex]) -> tuple[int, int, int, int]:
  min_x = int(min(elf.real for elf in map))
  min_y = int(min(elf.imag for elf in map))
  max_x = int(max(elf.real for elf in map))
  max_y = int(max(elf.imag for elf in map))

  return min_x, min_y, max_x, max_y

def display(map: set[complex]) -> str:
  min_x, min_y, max_x, max_y = bounds(map)

  return "\n".join("".join(
    '#' if x + y * 1j in map else '.'
    for x in range(min_x, max_x + 1)
  ) for y in range(min_y, max_y + 1))

def empty_area(map: set[complex]) -> int:
  min_x, min_y, max_x, max_y = bounds(map)
  return (max_x - min_x + 1) * (max_y - min_y + 1) - len(map)

def run() -> None:
  file = aoc.inputfile('input.txt')
  map = read_positions(file)

  sim = simulation(map)
  for _ in range(10):
    map = next(sim)

  print(f'Empty area: {empty_area(map)}')

  round = 10 + sum(1 for _ in sim)
  print(f'Final round: {round}')

if __name__ == '__main__':
  run()
  sys.exit(0)
