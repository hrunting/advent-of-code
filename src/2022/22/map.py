#!/usr/bin/env python3

import pathlib
import re
import sys

from dataclasses import dataclass
from typing import Callable, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

@dataclass(frozen=True)
class XYZ():
  x: int
  y: int
  z: int

  def __add__(self: Self, other: Self) -> Self:
    return self.__class__(self.x + other.x, self.y + other.y, self.z + other.z)

  def __sub__(self: Self, other: Self) -> Self:
    return self.__class__(self.x - other.x, self.y - other.y, self.z - other.z)

  def __mul__(self: Self, n: int) -> Self:
    return self.__class__(self.x * n, self.y * n, self.z * n)

  # a rotation function for the X/Y vectors
  def __matmul__(self: Self, other: Self) -> Self:
    return self.__class__(
      self.y * other.z - self.z * other.y,
      self.z * other.x - self.x * other.z,
      self.x * other.y - self.y * other.x
    )

  def dot(self: Self, other: Self) -> int:
    return self.x * other.x + self.y * other.y + self.z * other.z

Map = list[list[str]]
MapPath = list[str]
DirFunc = Callable[[int, int], tuple[int, int]]
FaceMap = dict[tuple[int, int], tuple[XYZ, XYZ, XYZ]]
EdgeMap = dict[tuple[XYZ, XYZ], tuple[int, int]]

def read_directions(file: pathlib.Path) -> tuple[Map, MapPath]:
  map = []
  path = []

  with open(file) as f:
    longest = 0
    while line := f.readline().rstrip():
      map.append(line)
      longest = max(longest, len(line))

    for r in range(len(map)):
      map[r] += " " * (longest - len(map[r]))

    path = re.split(r'([LR])', f.readline().rstrip())

  return map, path

def map_cube(map: Map, r: int, c: int) -> tuple[int, FaceMap, EdgeMap]:

  # six square faces, length is square root of one of them
  side = int((sum(int(c != ' ') for row in map for c in row) // 6) ** 0.5)

  # origin row, origin column
  # origin cube position (x, y, z), x-axis direction, y-axis direction
  faces: FaceMap = {}
  edges: EdgeMap = {}

  # map each face's origin (top left corner, x-positive right, y-positive down)
  # to a cube position on a len x len cube, face positions index from 0 to len - 1
  # and save each 2D axis direction to a 3D vector
  #
  # cube corners at:
  #   top: (0, 0, 0), (n, 0, 0), (n, 0, -n), (0, 0, -n)
  #   bot: (0, n, 0), (n, n, 0), (n, n, -n), (0, n, -n)
  #
  q: list[tuple[int, int, XYZ, XYZ, XYZ]] = []
  q.append((r, c, XYZ(0, 0, 0), XYZ(1, 0, 0), XYZ(0, 1, 0)))

  while len(q):
    y, x, pos, x_vec, y_vec = q.pop()

    # this origin is off the map or not part of the cube
    if y < 0 or y >= len(map) or x < 0 or x >= len(map[0]) or map[y][x] == ' ':
      continue

    # we've already processed this face
    if (y, x) in faces:
      continue

    # the perpendicular direction for this face
    normal = y_vec @ x_vec 

    # map the 3D edges of this face to their 2D coordinates
    # - order: y-axis edge, x-axis edge, y-axis + n edge, x-axis + n edge
    # - key: 3D edge position and normal vector for the face
    for dist in range(side):
      edges[pos + y_vec * dist, normal] = y + dist, x
      edges[pos + x_vec * dist, normal] = y, x + dist
      edges[pos + y_vec * dist + x_vec * (side - 1), normal] = y + dist, x + side - 1
      edges[pos + x_vec * dist + y_vec * (side - 1), normal] = y + side - 1, x + dist

    faces[y, x] = (pos, y_vec, x_vec)

    # DFS face processing in the following order: up, down, left, right
    q.append((y, x - side, pos + y_vec @ x_vec * (side - 1), x_vec @ y_vec, y_vec))
    q.append((y, x + side, pos + x_vec * (side - 1), y_vec @ x_vec, y_vec))
    q.append((y - side, x, pos + y_vec @ x_vec * (side - 1), x_vec, x_vec @ y_vec))
    q.append((y + side, x, pos + y_vec * (side - 1), x_vec, y_vec @ x_vec))

  return side, faces, edges

def flat_move(map: Map, moves: int, r: int, c: int, v: int, h: int) -> tuple[int, int]:
  v_skip, h_skip = 0, 0

  while moves > 0:
    nr, nc = (r + v_skip + v) % len(map), (c + h_skip + h) % len(map[r])
    match map[nr][nc]:
      case '#':
        break
      case ' ':
        h_skip += h
        v_skip += v
      case '.':
        moves -= 1
        r, c = nr, nc
        v_skip, h_skip = 0, 0

  return r, c

def cube_move(
    map: Map, moves: int,
    r: int, c: int, v: int, h: int,
    side: int, faces: FaceMap, edges: EdgeMap) -> tuple[int, int, int, int]:

  while moves > 0:
    nv, nh = v, h
    nr, nc = r + nv, c + nh

    while not (0 <= nr < len(map) and 0 <= nc < len(map[nr]) and map[nr][nc] != ' '):
      o_pos, y_vec, x_vec = faces[r // side * side, c // side * side]

      pos = o_pos + y_vec * (r % side) + x_vec * (c % side)

      # wrap around the cube face
      # the lookup provides the 2D coordinates of the wrapped face
      nr, nc = edges[pos, y_vec * -nv + x_vec * -nh]

      # lookup the new x- and y-axis vectors for the new face
      normal = y_vec @ x_vec
      _, n_y_vec, n_x_vec = faces[nr // side * side, nc // side * side]

      # calculate the 2D direction from the axis vectors and the face vector
      # this will point towards the interior of the face
      nv, nh = n_y_vec.dot(normal), n_x_vec.dot(normal)

    match map[nr][nc]:
      case '#':
        break
      case '.':
        moves -= 1
        r, c = nr, nc
        v, h = nv, nh

  return r, c, v, h

def find_password(map: Map, path: MapPath, is_cube: bool = False) -> int:
  r, c = 0, map[0].index('.')
  v, h = 0, 1

  # the logic for the cube setup comes from this solution
  # https://www.reddit.com/r/adventofcode/comments/zsct8w/comment/j18dzaa/?context=3
  #
  # essentially, we create a 3D map of the faces and the edges of the faces
  # then use the maps to perform some 3D calculations when we hit edges

  side, faces, edges = map_cube(map, r, c) if is_cube else (0, {}, {})

  for cmd in path:
    match cmd:
      case "L":
        v, h = -h, v
      case "R":
        v, h = h, -v
      case _: 
        if is_cube:
          r, c, v, h = cube_move(map, int(cmd), r, c, v, h, side, faces, edges)
        else:
          r, c = flat_move(map, int(cmd), r, c, v, h)

  match v, h:
    case 0, 1:
      score = 0
    case 1, 0:
      score = 1
    case 0, -1:
      score = 2
    case _:
      score = 3

  return score + (r + 1) * 1000 + (c + 1) * 4

def run() -> None:
  file = aoc.inputfile('input.txt')
  map, path = read_directions(file)

  password = find_password(map, path)
  print(f'Flat map password: {password}')

  password = find_password(map, path, True)
  print(f'Cube map password: {password}')

if __name__ == '__main__':
  run()
  sys.exit(0)
