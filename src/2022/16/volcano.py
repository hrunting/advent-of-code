#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict
from itertools import combinations, permutations

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

FlowMap = dict[str, int]
DistMap = dict[tuple[str, str], int]
ClosedSet = int

def read_scan(file: pathlib.Path) -> tuple[FlowMap, DistMap]:
  valves = {}
  flows = {}
  distances = {}

  parser = re.compile(r'Valve (..) has flow rate=(\d+); tunnels? leads? to valves? (.+)')

  with open(file) as f:
    while line := f.readline().rstrip():
      if m := parser.match(line):
        valve, flow, tunnels = m.groups()
        valves[valve] = set(tunnels.split(", "))

        if flow != "0":
          flows[valve] = int(flow)

        distances[valve, valve] = 0
        for other in valves[valve]:
          distances[valve, other] = 1

  # Floyd-Warshall algorithm
  # don't bother populating distances for non-existent paths
  for k, i, j in permutations(valves.keys(), 3):
    if (i, k) not in distances or (k, j) not in distances:
      continue
    distance = distances[i, k] + distances[k, j]
    distances[i, j] = min(distances.get((i, j), distance), distance)

  return flows, distances

def max_release(flows: FlowMap, distances: DistMap, minutes: int) -> dict[ClosedSet, int]:
  valves = {valve: 1 << i for i, valve in enumerate(flows)}
  results: dict[ClosedSet, int] = defaultdict(int)

  stack: list[tuple[str, int, ClosedSet, int]] = []
  stack.append(('AA', minutes, 0, 0))

  while len(stack):
    valve, minutes, opened, pressure = stack.pop()
    results[opened] = max(results[opened], pressure)

    minutes -= 1

    for dest, flow in flows.items():
      remaining = minutes - distances[valve, dest]

      # is there enough time to get to the destination valve and open it
      # and is the destination valve still closed?
      if remaining <= 0 or opened & valves[dest] != 0:
        continue

      stack.append((dest, remaining, opened | valves[dest], pressure + flow * remaining))

  return results

def run() -> None:
  file = aoc.inputfile('input.txt')
  flows, distances = read_scan(file)

  released = max_release(flows, distances, 30)
  highest_energy = max(released.values())
  print(f'Max release after 30 minutes: {highest_energy}')

  released = max_release(flows, distances, 26)
  highest_energy = max(
    released[elf] + released[elephant]
    for elf, elephant in combinations(released.keys(), 2)
    if elf & elephant == 0
  )
  print(f'Max release with 1 helper: {highest_energy}')

if __name__ == '__main__':
  run()
  sys.exit(0)
