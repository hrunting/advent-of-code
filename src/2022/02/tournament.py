#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

play_scores: dict[tuple[str, str], int] = {
  ('A', 'X'): 1 + 3,
  ('B', 'X'): 1 + 0,
  ('C', 'X'): 1 + 6,
  ('A', 'Y'): 2 + 6,
  ('B', 'Y'): 2 + 3,
  ('C', 'Y'): 2 + 0,
  ('A', 'Z'): 3 + 0,
  ('B', 'Z'): 3 + 6,
  ('C', 'Z'): 3 + 3
}

result_scores: dict[tuple[str, str], int] = {
  ('A', 'X'): 3 + 0,
  ('B', 'X'): 1 + 0,
  ('C', 'X'): 2 + 0,
  ('A', 'Y'): 1 + 3,
  ('B', 'Y'): 2 + 3,
  ('C', 'Y'): 3 + 3,
  ('A', 'Z'): 2 + 6,
  ('B', 'Z'): 3 + 6,
  ('C', 'Z'): 1 + 6
}

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  score_played = 0
  score_result = 0

  for p1, p2 in (line.strip().split() for line in open(input_file).readlines()):
    score_played += play_scores.get((p1, p2), 0)
    score_result += result_scores.get((p1, p2), 0)

  print(f'Total score according to play-based guide: {score_played}')
  print(f'Total score according to result-based guide: {score_result}')

if __name__ == '__main__':
  run()
  sys.exit(0)
