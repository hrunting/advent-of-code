#!/usr/bin/env python3

import pathlib
import sys

from queue import Queue

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Node = tuple[int, int, int]
Face = tuple[int, int, int, int, int, int]

def faces(x: int, y: int, z: int) -> set[Face]:
  return {
    (x - 1, x, y, y, z, z),
    (x, x + 1, y, y, z, z),
    (x, x, y - 1, y, z, z),
    (x, x, y, y + 1, z, z),
    (x, x, y, y, z - 1, z),
    (x, x, y, y, z, z + 1)
  }

def surfaces(droplets: set[Node]) -> set[Face]:
  exposed = set()
  for (x, y, z) in droplets:
    exposed ^= faces(x, y, z)
  return exposed

def interior_surfaces(droplets: set[Node], surfaces: set[Face]) -> set[Face]:
  interior = surfaces.copy()

  max_x, max_y, max_z = (max(vals) + 1 for vals in zip(*droplets))
  min_x, min_y, min_z = (min(vals) - 1 for vals in zip(*droplets))

  checked = {
    (min_x, min_y, min_z),
    (min_x, min_y, max_z),
    (min_x, max_y, min_z),
    (min_x, max_y, max_z),
    (max_x, min_y, min_z),
    (max_x, min_y, max_z),
    (max_x, max_y, min_z),
    (max_x, max_y, max_z)
  }

  q = Queue()
  for pos in checked:
    q.put(pos)

  while not q.empty():
    (x, y, z) = q.get()
    for face in faces(x, y, z):

      # remove any face found on the exterior surface
      if face in surfaces:
        interior -= {face}
        continue

      (x1, x2, y1, y2, z1, z2) = face

      # check whether the face is within our droplet bounds
      if min(x1, x2) < min_x or min(y1, y2) < min_y or min(z1, z2) < min_z:
        continue
      if max(x1, x2) > max_x or max(y1, y2) > max_y or max(z1, z2) > max_z:
        continue

      # get the associated node for this face
      match (x1 == x2, y1 == y2, z1 == z2):
        case (False, True, True):
          node = (x2 if x == x1 else x1, y1, z1)
        case (True, False, True):
          node = (x1, y2 if y == y1 else y1, z1)
        case _:
          node = (x1, y1, z2 if z == z1 else z1)

      if node not in checked:
        checked.add(node)
        q.put(node)

  return interior

def run() -> None:
  droplets = set()
  with open(aoc.inputfile('input.txt')) as f:
    for line in f.readlines():
      (x, y, z) = (int(x) for x in line.rstrip().split(",", 2))
      droplets.add((x, y, z))

  exposed = surfaces(droplets)
  surface_area = len(exposed)
  print(f"Surface area: {surface_area}")

  interior = interior_surfaces(droplets, exposed)
  exterior_surface_area = surface_area - len(interior)
  print(f"Exterior surface area: {exterior_surface_area}")

if __name__ == '__main__':
  run()
  sys.exit(0)
