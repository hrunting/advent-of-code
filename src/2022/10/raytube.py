#!/usr/bin/env python3

import itertools
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_program(file: pathlib.Path) -> list[tuple[str, int]]:
  with open(file) as f:
    return [(cmd[:4], int(cmd[5:] or 0)) for cmd in f]

def read_pixels(pixels: list[bool]) -> str:
  return "\n".join("".join('#' if pixels[r * 40 + c] else ' ' for c in range(40)) for r in range(6))

def run() -> None:
  program = read_program(aoc.inputfile('input.txt'))

  cycles = 0
  register = 1
  pixels = [False for _ in range(240)]
  pixel = 0

  cycle_mark = itertools.count(20, 40)
  next_mark = next(cycle_mark)
  signal_sums = 0

  for op, arg in program:
    match op:
      case "noop":
        cycles += 1
      case "addx":
        cycles += 2

    if cycles >= next_mark:
      signal_sums += register * next_mark
      next_mark = next(cycle_mark)

    for p in range(pixel, cycles):
      pixels[p] = register - 1 <= p % 40 <= register + 1

    pixel = cycles
    register += arg

  print(f'Sum of 6 signal strengths: {signal_sums}')
  print(f'Screen:\n{read_pixels(pixels)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
