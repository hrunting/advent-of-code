#!/usr/bin/env python3

import math
import pathlib
import sys

from copy import deepcopy
from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

MonkeyOp = Callable[[int], int]
MonkeyTest = Callable[[int], int]
Monkey = tuple[list[int], MonkeyOp, MonkeyTest]

def parse_monkeys(file: pathlib.Path) -> tuple[list[Monkey], int]:
  monkeys = []
  overall_modulo = 1

  items = []
  op = lambda x: x    # noqa: E731
  test = lambda x: x  # noqa: E731
  modulo = 1
  dests = []

  with open(file) as f:
    while line := f.readline():
      line = line.strip()

      match line[:5]:
        case "":
          monkeys.append((items, op, test))
          items = []
        case "Start":
          items.extend(list(int(x) for x in line[16:].split(", ")))
        case "Opera":
          op = eval(f"lambda old: {line[17:]}")
        case "Test:":
          modulo = int(line.rsplit(" ", 1)[-1])
          overall_modulo *= modulo
        case "If tr" | "If fa":
          dests.append(int(line.rsplit(" ", 1)[-1]))
          if len(dests) == 2:
            test = eval(f'lambda n: {dests[0]} if n % {modulo} == 0 else {dests[1]}')
            dests = []
    else:
      monkeys.append((items, op, test))

  return monkeys, overall_modulo

def monkey_business(
    monkeys: list[Monkey], rounds: int,
    worry: Callable[[int], int] = lambda x: x // 3) -> int:

  monkeys = deepcopy(monkeys)
  counts = [0] * len(monkeys)

  for _ in range(rounds):
    for i, (items, op, dest) in enumerate(monkeys):
      for item in items:
        item = worry(op(item))
        monkeys[dest(item)][0].append(item)
      counts[i] += len(items)
      del items[0:]

  return math.prod(sorted(counts)[-2:])

def run() -> None:
  monkeys, modulo = parse_monkeys(aoc.inputfile('input.txt'))
  print(f'Monkey business (20 rounds): {monkey_business(monkeys, 20)}')
  print(f'Monkey business (10000 rounds): {monkey_business(monkeys, 10000, lambda x: x % modulo)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
