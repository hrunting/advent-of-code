#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def forest_from_file(file: pathlib.Path) -> list[list[int]]:
  trees: list[list[int]] = []
  with open(file) as f:
    while line := f.readline().rstrip():
      trees.append([int(x) for x in line])

  return trees

def run() -> None:
  trees = forest_from_file(aoc.inputfile('input.txt'))
  visible = [[False for _ in row] for row in trees]
  scores = [[1 for _ in row] for row in trees]

  for _ in range(4):
    for r, row in enumerate(trees):
      maxheight = -1
      indices = [0]

      for c, tree in enumerate(row):
        if tree > maxheight:
          visible[r][c] = True
          maxheight = tree

        while indices[-1] > 0 and trees[r][indices[-1]] < tree:
          indices.pop()

        scores[r][c] *= c - indices[-1]
        indices.append(c)

    trees = [list(row) for row in zip(*trees[::-1])]
    visible = [list(row) for row in zip(*visible[::-1])]
    scores = [list(row) for row in zip(*scores[::-1])]

  visible_count = sum(int(x) for row in visible for x in row)
  highest_score = max(score for row in scores for score in row)

  print(f'Number of visible trees: {visible_count}')
  print(f'Highest scenic score: {highest_score}')

if __name__ == '__main__':
  run()
  sys.exit(0)
