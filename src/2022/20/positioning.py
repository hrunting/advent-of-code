#!/usr/bin/env python3

import pathlib
import sys

from collections import deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def decrypt_nums(nums: list[int], key: int = 1, rounds: int = 1) -> int:
  chain = deque(list(range(len(nums))))
  vals = [n * key for n in nums]
  mid = len(nums) // 2

  for _ in range(rounds):
    for idx in range(len(nums)):
      i = chain.index(idx)
      chain.rotate(-i if i <= mid else len(nums) - i)

      chain.popleft()

      n = vals[idx]
      chain.rotate(-n if n < mid else len(nums) - 1 - n)
      chain.appendleft(idx)

  i = chain.index(vals.index(0))
  chain.rotate(-i if i <= mid else len(nums) - i)

  return sum(vals[chain[i * 1000 % len(nums)]] for i in range(1, 4))

def run() -> None:
  file = aoc.inputfile('input.txt')
  with open(file) as f:
    nums = [int(x.rstrip()) for x in f.readlines()]

  basic_sum = decrypt_nums(nums)
  print(f'Sum of basic grove coordinates: {basic_sum}')

  full_sum = decrypt_nums(nums, 811589153, 10)
  print(f'Sum of full grove coordinates: {full_sum}')

if __name__ == '__main__':
  run()
  sys.exit(0)
