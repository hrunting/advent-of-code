#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def is_contained(lo1: int, hi1: int, lo2: int, hi2: int) -> bool:
  return (lo1 <= lo2 and hi1 >= hi2) or (lo2 <= lo1 and hi2 >= hi1)

def is_overlapping(lo1: int, hi1: int, lo2: int, hi2: int) -> bool:
  return (lo1 <= hi2 and hi1 >= lo2) or (lo2 <= hi1 and hi2 >= lo1)

def run() -> None:
  fully_contained = 0
  overlapping = 0

  with open(aoc.inputfile('input.txt')) as f:
    for line in (line.rstrip() for line in f.readlines()):
      if not line:
        continue
      elf1_lo, elf1_hi, elf2_lo, elf2_hi = (int(x) for x in re.split(r'[,-]', line))
      fully_contained += int(is_contained(elf1_lo, elf1_hi, elf2_lo, elf2_hi))
      overlapping += int(is_overlapping(elf1_lo, elf1_hi, elf2_lo, elf2_hi))

  print(f'Number of fully contained assignment pairs: {fully_contained}')
  print(f'Number of overlapping assignment pairs: {overlapping}')

if __name__ == '__main__':
  run()
  sys.exit(0)
