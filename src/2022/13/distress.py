#!/usr/bin/env python3

import pathlib
import sys

from functools import cmp_to_key
from math import prod

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Packet = int | list['Packet']

def read_packets(file: pathlib.Path) -> list[list[Packet]]:
  with open(file) as f:
    packets = [eval(output) for output in (line.strip() for line in f) if len(output)]
  return packets

def packet_cmp(a: list[Packet], b: list[Packet]) -> int:
  result = 0

  for i in range(min(len(a), len(b))):
    match isinstance(a[i], list), isinstance(b[i], list):
      case True, True:
        result = packet_cmp(a[i], b[i])         # type: ignore
      case True, False:
        result = packet_cmp(a[i], [b[i]])       # type: ignore
      case False, True:
        result = packet_cmp([a[i]], b[i])       # type: ignore
      case _:
        result = (a[i] > b[i]) - (a[i] < b[i])  # type: ignore

    if result:
      return result

  return (len(a) > len(b)) - (len(a) < len(b))

def run() -> None:
  packets = read_packets(aoc.inputfile('input.txt'))

  ooo_index_sum = sum(
    i + 1
      for i, (a, b) in enumerate(zip(packets[::2], packets[1::2]))
      if packet_cmp(a, b) < 0
  )
  print(f"Out-of-order index sum: {ooo_index_sum}")

  packets.extend([[[2]], [[6]]])
  packets.sort(key=cmp_to_key(packet_cmp))

  decoder_key = prod(
    i + 1
      for i, p in enumerate(packets)
      if packet_cmp(p, [[2]]) == 0 or packet_cmp(p, [[6]]) == 0
  )
  print(f"Decoder key: {decoder_key}")

if __name__ == '__main__':
  run()
  sys.exit(0)
