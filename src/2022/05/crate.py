#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Callable, TextIO

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

StackType = list[str]
CrateMoveType = tuple[int, int, int]
MoverFuncType = Callable[[list[StackType], int, int, int], None]

def parse_input(f: TextIO) -> tuple[list[StackType], list[CrateMoveType]]:
  stacks = []
  directions = []

  # stacks are described until the first empty line
  while line := f.readline().rstrip():

    # skip stack labeling
    if line.startswith(" 1 "):
      continue

    for stack, idx in enumerate(range(1, len(line), 4)):
      if stack >= len(stacks):
        stacks.append([])
      if line[idx] != " ":
        stacks[stack].insert(0, line[idx])

  # everything else is a crate move
  matcher = re.compile(r'move (\d+) from (\d+) to (\d+)')
  while line := f.readline().rstrip():
    if m := matcher.match(line):
      (count, src, dest) = (int(x) for x in m.groups())
      directions.append((count, src - 1, dest - 1))
  
  return stacks, directions

def cm9000_mover(stacks: list[StackType], count: int, src: int, dest: int) -> None:
  stacks[dest].extend(stacks[src][:-(count + 1):-1])
  stacks[src] = stacks[src][:-count]

def cm9001_mover(stacks: list[StackType], count: int, src: int, dest: int) -> None:
  stacks[dest].extend(stacks[src][-count:])
  stacks[src] = stacks[src][:-count]

def move_crates(input_file: pathlib.Path, mover: MoverFuncType) -> str:
  with open(input_file) as f:
    stacks, directions = parse_input(f)

  for count, src, dest in directions:
    mover(stacks, count, src, dest)
  
  return''.join(stack[-1] for stack in stacks)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  print(f'Crates at top of each stack (CM9000): {move_crates(input_file, cm9000_mover)}')
  print(f'Crates at top of each stack (CM9001): {move_crates(input_file, cm9001_mover)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
