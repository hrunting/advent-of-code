#!/usr/bin/env python3

import pathlib
import sys

from heapq import heappop, heappush
from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_map(file: pathlib.Path) -> tuple[dict[complex, int], complex, complex]:
  map = {}
  start = end = 0 + 0j

  with open(file) as f:
    for r, line in enumerate(line.strip() for line in f.readlines()):
      for c, height in enumerate(line):
        pos = r + c * 1j
        match height:
          case 'S':
            start = pos
            height = 'a'
          case 'E':
            end = pos
            height = 'z'
        map[pos] = ord(height)

    return map, start, end

def shortest_path_len(
    map: dict[complex, int],
    start: complex,
    target: set[complex],
    check: Callable[[int, int], bool]) -> int:

  steps, pos = 0, start
  visited = set()
  pushes = 0

  q: list[tuple[int, int, complex]] = [(0, 0, start)]

  while len(q) > 0:
    steps, _, pos = heappop(q)
    if pos in visited:
      continue
    if pos in target:
      break

    visited.add(pos)

    for move in (pos + 1j, pos + 1, pos - 1j, pos - 1):
      if move not in visited and move in map and check(map[pos], map[move]):
        pushes += 1
        heappush(q, (steps + 1, pushes, move))

  return steps

def run() -> None:
  map, start, end = read_map(aoc.inputfile('input.txt'))

  signal_steps = shortest_path_len(map, start, {end}, lambda a, b: b <= a + 1)
  scenic_steps = shortest_path_len(
    map,
    end,
    {k for k, v in map.items() if v == ord("a")},
    lambda a, b: b >= a - 1
  )

  print(f'Steps to best signal: {signal_steps}')
  print(f'Steps of most scenic path: {scenic_steps}')

if __name__ == '__main__':
  run()
  sys.exit(0)
