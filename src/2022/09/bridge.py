#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def single_knot_visited(motions: list[tuple[str, int]]) -> int:
  head = tail = 0 + 0j
  visited = {tail}

  for move, dist in motions:
    match move:
      case 'U':
        head += dist * 1j
        if abs(head - tail) >= 2:
          visited |= {head - i * 1j for i in range(1, int(head.imag - tail.imag))}
          tail = head - 1j
      case 'D':
        head -= dist * 1j
        if abs(head - tail) >= 2:
          visited |= {head + i * 1j for i in range(1, int(tail.imag - head.imag))}
          tail = head + 1j
      case 'L':
        head -= dist
        if abs(head - tail) >= 2:
          visited |= {head + i for i in range(1, int(tail.real - head.real))}
          tail = head + 1
      case 'R':
        head += dist
        if abs(head - tail) >= 2:
          visited |= {head - i for i in range(1, int(head.real - tail.real))}
          tail = head - 1

  return len(visited)

def multi_knot_visited(knots: int, motions: list[tuple[str, int]]) -> int:
  chain = [0 + 0j for _ in range(knots)]
  visited = {chain[-1]}

  for move, dist in motions:
    for i in range(dist):
      match move:
        case 'U':
          chain[0] += 1j
        case 'D':
          chain[0] -= 1j
        case 'L':
          chain[0] -= 1
        case 'R':
          chain[0] += 1

      for knot in range(1, knots):
        if abs(chain[knot] - chain[knot - 1]) < 2:
          break

        if chain[knot].imag == chain[knot - 1].imag:
          chain[knot] += 1 if chain[knot - 1].real > chain[knot].real else -1
        elif chain[knot].real == chain[knot - 1].real:
          chain[knot] += 1j if chain[knot - 1].imag > chain[knot].imag else -1j
        else:
          chain[knot] += 1 if chain[knot - 1].real > chain[knot].real else -1
          chain[knot] += 1j if chain[knot - 1].imag > chain[knot].imag else -1j

      visited.add(chain[-1])

  return len(visited)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    motions = [(move, int(dist)) for move, dist in (line.rstrip().split(" ") for line in f)]

  single_knot = single_knot_visited(motions)
  print(f'Positions visited (one knot): {single_knot}')

  multi_knot = multi_knot_visited(10, motions)
  print(f'Positions visited (10 knots): {multi_knot}')

if __name__ == '__main__':
  run()
  sys.exit(0)
