#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class BluePrint():
  def __init__(self: Self, blueprint: str) -> None:
    id, robots = blueprint[10:-1].split(": ")
 
    self.id = int(id)
    self.robots = {}
 
    for i, robot in enumerate(reversed(robots.split(". "))):
      typ, costs = robot[5:].split(" robot costs ")
      self.robots[typ] = {
        resource: int(cnt)
          for cnt, resource in (cost.split(" ")
          for cost in costs.split(" and "))
      }

    self.maxes = {
      t: max(res.get(t, 0)
        for res in self.robots.values())
        for t in self.robots.keys()
    }

  def __str__(self: Self) -> str:
    return f'Blueprint {self.id}: {self.robots}'

  def geodes(self: Self, minutes: int) -> int:
    resources = {t: 0 for t in self.robots}
    robots = {t: int(t == "ore") for t in self.robots}

    q = []
    q.append((minutes, resources, robots, None))
    max_geodes = 0

    while len(q):
      time, resources, robots, last = q.pop()

      # we reached the end, consider the candidate for the max
      if time == 0:
        max_geodes = max(max_geodes, resources["geode"])
        continue

      # this path can never beat our current maximum geode count
      if max_geodes - resources["geode"] >= (time * (2 * robots["geode"] + time - 1)) // 2:
        continue

      time -= 1
      wait = False

      for typ, res in self.robots.items():

        # if we already generate enough resources for this type
        # we don't need to create another robot to generate more
        if typ != "geode" and robots[typ] * time + resources[typ] > self.maxes[typ] * time:
          continue

        # don't create one of these if we could have created one last time
        if last is None and all(v <= resources[t] - robots[t] for t, v in res.items()):
          continue

        # we don't have enough resources to create a robot
        # if other robots did something, we could get enough resources, though
        if any(resources[t] < v for t, v in res.items()):
          wait = wait or all(robots[t] > 0 for t in res.keys())
          continue

        next_resources = {t: v + robots[t] - res.get(t, 0) for t, v in resources.items()}
        next_robots = {t: v + int(t == typ) for t, v in robots.items()}

        q.append((time, next_resources, next_robots, typ))

      if wait:
        next_resources = {t: v + robots[t] for t, v in resources.items()}
        q.append((time, next_resources, robots, None))

    return max_geodes

def read_blueprints(file: pathlib.Path) -> list[BluePrint]:
  with open(file) as f:
    blueprints = [BluePrint(line.rstrip()) for line in f.readlines()]
  return blueprints

def run() -> None:
  file = aoc.inputfile('input.txt')
  blueprints = read_blueprints(file)

  quality_level = sum(bp.id * bp.geodes(24) for bp in blueprints)
  print(f'Total quality level: {quality_level}')

  geodes = [bp.geodes(32) for bp in blueprints[:3]]
  print(f'First 3 quality levels multiplied: {math.prod(geodes)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
