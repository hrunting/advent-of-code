#!/usr/bin/env python3

import pathlib
import sys

from collections import Counter

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def start_marker(signal: str, size: int) -> int:
  window = Counter(signal[:size])
  idx = size

  for idx in range(size, len(signal)):
    if len(window) == size:
      break

    window -= Counter(signal[idx - size])
    window += Counter(signal[idx])

  return idx

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as f:
    signal = f.read().rstrip()

  print(f'Start-of-packet position: {start_marker(signal, 4)}')
  print(f'Start-of-message position: {start_marker(signal, 14)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
