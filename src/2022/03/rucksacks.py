#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

PRIORITIES = \
  {chr(idx + 96): idx for idx in range(1, 27)} | \
  {chr(idx + 38): idx for idx in range(27, 53)}

def rucksack_priority(contents: str) -> int:
  first_compartment = set()
  second_compartment = set()

  mid = len(contents) // 2

  for i in range(mid):
    first_compartment.add(contents[i])
    second_compartment.add(contents[i + mid])

    if contents[i] in second_compartment:
      return PRIORITIES[contents[i]]
    elif contents[i + mid] in first_compartment:
      return PRIORITIES[contents[i + mid]]

  return 0

def run() -> None:
  priority_sum = 0
  badge_sum = 0
  group = []

  with open(aoc.inputfile('input.txt')) as f:
    for contents in (line.strip() for line in f.readlines()):
      priority_sum += rucksack_priority(contents)

      group.append(set(contents))
      if len(group) == 3:
        badge_sum += PRIORITIES[list(set.intersection(*group))[0]]
        group = []

  print(f'Sum of priorities for mis-packed items: {priority_sum}')
  print(f'Sum of badge priorities: {badge_sum}')

if __name__ == '__main__':
  run()
  sys.exit(0)
