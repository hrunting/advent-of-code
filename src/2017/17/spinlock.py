#!/usr/bin/env python3

import sys

from collections import deque

STEP = 335

def run() -> None:
  d = deque([0])
  for i in range(1, 2018, 1):
    d.rotate(STEP)
    d.insert(0, i)

  print(f"Value after 2017: {d[-1]}")

  for i in range(2018, 50000001, 1):
    d.rotate(STEP)
    d.insert(0, i)

  print(f"Value after 0 (after 50,000,000 inserts): {d[d.index(0) - 1]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
