#!/usr/bin/env python3

import math
import pathlib
import re
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Vector3D = tuple[int, int, int]
Point3D = tuple[int, int, int]

class Particle:
  def __init__(self: Self, p: Point3D, v: Vector3D, a: Vector3D ):
    self.p = p
    self.v = v
    self.a = a

  def _sign(self: Self, v: int) -> float:
    return math.copysign(1, v) if v != 0 else 0.0

  def distance(self: Self, p: tuple[int, int, int] = (0, 0, 0)) -> int:
    return abs(self.p[0] - p[0]) + abs(self.p[1] - p[1]) + abs(self.p[2] - p[2])

  def outbound(self):
    away_x, vel_x = \
      self._sign(self.v[0]) == self._sign(self.a[0] if self.a[0] != 0 else self.v[0]) and \
      self._sign(self.v[0]) == self._sign(self.p[0] if self.p[0] != 0 else self.v[0]), \
      abs(self.v[0])

    away_y, vel_y = \
      self._sign(self.v[1]) == self._sign(self.a[1] if self.a[1] != 0 else self.v[1]) and \
      self._sign(self.v[1]) == self._sign(self.p[1] if self.p[1] != 0 else self.v[1]), \
      abs(self.v[1])

    away_z, vel_z = \
      self._sign(self.v[2]) == self._sign(self.a[2] if self.a[2] != 0 else self.v[2]) and \
      self._sign(self.v[2]) == self._sign(self.p[2] if self.p[2] != 0 else self.v[2]), \
      abs(self.v[2])

    net_v = 0
    net_v += vel_x if away_x else -vel_x
    net_v += vel_y if away_y else -vel_y
    net_v += vel_z if away_z else -vel_z

    return net_v > 0

  def move(self: Self) -> None:
    self.v = (self.v[0] + self.a[0], self.v[1] + self.a[1], self.v[2] + self.a[2])
    self.p = (self.p[0] + self.v[0], self.p[1] + self.v[1], self.p[2] + self.v[2])

  def velocity(self: Self) -> int:
    return sum(abs(x) for x in self.v)

  def acceleration(self: Self) -> int:
    return sum(abs(x) for x in self.a)

def parse(data: str) -> list[Particle]:
  particles = []

  for line in data.split("\n"):
    if m := re.match(r"p=<([^>]+)>, v=<([^>]+)>, a=<([^>]+)>", line):
      p, v, a = m.group(1, 2, 3)
      (px, py, pz) = (int(d) for d in p.split(","))
      (vx, vy, vz) = (int(d) for d in v.split(","))
      (ax, ay, az) = (int(d) for d in a.split(","))
      particles.append(Particle((px, py, pz), (vx, vy, vz), (ax, ay, az)))

  return particles

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  particles = parse(data)
  inbound = set(range(len(particles)))

  while len(inbound) > 0:
    for i, p in enumerate(particles):
      p.move()
      if p.outbound():
        inbound.discard(i)

  min_particle = sorted(
    range(len(particles)),
    key=lambda i: (particles[i].acceleration(), particles[i].velocity(), particles[i].distance())
  )[0]

  print(f"Closest particle: {min_particle}")

  particles = parse(data)
  while True:
    seen = {}
    for i, p in enumerate(particles):
      p.move()

      if p.p in seen:
        seen[p.p].append(i)
      else:
        seen[p.p] = [i]

    removed = set()
    for _, found in seen.items():
      if len(found) <= 1:
        continue

      for i in found:
        removed.add(i)

    particles = [p for i, p in enumerate(particles) if i not in removed]

    inbound = sum(1 for p in particles if not p.outbound())
    if inbound == 0:

      # this is not /actually/ true
      break

      # for some cases, you could have a situation
      # where the particle is way out and outbound,
      # but eventually a closer particle out-accelerates it
      # and collides with it
      #
      # I believe the following code solves for that, but it
      # takes way too long to simulate with the particle set given
      # and the particle set given resolves the moment everything
      # is outbound
      p_by_accel = sorted(
        range(len(particles)),
        key=lambda i: (particles[i].acceleration(), particles[i].velocity())
      )
      p_by_distance = sorted(range(len(particles)), key=lambda i: particles[i].distance())

      if p_by_accel == p_by_distance:
        break

  print(f"Particles left: {len(particles)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
