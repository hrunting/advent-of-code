#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  def __init__(self: Self, instrs: list[str]) -> None:
    self.state = ""
    self.steps = 0
    self.states = {}
    self.tape = {}
    self.pos = 0

    state = {}
    value = 0

    for line in instrs:
      if line.startswith("Begin"):
        self.state = line[-2:-1]
      elif line.startswith("Perform"):
        self.steps = int(line[line.index("after ") + 6:line.index(" steps")])
      elif line.startswith("In state"):
        state_id = line[-2:-1]
        self.states[state_id] = {}
        state = self.states[state_id]
      elif line.startswith("  If the current value"):
        value = int(line[-2:-1])
        state[value] = [None, None, None]
      elif line.startswith("    - Write the value"):
        state[value][0] = int(line[-2:-1])
      elif line.startswith("    - Move one slot to the"):
        move = -1 if line[line.index("Move") + 21:-1] == "left" else 1
        state[value][1] = move
      elif line.startswith("    - Continue with state"):
        state[value][2] = line[-2:-1]

  def run_diagnostic(self: Self) -> int:
    for _ in range(self.steps):
      write_val, move_dir, next_state = self.states[self.state][self.tape.get(self.pos, 0)]
      self.tape[self.pos] = write_val
      self.pos += move_dir
      self.state = next_state

    return sum(self.tape.values())

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  machine = Machine(data.split("\n"))
  checksum = machine.run_diagnostic()
  print(f"Checksum: {checksum}")

if __name__ == '__main__':
  run()
  sys.exit(0)
