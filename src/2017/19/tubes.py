#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(data: str) -> tuple[dict[tuple[int, int], str], tuple[int, int]]:
  grid = {}
  start = (0, 0)

  for y, row in enumerate(data.split("\n")):
    for x, cell in enumerate(row):
      if cell == " ":
       continue

      grid[(y, x)] = cell
      if y == 0 and cell == "|":
        start = (y, x)

  return grid, start

def walk_path(grid: dict[tuple[int, int], str], start: tuple[int, int]) -> tuple[str, int]:
  sequence = ""
  steps = 0

  pos = start
  d = (1, 0)

  while True:
    if pos not in grid:
      break

    elif grid[pos] == "+":
      adj = ((1, 0), (-1, 0)) if d[0] == 0 else ((0, 1), (0, -1))
      d = next(a for a in adj if (pos[0] + a[0], pos[1] + a[1]) in grid)

    elif grid[pos].isupper():
      sequence += grid[pos]

    steps += 1
    pos = (pos[0] + d[0], pos[1] + d[1])

  return sequence, steps

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  tubes, start = parse(data)
  sequence, steps = walk_path(tubes, start)
  print(f"Sequence: {sequence}")
  print(f"Steps: {steps}")

if __name__ == '__main__':
  run()
  sys.exit(0)
