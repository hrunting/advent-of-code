#!/usr/bin/env python3

import pathlib
import sys

from itertools import combinations

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  rows = [[int(x) for x in row.split("\t")] for row in input_file.open().read().splitlines()]

  checksum = sum(max(row) - min(row) for row in rows)
  print(f"Checksum: {checksum}")

  result = sum(next(y // x for x, y in combinations(sorted(row), 2) if y % x == 0) for row in rows)
  print(f"Divisible sum: {result}")

if __name__ == '__main__':
  run()
  sys.exit(0)
