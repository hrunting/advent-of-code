#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Any

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(line: str) -> tuple[str, int, list[str]]:
  if m := re.search(r"^(\S+) \((\d+)\)(?: -> (.*))?", line):
    program, weight, supporting = m.group(1, 2, 3)
    return program, int(weight), supporting.split(", ") if supporting is not None else []
  else:
    return "", -1, []

def tree_weight(tree: dict[str, dict[str, Any]], node: str, adjusted: list[int]) -> int:
  node_weight = tree[node]["weight"]
  adjustment = 0

  stack_weights = [tree_weight(tree, n, adjusted) for n in tree[node]["stack"]]

  if len(tree[node]["stack"]) > 3:
    min_weight = min(stack_weights)
    max_weight = max(stack_weights)

    if min_weight != max_weight:
      if stack_weights.count(min_weight) == 1:
        bad_weight = min_weight
        adjustment = max_weight - min_weight
      else:
        bad_weight = max_weight
        adjustment = min_weight - max_weight

      idx = stack_weights.index(bad_weight)
      adjusted.append(tree[tree[node]["stack"][idx]]["weight"] + adjustment)

  return node_weight + sum(stack_weights) + adjustment

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  tree = {}
  parents = {}

  for line in data.split("\n"):
    program, weight, stack = parse(line)
    tree[program] = {"weight": weight, "stack": stack}
    for p in stack:
      parents[p] = program

  bottom = next(p for p in tree if p not in parents)
  print(f"Bottom program: {bottom}")

  adjusted_weights = []
  weight = tree_weight(tree, bottom, adjusted_weights)
  print(f"Adjusted weight: {adjusted_weights[0]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
