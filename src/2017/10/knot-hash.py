#!/usr/bin/env python3

import functools
import operator
import pathlib
import sys

from collections import deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def hash_round(d: deque, lengths: list[int], skip: int = 0, pos: int = 0) -> tuple[int, int]:
  for length in lengths:
    d.extendleft([d.popleft() for _ in range(length)])
    d.rotate(-(length + skip))
    pos += length + skip
    skip += 1

  return skip, pos % len(d)

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  lengths = [int(x) for x in data.split(",")]
  d = deque(range(256))
  _, pos = hash_round(d, lengths)
  d.rotate(pos)
  print(f"First two product: {d[0] * d[1]}")

  d = deque(range(256))
  lengths = [ord(x) for x in data] + [17, 31, 73, 47, 23]

  skip, pos = 0, 0
  for _ in range(64):
    skip, pos = hash_round(d, lengths, skip, pos)
  d.rotate(pos)

  sparse = list(d)
  dense = [functools.reduce(operator.xor, sparse[i:i + 16]) for i in range(0, len(sparse), 16)]
  knot_hash = "".join(f"{x:02x}" for x in dense)
  print(f"Knot hash: {knot_hash}")

if __name__ == '__main__':
  run()
  sys.exit(0)
