#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Port = tuple[int, int]

def parse(data: str) -> dict[int, set[Port]]:
  ports = {}
  for port in data.split("\n"):
    a, b = port.split(r"/")
    ports[int(a)] = ports.get(int(a), set()) | {(int(a), int(b))}
    ports[int(b)] = ports.get(int(b), set()) | {(int(a), int(b))}
  return ports

def longest_bridge(
    ports: dict[int, set[Port]],
    pins: int = 0,
    used: set[Port] = set(),
    cache: dict[str, Port] = {}) -> tuple[int, int]:

  cachekey = str(pins) + ":" + "".join(str(p) for p in sorted(used))

  if cachekey not in cache:
    bridges: list[tuple[int, int]] = [(0, 0)]

    for port in ports[pins]:
      if port in used:
        continue

      other_pins = port[1] if port[0] == pins else port[0]
      length, strength = longest_bridge(ports, other_pins, used | {port}, cache)
      bridges.append((1 + length, sum(port) + strength))

    cache[cachekey] = max(bridges)

  return cache[cachekey]

def strongest_bridge(
    ports: dict[int, set[Port]],
    pins: int = 0,
    used: set[Port] = set(),
    cache: dict[str, int] = {}) -> int:

  cachekey = str(pins) + ":" + "".join(str(p) for p in sorted(used))

  if cachekey not in cache:
    strengths = [0]

    for port in ports[pins]:
      if port in used:
        continue

      other_pins = port[1] if port[0] == pins else port[0]
      strength = strongest_bridge(ports, other_pins, used | {port}, cache)
      strengths.append(sum(port) + strength)

    cache[cachekey] = max(strengths)

  return cache[cachekey]

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  ports = parse(data)

  strength = strongest_bridge(ports)
  print(f"Strength of strongest bridge: {strength}")

  _, strength = longest_bridge(ports)
  print(f"Strength of longest bridge: {strength}")

if __name__ == '__main__':
  run()
  sys.exit(0)
