#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  max_register_value = 0
  registers = {}

  ops = {
    "inc": lambda x, y: x + y,
    "dec": lambda x, y: x - y
  }

  cmp_ops = {
    ">": lambda x, y: x > y,
    "<": lambda x, y: x < y,
    ">=": lambda x, y: x >= y,
    "<=": lambda x, y: x <= y,
    "!=": lambda x, y: x != y,
    "==": lambda x, y: x == y
  }

  for cmd in data.split("\n"):
    r1, op, val, _, r2, cmp_op, cmp_val = cmd.split(" ")

    val = int(val)
    cmp_val = int(cmp_val)

    if cmp_ops[cmp_op](registers.get(r2, 0), cmp_val):
      registers[r1] = ops[op](registers.get(r1, 0), val)
      max_register_value = max((registers[r1], max_register_value))

  print(f"Max register value left: {max(registers.values())}")
  print(f"Max register value ever: {max_register_value}")

if __name__ == '__main__':
  run()
  sys.exit(0)
