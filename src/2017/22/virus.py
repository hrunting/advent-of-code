#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(data: str) -> tuple[dict[complex, int], complex]:
  grid = {}

  rows = data.split("\n")
  start = len(rows[0]) // 2 + (len(rows) // 2) * 1j

  for y, row in enumerate(rows):
    for x, val in enumerate(row):
      if val == "#":
        grid[x + y * 1j] = 1

  return grid, start

def render(grid: dict[complex, int]) -> str:
  output = ""

  min_x = int(min(k.real for k in grid.keys()))
  max_x = int(max(k.real for k in grid.keys()))
  min_y = int(min(k.imag for k in grid.keys()))
  max_y = int(max(k.imag for k in grid.keys()))

  for y in range(min_y, max_y + 1):
    for x in range(min_x, max_x + 1):
      pos = x + y *  1j
      if grid.get(pos, 0) == 0:
        output += "."
      elif grid[pos] == 1:
        output += "#"
      elif grid[pos] == 2:
        output += "W"
      else:
        output += "F"

    output += "\n"

  return output

def burst(grid: dict[complex, int], start: complex, n: int) -> int:
  infections = 0
  pos = start
  d = -1j

  for _ in range(n):
    if grid.get(pos, 0) == 0:
      d *= -1j
      grid[pos] = 1
      infections += 1
    else:
      d *= 1j
      grid[pos] = 0

    pos += d

  return infections

def superburst(grid: dict[complex, int], start: complex, n: int) -> int:
  infections = 0
  pos = start
  d = -1j

  for _ in range(n):
    node = grid.get(pos, 0)

    if node == 0:
      d *= -1j
      grid[pos] = 2
    elif node == 1:
      d *= 1j
      grid[pos] = 3
    elif node == 2:
      grid[pos] = 1
      infections += 1
    else:
      d *= -1
      grid[pos] = 0

    pos += d

  return infections

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  grid, start = parse(data)
  infections = burst(grid, start, 10000)
  print(f"Infections: {infections}")

  grid, start = parse(data)
  infections = superburst(grid, start, 10000000)
  print(f"Infections: {infections}")

if __name__ == '__main__':
  run()
  sys.exit(0)
