#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def spin(grp: list[str], n: int) -> list[str]:
  return grp[-n:] + grp[:-n]

def exchange(grp: list[str], a: int, b: int) -> list[str]:
  grp[a], grp[b] = grp[b], grp[a]
  return grp

def partner(grp: list[str], a: str, b: str) -> list[str]:
  return exchange(grp, grp.index(a), grp.index(b))

def dance(group: list[str], moves: list[str]) -> list[str]:
  for move in moves:
    t, rest = move[0], move[1:]

    if t == "s":
      group = spin(group, int(rest))
    elif t == "x":
      a, b = rest.split("/")
      group = exchange(group, int(a), int(b))
    elif t == "p":
      a, b = rest.split("/")
      group = partner(group, a, b)

  return group

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  moves = data.split(",")
  group = dance(list(chr(ord("a") + x) for x in range(16)), moves)
  print(f"First dance order: {''.join(group)}")

  dances = 1
  states = {"abcdefghijklmnop": 0, "".join(group): 1}

  while dances < 1000000000:
    dances += 1
    group = dance(group, moves)
    state = "".join(group)

    if state in states:
      loop = dances - states[state]
      dances += (1000000000 - dances) // loop * loop
      states = {}

  print(f"Order after 1B dances: {''.join(group)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
