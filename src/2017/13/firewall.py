#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(data: str) -> dict[int, int]:
  firewall = {}
  for line in data.split("\n"):
    k, v = line.split(": ", 1)
    firewall[int(k)] = int(v)
  return firewall

def penalty(firewall: dict[int, int], delay: int = 0) -> int:
  return sum(k * v for k, v in firewall.items() if k % ((v - 1) * 2) == 0)

def caught(firewall: dict[int, int], delay: int = 0) -> bool:
  for k, v in firewall.items():
    repeat = (v - 1) * 2
    if ((delay % repeat) + (k % repeat)) % repeat == 0:
      return True

  return False

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  firewall = parse(data)
  severity = penalty(firewall)
  print(f"Penalty: {severity}")

  delay = 0
  while True:
    delay += 1
    if not caught(firewall, delay):
      break

  print(f"Fewest picoseconds: {delay}")

if __name__ == '__main__':
  run()
  sys.exit(0)
