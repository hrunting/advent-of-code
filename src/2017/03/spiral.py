#!/usr/bin/env python3

import math
import sys

def run() -> None:
  DATA = 312051

  last_ring_size = int(math.sqrt(DATA))
  if last_ring_size % 2 == 0:
    last_ring_size -= 1

  # number of hops from center position to our ring
  distance = math.ceil(last_ring_size / 2)

  # number of hops from next greatest corner of our ring to our position
  corner_distance = (DATA - (last_ring_size ** 2)) % (last_ring_size + 1)

  distance += abs(distance - corner_distance)
  print(f"Distance: {distance}")

  # precomputed list of adjacencies
  adjacencies = [r + i for r in (-1, 0, 1) for i in (-1j, 0j, 1j) if r != i]

  pos = 0 + 0j
  turn = -1j
  nodes = { pos: 1 }

  while nodes[pos] <= DATA:

    # we start by trying to turn left (1j)
    # if that spot is taken, we keep going the same direction
    next_pos = pos + turn * 1j
    if next_pos not in nodes:
      turn *= 1j
    else:
      next_pos = pos + turn

    nodes[next_pos] = sum(nodes.get(next_pos + p, 0) for p in adjacencies)
    pos = next_pos

  print(f"First value written: {nodes[pos]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
