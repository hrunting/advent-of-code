#!/usr/bin/env python3

import pathlib
import sys

from typing import Generator, Optional

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

FACTOR_A = 16807
FACTOR_B = 48271

def spinner() -> Generator[str, None, None]:
  while True:
    for cursor in "|/-\\":
      yield cursor

def spin(s: Generator[str, None, None]) -> None:
  sys.stdout.write(next(s))
  sys.stdout.flush()
  sys.stdout.write("\b")

def generate(g: int, factor: int, mult: Optional[int] = None) -> int:
  g *= factor
  if g > 0x7fffffff:
    g = (g >> 31) + (g & 0x7fffffff)
  if g >> 31:
    g -= 0x7fffffff

  if mult is not None:
    mult -= 1
    while g & mult != 0:
      g = generate(g, factor)

  return g

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  lines = data.split("\n")

  gen_a = int(lines[0].split(" ")[-1])
  gen_b = int(lines[1].split(" ")[-1])

  pairs = []
  s = spinner()

  for i in range(40000000):
    if i % 100000 == 0:
      spin(s)

    gen_a = generate(gen_a, FACTOR_A)
    gen_b = generate(gen_b, FACTOR_B)
  
    if gen_a & 0xffff == gen_b & 0xffff:
      pairs.append((gen_a, gen_b))

  print(f"Pairs: {len(pairs)}")

  gen_a = int(lines[0].split(" ")[-1])
  gen_b = int(lines[1].split(" ")[-1])

  pairs = []

  for i in range(5000000):
    if i % 10000 == 0:
      spin(s)

    gen_a = generate(gen_a, FACTOR_A, 4)
    gen_b = generate(gen_b, FACTOR_B, 8)

    if gen_a & 0xffff == gen_b & 0xffff:
      pairs.append((gen_a, gen_b))

  print(f"Aligned pairs: {len(pairs)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
