#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Program:
  R_HALT = 1

  def __init__(self: Self, program: str) -> None:
    self.cmds = self._parse(program)
    self.ptr = 0

    self.registers = {chr(ord("a") + i): 0 for i in range(8)}

  def run(self: Self, op: Optional[str] = None, debug: bool = False) -> int:
    if debug:
      self.registers["a"] = 1

    count = 0

    while self.ptr < len(self.cmds):
      cmd, a, b = self.cmds[self.ptr]
      self.ptr += 1

      if op is not None and cmd == op:
        count += 1

      if cmd == "set":
        self.registers[a] = self._value(b)

      elif cmd == "sub":
        self.registers[a] -= self._value(b)
        if a == "h":
          print(
            f"Registers: a={self.registers['a']} b={self.registers['b']} "
            f"c={self.registers['c']} d={self.registers['d']} e={self.registers['e']} "
            f"f={self.registers['f']} g={self.registers['g']} h={self.registers['h']}"
          )

      elif cmd == "mul":
        self.registers[a] *= self._value(b)

      elif cmd == "jnz":
        if self._value(a) != 0:
          self.ptr += -1 + self._value(b)
          continue

    return count

  def _value(self: Self, x: str) -> int:
    return self.registers[x] if x in self.registers else int(x)

  def _parse(self: Self, data: str) -> list[tuple[str, str, str]]:
    cmds = []
    for line in data.split("\n"):
      cmd, a, b = line.strip().split(" ")
      cmds.append((cmd, a, b))

    return cmds

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  program = Program(data)
  count = program.run("mul")
  print(f"'mul' invocations: {count}")

  # optimized disassembly of co-proc code
  b = 65
  b *= 100
  b += 100000

  h = 0

  for n in range(1001):
    f = 1

    for x in range(2, b + 17 * n):
      if (b + 17 * n) % x == 0:
        f = 0
        break

    if f == 0:
      h += 1

  print(f"Register h: {h}")

if __name__ == '__main__':
  run()
  sys.exit(0)
