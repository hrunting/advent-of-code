#!/usr/bin/env python3

import pathlib
import sys

from typing import Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Program:
  R_HALT = 1
  R_SND = 2
  R_RCV = 3

  def __init__(self: Self, program: str, program_id: int = 0) -> None:
    self.cmds = self._parse(program)
    self.ptr = 0

    self.registers = {chr(ord("a") + i): 0 for i in range(16)}
    self.registers["p"] = program_id

    self.buffer = []
    self.waiting = 0

  def run(self: Self) -> tuple[int, Optional[int]]:
    while self.ptr < len(self.cmds):
      cmd, a, b = self.cmds[self.ptr]
      self.ptr += 1

      if cmd == "snd":
        return (Program.R_SND, self._value(a))

      elif cmd == "rcv":
        if len(self.buffer) == 0:
          self.ptr -= 1

          self.waiting += 1
          if self.waiting >= 4:
            return (Program.R_HALT, None)
            
          return (Program.R_RCV, None)
        else:
          self.registers[a] = self.buffer.pop(0)
          return (Program.R_RCV, self.registers[a])

      elif cmd == "set":
        self.registers[a] = self._value(b)

      elif cmd == "add":
        self.registers[a] += self._value(b)

      elif cmd == "mul":
        self.registers[a] *= self._value(b)

      elif cmd == "mod":
        self.registers[a] %= self._value(b)

      elif cmd == "jgz":
        if self._value(a) > 0:
          self.ptr += -1 + self._value(b)
          continue

    return (Program.R_HALT, None)

  def send(self: Self, val: int) -> None:
    self.buffer.append(val)

  def _value(self: Self, x: str) -> int:
    return self.registers[x] if x in self.registers else int(x)

  def _parse(self: Self, data: str) -> list[tuple[str, str, str]]:
    cmds = []
    for line in data.split("\n"):
      cmd, rest = line.strip().split(" ", 1)
      if cmd == "snd" or cmd == "rcv":
        a = rest
        b = None

      else:
        a, b = rest.split(" ")
        
      cmds.append((cmd, a, b))

    return cmds

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  program = Program(data)
  while True:
    res, val = program.run()

    if res == Program.R_HALT:
      break

    elif res == Program.R_SND and val is not None:
      program.send(val)

    elif res == Program.R_RCV:
      if val != 0:
        break

  print(f"First recovered value: {program.buffer[-1]}")

  programs = {i: Program(data, i) for i in range(2)}
  counter = 0
  running = True

  while running:
    running = False

    for i, program in programs.items():
      res, val = program.run()
      if res == Program.R_HALT:
        break

      elif res == Program.R_SND and val is not None:
        programs[int(not i)].send(val)
        if i == 1:
          counter += 1

      running = True

  print(f"Number of times Program 1 sent a value: {counter}")

if __name__ == '__main__':
  run()
  sys.exit(0)
