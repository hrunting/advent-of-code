#!/usr/bin/env python3

import functools
import operator
import sys

from collections import deque

INPUT = "oundnydw"

def hash_round(d: deque, lengths: list[int], skip: int = 0, pos: int = 0) -> tuple[int, int]:
  for length in lengths:
    d.extendleft([d.popleft() for _ in range(length)])
    d.rotate(-(length + skip))
    pos += length + skip
    skip += 1

  return skip, pos % len(d)

def knot_hash(s: str) -> str:
  d = deque(range(256))
  lengths = [ord(x) for x in s] + [17, 31, 73, 47, 23]

  skip, pos = 0, 0
  for _ in range(64):
    skip, pos = hash_round(d, lengths, skip, pos)
  d.rotate(pos)

  sparse = list(d)
  dense = [functools.reduce(operator.xor, sparse[i:i + 16]) for i in range(0, len(sparse), 16)]
  return "".join(f"{y:02x}" for y in dense)

def run() -> None:
  grid = [bin(int(knot_hash(INPUT + "-" + str(i)), 16))[2:].zfill(128) for i in range(128)]

  used = sum(row.count("1") for row in grid)
  print(f"Used squares: {used}")

  q = []
  visited = {(x, y) for y in range(len(grid)) for x in range(len(grid[y])) if grid[y][x] == "0"}
  regions = 0

  for y in range(len(grid)):
    for x in range(len(grid[y])):
      if (x, y) in visited:
        continue

      regions += 1
      visited.add((x, y))
      q.append((x, y))

      while len(q):
        qx, qy = q.pop(0)

        for ax, ay in ((qx + 1, qy), (qx, qy + 1), (qx - 1, qy), (qx, qy - 1)):
          if ay < 0 or ax < 0 or ay >= len(grid) or ax >= len(grid[ay]):
            continue

          if (ax, ay) in visited:
            continue

          visited.add((ax, ay))
          q.append((ax, ay))

  print(f"Regions: {regions}")

if __name__ == '__main__':
  run()
  sys.exit(0)
