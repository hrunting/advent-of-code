#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  scores = []
  score = 0
  chars = 0

  garbage = False
  skip = False
  
  for x in data:
    if skip:
      skip = False
    elif x == "!":
      skip = True
    elif x == ">":
      garbage = False
    elif not garbage and x == "<":
      garbage = True
    elif not garbage and x == "}":
      scores.append(score)
      score -= 1
    elif not garbage and x == "{":
      score += 1
    elif garbage:
      chars += 1

  print(f"Total score: {sum(scores)}")
  print(f"Total garbage: {chars}")

if __name__ == '__main__':
  run()
  sys.exit(0)
