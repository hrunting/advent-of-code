#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

dirs = {
  "n": (0, 1, 1),
  "ne": (1, 0, 1),
  "se": (1, -1, 0),
  "s": (0, -1, -1),
  "sw": (-1, 0, -1),
  "nw": (-1, 1, 0)
}

Coord = tuple[int, int, int]

def move(pt: Coord, d: Coord) -> Coord:
  return (pt[0] + d[0], pt[1] + d[1], pt[2] + d[2])

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  pos = (0, 0, 0)
  max_steps = 0

  for d in data.split(","):
    pos = move(pos, dirs[d])
    max_steps = max((max_steps, *pos))

  print(f"Position: {pos}")

  steps = max(pos)
  print(f"Steps: {steps}")
  print(f"Furthest Steps: {max_steps}")

if __name__ == '__main__':
  run()
  sys.exit(0)
