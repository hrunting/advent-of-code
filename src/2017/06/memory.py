#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  banks = [int(x) for x in data.split("\t")]
  states = {}

  while tuple(banks) not in states:
    states[tuple(banks)] = len(states)
    ptr = banks.index(max(banks))

    blocks = banks[ptr]
    banks[ptr] = 0
    while blocks > 0:
      ptr += 1
      if ptr == len(banks):
        ptr = 0

      banks[ptr] += 1
      blocks -= 1

  print(f"Cycles until repeat: {len(states)}")

  loop_cycles = len(states) - states[tuple(banks)]
  print(f"Cycles in loop: {loop_cycles}")

if __name__ == '__main__':
  run()
  sys.exit(0)
