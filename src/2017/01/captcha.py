#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  digits = input_file.open().read().strip()

  solution = sum(int(d) for d in re.findall(r"(\d)(?=\1)", digits + digits[0]))
  print(f"Part 1 captcha solution: {solution}")

  halfway = len(digits) // 2
  pattern = r"(\d)(?=.{" + str(halfway - 1) + r"}\1)"
  solution = sum(int(d) for d in re.findall(pattern, digits + digits[:halfway]))
  print(f"Part 2 captcha solution: {solution}")

if __name__ == '__main__':
  run()
  sys.exit(0)
