#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(data: str) -> dict[str, list[str]]:
  pipes = {}
  for line in data.split("\n"):
    program, _, partners = line.split(" ", 2)
    pipes[program] = partners.split(", ")
  return pipes

def walk_program(pipes: dict[str, list[str]], program_id: str = "0") -> set[str]:
  q = [program_id]
  visited = {program_id}

  while len(q):
    program_id = q.pop(0)

    for partner in pipes[program_id]:
      if partner not in visited:
        visited.add(partner)
        q.append(partner)

  return visited

def walk_groups(pipes: dict[str, list[str]]) -> int:
  visited = walk_program(pipes, "0")
  groups = 1

  for program_id in pipes.keys():
    if program_id not in visited:
      visited |= walk_program(pipes, program_id)
      groups += 1

  return groups

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  pipes = parse(data)
  programs = walk_program(pipes)
  print(f"Number of programs: {len(programs)}")

  groups = walk_groups(pipes)
  print(f"Number of groups: {groups}")

if __name__ == '__main__':
  run()
  sys.exit(0)
