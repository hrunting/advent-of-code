#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def has_dupes(phrase: str) -> bool:
  seen = set()
  for word in phrase.split(" "):
    if word in seen:
      return True
    seen.add(word)
  return False

def has_anagrams(phrase: str) -> bool:
  seen = set()
  for word in phrase.split(" "):
    normalized = "".join(sorted(x for x in word))
    if normalized in seen:
      return True
    seen.add(normalized)
  return False

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  passphrases = data.split("\n")

  valid = [x for x in passphrases if not has_dupes(x)]
  print(f"Passphrases w/o dupes; {len(valid)}")

  valid = [x for x in passphrases if not has_anagrams(x)]
  print(f"Passphrases w/o anagrams: {len(valid)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
