#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().strip()

  offsets = [int(x) for x in data.split("\n")]
  ptr = 0
  steps = 0

  while 0 <= ptr and ptr < len(offsets):
    steps += 1
    offsets[ptr] += 1
    ptr += offsets[ptr] - 1

  print(f"Steps to exit: {steps}")

  offsets = [int(x) for x in data.split("\n")]
  ptr = 0
  steps = 0

  while 0 <= ptr and ptr < len(offsets):
    steps += 1
    jump = offsets[ptr]
    offsets[ptr] += 1 if jump < 3 else -1
    ptr += jump

  print(f"Steps to exit: {steps}")

if __name__ == '__main__':
  run()
  sys.exit(0)
