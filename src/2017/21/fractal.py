#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse(data: str) -> dict[tuple[str, ...], list[str]]:
  enhancements = {}

  for line in data.split("\n"):
    before, after = line.split(" => ")
    before = before.replace("/", "")
    after = after.split("/")

    # 2x2 enhancement pattern
    if len(before) == 4:
      for n in range(4):
        enhancements[(before[:2], before[2:])] = after
        enhancements[(before[2:], before[:2])] = after
        enhancements[(before[1::-1], before[:1:-1])] = after
        before = "".join(before[i] for i in (1, 3, 0, 2))

    # 3x3 enhancement pattern
    else:
      for _ in range(4):
        enhancements[(before[:3], before[3:6], before[6:])] = after
        enhancements[(before[6:], before[3:6], before[:3])] = after
        enhancements[(before[2::-1]), before[5:2:-1], before[:5:-1]] = after
        before = "".join(before[i] for i in (2, 5, 8, 1, 4, 7, 0, 3, 6))

  return enhancements

def iterate(
    n: int,
    pattern: tuple[str, ...],
    book: dict[tuple[str, ...], list[str]]) -> tuple[str, ...]:

  for n in range(n):
    if len(pattern) % 2 == 0:
      enhanced = ["" for _ in range(len(pattern) // 2 * 3)]
      for y in range(0, len(pattern), 2):
        for x in range(0, len(pattern), 2):
          match = (pattern[y][x:x + 2], pattern[y + 1][x:x + 2])
          for i, row in enumerate(book[match]):
            enhanced[(y // 2) * 3 + i] += row
      
    else:
      enhanced = ["" for _ in range(len(pattern) // 3 * 4)]
      for y in range(0, len(pattern), 3):
        for x in range(0, len(pattern), 3):
          match = (pattern[y][x:x + 3], pattern[y + 1][x:x + 3], pattern[y + 2][x:x + 3])
          for i, row in enumerate(book[match]):
            enhanced[(y // 3) * 4 + i] += row

    pattern = tuple(enhanced)

  return pattern

def run() -> None:
  input_file = aoc.inputfile("input.txt")
  data = input_file.open().read().rstrip()

  enhancements = parse(data)
  pattern = (".#.", "..#", "###")

  result = iterate(5, pattern, enhancements)
  print(f"Pixels on after 5 iterations: {sum(x.count('#') for x in result)}")

  result = iterate(18, pattern, enhancements)
  print(f"Pixels on after 18 iterations: {sum(x.count('#') for x in result)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
