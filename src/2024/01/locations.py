#!/usr/bin/env python3

import pathlib
import sys

from collections import Counter

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  ids_1: list[int] = []
  ids_2: list[int] = []

  with open(aoc.inputfile('input.txt')) as f:
    for line in (s.strip() for s in f.readlines()):
      id1, id2 = line.split()
      ids_1.append(int(id1))
      ids_2.append(int(id2))

  distance = sum(
    abs(id1 - id2)
    for (id1, id2) in zip(sorted(ids_1), sorted(ids_2))
  )

  print(f"Total distance: {distance}")

  counts = Counter(ids_2)
  similarity = sum(id1 * counts[id1] for id1 in ids_1)

  print(f"Similarity score: {similarity}")

if __name__ == '__main__':
  run()
  sys.exit(0)