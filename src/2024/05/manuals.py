#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict
from functools import cmp_to_key
from typing import Sequence

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def is_valid(updates: Sequence[str], rules: defaultdict[str, set[str]]) -> bool:
  seen = set()
  for page in updates:
    if seen & rules[page]:
      return False
    seen.add(page)
  return True

def fix_invalid(updates: Sequence[str], rules: defaultdict[str, set[str]]) -> Sequence[str]:
  def cmp(a: tuple[int, str], b: tuple[int, str]) -> int:
    if a[1] in rules[b[1]]:
      return 1
    if b[1] in rules[a[1]]:
      return -1
    return a[0] - b[0]

  return [x for _, x in sorted(enumerate(updates), key=cmp_to_key(cmp))]

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    text = f.read().strip()
    rulelist, updatelist = (section.split() for section in text.split("\n\n"))

  rules = defaultdict(set)
  for before, after in (rule.split('|') for rule in rulelist):
    rules[before].add(after)

  valid_pages = []
  fixed_pages = []
  for updates in (line.split(',') for line in updatelist):
    if is_valid(updates, rules):
      valid_pages.append(int(updates[(len(updates) // 2)]))
    else:
      updates = fix_invalid(updates, rules)
      fixed_pages.append(int(updates[(len(updates) // 2)]))

  print(f"Sum of valid middle pages: {sum(valid_pages)}")
  print(f"Sum of fixed middle pages: {sum(fixed_pages)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
