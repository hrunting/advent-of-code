#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

MUL_RE = r"mul\((\d{1,3}),(\d{1,3})\)"
COND_RE = r"(do(?:n't)?)\(\)|" + MUL_RE

def multiplications(s: str, conditionals: bool = False) -> int:
  pattern = COND_RE if conditionals else MUL_RE
  summed = 0
  on = True

  for m in re.findall(pattern, s):
    if m[0] == "do":
      on = True
    elif m[0] == "don't":
      on = False
    else:
      a, b = m[1:] if m[0] == "" else m
      summed += on * int(a) * int(b)

  return summed

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    program = f.read()

  print(f"Result sum: {multiplications(program)}")
  print(f"Conditional result sum: {multiplications(program, True)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
