#!/usr/bin/env python3

import pathlib
import sys

from heapq import heappop, heappush

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Position = tuple[int, int]

def shortest_path(
  map: list[str],
  node: Position,
  dir: int,
  end: Position
) -> tuple[int, set[Position]]:
  target = -1
  costs = {}
  prevs = {}

  q: list[tuple[int, Position, int, tuple[Position, int] | None]] = [(0, node, dir, None)]
  while q:
    cost, node, dir, prev = heappop(q)
    if target > 0 and cost > target:
      break

    if (node, dir) in costs:
      if cost == costs[node, dir] and prev:
        prevs[node, dir].add(prev)
      continue

    costs[node, dir] = cost
    prevs[node, dir] = {prev} if prev else set()

    if node == end:
      target = min((cost, ) if target < 0 else (target, cost))
      continue

    prev = (node, dir)
    (x, y) = (node[0] + (1, 0, -1, 0)[dir], node[1] + (0, 1, 0, -1)[dir])

    if map[y][x] != '#':
      heappush(q, (cost + 1, (x, y), dir, prev))
    heappush(q, (cost + 1000, node, (dir - 1) % 4, prev))
    heappush(q, (cost + 1000, node, (dir + 1) % 4, prev))

  seen, seats = set(), set()
  for dir in range(4):
    stack = [(end, dir)]
    while stack:
      node, dir = stack.pop()
      if (node, dir) not in seen:
        seen.add((node, dir))
        seats.add(node)
        stack.extend(list(prevs.get((node, dir), set())))

  return target, seats

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    map = [line.strip() for line in f.readlines()]

  mx, my = len(map[0]), len(map)
  start = min((x, y) for y in range(my) for x in range(mx) if map[y][x] == 'S')
  end = min((x, y) for y in range(my) for x in range(mx) if map[y][x] == 'E')

  score, seats = shortest_path(map, start, 0, end)
  print(f"Lowest score: {score}")
  print(f"Optimal seats: {len(seats)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
