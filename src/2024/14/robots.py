#!/usr/bin/env python3

import math
import pathlib
import re
import sys

from itertools import groupby
from statistics import variance

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

FAST = True
DISPLAY = True

def iteration(points: list[tuple[int, ...]], w: int, h: int, t: int) -> list[tuple[int, int]]:
  return [((x + vx * t) % w, (y + vy * t) % h) for x, y, vx, vy in points]

def display(robots: set[tuple[int, int]], w: int, h: int) -> str:
  return '\n'.join(''.join('#' if (x, y) in robots else '.' for x in range(w)) for y in range(h))

# assumes the tree has a contiguous horizontal line of robots
# tree may be sparse or very large
# finds the time at which such a horizontal line is seen
def find_tree(points: list[tuple[int, ...]], w: int, h: int) -> int:
  for seconds in range(1, (w * h)):
    robots = set(iteration(points, w, h, seconds))

    max_contiguous = any(
      any(
        len(list(g)) > 10
        for (k, g) in groupby((x, y) in robots for x in range(w))
        if k
      )
      for y in range(h)
    )

    if max_contiguous:
      return seconds

  return -1

# assumes the tree is solid and robots are clustered together
# finds the time with least variance of robot positions
#
# https://www.reddit.com/r/adventofcode/comments/1he0asr/comment/m1zzfsh/
def calc_tree(points: list[tuple[int, ...]], w: int, h: int) -> int:
  tx = min(range(w), key=lambda secs: variance((x + secs * vx) % w for x, _, vx, _ in points))
  ty = min(range(h), key=lambda secs: variance((y + secs * vy) % h for _, y, _, vy in points))
  return tx + (((pow(w, -1, h) * (ty - tx)) % h) * w)

def run() -> None:
  w, h = 101, 103
  iterations = 100
  quadrants = [0] * 4

  with open(aoc.inputfile('input.txt')) as f:
    points = [tuple(int(x) for x in re.findall(r'-?\d+', line.strip())) for line in f.readlines()]

  for fx, fy in iteration(points, w, h, iterations):
    if fx != w // 2 and fy != h // 2:
      quadrants[(fy > w // 2) * 2 + (fx > h // 2)] += 1

  secs = calc_tree(points, w, h) if FAST else find_tree(points, w, h)

  if DISPLAY:
    print(display(set(iteration(points, w, h, secs)), w, h))

  print(f"Safety factor: {math.prod(quadrants)}")
  print(f"Seconds to tree: {secs}")

if __name__ == '__main__':
  run()
  sys.exit(0)
