#!/usr/bin/env python3

import pathlib
import re
import sys

from functools import cache
from itertools import groupby

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    sections = f.read().strip().split("\n\n")
    towels = {k: set(g) for k, g in groupby(sorted(re.split('[^a-z]+', sections[0]), key=len), len)}
    designs = sections[1].split("\n")

  @cache
  def ways(s: str) -> int:
    return sum(
      s[:cnt] in possible and (len(s) == cnt or ways(s[cnt:]))
      for cnt, possible in towels.items()
      if cnt <= len(s)
    )

  patterns = list(ways(design) for design in designs)
  print(f"Possible designs: {sum(n > 0 for n in patterns)}")
  print(f"Possible ways: {sum(patterns)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
