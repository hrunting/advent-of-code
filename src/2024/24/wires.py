#!/usr/bin/env python3

import pathlib
import sys

from functools import cache
from itertools import combinations
from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

WIRES = dict[str, Callable[[], int]]
GATES = list[tuple[str, str, str, str]]
FAST = True

sys.setrecursionlimit(30)

def generate_circuit(data: str) -> WIRES:
  wires = {}

  ops = {
    'VAL': lambda val: lambda: int(val),
    'AND': lambda wire1, wire2: lambda: wires[wire1]() & wires[wire2](),
    'OR': lambda wire1, wire2: lambda: wires[wire1]() | wires[wire2](),
    'XOR': lambda wire1, wire2: lambda: wires[wire1]() ^ wires[wire2]()
  }

  init, gates = data.split("\n\n")

  for wire, val in (line.split(": ") for line in init.split("\n")):
    wires[wire] = ops['VAL'](val)

  for wire1, op, wire2, _, res in (line.split() for line in gates.split("\n")):
    wires[res] = cache(ops[op](wire1, wire2))

  return wires

def map_gates(data: str) -> GATES:
  _, gates = data.split("\n\n")
  return [(op, wire1, wire2, res) for wire1, op, wire2, _, res in (line.split() for line in gates.split("\n"))]

def init(wires: WIRES, x: int, y: int) -> None:
  for wire in wires.keys():
    if wire[0] == 'x':
      wires[wire] = (lambda n: lambda: n)(int(x & (1 << int(wire[1:])) != 0))
    elif wire[0] == 'y':
      wires[wire] = (lambda n: lambda: n)(int(y & (1 << int(wire[1:])) != 0))
    else:
      wires[wire].cache_clear()

def exec(wires: dict[str, Callable[[], int]]) -> int:
  result = 0
  for wire in sorted(w for w in wires.keys() if w[0] == 'z'):
    result |= wires[wire]() << int(wire[1:])
  return result

def test(wires: WIRES, bit: int) -> bool:
  x, y = 2 ** bit - 1, 0
  while y <= x:
    init(wires, x, y)
    if exec(wires) != x + y:
      return False
    y = y * 2 + 1
  return True

def swap_scan(wires: WIRES) -> set[str]:
  swaps, bad = set(), set()
  bits = max(int(wire[1:]) for wire in wires.keys() if wire[0] == 'x')

  def _swap(bit: int) -> None:
    for gate, other in combinations((
      k
      for k in wires.keys()
      if k[0] not in {'x', 'y'} and k not in swaps
    ), 2):
      if (gate, other) in bad or gate[0] == other[0] == 'z':
        continue

      wires[gate], wires[other] = wires[other], wires[gate]

      try:
        if test(wires, bit):
          swaps.add(gate)
          swaps.add(other)
          return
      except RecursionError:
          bad.add((gate, other))

      wires[gate], wires[other] = wires[other], wires[gate]

    raise Exception(f"unable to solve bit {bit} with swap")

  for bit in range(bits):
    if test(wires, bit):
      continue
    _swap(bit + 1)
    if len(swaps) == 8:
      break
  return swaps

def bad_gates(circuit: GATES) -> set[str]:
  wrong = set()
  highest_out = max(out for _, _, _, out in circuit if out[0] == 'z')

  for op, in1, in2, out in circuit:
    if op == 'XOR':
      if all(id[0] not in {'x', 'y', 'z'} for id in (in1, in2, out)):
        wrong.add(out)
      else:
        for subop, subin1, subin2, _ in circuit:
          if subop == 'OR'and out in {subin1, subin2}:
            wrong.add(out)
            break

    elif op != 'XOR' and out[0] == 'z' and out != highest_out:
      wrong.add(out)

    elif op == 'AND' and in1 != 'x00' and in2 != 'x00':
      for subop, subin1, subin2, _ in circuit:
        if subop != 'OR' and out in {subin1, subin2}:
          wrong.add(out)
          break

  return wrong

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    data = f.read().strip()

  wires = generate_circuit(data)
  circuit = map_gates(data)

  print(f"Decimal output: {exec(wires)}")

  if FAST:
    print(f"Swapped gates: {','.join(sorted(bad_gates(circuit)))}")
  else:
    print(f"Swapped gates: {','.join(sorted(swap_scan(wires)))}")

if __name__ == '__main__':
  run()
  sys.exit(0)
