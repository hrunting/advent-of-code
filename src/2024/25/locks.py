#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def pinout(key: list[str], pin: str) -> tuple[int, ...]:
  return tuple(sum(1 for s in key if s[i] == pin) for i in range(len(key[0])))

def overlaps(key: tuple[int, ...], lock: tuple[int, ...]) -> bool:
  return any(keypins > lockpins for keypins, lockpins in zip(key, lock))

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    schematics = [chunk.strip() for chunk in f.read().strip().split("\n\n")]

  keys = [pinout(s.split("\n"), '#') for s in schematics if s[0] == '#']
  locks = [pinout(s.split("\n"), '.') for s in schematics if s[0] == '.']

  fits = [(key, lock) for key in keys for lock in locks if not overlaps(key, lock)]
  print(f"Non-overlapping key/lock pairs: {len(fits)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
