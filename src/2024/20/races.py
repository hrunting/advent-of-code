#!/usr/bin/env python3

import pathlib
import sys

from typing import Iterator

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Pos = tuple[int, int]

DIRS = ((1, 0), (0, 1), (-1, 0), (0, -1))

def moves(pos: Pos, dir: int) -> Iterator[tuple[Pos, int]]:
  for d in ((dir - 1) % 4, dir, (dir + 1) % 4):
    yield (pos[0] + DIRS[d][0], pos[1] + DIRS[d][1]), d

def distance(a: Pos, b: Pos) -> int:
  return abs(b[0] - a[0]) + abs(b[1] - a[1])

def path(map: list[str]) -> list[Pos]:
  goal = next((x, y) for y in range(len(map)) for x in range(len(map[0])) if map[y][x] == 'S')
  pos = next((x, y) for y in range(len(map)) for x in range(len(map[0])) if map[y][x] == 'E')

  steps = []
  dir = next(
    d
    for d, dir in enumerate(DIRS)
    if map[pos[1] + dir[1]][pos[0] + dir[0]] != '#'
  )

  while pos != goal:
    steps.append(pos)

    for (nx, ny), ndir in moves(pos, dir):
      if map[ny][nx] != '#':
        pos = (nx, ny)
        dir = ndir
        break

  steps.append(goal)
  return steps

def count_cheats(path: list[Pos], times: tuple[int, ...], threshold: int) -> dict[int, int]:
  cheats = {t: 0 for t in times}

  for idx, start in enumerate(path):
    jump = idx + threshold
    while jump < len(path):
      dist = distance(start, path[jump])
      if dist > times[-1]:
        jump += dist - times[-1]
        continue

      if jump - idx - dist >= threshold:
        for t in times:
          cheats[t] += dist <= t

      jump += 1

  return {t: cheats[t] for t in times}

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    map = [line.strip() for line in f.readlines()]

  cheats = count_cheats(path(map), (2, 20), 100)
  print(f"2-step cheats saving >= 100 picoseconds: {cheats[2]}")
  print(f"20-step cheats saving >= 100 picoseconds: {cheats[20]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
