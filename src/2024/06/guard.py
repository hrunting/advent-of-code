#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Dir = int
Position = tuple[int, int]
PositionDir = tuple[int, int, int]

Jump = dict[PositionDir, tuple[Position, Dir]]

UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3

def build_jump_table(map: dict[Position, str], width: int, height: int) -> Jump:
  jump = {}

  for y in range(height):
    left, right = ((-1, y), UP), ((width, y), DOWN)
    for x in range(width):
      if map.get((x, y)) == '#':
        left = ((x + 1, y), UP)
      if map.get((width - 1 - x, y)) == '#':
        right = ((width - 2 - x, y), DOWN)

      jump[x, y, LEFT] = left
      jump[width - 1 - x, y, RIGHT] = right

  for x in range(width):
    up, down = ((x, -1), RIGHT), ((x, height), LEFT)
    for y in range(height):
      if map.get((x, y)) == '#':
        up = ((x, y + 1), RIGHT)
      if map.get((x, height - 1 - y)) == '#':
        down = ((x, height - 2 - y), LEFT)

      jump[x, y, UP] = up
      jump[x, height - 1 - y, DOWN] = down

  return jump

def move(pos: tuple[int, int], dir: int) -> tuple[int, int]:
  return (
    pos[0] + (0, 1, 0, -1)[dir],
    pos[1] + (-1, 0, 1, 0)[dir]
  )

def loop_check(map: dict[Position, str], jump: Jump, pos: Position, dir: Dir, obstacle: Position) -> bool:
    visited = set()

    while pos in map and (pos, dir) not in visited:
      visited.add((pos, dir))
      if pos[0] != obstacle[0] and pos[1] != obstacle[1]:
        pos, dir = jump[(*pos, dir)]
      else:
        while map.get(npos := move(pos, dir)) == '#' or npos == obstacle:
          dir = (dir + 1) % 4
        pos = npos

    return (pos, dir) in visited

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    map = {
      (x, y): c
      for y, line in enumerate(f.readlines())
      for x, c in enumerate(line.strip())
    }

  width, height = max((x + 1, y + 1) for x, y in map.keys())
  pos = min(k for k, v in map.items() if v == '^')

  jump = build_jump_table(map, width, height)

  obstacles = 0
  dir, visited = UP, set()

  while pos in map:
    visited.add(pos)
    while map.get(npos := move(pos, dir)) == '#':
      dir = (dir + 1) % 4
      npos = move(pos, dir)

    if npos not in visited and map.get(npos) == '.':
      obstacles += loop_check(map, jump, pos, dir, npos)

    pos = npos

  print(f"Distinct points visited: {len(visited)}")
  print(f"Obstruction positions: {obstacles}")

if __name__ == '__main__':
  run()
  sys.exit(0)
