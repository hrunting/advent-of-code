#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    codes = [int(line.strip()) for line in f.readlines()]

  seen = [-1] * 130321
  bananas = [0] * 130321
  summed = 0

  for buyer, code in enumerate(codes):
    n = code
    price = code % 10
    last_price = 0
    seq = 0

    for i in range(2000):
      n = n ^ ((n & 262143) * 64)
      n = n ^ (n // 32)
      n = n ^ ((n % 8192) * 2048)

      last_price, price = price, n % 10
      seq = seq // 19 + (price - last_price + 9) * 6859
      if seen[seq] != buyer and i >= 3:
        bananas[seq] += price
        seen[seq] = buyer

    summed += n

  print(f"Sum of 2000th secret: {summed}")
  print(f"Most bananas: {max(bananas)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
