#!/usr/bin/env python3

import pathlib
import sys

from collections import deque
from typing import Sequence

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def seq_sum(n: int, a :int, d: int) -> int:
  return int(n * (2 * a + (n - 1) * d) / 2)

def checksum(files: Sequence[tuple[int, int, int]]) -> int:
  return sum(id * seq_sum(r, idx, 1) for idx, id, r in files)

def fragment_checksum(ranges: list[int]) -> int:
  files = deque()
  free = deque()

  idx = 0
  for i, r in enumerate(ranges):
    if i % 2 == 0:
      files.append((idx, i // 2, r))
    else:
      free.appendleft((idx, r))
    idx += r

  reordered = [files.popleft()]

  while files and free:
    (i, free_bytes) = free.pop()
    if free_bytes <= 0:
      reordered.append(files.popleft())
      continue

    (idx, id, file_bytes) = files.pop()
    reordered.append((i, id, min(free_bytes, file_bytes)))

    if file_bytes > free_bytes:
      files.append((idx, id, file_bytes - free_bytes))
    free.append((i + file_bytes, free_bytes - file_bytes))

  return checksum(reordered)

def move_checksum(ranges: list[int]) -> int:
  files = []
  spaces = []
  reordered = []
  idx = 0

  for i, r in enumerate(ranges):
    if i % 2 == 0:
      files.append((idx, i // 2, r))
    elif r > 0:
      spaces.append((idx, r))
    idx += r

  available = [0] * 10

  for idx, id, file in reversed(files):
    while available[file] < len(spaces) and spaces[available[file]][1] < file:
      available[file] += 1

    if available[file] >= len(spaces) or spaces[available[file]][0] > idx:
      reordered.append((idx, id, file))
    else:
      i, free = spaces[available[file]]
      reordered.append((i, id, file))
      spaces[available[file]] = (i + file, free - file)
      available[file] += 1

  return checksum(reordered)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    ranges = [int(n) for n in f.read().strip()]

  print(f"Filesystem checksum: {fragment_checksum(ranges)}")
  print(f"Filesystem checksum: {move_checksum(ranges)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
