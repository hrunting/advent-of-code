#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

Config = tuple[tuple[int, int], tuple[int, int], tuple[int, int]]

ERROR_ADJUSTMENT = 10_000_000_000_000

def min_tokens(machine: Config, error: bool = False) -> float:
  (ax, ay), (bx, by), (prize_x, prize_y) = machine

  if error:
    prize_x += ERROR_ADJUSTMENT
    prize_y += ERROR_ADJUSTMENT

  a = (prize_x * by + prize_y * -bx) // (ax * by + ay * -bx)
  b = (prize_x - ax * a) // bx

  if ax * a + bx * b != prize_x or ay * a + by * b != prize_y:
    return 0

  return 3 * a + b

def parse_machine(config: str) -> Config:
  (ax, ay, bx, by, prize_x, prize_y) = re.findall(r'\d+', config, flags=re.DOTALL)
  return ((int(ax), int(ay)), (int(bx), int(by)), (int(prize_x), int(prize_y)))

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    machines = [parse_machine(config) for config in f.read().strip().split("\n\n")]

  total_tokens = sum(min_tokens(machine) for machine in machines)
  print(f"Fewest tokens required: {total_tokens}")

  error_tokens = sum(min_tokens(machine, True) for machine in machines)
  print(f"Fewest tokens required (adjusted): {error_tokens}")

if __name__ == '__main__':
  run()
  sys.exit(0)
