#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

from collections import deque
from typing import Iterator

import aoc

Pos = tuple[int, int]

DIRS = [(1, 0), (0, 1), (-1, 0), (0, -1)]

def moves(pos: Pos, dir: int) -> Iterator[tuple[Pos, int]]:
  for d in ((dir - 1) % 4), dir, ((dir + 1) % 4):
    yield (pos[0] + DIRS[d][0], pos[1] + DIRS[d][1]), d

def shortest_path(corrupted: set[Pos], start: Pos, end: Pos) -> tuple[int, set[Pos]]:
  steps = 0
  q: deque[tuple[int, Pos, int]] = deque([(0, start, 0)])
  seen: dict[Pos, tuple[int, Pos]] = {start: (0, (-1, -1))}

  while q:
    steps, (x, y), dir = q.popleft()
    if (x, y) == end:
      break

    for (nx, ny), d in moves((x, y), dir):
      if not 0 <= nx <= end[0] or not 0 <= ny <= end[1]:
        continue
      if (nx, ny) in corrupted:
        continue
      if (nx, ny) in seen and seen[nx, ny][0] <= steps + 1:
        continue

      seen[nx, ny] = (steps + 1, (x, y))
      q.append((steps + 1, (nx, ny), d))
  else:
    return 0, set()

  path = set()
  while (x, y) in seen:
    path.add((x, y))
    _, (x, y) = seen[x, y]

  return steps, path

def run() -> None:
  mx, my = 70, 70
  initial = 1024

  bytes = []
  with open(aoc.inputfile('input.txt')) as f:
    for line in f.readlines():
      x, y = line.strip().split(',')
      bytes.append((int(x), int(y)))

  grid = {n for n in bytes[:initial]}
  steps, nodes = shortest_path(grid, (0, 0), (mx, my))
  print(f"Shortest path after {initial} bytes: {steps}")

  for byte in bytes[initial:]:
    grid.add(byte)
    if byte in nodes:
      steps, nodes = shortest_path(grid, (0, 0), (mx, my))
      if not steps:
        break
  print(f"First blocking byte: {byte}")

if __name__ == '__main__':
  run()
  sys.exit(0)
