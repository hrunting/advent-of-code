#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def is_safe(report: tuple[int, ...], dampen: bool = False) -> bool:
  safe = True
  last_diff = report[1] - report[0]

  for i in range(1, len(report)):
    diff = report[i] - report[i - 1]

    if not (0 < abs(diff) <= 3) or diff * last_diff <= 0:

      # when dampening:
      # - try removing the item before this position
      # - try removing the item at this position
      # - try removing the first item (only matters if the 3rd level is bad)
      #   e.g. 9 11 9 7 6 5
      safe = dampen and (
        is_safe(report[0:(i - 1)] + report[i:]) or
        is_safe(report[0:i] + report[(i + 1):]) or
        (i == 2 and is_safe(report[0:(i - 2)] + report[(i - 1):]))
      )
      break

    last_diff = diff

  return safe

def run() -> None:
  reports = []
  with open(aoc.inputfile('input.txt')) as f:
    for line in (s.strip() for s in f.readlines()):
      reports.append(tuple(int(x) for x in line.split()))

  safe = sum(is_safe(report) for report in reports)
  print(f"Safe reports: {safe}")

  safe = sum(is_safe(report, True) for report in reports)
  print(f"Safe dampened reports: {safe}")

if __name__ == '__main__':
  run()
  sys.exit(0)
