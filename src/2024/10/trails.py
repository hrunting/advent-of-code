#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def trailheads(map: list[list[int]], distinct: bool = False) -> list[int]:
  trailheads = {(x, y): 0 for y in range(len(map)) for x in range(len(map[0])) if map[y][x] == 0}
  peaks = {(x, y): {(x, y)} for y in range(len(map)) for x in range(len(map[0])) if map[y][x] == 9}

  q = [(peak, peak) for peak in peaks.keys()]
  while q:
    peak, (x, y) = q.pop()
    if (x, y) in trailheads:
      trailheads[x, y] += 1
    else:
      for nx, ny in ((x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)):
        if (
          0 <= nx < len(map[0]) and
          0 <= ny < len(map) and
          map[y][x] - map[ny][nx] == 1 and
          (distinct or (nx, ny) not in peaks[peak])
        ):
          peaks[peak].add((nx, ny))
          q.append((peak, (nx, ny)))

  return list(trailheads.values())

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    map = [[int(n) for n in line.strip()] for line in f.readlines()]

  print(f"Trailhead score sum: {sum(trailheads(map))}")
  print(f"Trailhead rating sum: {sum(trailheads(map, True))}")

if __name__ == '__main__':
  run()
  sys.exit(0)
