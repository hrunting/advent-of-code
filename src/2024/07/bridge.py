#!/usr/bin/env python3

import math
import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def tens(n: int) -> int:
  return 10 ** (int(math.log10(n)) + 1)

def is_valid(answer: int, operands: list[int], concat: bool = False) -> bool:
  if answer < 0 or len(operands) == 0:
    return False

  val = operands[-1]
  if len(operands) == 1 and val == answer:
    return True

  return (
    (concat and answer % tens(val) == val and is_valid(answer // tens(val), operands[:-1], concat)) or
    (answer % operands[-1] == 0 and is_valid(answer // operands[-1], operands[:-1], concat)) or
    is_valid(answer - operands[-1], operands[:-1], concat)
  )

def run() -> None:
  results = [0, 0]

  with open(aoc.inputfile('input.txt')) as f:
    for line in f.readlines():
      answer, *operands = (int(x) for x in re.split(r'[:\s]+', line.strip()))
      if is_valid(answer, operands):
        results[0] += answer
        results[1] += answer
      elif is_valid(answer, operands, True):
        results[1] += answer

  print(f"Total calibration result: {results[0]}")
  print(f"Total calibration result (third operator): {results[1]}")

if __name__ == '__main__':
  run()
  sys.exit(0)
