#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

POS = tuple[int, int]
DIR = tuple[int, int]
GRID = dict[POS, str]

UP = (0, -1)
RIGHT = (1, 0)
DOWN = (0, 1)
LEFT = (-1, 0)
MOVES: dict[str, DIR] = {'^': UP, '>': RIGHT, 'v': DOWN, '<': LEFT }

def display(grid: GRID) -> str:
  mx, my = max(p for p in grid)
  return "\n".join("".join(grid[x, y] if (x, y) in grid else '.' for x in range(mx + 1)) for y in range(my + 1))

def gps_coord(x: int, y: int) -> int:
  return y * 100 + x

def move(pos: POS, dir: POS) -> POS:
  return (pos[0] + dir[0], pos[1] + dir[1])

def move_robot(grid: GRID, pos: POS, dir: str, double: bool = False) -> POS:
  action = MOVES[dir]
  dest = move(pos, action)

  if dest not in grid:
    grid[dest] = grid[pos]
    del grid[pos]
    return dest

  if grid[dest] == '#':
    return pos

  crate = dest

  if double:
    prev = pos
    moves = []

    # x-axis moves
    if action[0] != 0:
      while True:
        moves.append((prev, crate))
        if crate not in grid:
          break
        if grid[crate] == '#':
          return pos

        prev = crate
        crate = move(crate, action)

    # y-axis moves
    else:
      layer = defaultdict(set)
      layer[crate].add(pos)

      while layer:
        nlayer = defaultdict(set)
        for crate in layer:
          moves.extend((prev, move(prev, action)) for prev in layer[crate])
          if crate not in grid:
            continue
          if grid[crate] == '#':
            return pos

          nlayer[move(crate, action)].add(crate)
          if grid[crate] == ']':
            other = move(crate, LEFT)
            nlayer[move(other, action)].add(other)
          elif grid[crate] == '[':
            other = move(crate, RIGHT)
            nlayer[move(other, action)].add(other)

        layer = nlayer

    while moves:
      old, new = moves.pop()
      grid[new] = grid[old]
      del grid[old]

  else:
    while crate in grid and grid[crate] == 'O':
      crate = move(crate, action)

    if crate in grid:
      return pos

    grid[crate] = grid[dest]
    grid[dest] = grid[pos]
    del grid[pos]

  return dest

def generate_grid(map: str, double: bool = False) -> tuple[GRID, POS]:
  grid = double_grid(map) if double else single_grid(map)
  pos = [(x, y) for (x, y), c in grid.items() if c == '@'][0]
  return grid, pos

def single_grid(map: str) -> GRID:
  return {
    (x, y): c
    for y, line in enumerate(map.split("\n"))
    for x, c in enumerate(line)
    if c != '.'
  }

def double_grid(map: str) -> GRID:
  grid = {}

  for y, line in enumerate(map.split("\n")):
    for x, c in enumerate(line):
      if c == 'O':
        grid[x * 2, y] = '['
        grid[x * 2 + 1, y] = ']'
      elif c == '#':
        grid[x * 2, y] = grid[x * 2 + 1, y] = c
      elif c == '@':
        grid[x * 2, y] = c

  return grid

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    map, moves = f.read().strip().split("\n\n")

  moves = moves.replace("\n", "")

  for wide in (False, True):
    grid, pos = generate_grid(map, wide)
    for move in moves:
      pos = move_robot(grid, pos, move, wide)

    coord_sum = sum(gps_coord(x, y) for x, y in grid.keys() if grid[x, y] in {'O', '['})
    print(f"Sum of ({'wide-' if wide else ''}box GPS coordinates: {coord_sum}")

if __name__ == '__main__':
  run()
  sys.exit(0)
