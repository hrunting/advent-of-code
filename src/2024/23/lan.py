#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def find_triplets(lan: defaultdict[str, set[str]]) -> set[tuple[str, str, str]]:
  triplets = set()
  for a in lan.keys():
    if a[0] != 't':
      continue

    for b in lan[a]:
      if b[0] == 't' and b < a:
        continue

      for c in lan[b]:
        if a not in lan[c]:
          continue
        if c[0] == 't' and c < a:
          continue

        triplets.add(tuple(sorted((a, b, c))))

  return triplets

def bron_kerbosch(lan: dict[str, set[str]]) -> set[str]:
  def degree(n):
    return len(lan[n])

  longest = set()
  q = [(set(), set(lan.keys()), set())]

  while q:
    maximal, partial, eliminated = q.pop()
    if len(partial) == len(eliminated) == 0:
      if len(maximal) > len(longest):
        longest = maximal
      continue

    pivot = max(partial | eliminated, key=degree)
    for node in partial - lan[pivot]:
      q.append((maximal | {node}, partial & lan[node], eliminated & lan[node]))
      partial = partial - {node}
      eliminated = eliminated | {node}

  return longest

def run() -> None:
  lan: defaultdict[str, set[str]] = defaultdict(set)
  with open(aoc.inputfile('input.txt')) as f:
    for line in f.readlines():
      a, b = line.strip().split("-")
      lan[a].add(b)
      lan[b].add(a)

  triplets = find_triplets(lan)
  print(f"Triplets with 't' computer: {len(triplets)}")

  longest = bron_kerbosch(lan)
  print(f"LAN party password: {','.join(sorted(longest))}")

if __name__ == '__main__':
  run()
  sys.exit(0)
