#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

DIRS = (
  (-1, -1),
  (0, -1),
  (1, -1),
  (-1, 0),
  (1, 0),
  (-1, 1),
  (0, 1),
  (1, 1)
)

NEXT = {'X': 'M', 'M': 'A', 'A': 'S'}

def find_xmas(board: list[str], x: int, y: int, dx: int, dy: int) -> bool:
  c = board[y][x]
  if c != 'X':
    return False

  try:
    while c != 'S':
      nx, ny = x + dx, y + dy
      if min(nx, ny) < 0:
        raise IndexError

      nc = board[ny][nx]
      if NEXT.get(c, '') != nc:
        return False

      c, x, y = nc, nx, ny
    return True

  except IndexError:
    return False

def find_x_mas(board: list[str], x: int, y: int) -> bool:
  c = board[y][x]
  if c != 'A':
    return False

  try:
    for (px, py), (nx, ny) in (
      ((x - 1, y - 1), (x + 1, y + 1)),
      ((x + 1, y - 1), (x - 1, y + 1))
    ):
      if min(px, py, nx, ny) < 0:
        raise IndexError

      if {board[py][px], board[ny][nx]} != {'M', 'S'}:
        return False

    return True
  except IndexError:
    return False

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    board = [line.strip() for line in f.readlines()]

  xmas_count = 0
  x_mas_count = 0
  for y, line in enumerate(board):
    for x in range(len(line)):
      x_mas_count += find_x_mas(board, x, y)
      for (dx, dy) in DIRS:
        xmas_count += find_xmas(board, x, y, dx, dy)

  print(f"XMAS count: {xmas_count}")
  print(f"X-MAS count: {x_mas_count}")

if __name__ == '__main__':
  run()
  sys.exit(0)
