#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict
from itertools import combinations

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  antennas = defaultdict(list)
  antinodes = set()
  harmonics = set()

  with open(aoc.inputfile('input.txt')) as f:
    for y, line in enumerate(f.readlines()):
      for x, c in enumerate(line.strip()):
        if c != '.':
          antennas[c].append((x, y))
    else:
      mx, my = x, y

  for locations in antennas.values():
    for (ax, ay), (bx, by) in combinations(locations, 2):
      harmonics.add((ax, ay))
      harmonics.add((bx, by))

      cx, cy = ax - (bx - ax), ay - (by - ay)
      dx, dy = bx + (bx - ax), by + (by - ay)

      if 0 <= cx <= mx and 0 <= cy <= my:
        antinodes.add((cx, cy))
      if 0 <= dx <= mx and 0 <= dy <= my:
        antinodes.add((dx, dy))

      while 0 <= cx <= mx and 0 <= cy <= my:
        harmonics.add((cx, cy))
        cx -= bx - ax
        cy -= by - ay
      while 0 <= dx <= mx and 0 <= dy <= my:
        harmonics.add((dx, dy))
        dx += bx - ax
        dy += by - ay

  print(f"Unique antinode locations: {len(antinodes)}")
  print(f"Unique harmonic antinode locations: {len(harmonics)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
