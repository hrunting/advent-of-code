#!/usr/bin/env python3

import functools
import math
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def tens(n: int) -> int:
  return int(math.log10(n)) + 1

@functools.cache
def transform(stone: int, times: int) -> int:
  if times == 0:
    return 1
  if stone == 0:
    return transform(1, times - 1)
  if (digits := tens(stone)) % 2 == 0:
    left, right = divmod(stone, 10 ** (digits // 2))
    return transform(left, times - 1) + transform(right, times - 1)
  return transform(stone * 2024, times - 1)

def iterate(stones: tuple[int, ...], times: int) -> int:
  return sum(transform(stone, times) for stone in stones)

def run() -> None:
  with open(aoc.inputfile('input.txt')) as f:
    stones = tuple(int(n) for n in f.read().strip().split())

  print(f"Length after 25 blinks: {iterate(stones, 25)}")
  print(f"Length after 75 blinks: {iterate(stones, 75)}")

if __name__ == '__main__':
  run()
  sys.exit(0)
