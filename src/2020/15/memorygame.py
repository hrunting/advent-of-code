#!/usr/bin/env python3

import sys

def memory_game(nums: list[int], turns: int) -> int:
  history = {num: i + 1 for i, num in enumerate(nums[:-1])}
  last = nums[-1]

  for turn in range(len(nums) + 1, turns + 1):
    spoken = (turn - history[last] - 1) if last in history else 0
    history[last] = turn - 1
    last = spoken

  return last

def run() -> None:
  input = "11,0,1,10,5,19"
  nums = [int(x) for x in input.split(",")]

  spoken = memory_game(nums, 2020)
  print(f'2020th number spoken: {spoken}')

  spoken = memory_game(nums, 30000000)
  print(f'30,000,000th number spoken: {spoken}')

if __name__ == '__main__':
  run()
  sys.exit(0)
