#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def find_earliest_departure(ts: int, busses: list[int]) -> tuple[int, int]:
  first_bus, departure = -1, max(busses)

  for bus in busses:
    next_departure = bus - ((ts + bus) % bus)
    if next_departure < departure:
      departure = next_departure
      first_bus = bus

  return first_bus, departure


# Extended Euclidean GCD from
# https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
def bezout_coefficients(a: int, b: int) -> tuple[int, int]:
  os, s = (1, 0)
  ot, t = (0, 1)

  while b != 0:
    q = a // b
    a, b = b, a - q * b

    os, s = s, os - q * s
    ot, t = t, ot - q * t

  return os, ot

def find_contest_departure(busses: list[int]) -> int:
  modulos = [(bus, (bus - i) % bus) for i, bus in enumerate(busses) if bus > 0]

  # Chinese remainder theorem from
  # https://en.wikipedia.org/wiki/Chinese_remainder_theorem
  mod, time = modulos[0]

  for next_mod, offset in modulos[1:]:
    m1, m2 = bezout_coefficients(mod, next_mod)
    time = time * m2 * next_mod + offset * m1 * mod
    mod = mod * next_mod
    time %= mod

  return time

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  with open(input_file) as fh:
    timestamp = int(fh.readline().strip())
    busses = [0 if bus == "x" else int(bus) for bus in fh.readline().strip().split(",")]

  bus, departure = find_earliest_departure(timestamp, [x for x in busses if x > 0])
  print(f'First departure multiple: {bus * departure}')

  departure = find_contest_departure(busses)
  print(f'Contest departure time: {departure}')

if __name__ == '__main__':
  run()
  sys.exit(0)
