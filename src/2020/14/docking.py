#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def sum_registers_v1(input_file: pathlib.Path) -> int:
  and_mask = 0
  or_mask = 0
  registers = {}

  for line in open(input_file):
    address, val = line.strip().split(" = ")

    if address == "mask":
      and_mask = int(val.replace("X", "1"), 2)
      or_mask = int(val.replace("X", "0"), 2)

    else:
      register = int(address[4:-1])
      registers[register] = int(val) & and_mask | or_mask

  return sum(registers.values())

def sum_registers_v2(input_file: pathlib.Path) -> int:
  base_mask = ''
  registers = {}

  for line in open(input_file):
    address, val = line.strip().split(" =" )

    if address == "mask":
      base_mask = val.strip()

    else:
      register = int(address[4:-1])
      register_mask = ''

      for i in range(len(base_mask)):
        if base_mask[i] == 'X':
          register_mask += 'X'
        else:
          register_mask += str(int(base_mask[i]) | ((register >> len(base_mask) - 1 - i) & 1))
      
      for register in apply_masks(register_mask):
        registers[register] = int(val)

  return sum(registers.values())

def apply_masks(mask: str) -> list[int]:
  zeroes = mask.replace('X', '0', 1)
  if zeroes == mask:
    return [int(mask, 2)]

  return apply_masks(zeroes) + apply_masks(mask.replace('X', '1', 1))

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  print(f'Register sum v1: {sum_registers_v1(input_file)}')
  print(f'Register sum v2: {sum_registers_v2(input_file)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
