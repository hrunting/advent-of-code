#!/usr/bin/env python3

import math
import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Tile:
  def __init__(self: Self, tile_id: int, data: list[str]) -> None:
    self.id = tile_id
    self.data = data

    self.edges = [
      "".join(row[-1] for row in data),
      self.data[-1],
      "".join(row[0] for row in data),
      self.data[0]
    ]

    self.uniqueid = "".join(self.edges)

  def left(self: Self) -> str:
    return self.edges[2]

  def right(self: Self) -> str:
    return self.edges[0]

  def top(self: Self) -> str:
    return self.edges[3]

  def bottom(self: Self) -> str:
    return self.edges[1]

  def rotate(self: Self) -> Self:
    return Tile(self.id, ["".join(r) for r in zip(*self.data[::-1])])

  def mirror_horizontal(self: Self) -> Self:
    return Tile(self.id, [r[::-1] for r in self.data])

  def mirror_vertical(self: Self) -> Self:
    return Tile(self.id, self.data[::-1])

  def image(self: Self) -> list[str]:
    return [row[1:-1] for row in self.data[1:-1]]

def parse_tiles(input_file: pathlib.Path) -> list[Tile]:
  tiles = []

  tile_id = -1 
  tile_data = []

  for line in open(input_file):
    line = line.strip()

    if line == "":
      tiles.append(Tile(tile_id, tile_data))
      tile_id = -1
      tile_data = []

    elif line.startswith("Tile "):
      tile_id = int(line[5:-1])

    else:
      tile_data.append(line)

  if tile_id >= 0:
    tiles.append(Tile(tile_id, tile_data))

  return tiles

def map_grid(
    size: int, tiles: list[Tile], grid: list[list[Tile]],
    seen: set[int], pos: tuple[int, int] = (0, 0)) -> bool:

  x, y = pos
  if y == size:
    return True

  if x == 0:
    grid.append([])

  for tile in tiles:
    if tile.id in seen:
      continue

    if x > 0 and grid[y][x - 1] is not None and grid[y][x - 1].right() != tile.left():
      continue

    if y > 0 and grid[y - 1][x] is not None and grid[y - 1][x].bottom() != tile.top():
      continue

    grid[y].append(tile)
    result = map_grid(
      size,
      tiles,
      grid,
      seen | {tile.id},
      (x + 1, y) if x < size - 1 else (0, y + 1)
    )

    if result:
      break

    grid[y].pop()

  else:
    result = False
    if x == 0:
      grid.pop()

  return result

def generate_grid(tiles: list[Tile], size: int) -> list[list[Tile]]:
  candidates = []

  for tile in tiles:
    for _ in range(2):
      candidates.append(tile)
      candidates.append(tile.mirror_horizontal())
      candidates.append(tile.mirror_vertical())
      candidates.append(candidates[-1].mirror_horizontal())
      tile = tile.rotate()

  grid = []
  map_grid(size, candidates, grid, set())
  return grid

def generate_pixels(grid: list[list[Tile]]) -> list[list[str]]:
  pixels = []
  for row in grid:
     pixel_row = [[] for _ in range(len(row[0].left()) - 2)]
     for tile in row:
       for i, image_row in enumerate(tile.image()):
         pixel_row[i] += [c for c in image_row]
     pixels += pixel_row

  return pixels

def find_sea_monsters(grid: list[list[Tile]]) -> list[list[str]]:
  def rotate(pixels: list[list[str]]) -> list[list[str]]:
    return [list(row) for row in zip(*pixels[::-1])]

  def mirror_horizontal(pixels: list[list[str]]) -> list[list[str]]:
    return [r[::-1] for r in pixels]

  def mirror_vertical(pixels: list[list[str]]) -> list[list[str]]:
    return pixels[::-1]

  pixels = generate_pixels(grid)
  for _ in range(2):
    if tag_sea_monsters(pixels):
      break

    pixels = mirror_horizontal(pixels)
    if tag_sea_monsters(pixels):
      break

    pixels = mirror_vertical(pixels)
    if tag_sea_monsters(pixels):
      break

    pixels = mirror_horizontal(pixels)
    if tag_sea_monsters(pixels):
      break

    pixels = rotate(mirror_vertical(pixels))

  return pixels

def tag_sea_monsters(pixels: list[list[str]]) -> int:
  x, y = 0, 0
  sea_monsters = 0

  rows = len(pixels)
  cols = len(pixels[0])

  while y < rows - 3:
    while x < cols - 20:
      if pixels[y + 1][x] == '#' and \
         pixels[y + 2][x + 1] == '#' and \
         pixels[y + 2][x + 4] == '#' and \
         pixels[y + 1][x + 5] == '#' and \
         pixels[y + 1][x + 6] == '#' and \
         pixels[y + 2][x + 7] == '#' and \
         pixels[y + 2][x + 10] == '#' and \
         pixels[y + 1][x + 11] == '#' and \
         pixels[y + 1][x + 12] == '#' and \
         pixels[y + 2][x + 13] == '#' and \
         pixels[y + 2][x + 16] == '#' and \
         pixels[y + 1][x + 17] == '#' and \
         pixels[y][x + 18] == '#' and \
         pixels[y + 1][x + 18] == '#' and \
         pixels[y + 1][x + 19] == '#':

        sea_monsters += 1
        pixels[y + 1][x] = '0'
        pixels[y + 2][x + 1] = 'O'
        pixels[y + 2][x + 4] = 'O'
        pixels[y + 1][x + 5] = 'O'
        pixels[y + 1][x + 6] = 'O'
        pixels[y + 2][x + 7] = 'O'
        pixels[y + 2][x + 10] = 'O'
        pixels[y + 1][x + 11] = 'O'
        pixels[y + 1][x + 12] = 'O'
        pixels[y + 2][x + 13] = 'O'
        pixels[y + 2][x + 16] = 'O'
        pixels[y + 1][x + 17] = 'O'
        pixels[y][x + 18] = 'O'
        pixels[y + 1][x + 18] = 'O'
        pixels[y + 1][x + 19] = 'O'

        x += 20

      else:
        x += 1

    x, y = 0, y + 1

  return sea_monsters

def calc_choppiness(image: list[list[str]]) -> int:
  return sum(row.count('#') for row in image)

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  tiles = parse_tiles(input_file)
  size = int(math.sqrt(len(tiles)))

  grid = generate_grid(tiles, size)
  corners = grid[0][0].id * grid[0][-1].id * grid[-1][0].id * grid[-1][-1].id
  print(f'Corner multiple: {corners}')

  image = find_sea_monsters(grid)
  print(f'Choppiness: {calc_choppiness(image)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
