#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def flip_cmd(cmd: str) -> str:
  return "jmp" if cmd == "nop" else "nop"

def run_code(code: list[tuple[str, int]], fix: bool = False) -> int:
  acc = 0
  ptr = 0

  seen = set()
  stack = []
  restore = -1

  while ptr < len(code):
    if ptr in seen:
      if not fix:
        break

      if restore >= 0:
        cmd, val = code[restore]
        code[restore] = (flip_cmd(cmd), val)

      ptr, acc, seen = stack.pop()

      cmd, val = code[ptr]
      code[ptr] = (flip_cmd(cmd), val)
      restore = ptr

    cmd, val = code[ptr]
    jmp = 1
   
    if cmd == "acc":
      acc += val

    else:
      if fix and restore < 0:
        stack.append((ptr, acc, seen.copy()))

      if cmd == "jmp":
        jmp = val

    seen.add(ptr)
    ptr += jmp

  return acc

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  code = []
  for line in open(input_file).readlines():
    cmd, val = tuple(line.split(" "))
    code.append((cmd, int(val)))

  accumulator = run_code(code)
  print(f'Accumulator (before loop): {accumulator}')

  accumulator = run_code(code, True)
  print(f'Accumulator (fixed code): {accumulator}')

if __name__ == '__main__':
  run()
  sys.exit(0)
