#!/usr/bin/env python3

import pathlib
import re
import sys

from collections import defaultdict

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def read_ingredients(input_file: pathlib.Path) -> tuple[dict[str, int], dict[str, set[str]]]:
  p = re.compile(r'^(.+?)(?: \(contains (.+)\))?$')

  ingredient_counts = defaultdict(int)
  allergen_tracker = {}

  for line in open(input_file):
    m = p.match(line)
    if not m:
      continue

    ingredients = m.group(1).split(" ")
    allergens = m.group(2).split(", ") if m.group(2) is not None else []

    for ingredient in ingredients:
      ingredient_counts[ingredient] += 1

    for allergen in allergens:
      if allergen in allergen_tracker:
        allergen_tracker[allergen] &= set(ingredients)
      else:
        allergen_tracker[allergen] = set(ingredients)

  return ingredient_counts, allergen_tracker

def bad_ingredient_list(allergens: dict[str, set[str]]) -> str:
  ingredient_map = {}

  for allergen, ingredients in allergens.items():
    for ingredient in ingredients:
      if ingredient not in ingredient_map:
        ingredient_map[ingredient] = set()
      ingredient_map[ingredient].add(allergen)

  singles = [(i, next(iter(v))) for i, v in ingredient_map.items() if len(v) == 1]
  while singles:
    ingredient, allergen = singles.pop()

    for i in allergens[allergen]:
      if len(ingredient_map[i]) > 1:
        ingredient_map[i].remove(allergen)
        if len(ingredient_map[i]) == 1:
          singles.append((i, next(iter(ingredient_map[i]))))

  return ",".join(sorted(ingredient_map.keys(), key=lambda x: next(iter(ingredient_map[x]))))

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  ingredients, allergens = read_ingredients(input_file)
  bad_ingredients = set().union(*(allergens.values()))

  count = sum(c for i, c in ingredients.items() if i not in bad_ingredients)
  print(f'Safe ingredient count: {count}')

  print(f'Bad ingredient list: {bad_ingredient_list(allergens)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
