#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def two_sum(expenses: list[int], value: int) -> int:
  seen = set()
  result = -1

  for expense in expenses:
    diff = value - expense
    if diff in seen:
      result = diff * expense
      break
    seen.add(expense)

  return result

def three_sum(expenses: list[int], value: int) -> int:
  result = -1

  for i, expense in enumerate(expenses):
    remaining = value - expense
    product = two_sum(expenses[i + 1:], remaining)
    if product > 0:
      result = product * expense
      break

  return result

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  expenses = [int(x) for x in open(input_file).readlines()]
  print(f'Two-entry Product: {two_sum(expenses, 2020)}')
  print(f'Three-entry Product: {three_sum(expenses, 2020)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
