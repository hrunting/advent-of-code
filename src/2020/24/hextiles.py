#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

MOVES = {
  'e': (1, -1, 0),
  'ne': (1, 0, -1),
  'nw': (0, 1, -1),
  'w': (-1, 1, 0),
  'sw': (-1, 0, 1),
  'se': (0, -1, 1)
}

Tile = tuple[int, int, int]

def tile_coords(tile: str, x: int = 0, y: int = 0, z: int = 0) -> Tile:
  idx = 0
  d = ""

  while idx < len(tile):
    d += tile[idx]
    idx += 1

    if d in {"n", "s"}:
      continue

    dx, dy, dz = MOVES[d]

    x += dx
    y += dy
    z += dz

    d = ""

  return x, y, z

def build_grid(input_file: pathlib.Path) -> dict[Tile, bool]:
  grid = {}

  for line in open(input_file):
    coords = tile_coords(line.strip())
    grid[coords] = not grid.get(coords, False)

  return grid

def flip_tiles(grid: dict[Tile, bool], days: int) -> dict[Tile, bool]:
  for _ in range(days):
    flipped_grid = {}
    adjacents = set()

    for tile in grid:
      black_count = 0
      for dx, dy, dz in MOVES.values():
        adj = (tile[0] + dx, tile[1] + dy, tile[2] + dz)
        if adj not in grid:
          adjacents.add(adj)

        black_count += int(grid.get(adj, False))

      if grid[tile] and black_count not in {1, 2}:
        flipped_grid[tile] = not grid[tile]
      elif not grid[tile] and black_count == 2:
        flipped_grid[tile] = not grid[tile]
      else:
        flipped_grid[tile] = grid[tile]

    for tile in adjacents:
      black_count = 0
      for dx, dy, dz in MOVES.values():
        adj = (tile[0] + dx, tile[1] + dy, tile[2] + dz)
        black_count += int(grid.get(adj, False))

      flipped_grid[tile] = black_count == 2

    grid = flipped_grid

  return grid

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  grid = build_grid(input_file)
  print(f'Black tiles: {list(grid.values()).count(True)}')

  grid = flip_tiles(grid, 100)
  print(f'Black tiles (after 100 days): {list(grid.values()).count(True)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
