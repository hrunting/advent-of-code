#!/usr/bin/env python3

import sys

from collections import deque

def play_game(cups: deque[int], moves: int) -> str:
  wrap = len(cups) - 1

  for move in range(moves):
    val = cups[0]
    cups.rotate(-1)
    pick_up = [cups.popleft(), cups.popleft(), cups.popleft()]

    destination = val - (1 if val > 1 else -wrap)
    while destination in pick_up:
      destination -= (1 if destination > 1 else -wrap)

    idx = cups.index(destination)
    for i, v in enumerate(pick_up):
      cups.insert(idx + i + 1, v)

  idx = cups.index(1)
  cups.rotate(-idx - 1)

  return "".join(str(cups[i]) for i in range(len(cups) - 1))

def play_big_game(cups: list[int], size: int, moves: int) -> int:
  ptrs = list(range(1, size + 1))

  for i, cup in enumerate(cups):
    ptrs[cup - 1] = cups[i + 1] - 1 if i + 1 < len(cups) else len(cups)
  ptrs[-1] = cups[0] - 1

  pos = cups[0] - 1

  for _ in range(moves):
    a = ptrs[pos]
    b = ptrs[a]
    c = ptrs[b]

    next_pos = ptrs[c]

    target_pos = pos
    while target_pos in (pos, a, b, c):
      target_pos = target_pos - 1 if target_pos > 0 else len(ptrs) - 1

    after_pos = ptrs[target_pos]
    ptrs[target_pos] = a
    ptrs[c] = after_pos

    ptrs[pos] = next_pos
    pos = next_pos

  return (ptrs[0] + 1) * (ptrs[ptrs[0]] + 1)

def run() -> None:
  input = "459672813"
  cups = deque([int(x) for x in input], len(input))
  result = play_game(cups, 100)
  print(f'Result: {result}')

  cups = [int(x) for x in input]
  result = play_big_game(cups, 1_000_000, 10_000_000)
  print(f'Result (big): {result}')

if __name__ == '__main__':
  run()
  sys.exit(0)
