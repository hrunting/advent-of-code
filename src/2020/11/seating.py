#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def occupied_seats(seats: list[list[str]]) -> int:
  rows = len(seats)
  cols = len(seats[0])

  while True:
    changed = False
    new_seats = []

    for y, row in enumerate(seats):
      new_row = []
      for x, seat in enumerate(row):
        if seat in {'L', '#'}:
          adjacent = 0
          for dx, dy in ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)):
            if 0 <= y + dy < rows and 0 <= x + dx < cols and seats[y + dy][x + dx] == '#':
              adjacent += 1

          if seat == 'L' and adjacent == 0:
            seat = '#'
          elif seat == '#' and adjacent >= 4:
            seat = 'L'

        new_row.append(seat)
        changed = changed or seat != seats[y][x]

      new_seats.append(new_row)

    seats = new_seats
    if not changed:
      break

  return sum(row.count('#') for row in seats)

def directional_occupied_seats(seats: list[list[str]]) -> int:
  rows = len(seats)
  cols = len(seats[0])

  while True:
    changed = False
    new_seats = []

    for y, row in enumerate(seats):
      new_row = []
      for x, seat in enumerate(row):
        if seat in {'L', '#'}:
          adjacent = 0
          for dx, dy in ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)):
            step = 1
            while 0 <= y + dy * step < rows and 0 <= x + dx * step < cols:
              adj_seat = seats[y + dy * step][x + dx * step]
              if adj_seat != '.':
                if adj_seat == '#':
                  adjacent += 1
                break

              step += 1

          if seat == 'L' and adjacent == 0:
            seat = '#'
          elif seat == '#' and adjacent >= 5:
            seat = 'L'

        new_row.append(seat)
        changed = changed or seat != seats[y][x]

      new_seats.append(new_row)

    seats = new_seats
    if not changed:
      break

  return sum(row.count('#') for row in seats)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  seats = [[x for x in row.strip()] for row in open(input_file).readlines()]

  count = occupied_seats(seats)
  print(f'Occupied seats: {count}')

  count = directional_occupied_seats(seats)
  print(f'Directional occupied seats: {count}')

if __name__ == '__main__':
  run()
  sys.exit(0)
