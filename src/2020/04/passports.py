#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_valid_passports(lines: list[str], validate: bool = False) -> int:
  required = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}
  passport = {}
  count = 0

  for line in lines:
    if line == '':
      if set(passport.keys()) >= required:
        count += 1
      passport = {}

    else:
      for key, val in [x.split(':') for x in line.split(' ')]:
        if validate:
          if key == 'byr':
            if not (1920 <= int(val) <= 2002):
              continue

          elif key == 'iyr':
            if not (2010 <= int(val) <= 2020):
              continue

          elif key == 'eyr':
            if not (2020 <= int(val) <= 2030):
              continue

          elif key == 'hgt':
            m = re.search(r'^(\d+)(in|cm)$', val)
            if not m:
              continue

            height, units = m.groups()
            if units == 'cm' and not (150 <= int(height) <= 193):
              continue
            elif units == 'in' and not (59 <= int(height) <= 76):
              continue

          elif key == 'hcl':
            if not re.search(r'^#[0-9a-f]{6}$', val):
              continue

          elif key == 'ecl':
            if val not in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}:
              continue

          elif key == 'pid':
            if not re.search(r'^[0-9]{9}$', val):
              continue

        passport[key] = val

  if set(passport.keys()) >= required:
    count += 1

  return count

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file).readlines()]

  count = count_valid_passports(lines)
  print(f'Valid Passports: {count}')

  count = count_valid_passports(lines, True)
  print(f'Validated Passports: {count}')

if __name__ == '__main__':
  run()
  sys.exit(0)
