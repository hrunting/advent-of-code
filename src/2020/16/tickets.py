#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Range:
  def __init__(self: Self, lo: int, hi: int) -> None:
    self.lo = lo
    self.hi = hi

  def __contains__(self: Self, val: int) -> bool:
    return self.lo <= val <= self.hi

Fields = dict[str, list[Range]]
Ticket = list[int]

def parse_input(input_file: pathlib.Path) -> tuple[Fields, Ticket, list[Ticket]]:
  mode = "schedule"
  fields = {}
  ticket = []
  nearby = []

  for line in open(input_file):
    line = line.strip()
    if line == "":
      continue

    elif line.startswith("your ticket"):
      mode = "ticket"

    elif line.startswith("nearby"):
      mode = "nearby"

    elif mode == "nearby":
      nearby.append([int(x) for x in line.split(",")])

    elif mode == "ticket":
      ticket = [int(x) for x in line.split(",")]

    else:
      field, ranges = line.split(": ")
      fields[field] = []
      for spec in ranges.split(" or "):
        lo, hi = (int(x) for x in spec.split("-"))
        fields[field].append(Range(lo, hi))

  return fields, ticket, nearby

def validate_tickets(fields: Fields, tickets: list[Ticket]) -> tuple[int, list[Ticket]]:
  error_sum = 0
  valid_tickets = []
  ranges = [r for val in fields.values() for r in val]

  for ticket in tickets:
    valid = True
    for field in ticket:
      if not any(r for r in ranges if field in r):
        error_sum += field
        valid = False

    if valid:
      valid_tickets.append(ticket)

  return error_sum, valid_tickets

def remove_candidate(positions: dict[str, set[int]], field: str, candidate: int) -> None:
  positions[field].remove(candidate)
  if len(positions[field]) > 1:
    return

  candidate = next(iter(positions[field]))
  for other in positions:
    if field != other and candidate in positions[other]:
      remove_candidate(positions, other, candidate)

def departure_multiple(fields: dict[str, list[Range]], tickets: list[list[int]]) -> int:
  valid_positions = {field: set(range(len(tickets[0]))) for field in fields}

  for ticket in tickets:
    for i, val in enumerate(ticket):
      for field in valid_positions:
        if i in valid_positions[field] and not any(r for r in fields[field] if val in r):
          remove_candidate(valid_positions, field, i)

  multiple = 1
  for field in fields:
    if not field.startswith("departure"):
      continue
    multiple *= tickets[0][next(iter(valid_positions[field]))]

  return multiple

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  fields, ticket, nearby = parse_input(input_file)

  error_rate, valid = validate_tickets(fields, nearby)
  print(f'Ticket error rate: {error_rate}')

  multiple = departure_multiple(fields, [ticket] + valid)
  print(f'Departure multiple: {multiple}')

if __name__ == '__main__':
  run()
  sys.exit(0)
