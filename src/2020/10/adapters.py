#!/usr/bin/env python3

import pathlib
import sys

from collections import defaultdict, deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  nums = [int(x.strip()) for x in open(input_file).readlines()]

  joltages = [0] + sorted(nums) + [max(nums) + 3]

  diffs = defaultdict(int)
  for idx in range(1, len(joltages)):
    diffs[joltages[idx] - joltages[idx - 1]] += 1

  print(f'Differences Product: {diffs[1] * diffs[3]}')

  dp = deque([0, 0, 1])
  for idx in range(1, len(joltages)):
    ways = 0

    if idx >= 3 and 0 < (joltages[idx] - joltages[idx - 3]) <= 3:
      ways += dp[0]
    if idx >= 2 and 0 < (joltages[idx] - joltages[idx - 2]) <= 3:
      ways += dp[1]
    if idx >= 1 and 0 < (joltages[idx] - joltages[idx - 1]) <= 3:
      ways += dp[2]

    dp.popleft()
    dp.append(ways)

  print(f'Possible combinations: {dp[2]}')

if __name__ == '__main__':
  run()
  sys.exit(0)
