#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_valid_passwords(lines: list[str]) -> tuple[int, int]:
  count_valid = 0
  position_valid = 0
  pattern = re.compile(r'(\d+)-(\d+)\s+(\S+):\s+(\S+)')

  for line in lines:
    m = pattern.match(line)
    if not m:
      continue

    lo, hi, char, pw = m.groups()

    lo = int(lo)
    hi = int(hi)

    count = pw.count(char)
    if count >= lo and count <= hi:
      count_valid += 1

    if (pw[lo - 1] == char or pw[hi - 1] == char) and pw[lo - 1] != pw[hi - 1]:
      position_valid += 1

  return count_valid, position_valid

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file).readlines()]

  valid_count, valid_position = count_valid_passwords(lines)

  print(f'Valid Passwords (Count): {valid_count}')
  print(f'Valid Passwords (Position): {valid_position}')

if __name__ == '__main__':
  run()
  sys.exit(0)
