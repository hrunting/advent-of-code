#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse_file(input_file: pathlib.Path, changed: bool = False) -> tuple[dict[int, str], list[str]]:
  rules = {}
  strings = []

  unparsed = set()
  read_rules = False

  for line in open(input_file):
    line = line.strip()

    if line == "":
      read_rules = True
      continue

    if read_rules:
      strings.append(line)
      continue

    num, rule = line.strip().split(": ")
    num = int(num)

    if rule.startswith('"'):
      rules[num] = (rule[1], set())

    else:
      rules[num] = [[[int(n) for n in part.split(" ")] for part in rule.split(" | ")], set()]
      rules[num][1] |= set(int(num) for combo in rules[num][0] for num in combo)
      unparsed.add(num)

  max_len = max(len(s) for s in strings)

  while len(unparsed):

    # We cannot modify the unparsed set while we iterate over it
    for num in list(unparsed):

      # All the required rules for this rule have been parsed
      # Build a regex that represents this rule set
      if rules[num][1] & unparsed == set():

        if changed and num in {8, 11}:

          # The data structures here are a bit obtuse
          # rules[num] = 0: rule set options, 1: set of required rules
          # rules[num][0] = list of rule set lists (e.g. [42], [42, 31], ...)
          # rules[num][0][0] = list of rules in rule set
          # rules[num][0][0][n] = the specific rule in the rule set

          # Rule 8: 42 | 42 8 == 42 [, 42 [, 42 [, 42 ...]]] == 42+
          if num == 8:
            rules[num][0] = "(?:" + rules[rules[num][0][0][0]][0] + "+)"

          # Rule 11: 42 31 | 42 11 31 == 42 [, 42 [, 42 ... 31,] 31,] 31 == 42{n}31{n}
          #
          # NOTE:
          # Rule 11 is NOT 42+31+
          # The number of matching 42 rules must equal the number of matching 31 rules
          #
          # We use the maximum length of our candidate strings to determine
          # an upper bound on the number of matching pairs we need
          elif num == 11:
            rules[num][0] = "(?:" + "|".join(
              rules[rules[num][0][0][0]][0] + "{" + str(n) + "}" + \
              rules[rules[num][0][0][1]][0] + "{" + str(n) + "}" \
              for n in range(1, max_len // 2)
            ) + ")"

        else:
          rules[num][0] = "(?:" + "|".join(
            ("(?:" + "".join(rules[x][0] for x in combo) + ")") \
            for combo in rules[num][0]
          ) + ")"

        rules[num][1] = set()
        unparsed.remove(num)

  # Remove all the required rules from the results
  return {num: val[0] for num, val in rules.items()}, strings

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  rules, strings = parse_file(input_file)
  rule = re.compile('^' + rules[0] + '$')
  count = sum(1 for s in strings if rule.match(s))
  print(f'Matching rules: {count}')

  rules, strings = parse_file(input_file, True)
  rule = re.compile('^' + rules[0] + '$')
  count = sum(1 for s in strings if rule.match(s))
  print(f'Matching rules (updated): {count}')

if __name__ == '__main__':
  run()
  sys.exit(0)
