#!/usr/bin/env python3

import operator
import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def eval_equation(equation: str, adv: bool = False, pos: int = 0) -> tuple[int,int]:
  stack = [0, operator.add, 0]

  while pos < len(equation):
    c = equation[pos]
    pos += 1

    if c == ')':
      break

    if c == '(':
      stack[-1], pos = eval_equation(equation, adv, pos)

    elif c == '+':
      stack.append(operator.add)

    elif c == '*':
      stack.append(operator.mul)

    elif c == ' ':
      if isinstance(stack[-1], int):
        if not adv or stack[-2] == operator.add:
          arg2, op, arg1 = stack.pop(), stack.pop(), stack.pop()
          stack.append(op(arg1, arg2))

      else:
        stack.append(0)

    else:
      stack[-1] = stack[-1] * 10 + int(c)

  while len(stack) != 1:
    arg2, op, arg1 = stack.pop(), stack.pop(), stack.pop()
    stack.append(op(arg1, arg2))

  return stack[0], pos

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  result = sum(eval_equation(line.strip())[0] for line in open(input_file))
  print(f'Result: {result}')

  result = sum(eval_equation(line.strip(), True)[0] for line in open(input_file))
  print(f'Result: {result}')

if __name__ == '__main__':
  run()
  sys.exit(0)
