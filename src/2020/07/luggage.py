#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def graphs(lines: list[str]) -> tuple[dict[str, set[str]], dict[str, dict[str,int]]]:
  contained_by_graph = {}
  contains_graph = {}

  for line in lines:
    container, contained = line.split(" contain ")

    container = re.sub(r' bag.*$', '', container)
    contained_list = [re.sub(r' bag.*$', '', bag) for bag in contained.split(", ")]

    contains_graph[container] = {}

    if contained.startswith("no other"):
      continue

    for contained_description in contained_list:
      count, color = contained_description.split(" ", 1)

      if color not in contained_by_graph:
        contained_by_graph[color] = set()
      contained_by_graph[color].add(container)

      contains_graph[container][color] = int(count)

  return contained_by_graph, contains_graph

def count_container_colors(graph: dict[str, set[str]], color: str) -> int:
  containers = set()
  stack = list(graph.get(color, set()))

  while stack:
    bag_color = stack.pop()
    if bag_color not in containers:
      containers.add(bag_color)
      stack.extend(graph.get(bag_color, set()))

  return len(containers)

def sum_required_bags(graph: dict[str, dict[str, int]], color: str) -> int:
  return sum((sum_required_bags(graph, c) + 1) * graph[color][c] for c in graph[color].keys())

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file).readlines()]

  contained_graph, container_graph = graphs(lines)

  print(f'Bag colors: {count_container_colors(contained_graph, "shiny gold")}')
  print(f'Required bags: {sum_required_bags(container_graph, "shiny gold")}')

if __name__ == '__main__':
  run()
  sys.exit(0)
