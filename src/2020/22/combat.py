#!/usr/bin/env python3

import pathlib
import sys

from collections import deque

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def parse_decks(input_file: pathlib.Path) -> tuple[deque[int], deque[int]]:
  deck = deque([])
  decks = []

  for line in open(input_file):
    if line.startswith('Player'):
      deck = deque([])
    else:
       line = line.strip()
       if line == "":
         decks.append(deck)
       else:
         deck.append(int(line))

  decks.append(deck)

  return decks[0], decks[1]

def play_combat(p1: deque[int], p2: deque[int], recursive: bool = False) -> tuple[int, int]:
  seen = set()
  winner = p1 if p1 else p2

  while p1 and p2:
    round_id = "/".join((":".join(str(x) for x in p1), ":".join(str(x) for x in p2)))
    if round_id in seen:
      winner = p1
      break

    seen.add(round_id)

    card1, card2 = p1.popleft(), p2.popleft()

    if recursive and card1 <= len(p1) and card2 <= len(p2):
      _, win_id = play_combat(
        deque(p1[n] for n in range(card1)),
        deque(p2[n] for n in range(card2)),
        True
      )
      winner, card1, card2 = (p1, card1, card2) if win_id == 0 else (p2, card2, card1)

    else:
      winner, card1, card2 = (p1, card1, card2) if card1 > card2 else (p2, card2, card1)

    winner.append(card1)
    winner.append(card2)

  score = sum((len(winner) - i) * v for i, v in enumerate(winner))
  return score, (0 if winner == p1 else 1)

def run() -> None:
  input_file = aoc.inputfile('input.txt')

  p1, p2 = parse_decks(input_file)
  score, _ = play_combat(p1, p2)
  print(f'Winning score: {score}')

  p1, p2 = parse_decks(input_file)
  score, _ = play_combat(p1, p2, True)
  print(f'Winning recursive score: {score}')

if __name__ == '__main__':
  run()
  sys.exit(0)
