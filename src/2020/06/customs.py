#!/usr/bin/env python3

import pathlib
import string
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  groups = open(input_file).read().split("\n\n")

  answers = set(string.ascii_lowercase)

  count_any = sum(len(set(x for x in group if x in answers)) for group in groups)
  print(f'Sum count of any: {count_any}')

  count_all = 0
  for group in groups:
    yes = set() | answers
    for passenger in group.split("\n"):
      yes &= set(x for x in passenger)
    count_all += len(yes)
  print(f'Sum count of all: {count_all}')

if __name__ == '__main__':
  run()
  sys.exit(0)
