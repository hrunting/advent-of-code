#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def seat_ids(lines: list[str]) -> list[int]:
  to_binary = str.maketrans('FLBR', '0011')
  return [int(x.translate(to_binary), 2) for x in lines]

def missing_seat(seats: list[int]) -> int:
  first = -1

  for i, seat in enumerate(sorted(seats)):
    if first < 0:
      first = seat
    if seat - first != i:
      return seat - 1

  return first + 1

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  seats = seat_ids([x.strip() for x in open(input_file).readlines()])

  print(f'Max seat ID: {max(seats)}')
  print(f'My seat ID: {missing_seat(seats)}')

if __name__ == '__main__':
  run()
  sys.exit(0)
