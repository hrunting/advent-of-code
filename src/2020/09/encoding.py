#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def find_wrong_number(nums: list[int], preamble_len: int) -> int:
  sums = {}
  for i in range(preamble_len):
    for j in range(i + 1, preamble_len):
      s = nums[i] + nums[j]
      sums[s] = sums.get(s, 0) + 1

  idx = preamble_len
  while nums[idx] in sums and idx < len(nums):
    for j in range(idx - preamble_len + 1, idx):
      old = nums[idx - preamble_len] + nums[j]
      new = nums[j] + nums[idx]

      if sums[old] == 1:
        del sums[old]
      else:
        sums[old] -= 1

      sums[new] = sums.get(new, 0) + 1

    idx += 1

  return nums[idx]

def find_encryption_weakness(nums: list[int], invalid_num: int) -> int:
  lo, hi = 0, 1
  window_sum = nums[lo] + nums[hi]

  while window_sum != invalid_num:
    if window_sum < invalid_num:
      hi += 1
      window_sum += nums[hi]

    else:
      window_sum -= nums[lo]
      lo += 1

  return min(nums[lo:hi + 1]) + max(nums[lo:hi + 1])

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  nums = [int(x.strip()) for x in open(input_file).readlines()]

  wrong_num = find_wrong_number(nums, 25)
  print(f'First wrong number: {wrong_num}')

  weakness = find_encryption_weakness(nums, wrong_num)
  print(f'Encryption weakness: {weakness}')

if __name__ == '__main__':
  run()
  sys.exit(0)
