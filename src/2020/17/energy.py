#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def active_cubes_3d(space: list[list[list[bool]]], cycles: int) -> int:
  iteration = []

  layers = len(space)
  rows = len(space[0])
  cols = len(space[0][0])

  def state_at_position(x: int, y: int, z: int) -> bool:
    return (0 <= z < layers and
            0 <= y < rows and
            0 <= x < cols and
            space[z][y][x])

  def active_neighbors(x: int, y: int, z: int) -> int:
    return sum(int(state_at_position(x + dx, y + dy, z + dz))
               for dx in range(-1, 2)
               for dy in range(-1, 2)
               for dz in range(-1, 2)
               if not dx == dy == dz == 0)

  for z in range(-1, layers + 1):
    layer = []

    for y in range(-1, rows + 1):
      row = []

      for x in range(-1, cols + 1):
        active = active_neighbors(x, y, z)
        row.append(3 - int(state_at_position(x, y, z)) <= active <= 3)

      layer.append(row)

    iteration.append(layer)

  if cycles > 1:
    return active_cubes_3d(iteration, cycles - 1)

  return sum(int(cell) for layer in iteration for row in layer for cell in row)

def active_cubes_4d(space: list[list[list[list[bool]]]], cycles: int) -> int:
  iteration = []

  worlds = len(space)
  layers = len(space[0])
  rows = len(space[0][0])
  cols = len(space[0][0][0])

  def state_at_position(x: int, y: int, z: int, w: int) -> bool:
    return (0 <= w < worlds and
            0 <= z < layers and
            0 <= y < rows and
            0 <= x < cols and
            space[w][z][y][x])

  def active_neighbors(x: int, y: int, z: int, w: int) -> int:
    return sum(int(state_at_position(x + dx, y + dy, z + dz, w + dw))
               for dx in range(-1, 2)
               for dy in range(-1, 2)
               for dz in range(-1, 2)
               for dw in range(-1, 2)
               if not dx == dy == dz == dw == 0)

  for w in range(-1, worlds + 1):
    world = []

    for z in range(-1, layers + 1):
      layer = []

      for y in range(-1, rows + 1):
        row = []

        for x in range(-1, cols + 1):
          active = active_neighbors(x, y, z, w)
          row.append(3 - int(state_at_position(x, y, z, w)) <= active <= 3)

        layer.append(row)

      world.append(layer)

    iteration.append(world)

  if cycles > 1:
    return active_cubes_4d(iteration, cycles - 1)

  return sum(int(cell) for world in iteration for layer in world for row in layer for cell in row)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  initial_state = open(input_file).read()

  space = [[[x == "#" for x in row.strip()] for row in initial_state.strip().split("\n")]]
  
  active = active_cubes_3d(space, 6)
  print(f'Active cubes (3D): {active}')
  
  active = active_cubes_4d([space], 6)
  print(f'Active cubes (4D): {active}')

if __name__ == '__main__':
  run()
  sys.exit(0)
