#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def path_distance(nav: list[tuple[str, int]]) -> int:
  pos = 0 + 0j
  dir = 1 + 0j

  for cmd, val in nav:
    if cmd == 'N':
      pos += val * 1j
    elif cmd == 'E':
      pos += val
    elif cmd == 'S':
      pos -= val * 1j
    elif cmd == 'W':
      pos -= val

    elif cmd == 'L':
      dir *= 1j ** (val // 90)
    elif cmd == 'R':
      dir *= (-1j) ** (val // 90)

    elif cmd == 'F':
      pos += val * dir

  return int(abs(pos.real) + abs(pos.imag))

def waypoint_distance(nav: list[tuple[str, int]]) -> int:
  pos = 0 + 0j
  wp = 10 + 1j

  for cmd, val in nav:
    if cmd == 'N':
      wp += val * 1j
    elif cmd == 'E':
      wp += val
    elif cmd == 'S':
      wp -= val * 1j
    elif cmd == 'W':
      wp -= val

    elif cmd == 'L':
      wp *= 1j ** (val // 90)
    elif cmd == 'R':
      wp *= (-1j) ** (val // 90)

    elif cmd == 'F':
      pos += wp * val

  return int(abs(pos.real) + abs(pos.imag))

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  nav = [(line[0], int(line[1:].strip())) for line in open(input_file).readlines()]

  distance = path_distance(nav)
  print(f'Path distance traveled: {distance}')

  distance = waypoint_distance(nav)
  print(f'Waypoint distance traveled: {distance}')

if __name__ == '__main__':
  run()
  sys.exit(0)
