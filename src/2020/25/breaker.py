#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

MODULUS = 20201227
SUBJECT_NUM = 7

def calc_loops(result: int) -> int:
  val = 1
  loops = 0
  while val != result:
    val *= SUBJECT_NUM
    if val > MODULUS:
      val %= MODULUS
    loops += 1

  return loops

def transform(subject_num: int, loops: int) -> int:
  return pow(subject_num, loops, MODULUS)

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  cpk, dpk = tuple(int(line.strip()) for line in open(input_file).readlines())
  print(f'Encryption Key: {transform(dpk, calc_loops(cpk))}')

if __name__ == '__main__':
  run()
  sys.exit(0)
