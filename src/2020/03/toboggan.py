#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def count_trees(lines: list[str], right: int, down: int) -> int:
  width = len(lines[0])
  x, y = right, down
  trees = 0

  while y < len(lines):
    if lines[y][x] == "#":
      trees += 1

    x = (x + right) % width
    y += down

  return trees

def run() -> None:
  input_file = aoc.inputfile('input.txt')
  lines = [x.strip() for x in open(input_file).readlines()]

  trees = count_trees(lines, 3, 1)

  print(f'Trees: {trees}')

  slopes = [(1, 1), (5, 1), (7, 1), (1, 2)]
  for right, down in slopes:
    trees *= count_trees(lines, right, down)

  print(f'Slopes Multplied: {trees}')

if __name__ == '__main__':
  run()
  sys.exit(0)
