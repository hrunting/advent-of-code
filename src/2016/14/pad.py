#!/usr/bin/env python3

import _md5 as hashlib  # pyright: ignore[reportMissingImports]
import collections
import multiprocessing as mp
import re
import sys

from itertools import count, takewhile
from typing import Callable

def md5_initialize(function: Callable[[int], str], salt: str, stretch: bool) -> None:
  function.hash = hashlib.md5(salt.encode(), usedforsecurity=False)
  function.stretch = stretch

def md5_process(index: int) -> str:
  h = md5_process.hash.copy()
  h.update(str(index).encode())

  digest = h.hexdigest()
  if md5_process.stretch:
    for _ in range(2016):
      digest = hashlib.md5(digest.encode()).hexdigest()

  return digest

def run() -> None:
  salt = "jlmsuwbz"

  match3 = re.compile(r'(.)\1{2,}')
  match5 = re.compile(r'(.)\1{4,}')

  for part in range(2):
    stretch = part > 0

    keys = []
    matches = collections.defaultdict(list)

    with mp.Pool(None, md5_initialize, (md5_process, salt, stretch)) as pool:
      for idx, hash in enumerate(pool.imap(md5_process, takewhile(lambda _: len(keys) < 64, count()), 40)):
        if m := match3.search(hash):
          for c in match5.findall(hash):
            for i, key in matches[c]:
              if idx - i <= 1000:
                keys.append((i, key))

            del matches[c]

          matches[m.group(1)].append((idx, hash))

    print(f"Part {part + 1} 64th Key Index: {sorted(keys)[63]}")

if __name__ == "__main__":
  run()
  sys.exit(0)
