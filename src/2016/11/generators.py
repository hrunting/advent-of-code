#!/usr/bin/env python3

import sys

from copy import deepcopy
from heapq import heappop, heappush
from itertools import combinations
from typing import Self

class Building:
  def __init__(self: Self, floors: list[list[str]], step: int) -> None:
    self.unit_map = {}
    self.floor_map = {x: set() for x in range(len(floors))}
    self.elevator = 0
    self.floors = len(floors)
    self.step = step

    for i, floor in enumerate(floors):
      for unit in floor:
        self.unit_map[unit] = i
        self.floor_map[i].add(unit)

  def __str__(self: Self) -> str:
    units = sorted(self.unit_map.keys())
    display = ""
    for floor_id in reversed(sorted(self.floor_map.keys())):
      floor = self.floor_map[floor_id]
      display += "\n"
      display += " ".join(
        ["E" if self.elevator == floor_id else " "] +
        [u if u in floor else " . " for u in units]
      )
    return display

  def score(self: Self) -> int:
    return self.step - len(self.floor_map[3]) * 10

  def xid(self: Self) -> str:
    floors = []
    for floor_id in reversed(range(self.floors)):
      floor = self.floor_map[floor_id]
      chips = {u[1:] for u in floor if u[0] == "M"}
      gens = {u[1:] for u in floor if u[0] == "G"}

      units = []

      units += ["Pair" for x in gens.intersection(chips)]
      units += sorted(["G" + str(self.unit_map["M" + x]) for x in gens.difference(chips)])
      units += sorted(["M" + str(self.unit_map["G" + x]) for x in chips.difference(gens)])

      floors += [units]

    return "|".join([str(self.elevator)] + [" ".join(x) for x in floors])

  def possible_moves(self: Self) -> list[list[tuple[int] | tuple[int, int] | int]]:
    moves = []
    floor = self.floor_map[self.elevator]
    min_floor = min([x for x in range(self.floors) if len(self.floor_map[x]) > 0])

    if self.elevator < self.floors - 1:
      moves += [[x, 1] for x in sorted(combinations(floor, 2))] + \
               [[(x,), 1] for x in sorted(floor)]

    if self.elevator != min_floor:
      moves += [[(x,), -1] for x in sorted(floor)] + \
               [[x, -1] for x in sorted(combinations(floor, 2))]

    return moves

  def move(self: Self, units: list[list[str]], move: int) -> Self | None:
    new = deepcopy(self)
    for unit in units:
      new.floor_map[self.elevator].remove(unit)
      new.floor_map[self.elevator + move].add(unit)
      new.unit_map[unit] = self.elevator + move

    new.elevator = self.elevator + move
    new.step = self.step + 1
    if new.validate(self.elevator) and new.validate(new.elevator):
      return new

    return None

  def validate(self: Self, floor_id: int) -> bool:
    floor = self.floor_map[floor_id]
    microchips = [x for x in floor if x[0] == "M"]
    generators = floor.difference(microchips)

    if len(generators):
      for m in microchips:
        if "G" + m[1:] not in generators:
          return False

    return True

  def moved(self: Self) -> bool:
    top_floor = max(self.floor_map.keys())
    for floor_id in self.floor_map.keys():
      if floor_id != top_floor and len(self.floor_map[floor_id]) > 0:
        return False
    return True

def run() -> None:
  floors = [
    ["GPm", "MPm"],
    ["GCo", "GCm", "GRu", "GPu"],
    ["MCo", "MCm", "MRu", "MPu"],
    []
  ]

  for part in range(2):
    steps = 0
    if part:
      floors[0] += ["GEl", "MEl", "GDL", "MDL"]

    building = Building(floors, steps)
    scenarios = []
    seen = {}
    count = 0

    heappush(scenarios, (building.score(), building.step, count, building))

    while len(scenarios) > 0:
      (_, _, _, building) = heappop(scenarios)
      if building.xid() in seen and building.step >= seen[building.xid()]:
        continue

      seen[building.xid()] = building.step

      moves = building.possible_moves()

      move2up = False
      move1down = False

      for units, move in moves:
        new = building.move(units, move)

        if new:
          if new.moved():
            steps = new.step
            break

          elif new.xid() not in seen or new.step < seen[new.xid()]:
            unitcount = len(units)

            if move2up and move > 0 and unitcount == 1:
              continue
            elif move1down and move < 0 and unitcount == 2:
              continue

            if not move2up and move > 0:
              move2up = unitcount == 2
            elif not move1down and move < 0:
              move1down = unitcount == 1

            heappush(scenarios, (new.score(), new.step, count, new))
            count += 1

      if steps != 0:
        break

    print(f"Part {part + 1} - Minimum Steps: {steps}")

if __name__ == "__main__":
  run()
  sys.exit(0)
