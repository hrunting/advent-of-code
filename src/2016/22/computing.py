#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def distance(n1: tuple[int, int], n2: tuple[int, int]) -> int:
  return abs(n1[0] - n2[0]) + abs(n1[1] - n2[1])

def find_wall_gaps(cells: list[list[str]], x: int, y: int):
  gaps = []

  if cells[0][0] == cells[1][0]:
    for i in range(y):
      if (cells[0][0], i) not in cells:
        gaps.append((cells[0][0], i))
  else:
    for i in range(x):
      if (i, cells[0][1]) not in cells:
        gaps.append((i, cells[0][1]))

  return gaps

def run() -> None:
  devices = {}
  node_re = re.compile(r'/dev/grid/node-x(\d+)-y(\d+)')
  (mx, my) = (0, 0)

  for line in open(aoc.inputfile("input.txt")):
    if line.find("/dev") != 0:
      continue

    (node, size, used, avail, use_perc) = line.strip().split()

    size = int(size[:-1])
    used = int(used[:-1])
    avail = int(avail[:-1])
    use_perc = int(use_perc[:-1])

    if m := node_re.match(node):
      (nx, ny) = (int(x) for x in m.group(1, 2))
      if nx > mx:
        mx = nx
      if ny > my:
        my = ny

      devices[(nx, ny)] = [node, size, used, avail, use_perc]

  count = len(devices)
  by_used = sorted(devices.values(), key=lambda x: x[2])
  by_avail = sorted(devices.values(), key=lambda x: x[3])

  viable_pairs = 0
  ptr = 0

  for (node, _, used, _, _) in by_used:
    if used == 0:
      continue

    while ptr < count and by_avail[ptr][3] < used:
      ptr += 1

    viable_pairs += len([x for x in by_avail[ptr:] if x[0] != node])

  print(f"Viable Pairs: {viable_pairs}")

  last_x = 0

  wall_cells = []
  empty_cell = (-1, -1)
  goal_cell = (0, 0)
  data_cell = (mx, 0)

  for (x, y), (_, _, used, _, use_perc) in sorted(devices.items()):
    if x != last_x:
      print("")
      last_x = x

    c = "."
    if use_perc > 95:
      c = "#"
      wall_cells.append((x, y))
    elif used == 0:
      c = "_"
      empty_cell = (x, y)
    elif (x, y) == data_cell:
      c = "!"
    elif (x, y) == goal_cell:
      c = "G"

    print(f"{c}", end="")

  print("")

  # At this point, you can visualize the problem and see the wall of cells
  # that can't be used for moving and the location of our empty cell.
  # We need to move the empty cell to (mx - 1, 0), then use a common
  # pattern to move the goal up.
  #
  # Moving the empty cell to (mx - 1, 0) is a calculation of the
  # taxicab distance to the nearest gap in the wall, then past the wall,
  # then over to the spot between the goal and the data cell. Moving the
  # data cell to our cell is 5 moves for each cell you move up

  # calculate the number of steps through the nearest wall gap
  # to the cell next to the data cell in the line between the data cell
  # and the goal cell
  (steps, wall_gap) = min([
    (distance(empty_cell, c) + distance((data_cell[0] - 1, 0), c), c)
      for c in find_wall_gaps(wall_cells, mx, my)
  ])

  print(f"Empty Cell: {empty_cell}")
  print(f"Wall Gap: {wall_gap}")

  # move data cell to the empty cell
  steps += 1

  # move the data cell to the goal cell by moving the empty cell
  # from its previous spot (behind the data cell) to the move spot
  # (ahead of the data cell) -- takes 5 steps
  steps += 5 * (data_cell[0] - 1)

  print(f"Steps: {steps}")

if __name__ == "__main__":
  run()
  sys.exit(0)
