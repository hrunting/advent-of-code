#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  rules = []
  for line in open(aoc.inputfile("input.txt")):
    (start_ip, end_ip) = (int(x) for x in line.strip().split("-"))
    rules.append((start_ip, end_ip))

  rules = sorted(rules)

  min_ip = 4294967295
  latest_blocked = -1
  unblocked = 0

  for (start_ip, end_ip) in rules:
    if start_ip <= latest_blocked + 1:
      if end_ip > latest_blocked:
        latest_blocked = end_ip

    else:
      if min_ip > latest_blocked:
        min_ip = latest_blocked + 1
      unblocked += start_ip - 1 - latest_blocked
      latest_blocked = end_ip

  if latest_blocked != 4294967295:
    unblocked += 4294967295 - latest_blocked

  print(f"Minimum Available IP: {min_ip}")
  print(f"Unblocked IPs: {unblocked}")

if __name__ == "__main__":
  run()
  sys.exit(0)
