#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  chars = []
  message = ""
  modified = ""

  for line in open(aoc.inputfile("input.txt")):
    word = line.rstrip()
    if len(chars) < len(word):
      for i in range(len(chars), len(word)):
        chars += [{}]

    for i, char in enumerate(word):
      chars[i][char] = chars[i].get(char, 0) + 1

  for charlist in chars:
    message += max(charlist.keys(), key=lambda c: charlist[c])
    modified += min(charlist.keys(), key=lambda c: charlist[c])

  print(f"Message: {message}")
  print(f"Modified Message: {modified}")

if __name__ == "__main__":
  run()
  sys.exit(0)
