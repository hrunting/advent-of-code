#!/usr/bin/env python3

import sys

from typing import Self

class Item:
  def __init__(self: Self, i: int) -> None:
    self.id = i
    self.next = self
    self.prev = self

  def delete(self: Self) -> None:
    self.prev.next = self.next
    self.next.prev = self.prev

def run():
  count = 3012210

  for mode in (1, 2):
    skip_victim = bool(count % 2)

    # setup a linked list of elves
    elves = [Item(x + 1) for x in range(count)]
    for i in range(count):
      elves[i].next = elves[(i + 1) % count]
      elves[i].prev = elves[(i - 1) % count]

    # the starting elf and the elf across from it
    start = elves[0]
    if mode == 1:
      victim = elves[0].next
    else:
      victim = elves[count // 2]

    while start.id != start.next.id:
      victim.delete()

      # move to the next elf
      start = start.next

      # victim is next elf in the circle
      if mode == 1:
        victim = start.next

      # victim is the elf across the circle
      # (seated 1 or 2 spots ahead of current victim)
      else:
        victim = victim.next

        # skip a second elf if the number of elves remaining is odd
        if skip_victim:
          victim = victim.next

        skip_victim = not skip_victim

    print(f"Final Elf (Round {mode}): {start.id}")

if __name__ == "__main__":
  run()
  sys.exit(0)
