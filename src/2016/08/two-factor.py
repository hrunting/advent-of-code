#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Screen:
  def __init__(self: Self, width: int, height: int) -> None:
    self.width = width
    self.height = height
    self.pixels = [[0] * width for _ in range(height)]

  def rect_on(self: Self, width: int, height: int) -> None:
    width = min(width, self.width)
    height = min(height, self.height)

    pixel_set = [[1] * width for _ in range(height)]
    for r, row in enumerate(pixel_set):
      for c, cell in enumerate(row):
        self.pixels[r][c] |= cell

  def __str__(self: Self) -> str:
    display = ""
    for row in self.pixels:
      display += "\n"
      for cell in row:
        display += "#" if cell == 1 else "."
    return display

  def shift_row(self: Self, idx: int, amount: int) -> None:
    amount %= self.width
    if amount > 0:
      shift_idx = self.width - amount
      row = self.pixels[idx]
      self.pixels[idx] = row[shift_idx:] + row[0:shift_idx]

  def shift_col(self: Self, idx: int, amount: int) -> None:
    if amount > 0:
      shift_idx = self.height - amount
      col = [row[idx] for row in self.pixels]
      col = col[shift_idx:] + col[0:shift_idx]
      for c, cell in enumerate(col):
        self.pixels[c][idx] = cell

  def count_on(self: Self) -> int:
    return sum([cell for row in self.pixels for cell in row])

def run() -> None:
  screen = Screen(50, 6)

  for line in open(aoc.inputfile("input.txt")):
    (cmd, args) = line.strip().split(None, 1)
    if cmd == "rect":
      (width, height) = args.split("x")
      screen.rect_on(int(width), int(height))
    elif cmd == "rotate":
      (unit, idx, _, amount) = args.split()
      idx = idx[2:]
      if unit == "row":
        screen.shift_row(int(idx), int(amount))
      elif unit == "column":
        screen.shift_col(int(idx), int(amount))

  print(f"Active pixels: {screen.count_on()}")
  print(f"Screen: {screen}")

if __name__ == "__main__":
  run()
  sys.exit(0)
