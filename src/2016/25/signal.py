#!/usr/bin/env python3

import copy
import pathlib
import sys

from typing import Callable, Optional, Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  def __init__(self: Self) -> None:
    self.stack = []
    self.stacklen = 0
    self.state = []
    self.ptr = 0
    self.registers = {}

    self.clear()

  def __str__(self: Self) -> str:
    return (
      f"stack={self.stacklen} ptr={self.ptr} "
      f"{' '.join(f'{x}={self.registers[x]}' for x in ('a', 'b', 'c', 'd'))}"
    )

  def load(self: Self, instr: list[str]) -> Self:
    self.stack += [instr]
    self.stacklen += 1
    self.state += [{}]
    return self

  def clear(self: Self) -> Self:
    self.stacklen = len(self.stack)
    self.stack = []
    self.state = []
    return self.reset()

  def reset(self: Self) -> Self:
    self.ptr = 0
    self.registers = {"a": 0, "b": 0, "c": 0, "d": 0}
    return self

  def rval(self: Self, i: str | int) -> int:
    return int(self.registers.get(str(i), i))

  def step(self: Self, output: Optional[Callable[[int], None]] = None) -> Self:
    instr = self.stack[self.ptr]
    next_ptr = self.ptr + 1

    self.state[self.ptr] = copy.deepcopy(self.registers)

    if instr[0] == "tgl":
      t_ptr = self.rval(instr[1]) + self.ptr
      if t_ptr >= 0 and t_ptr < len(self.stack):
        self.toggle(t_ptr)
    elif instr[0] == "jnz":
      j_ptr = self.ptr + self.rval(instr[2])

      # basic loop detect and optimize
      if self.rval(instr[1]) != 0:

        # fixed step backwards == loop
        # calculate difference since last run and apply multiplier
        # we don't do this if the steps backwards comes from a register
        if (
          instr[2] not in self.registers and
          j_ptr < self.ptr and
          len([
            x
              for x in self.stack[j_ptr:self.ptr]
              if x[0] == "jnz" and x[2] not in self.registers.keys() and int(x[2]) > 0
          ]) == 0 and
          self.state[j_ptr]
        ):

          loops = self.rval(instr[1])
          for r in self.registers.keys():
            self.registers[r] += (self.registers[r] - self.state[j_ptr][r]) * loops

        else:
          next_ptr = j_ptr
    elif instr[0] == "cpy":
      if instr[2] in self.registers:
        self.registers[instr[2]] = self.rval(instr[1])
    elif instr[0] == "inc":
      if instr[1] in self.registers:
        self.registers[instr[1]] += 1
    elif instr[0] == "dec":
      if instr[1] in self.registers:
        self.registers[instr[1]] += -1
    elif instr[0] == "out":
      val = self.rval(instr[1])
      if output is not None:
        output(val)
      else:
        print(f"{val}")
        sys.stdout.flush()

    self.ptr = next_ptr
    return self

  def toggle(self: Self, ptr: int) -> Self:
    if self.stack[ptr][0] == "jnz":
      self.stack[ptr][0] = "cpy"
    elif self.stack[ptr][0] == "inc":
      self.stack[ptr][0] = "dec"
    elif len(self.stack[ptr][1:]) > 1:
      self.stack[ptr][0] = "jnz"
    else:
      self.stack[ptr][0] = "inc"

    return self

  def run(self: Self) -> Self:
    while self.ptr < self.stacklen:
      self.step()
    return self

def run() -> None:
  machine = Machine()
  for line in open(aoc.inputfile("input.txt")):
    machine.load(line.strip().split())

  # We effectively get to a loop where A gets B=362 * C=7 added, so our A
  # value gets 2534 added to it.
  #
  # The meat of the B output happens in a loop that ends at line 20
  # based on a value in the A-register. If the C register at this state
  # is 2, a 0 will be output. If the C register is 1, a 1 will
  # be output. This C register is based on the previous run's A
  # value: if A was odd, C is odd; if A was even, C is even. A halves
  # every loop through here (flooring if A is odd).
  #
  # Therefore, from the final state of A=0, C=1, we can work backwards:
  # A=0, C=1
  # A=1, C=2
  # A=2, C=1
  # A=5, C=2
  # A=10, C=1
  # A=21, C=2
  # A=42, C=1
  # A=85, C=2
  # A=170, C=1
  # A=341, C=2
  # A=682, C=1
  # A=1365, C=2
  # A=2730, C=1
  #
  # A>2534, so our starting value will be the difference: 2730 - 2534 = 196

  val = 0
  prev = -1
  seen = set()
  exit = 0

  def handler(v: int) -> None:
    nonlocal exit, prev

    # our values are not alternating
    if prev >= 0 and v != int(not prev):
      exit = -1
      return

    state = (v, machine.ptr, tuple(machine.registers[x] for x in ('a', 'b', 'c', 'd')))
    if state in seen:
      exit = 1
    else:
      seen.add(state)
      prev = v

  while True:
    prev = -1
    seen = set()
    exit = 0

    machine.reset()
    machine.registers["a"] = val

    while exit == 0 and machine.ptr < machine.stacklen:
      machine.step(handler)

    if exit > 0:
      break

    val += 1

  print(f"Register A value for endless loop: {val}")

if __name__ == "__main__":
  run()
  sys.exit(0)
