#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def is_valid(row: list[int]) -> bool:
  return sum(row) - max(row) > max(row)

def run() -> None:
  row_triangles = 0
  col_triangles = 0

  column_set = []

  for line in open(aoc.inputfile("input.txt")):
    sides = [int(x) for x in line.strip().split()]
    column_set.append(sides)

    row_triangles += int(is_valid(sides))
    if len(column_set) == 3:
      for idx in range(3):
        col_triangles += int(is_valid([row[idx] for row in column_set]))
      column_set = []

  print(f"Row Triangles: {row_triangles}")
  print(f"Column Triangles: {col_triangles}")

if __name__ == "__main__":
  run()
  sys.exit(0)
