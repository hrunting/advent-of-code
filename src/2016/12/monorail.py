#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  def __init__(self: Self) -> None:
    self.ptr = 0
    self.stack = []
    self.registers = {x: 0 for x in ('a', 'b', 'c', 'd')}

  def __str__(self: Self) -> str:
    return (
      f"stack={len(self.stack)} ptr={self.ptr} "
      f"{' '.join(f'{x}={self.registers[x]}' for x in ('a','b','c','d'))}"
    )

  def load(self: Self, instr: list[str]) -> Self:
    self.stack += [instr]
    return self

  def clear(self: Self) -> Self:
    self.stack = []
    return self.reset()

  def reset(self: Self) -> Self:
    self.ptr = 0
    self.registers = {"a": 0, "b": 0, "c": 0, "d": 0}
    return self

  def run(self: Self) -> Self:
    registers = self.registers
    instructions = self.stack
    ip = self.ptr

    while ip < len(instructions):
      instr = instructions[ip]
      ip += 1

      if instr[0] == "cpy":
        registers[instr[2]] = int(registers.get(instr[1], instr[1]))
      elif instr[0] == "jnz":
        if int(registers.get(instr[1], instr[1])) != 0:
          ip += int(registers.get(instr[2], instr[2])) - 1
      elif instr[0] == "inc":
        registers[instr[1]] += 1
      elif instr[0] == "dec":
        registers[instr[1]] += -1

    self.ptr = ip
    return self

def run() -> None:
  machine = Machine()
  for line in open(aoc.inputfile("input.txt")):
    machine.load(line.strip().split())
  machine.run()

  print(f"Part 1 machine state: {machine}")

  machine.reset()
  machine.registers["c"] = 1
  machine.run()

  print(f"Part 2 machine state: {machine}")

if __name__ == "__main__":
  run()
  sys.exit(0)
