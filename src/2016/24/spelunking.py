#!/usr/bin/env python3

import pathlib
import sys

from heapq import heappop, heappush

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  duct = []
  points = {}
  paths = {}

  for line in open(aoc.inputfile("input.txt")):
    row = [c for c in line.strip()]

    # find any points of interest in this row
    for i, c in enumerate(row):
      if c != "." and c != "#":
        points[c] = (i, len(duct))

    duct.append(row)

  # for each point, we want to find the shortest path to each
  # of the other points of interest
  for c, point in points.items():
    steps = {}
    path = []
    seen = set()

    heappush(path, (0, point))

    while len(steps.keys()) < len(points.keys()) - 1 and len(path):
      (count, (x, y)) = heappop(path)
      if (x, y) in seen:
        continue

      seen |= {(x, y)}

      if duct[y][x] != "." and duct[y][x] != c:
        steps[(min(duct[y][x], c), max(duct[y][x], c))] = count

      for i, j in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
        if not 0 < i < len(duct[0]) or not 0 < j < len(duct):
          continue
        elif duct[j][i] == "#":
          continue

        heappush(path, (count + 1, (i, j)))

    for path in steps.keys():
      paths[path] = steps[path]

  part1 = False

  path = []
  heappush(path, (0, ["0"]))

  while len(path) > 0:
    (steps, seen) = heappop(path)

    if len(seen) == len(points.keys()) + 1:
      print("Steps: {0}  Path: {1}".format(steps, seen))
      break

    elif len(seen) == len(points.keys()):
      if not part1:
        part1 = True
        print("Steps: {0}  Path: {1}".format(steps, seen))

      heappush(path, (steps + paths[min("0", seen[-1]), max("0", seen[-1])], seen + ["0"]))

    else:
      for c in points.keys():
        if c in seen:
          continue

        heappush(path, (steps + paths[min(c, seen[-1]), max(c, seen[-1])], seen + [c]))

if __name__ == "__main__":
  run()
  sys.exit(0)
