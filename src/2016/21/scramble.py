#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def swap(s: list[str], args: str) -> None:
  (_type, x, _, _, y) = args.split(" ")
  if _type == "position":
    x, y = int(x), int(y)
    s[x], s[y] = s[y], s[x]
  else:
    for i, v in enumerate(s):
      s[i] = x if v == y else y if v == x else s[i]

def rotate(s: list[str], args: str, rev: bool = False) -> None:
  (dir, args) = args.split(" ", 1)

  rotations = 0

  if dir == "based":
    (_, _, _, _, char) = args.split(" ")

    if rev:
      idx_table = {0: 1, 1: 1, 2: 6, 3: 2, 4: 7, 5: 3, 6: 8, 7: 4}
      rotations = idx_table[int(s.index(char))]
    else:
      rotations = s.index(char)
      rotations += 2 if rotations >= 4 else 1

  else:
    (rotations, _) = args.split(" ", 1)
    rotations = int(rotations)
    if dir == "left":
      rotations = -rotations

  if abs(rotations) > len(s):
    rotations -= len(s) * (abs(rotations) // rotations)

  if rev:
    rotations = -rotations

  # at this point, all rotations are "right" rotations
  # rotate appropriately
  s[0:len(s)] = s[-rotations:] + s[:-(rotations)]

def reverse(s: list[str], args: str) -> None:
  (_, x, _, y) = args.split(" ")
  x, y = int(x), int(y)
  s[x:y + 1] = reversed(s[x:y + 1])

def move(s: list[str], args: str, rev: bool = False) -> None:
  (_, x, _, _, y) = args.split(" ")
  x, y = (int(y), int(x)) if rev else (int(x), int(y))
  if x < y:
    s[0:len(s)] = s[0:x] + s[x + 1:y + 1] + s[x:x + 1] + s[y + 1:len(s)]
  elif x > y:
    s[0:len(s)] = s[0:y] + s[x:x + 1] + s[y:x] + s[x + 1:len(s)]

def scramble(s: str, rev: bool = False) -> str:
  pw = [x for x in s]

  cmds = []
  for line in open(aoc.inputfile("input.txt")):
    (cmd, data) = line.strip().split(" ", 1)
    cmds.append((cmd, data))
  if rev:
    cmds = reversed(cmds)

  for cmd, data in cmds:
    if cmd == "swap":
      swap(pw, data)
    elif cmd == "rotate":
      rotate(pw, data, rev)
    elif cmd == "reverse":
      reverse(pw, data)
    elif cmd == "move":
      move(pw, data, rev)

  return ''.join(pw)

def run() -> None:
  print(f"Scrambled Password: {scramble('abcdefgh')}")
  print(f"Unscrambled Password: {scramble('fbgdceah', True)}")

if __name__ == "__main__":
  run()
  sys.exit(0)
