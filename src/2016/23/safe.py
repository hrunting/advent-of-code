#!/usr/bin/env python3

import copy
import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Machine:
  def __init__(self: Self) -> None:
    self.stack = []
    self.stacklen = 0
    self.state = []
    self.ptr = 0
    self.registers = {}

    self.clear()

  def __str__(self: Self) -> str:
    return (
      f"stack={self.stacklen} ptr={self.ptr} "
      f"{' '.join(f'{x}={self.registers[x]}' for x in ('a', 'b', 'c', 'd'))}"
    )

  def load(self: Self, instr: list[str]) -> Self:
    self.stack += [instr]
    self.stacklen += 1
    self.state += [{}]
    return self

  def clear(self: Self) -> Self:
    self.stack = []
    self.stacklen = len(self.stack)
    return self.reset()

  def reset(self: Self) -> Self:
    self.ptr = 0
    self.registers = {x: 0 for x in ('a', 'b', 'c', 'd')}
    self.state = [{} for _ in range(self.stacklen)]
    return self

  def rval(self: Self, i: int | str) -> int:
    return int(self.registers.get(str(i), i))

  def step(self: Self) -> Self:
    instr = self.stack[self.ptr]
    next_ptr = self.ptr + 1

    self.state[self.ptr] = copy.deepcopy(self.registers)

    if instr[0] == "tgl":
      t_ptr = self.rval(instr[1]) + self.ptr
      if t_ptr >= 0 and t_ptr < len(self.stack):
        self.toggle(t_ptr)

    elif instr[0] == "jnz":
      j_ptr = self.ptr + self.rval(instr[2])

      # basic loop detect and optimize
      if self.rval(instr[1]) != 0:

        # fixed step backwards == loop
        # calculate difference since last run and apply multiplier
        # we don't do this if the steps backwards comes from a register
        if (
          instr[2] not in self.registers and
          j_ptr < self.ptr and
          len([
            x
              for x in self.stack[j_ptr:self.ptr]
              if x[0] == "jnz" and x[2] not in self.registers.keys() and int(x[2]) > 0
          ]) == 0 and
          self.state[j_ptr]
        ):

          loops = self.rval(instr[1])
          for r in self.registers.keys():
            self.registers[r] += (self.registers[r] - self.state[j_ptr][r]) * loops

        else:
          next_ptr = j_ptr

    elif instr[0] == "cpy":
      if instr[2] in self.registers:
        self.registers[instr[2]] = self.rval(instr[1])
    elif instr[0] == "inc":
      if instr[1] in self.registers:
        self.registers[instr[1]] += 1
    elif instr[0] == "dec":
      if instr[1] in self.registers:
        self.registers[instr[1]] += -1

    self.ptr = next_ptr
    return self

  def toggle(self: Self, ptr: int) -> Self:
    if self.stack[ptr][0] == "jnz":
      self.stack[ptr][0] = "cpy"
    elif self.stack[ptr][0] == "inc":
      self.stack[ptr][0] = "dec"
    elif len(self.stack[ptr][1:]) > 1:
      self.stack[ptr][0] = "jnz"
    else:
      self.stack[ptr][0] = "inc"

    return self

  def run(self: Self) -> Self:
    while self.ptr < self.stacklen:
      self.step()
    return self

def run() -> None:
  for eggs in (7, 12):
    machine = Machine()
    for line in open(aoc.inputfile("input.txt")):
      machine.load(line.strip().split())

    machine.registers["a"] = eggs
    machine.run()

    print(f"Machine ({eggs} eggs): {machine}")

if __name__ == "__main__":
  run()
  sys.exit(0)