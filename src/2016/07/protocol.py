#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def run() -> None:
  tls_ips = 0
  ssl_ips = 0

  for line in open(aoc.inputfile("input.txt")):
    ip = line.strip()
    if re.search(r'(.)(?!\1)(.)\2\1', ip) and \
       not re.search(r'\[[^\]]*([^\]])(?!\1)([^\]])\2\1[^\]]*\]', ip):
      tls_ips += 1

    m1 = re.search(r'(?:\A|\])[^\[]*?(.)(?!\1)(.)\1.*\[[^\]]*?\2\1\2', ip)
    m2 = re.search(r'\[[^\]]*?(.)(?!\1)(.)\1[^\]]*\](?:[^\[]*\[[^\]]*?\])*?[^\[]*?\2\1\2', ip)
    if m1 or m2:
      ssl_ips += 1

  print(f"TLS IPs: {tls_ips}")
  print(f"SSL IPs: {ssl_ips}")

if __name__ == "__main__":
  run()
  sys.exit(0)
