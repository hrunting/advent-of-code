#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def calc_row(r: list[list[bool]]) -> None:
  if len(r) > 2:
    r[0][1] = r[1][0]
    r[-1][1] = r[-2][0]
    for i in range(1, len(r) - 1):
      r[i][1] = r[i - 1][0] == r[i + 1][0]

def run() -> None:
  start = []

  for line in open(aoc.inputfile("input.txt")):
    start = [[True if x == "." else False, False] for x in line.strip()]
    calc_row(start)

  for rows in (40, 400000):
    floor = [start]
    safe = sum([int(x[0]) for x in start])

    for i in range(1, rows):
      floor.append([[x[1], False] for x in floor[-1]])
      calc_row(floor[-1])
      safe += sum([int(x[0]) for x in floor[-1]])

    print(f"Safe tiles in {len(floor)} rows: {safe}")

if __name__ == "__main__":
  run()
  sys.exit(0)
