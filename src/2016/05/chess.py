#!/usr/bin/env python3

import _md5 as hashlib  # pyright: ignore[reportMissingImports]
import multiprocessing as mp
import sys

from itertools import count, takewhile
from typing import Callable

# Optimizations:
#
# Using the built-in MD5 implementation (_md5) is faster than using
# the MD5 implementation chosen by hashlib (openssl.md5). For the
# small # messages being hashed here, the FFI overhead is greater
# than the C-implementation speed-up.
#
# The multiprocessing module allows using all cores independently
# to generate MD5 hashes.
#
# Starting with a copy of the MD5 digest pre-seeded with the door ID
# avoids some small concatenation and byte-encoding overhead.
#
# Using bit shifts and integer comparisons of the first 3-4 bytes
# is faster than generating a hexadecimal representation of the
# entire hash and doing string comparisons on the result.

def md5_initialize(function: Callable[[int], bytes], root: str) -> None:
  function.hash = hashlib.md5(root.encode("latin1"), usedforsecurity=False)

def md5_process(index: int) -> bytes:
  h = md5_process.hash.copy()
  h.update(str(index).encode("latin1"))
  return h.digest()[:4]

def run() -> None:
  door_id = "wtnhxymk"

  code1 = ""
  code2 = [''] * 8
  chars = 0

  with mp.Pool(None, md5_initialize, (md5_process, door_id)) as pool:
    for digest in pool.imap(md5_process, takewhile(lambda _: chars < 8, count()), 10000):
      if digest[0] == 0 and digest[1] == 0 and digest[2] >> 4 == 0:
        c = digest[2] & 0x0f
        if c < 8 and code2[c] == '':
          chars += 1
          code2[c] = f'{(digest[3] >> 4):x}'
        if len(code1) < 8:
          code1 += f'{c:x}'

  print(f"First Password: {code1}")
  print(f"Second Password: {''.join(code2)}")

if __name__ == "__main__":
  run()
  sys.exit(0)
