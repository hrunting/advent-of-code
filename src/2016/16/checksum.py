#!/usr/bin/env python3

import sys

def cksum(parity: int, input_length: int, size: int) -> str:
  parity_bits = []

  increment = size & -size
  previous_parity = 0

  for length in range(increment, size + 1, increment):
    dragons = length // (input_length + 1)
    cycles, remainder = divmod((length - dragons), (input_length * 2))

    p = dragon_parity(dragons)
    p ^= cycles & input_length
    p ^= parity >> remainder
    p &= 1

    parity_bits.append("10"[p ^ previous_parity])
    previous_parity = p

  return ''.join(parity_bits)

def input_parity(input: str) -> int:
  input_parity = 0
  parity = 0

  for i, c in enumerate(input):
    parity ^= int(c)
    input_parity ^= parity << (i + 1)

  for i, c in enumerate(reversed(input)):
    parity ^= 1 - int(c)
    input_parity ^= parity << (len(input) + i + 1)

  return input_parity

def dragon_parity(n: int) -> int:
  gray = n ^ (n >> 1)
  return (gray ^ int.bit_count(n & gray)) & 1

def run():
  input = "10111100110001111"
  parity = input_parity(input)

  print(f"Checksum (len=272): {cksum(parity, len(input), 272)}")
  print(f"Checksum (len=35651584): {cksum(parity, len(input), 35651584)}")

if __name__ == "__main__":
  run()
  sys.exit(0)
