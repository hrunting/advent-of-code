#!/usr/bin/env python3

import sys

from heapq import heappop, heappush

fn = 1362

def count_bits(i: int) -> int:
  return bin(i).count("1")

def can_move(c: tuple[int, int]) -> bool:
  (x, y) = c
  return x >= 0 and y >= 0 and (count_bits(x * x + 3 * x + 2 * x * y + y + y * y + fn) & 1) == 0

def distance(a: tuple[int, int], b: tuple[int, int]) -> int:
  return abs(a[0] - b[0]) + abs(a[1] - b[1])

def run() -> None:
  start = (1, 1)
  max_steps = 50

  for finish in ((31, 39), start):
    unlimited = start != finish
    floor = {}
    paths = []

    heappush(paths, (0, distance(start, finish), start))

    while len(paths) > 0:
      (steps, dist, (x, y)) = heappop(paths)
      if unlimited and dist == 0:
        print(f"Steps: {steps}")
        break

      if (x, y) in floor:
        continue

      floor[(x, y)] = can_move((x, y))

      if floor[(x, y)] and (unlimited or steps < max_steps):
        moves = [(x + 1, y), (x - 1, y), (x, y - 1), (x, y + 1)]
        for m in moves:
          heappush(paths, (steps + 1, distance(m, finish), m))

    if not unlimited:
      print(f"Distinct Locations: {len([k for k in floor.keys() if floor[k]])}")

if __name__ == "__main__":
  run()
  sys.exit(0)
