#!/usr/bin/env python3

import sys

from hashlib import md5
from heapq import heappop, heappush

DOORS = [('U', (0, -1)), ('D', (0, 1)), ('L', (-1, 0)), ('R', (1, 0))]

def open_doors(s: str, p: str) -> list[tuple[str, tuple[int, int]]]:
  h = md5((s + p).encode()).hexdigest()[:4]
  return [x for i, x in enumerate(DOORS) if h[i] in {'b', 'c', 'd', 'e', 'f'}]

def run() -> None:
  secret = "qtetzkpl"

  start = (0, 0)
  finish = (3, 3)

  paths = []

  max_steps = 0

  heappush(paths, (0, (start, '')))

  while len(paths) > 0:
    (steps, ((rx, ry), path)) = heappop(paths)
    if (rx, ry) == finish:
      if max_steps == 0:
        print(f"Shortest Path: {path}")
      if steps > max_steps:
        max_steps = steps
      continue

    doors = open_doors(secret, path)
    for move, (dx, dy) in doors:
      if 0 <= rx + dx < 4 and 0 <= ry + dy < 4:
        heappush(paths, (steps + 1, ((rx + dx, ry + dy), path + move)))

  print(f"Longest Path Length: {max_steps}")

if __name__ == "__main__":
  run()
  sys.exit(0)
