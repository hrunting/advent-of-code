#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def dist(pos: tuple[int, int] | None) -> int:
  if pos is None:
    return 0
  return abs(pos[0]) + abs(pos[1])

def run() -> None:
  compass = 0
  compass_points = [[0, 1], [1, 0], [0, -1], [-1, 0]]
  pos = (0, 0)

  visited = set()
  visited.add(pos)

  hq_pos = None

  for line in open(aoc.inputfile("input.txt")):
    moves = [s.strip() for s in line.split(",")]
    for move in moves:
      turn = move[:1].lower()
      blocks = int(move[1:])

      compass += -1 if turn == 'l' else 1
      path = compass_points[compass % len(compass_points)]
      for _ in range(blocks):
        pos = (pos[0] + path[0], pos[1] + path[1])
        if hq_pos is None and pos in visited:
          hq_pos = pos
        visited.add(pos)

  print(f"Travel Blocks: {dist(pos)} @ {pos}")
  print(f"HQ Blocks: {dist(hq_pos)} @ {hq_pos}")

if __name__ == "__main__":
  run()
  sys.exit(0)
