#!/usr/bin/env python3

import pathlib
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Keypad:
  IDEAL_KEYPAD = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9']
  ]

  ACTUAL_KEYPAD = [
    ['',  '',  '1', '',  '' ],
    ['',  '2', '3', '4', '' ],
    ['5', '6', '7', '8', '9'],
    ['',  'A', 'B', 'C', '' ],
    ['',  '',  'D', '',  '' ]
  ]

  def __init__(self: Self, keypad: list[list[str]], pos: tuple[int, int]) -> None:
    self.keypad = keypad
    self.pos = pos

  def move(self: Self, c: str) -> None:
    c = c.upper()
    x, y = self.pos

    if c == "U" and x > 0 and self.keypad[x - 1][y]:
      self.pos = (x - 1, y)
    elif c == "D" and x < len(self.keypad) - 1 and self.keypad[x + 1][y]:
      self.pos = (x + 1, y)
    elif c == "L" and y > 0 and self.keypad[x][y - 1]:
      self.pos = (x, y - 1)
    elif c == "R" and y < len(self.keypad[x]) - 1 and self.keypad[x][y + 1]:
      self.pos = (x, y + 1)

  def code(self: Self) -> str:
    return self.keypad[self.pos[0]][self.pos[1]]

def run() -> None:
  for (layout, start) in [
    (Keypad.IDEAL_KEYPAD, (1, 1)),
    (Keypad.ACTUAL_KEYPAD, (2, 0))
  ]:
    code = ''
    for line in open(aoc.inputfile("input.txt")):
      keypad = Keypad(layout, start)
      for c in line.strip():
        keypad.move(c)
      code += keypad.code()
    print(f"Code: {code}")

if __name__ == "__main__":
  run()
  sys.exit(0)
