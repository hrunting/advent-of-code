#!/usr/bin/env python3

import copy
import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def ratchet(discs: dict[int, list[int]], turns: int) -> int:
  for disc in discs.keys():
    discs[disc][1] = (discs[disc][1] + turns) % discs[disc][0]
  return turns

def is_capable(discs: dict[int, list[int]]) -> bool:
  return len([x for x in discs.keys() if discs[x][1] != discs[x][2]]) == 0

def run() -> None:
  inre = re.compile(r'Disc #(\d+) has (\d+) positions; at time=(\d+), it is at position (\d+)')

  first_discs = {}
  for line in open(aoc.inputfile("input.txt")):
    if m := inre.match(line):
      (disc, positions, pos) = [int(x) for x in m.group(1, 2, 4)]
      first_discs[disc] = [positions, pos, positions - disc]

  max_disc = max(first_discs.keys())
  second_discs = copy.deepcopy(first_discs)
  second_discs[max_disc + 1] = [11, 0, 11 - (max_disc + 1)]

  for discs in (first_discs, second_discs):
    time = 0
    min_disc = min(discs.keys(), key=lambda x: discs[x])
    min_ratchet = discs[min_disc][0]

    while not is_capable(discs) and discs[min_disc][1] != discs[min_disc][2]:
      time += ratchet(discs, 1)

    while not is_capable(discs):
      time += ratchet(discs, min_ratchet)

    print(f"First Time: {time}")

if __name__ == "__main__":
  run()
  sys.exit(0)
