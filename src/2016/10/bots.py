#!/usr/bin/env python3

import pathlib
import re
import sys

from typing import Self

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

class Bot:
  def __init__(self: Self, _id: int) -> None:
    self.chips = []
    self._id = _id
    self.low = None
    self.high = None

  def __str__(self: Self) -> str:
    return f"Bot-{self._id}"

  def add_rule(self: Self, low: Self, high: Self) -> None:
    self.low = low
    self.high = high

    if len(self.chips) == 2:
      self.run_logic()

  def add_chip(self: Self, chip: int) -> None:
    self.chips += [chip]
    if len(self.chips) == 2:
      self.run_logic()

  def run_logic(self: Self) -> None:
    if self.low is not None and self.high is not None:
      (low_chip, high_chip) = (min(self.chips), max(self.chips))
      if low_chip == 17 and high_chip == 61:
        print(f"Bot ID responsible for comparing microchips: {self._id}")
      self.chips = []
      self.low.add_chip(low_chip)
      self.high.add_chip(high_chip)

class Bin:
  def __init__(self: Self, _id: int) -> None:
    self.chips = []
    self._id = _id

  def __str__(self: Self) -> str:
    return f"Bin-{self._id}"

  def add_chip(self: Self, chip: int):
    self.chips += [chip]

def run() -> None:
  bots = {}
  bins = {}

  def get_bin(bin_id):
    if bin_id not in bins:
      bins[bin_id] = Bin(bin_id)
    return bins[bin_id]

  def get_bot(bot_id):
    if bot_id not in bots:
      bots[bot_id] = Bot(bot_id)
    return bots[bot_id]

  bot_input = re.compile(r'^value (\d+) goes to bot (\d+)')
  bot_rule = re.compile(r'^bot (\d+) gives low to (\S+) (\d+) and high to (\S+) (\d+)')

  for line in open(aoc.inputfile("input.txt")):
    if m := bot_input.match(line):
      (chip, bot_id) = [int(x) for x in m.group(1, 2)]
      get_bot(bot_id).add_chip(chip)

    else:
      if m := bot_rule.match(line):
        (bot_id, low_type, low_id, high_type, high_id) = m.group(1, 2, 3, 4, 5)
        (bot_id, low_id, high_id) = [int(x) for x in (bot_id, low_id, high_id)]

        low = get_bin(low_id) if low_type == "output" else get_bot(low_id)
        high = get_bin(high_id) if high_type == "output" else get_bot(high_id)

        get_bot(bot_id).add_rule(low, high)

  chip0 = get_bin(0).chips[0]
  chip1 = get_bin(1).chips[0]
  chip2 = get_bin(2).chips[0]

  print(f"{chip0} * {chip1} * {chip2} = {chip0 * chip1 * chip2}")

if __name__ == "__main__":
  run()
  sys.exit(0)
