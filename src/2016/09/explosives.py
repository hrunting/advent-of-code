#!/usr/bin/env python3

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def decompress_length(s: str, ver: int = 1) -> int:
  length = 0
  i = 0

  while i < len(s):
    if s[i] == "(":
      end_i = s.find(")", i, len(s))
      (chars, repeat) = [int(x) for x in s[(i + 1):end_i].split("x")]
      if ver == 1:
        length += chars * repeat
      else:
        length += decompress_length(s[(end_i + 1):(end_i + 1 + chars)], ver) * repeat
      i = end_i + chars
    else:
      length += 1

    i += 1

  return length

def run() -> None:
  compressed = ""
  for line in open(aoc.inputfile("input.txt")):
    compressed += line.strip()

  print(f"Decompressed Length (v1): {decompress_length(compressed)}")
  print(f"Decompressed Length (v2): {decompress_length(compressed, 2)}")

if __name__ == "__main__":
  run()
  sys.exit(0)
