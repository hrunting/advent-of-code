#!/usr/bin/env python3

import pathlib
import re
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parents[3] / 'lib' / 'python'))

import aoc

def room_decode(room: str, sector_id: int) -> str:
  room_name = ""
  for c in room:
    if c == "-":
      room_name += " "
    else:
      dec = ord(c) + (sector_id % 26)
      if dec > ord("z"):
        dec -= 26
      room_name += chr(dec)
  return room_name

def cksum(s: str) -> str:
  chars = {}

  for x in s.split("-"):
    for c in x:
      chars[c] = chars.get(c, 0) + 1

  return "".join(sorted(chars.keys(), key=lambda x: (-chars[x], ord(x)))[:5])

def run() -> None:
  sector_sum = 0
  room_sector_id = -1

  for line in open(aoc.inputfile("input.txt")):
    room, sector_id, checksum = "", "", ""

    if m := re.match(r"^(.*)\-(\d+)\[(.*?)\]$", line.strip()):
      (room, sector_id, checksum) = m.group(1, 2, 3)

    entrysum = cksum(room)
    if entrysum == checksum:
      sector_sum += int(sector_id)
      room_name = room_decode(room, int(sector_id))
      if room_name == "northpole object storage":
        room_sector_id = int(sector_id)

  print(f"Sector ID Sum: {sector_sum}")
  print(f"North Pole Object Storage: {room_sector_id}")

if __name__ == "__main__":
  run()
  sys.exit(0)
